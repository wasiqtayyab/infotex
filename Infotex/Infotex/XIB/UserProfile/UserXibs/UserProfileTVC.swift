//
//  UserProfileTVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 01/09/2021.
//

import UIKit

class UserProfileTVC: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
   
    @IBOutlet weak var contCollHeight: NSLayoutConstraint!
    

    @IBOutlet weak var cvLikedVideos: UICollectionView!
    var currentindex = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        
      contCollHeight.constant = (self.cvLikedVideos.frame.size.width / 3) * 5
        cvLikedVideos.delegate = self
        cvLikedVideos.dataSource = self
   
        
        
        cvLikedVideos.register(UINib(nibName: "ChildTwo", bundle: nil), forCellWithReuseIdentifier: "ChildTwo")
        cvLikedVideos.register(UINib(nibName: "childRounded", bundle: nil), forCellWithReuseIdentifier: "childRounded")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.scrollToIndex(notification:)), name: Notification.Name("updateScroll"), object: nil)
        
      
        
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("updateScroll"), object: nil)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func scrollToIndex(notification: Notification) {
      
        let index = notification.object as! IndexPath
        print(index)
        let rect = self.cvLikedVideos.layoutAttributesForItem(at: index)?.frame
        self.cvLikedVideos.scrollRectToVisible(rect!, animated: true)
      }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = contentView.frame.width / 3
        
        return CGSize(width: contentView.frame.width , height: size * 4)
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row  == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChildTwo", for: indexPath) as! ChildTwo
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "childRounded", for: indexPath) as! childRounded
            
            return cell
        }
       
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        currentindex = Int(scrollView.contentOffset.x / cvLikedVideos.frame.size.width)
        let aNum = currentindex
        
        NotificationCenter.default.post(name: Notification.Name("updateHeaderIcon"), object: aNum)
        print(aNum)
    }
}


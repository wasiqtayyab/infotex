//
//  UserProfileHV.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 01/09/2021.
//

import UIKit

class UserProfileHV: UITableViewHeaderFooterView, UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {



     @IBOutlet weak var cvHeaderItems: UICollectionView!
    
     @IBOutlet weak var btnHeart: UIButton!
     
     @IBOutlet weak var btnFavUser: UIButton!
     

    var arrItemas = [["img":"heart_white","imgSel":"heart_gradeint","isSelected":"1"],
                     ["img":"man_star","imgSel":"man_gradient","isSelected":"0"]]
    override func awakeFromNib() {
        super.awakeFromNib()
        cvHeaderItems.delegate = self
        cvHeaderItems.dataSource = self
        cvHeaderItems.register(UINib(nibName: "HeaderItemsCVC", bundle: nil), forCellWithReuseIdentifier: "HeaderItemsCVC")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeIconCell(notification:)), name: Notification.Name("updateHeaderIcon"), object: nil)
        
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("updateHeaderIcon"), object: nil)
    }
    
    @objc func changeIconCell(notification: Notification) {
        let aNum  =  notification.object as! Int
        print(aNum)
        for i in 0..<arrItemas.count {
            var obj   =  self.arrItemas[i]
            obj.updateValue("0", forKey: "isSelected")
            self.arrItemas.remove(at: i)
            self.arrItemas.insert(obj, at: i)
        }
        
        var obj   =  self.arrItemas[aNum]
        obj.updateValue("1", forKey: "isSelected")
        self.arrItemas.remove(at: aNum)
        self.arrItemas.insert(obj, at: aNum)
        
    
        self.cvHeaderItems.reloadData()
      }
    

    // MARK:-  CollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.cvHeaderItems.frame.size.width / CGFloat(arrItemas.count)), height: self.cvHeaderItems.frame.size.height)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrItemas.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderItemsCVC", for: indexPath) as! HeaderItemsCVC
        if arrItemas[indexPath.row]["isSelected"] == "0"{
            cell.imgItem.image = UIImage(named: "\(arrItemas[indexPath.row]["img"] ?? "")")
        }else{
            cell.imgItem.image = UIImage(named: "\(arrItemas[indexPath.row]["imgSel"] ?? "")")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
        
        for i in 0..<arrItemas.count {
            var obj   =  self.arrItemas[i]
            obj.updateValue("0", forKey: "isSelected")
            self.arrItemas.remove(at: i)
            self.arrItemas.insert(obj, at: i)
        }
        
        var obj   =  self.arrItemas[indexPath.row]
        obj.updateValue("1", forKey: "isSelected")
        self.arrItemas.remove(at: indexPath.row)
        self.arrItemas.insert(obj, at: indexPath.row)
        
        self.cvHeaderItems.reloadData()
        
        let index = indexPath
        NotificationCenter.default.post(name: Notification.Name("updateScroll"), object: index)
    }
    
}

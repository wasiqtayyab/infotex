//
//  UserTopTVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 01/09/2021.
//

import UIKit

class UserTopTVC: UITableViewCell {
    
    //MARK:- Outlets
    
    @IBOutlet weak var viewBackImage: UIView!
    @IBOutlet weak var viewImageBorder: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserNameBio: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var btnOpenSea: UIButton!
    @IBOutlet weak var viewOpenSea: UIView!
    @IBOutlet weak var viewUserOptions: UIView!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnUserStatus: UIButton!
    @IBOutlet weak var btnHeightUserStatus: NSLayoutConstraint!
    var myUser:[User]? {didSet{}}
    var userData:userMVC!
    var Suscribed = false
    var suggestedUser = [userMVC]()
    
    //MARK:- awakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        self.myUser = User.readUserFromArchive()
    
        self.viewBackImage.layer.cornerRadius = self.viewBackImage.frame.size.height / 2
        self.viewImageBorder.layer.cornerRadius = self.viewImageBorder.frame.size.height / 2
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height / 2
        
        self.viewImageBorder.layer.borderWidth = 1.8
        self.viewImageBorder.layer.borderColor = UIColor.white.cgColor
        
        self.viewBackImage.applyGradientView(colors: [#colorLiteral(red: 0.2269999981, green: 0.4939999878, blue: 0.7289999723, alpha: 1),#colorLiteral(red: 0.7799999714, green: 0.4269999862, blue: 0.6589999795, alpha: 1)])
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    //MARK:- SetupView
    func Setup(userData:userMVC,isOtherUser:Bool){
        
        self.userData = userData
        
        if userData.followBtn == "follow"{
            self.btnFollow.setTitle("Follow", for: .normal)
        }else {
            self.btnFollow.setTitle("Following", for: .normal)
        }
        
        
        if isOtherUser == false{
            self.viewOpenSea.isHidden =  false
            self.viewUserOptions.isHidden =  true
            self.btnHeightUserStatus.constant = 50
            self.btnUserStatus.layer.cornerRadius =  25
            
//            if userData.subscribe == 0{
//                self.btnUserStatus.setBackgroundImage(UIImage(named: "border_gradient"), for: .normal)
//                self.btnUserStatus.setTitle("Member", for: .normal)
//
//            }else{
//
            print("subscribe_package",userData.subscribe_package)

                if userData.subscribe_package == "0"{                // Free user
                    self.btnOpenSea.setBackgroundImage(UIImage(named: "border_gradient"), for: .normal)
                    self.btnOpenSea.setTitle("Member", for: .normal)
                    self.btnOpenSea.setImage(nil, for: .normal)
                    self.btnOpenSea.titleLabel?.textAlignment = .center

                }else if userData.subscribe_package == "1"{         //  OpenSea user
                    self.btnOpenSea.setBackgroundImage(UIImage(named: "gradient_"), for: .normal)
                    self.btnOpenSea.setTitle("Open Sea", for: .normal)
                    self.btnOpenSea.setImage(UIImage(named: "opensea_"), for: .normal)
                    self.btnOpenSea.titleEdgeInsets =  UIEdgeInsets(top: 0, left: 22, bottom: 0, right:0)
                    
                }else{                                             //   One_to_One Subscribe user
                    self.btnOpenSea.setBackgroundImage(UIImage(named: "border_gradient"), for: .normal)
                    self.btnOpenSea.setTitle("Member", for: .normal)
                    self.btnOpenSea.setImage(nil, for: .normal)
                    self.btnOpenSea.titleLabel?.textAlignment = .center



                }
                
           // }


        }else{
            self.viewOpenSea.isHidden =  true
            self.viewUserOptions.isHidden =  false
            self.btnHeightUserStatus.constant = 50
            self.btnUserStatus.layer.cornerRadius =  25
            
            //check user follow
            
            
            
            //Check Subscribtion
            
//            if userData.subscribe == 0{
//                self.btnUserStatus.setBackgroundImage(UIImage(named: "border_gradient"), for: .normal)
//                self.btnUserStatus.setTitle("Member", for: .normal)
//            }else {
                 print("subscribe_package",userData.subscribe_package)
                if userData.subscribe_package == "0"{                // Free user
                    self.btnUserStatus.setBackgroundImage(UIImage(named: "border_gradient"), for: .normal)
                    self.btnUserStatus.setTitle("Member", for: .normal)
                    self.btnUserStatus.setImage(nil, for: .normal)
                    self.btnUserStatus.titleLabel?.textAlignment = .center

                }else if userData.subscribe_package == "1"{         //  OpenSea user
                    self.btnUserStatus.setBackgroundImage(UIImage(named: "gradient_"), for: .normal)
                    self.btnUserStatus.setTitle("Open Sea", for: .normal)
                    self.btnUserStatus.setImage(UIImage(named: "opensea_"), for: .normal)
                    self.btnUserStatus.titleEdgeInsets =  UIEdgeInsets(top: 0, left: 22, bottom: 0, right:0)
                    
                }else{                                             //   One_to_One Subscribe user
                    self.btnUserStatus.setBackgroundImage(UIImage(named: "border_gradient"), for: .normal)
                    self.btnUserStatus.setTitle("Member", for: .normal)
                    self.btnUserStatus.setImage(nil, for: .normal)
                    self.btnUserStatus.titleLabel?.textAlignment = .center

                }
                
           // }
        }
        
    
        self.lblUsername.text = userData.first_name + " " +  userData.last_name
        
        self.lblUserNameBio.text = userData.bio
  
        let userImg = AppUtility?.detectURL(ipString: userData.userProfile_pic)
        let url = URL(string:userImg!)
        self.imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "noUserNew"))
    
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.userProfile(tap:)))
        viewBackImage.isUserInteractionEnabled =  true
        self.viewBackImage.addGestureRecognizer(tap)
        
       
        
    }
    
    //MARK:- Button actions
    @IBAction func btnFollowAction(_ sender: Any) {
        self.myUser = User.readUserFromArchive()
        if (myUser?.count == 0) || self.myUser == nil{
            return
        }
        
        self.followUser(rcvrID: self.userData.userID, userID: (self.myUser?[0].Id)!)
    }
    
    
    //MARK:- Delegate function
    
    @objc func userProfile(tap:UITapGestureRecognizer){
        print("Click on user profile")
        
        if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "influncer", bundle: nil)
            let vc =  storyMain.instantiateViewController(withIdentifier: "ShareProfileViewController") as! ShareProfileViewController
            vc.userData = self.userData
            rootViewController.present(vc, animated: true, completion: nil)

        }
    }
    func followUser(rcvrID:String,userID:String){
        
        print("Recid: ",rcvrID)
        print("senderID: ",userID)
        
        ApiHandler.sharedInstance.followUser(sender_id: userID, receiver_id: rcvrID) { (isSuccess, response) in
            if isSuccess {
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    if let res =  response?.value(forKey: "msg") as? [String:Any]{
                        let receiverobj =  res["receiver"] as! [String:Any]
                        let userobj =  receiverobj["User"] as! [String:Any]
                        if userobj["button"] as! String == "follow"{
                            self.btnFollow.setTitle("Follow", for: .normal)
                        }else{
                            self.btnFollow.setTitle("Following", for: .normal)
                        }
                    }
                }else{
                  print("Error Code 201")
                }
                
            }else{
                print("Something Went Wrong")
            }
        }
    }
}

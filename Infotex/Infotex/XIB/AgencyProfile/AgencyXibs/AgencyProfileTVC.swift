//
//  AgencyProfileTVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 09/09/2021.
//

import UIKit

class AgencyProfileTVC: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    //MARK:- Outlets
    
    @IBOutlet weak var contCollHeight: NSLayoutConstraint!
    @IBOutlet weak var cvLikedVideos: UICollectionView!
    var currentindex = 0
    var myUser:[User]? {didSet{}}
    //MARK:- awakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.myUser = User.readUserFromArchive()
        self.contCollHeight.constant = (self.cvLikedVideos.frame.size.width / 3) * 5
        
        let sdf = (self.cvLikedVideos.frame.size.width / 3) * 5
        print("123",sdf)
        self.cvLikedVideos.delegate = self
        self.cvLikedVideos.dataSource = self

        self.cvLikedVideos.register(UINib(nibName: "QrCVC", bundle: nil), forCellWithReuseIdentifier: "QrCVC")
        self.cvLikedVideos.register(UINib(nibName: "childRounded", bundle: nil), forCellWithReuseIdentifier: "childRounded")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.scrollToIndex(notification:)), name: Notification.Name("updateScroll"), object: nil)
        
        
        
    }
    //MARK:- deinit
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("updateScroll"), object: nil)
    }
    
    @objc func scrollToIndex(notification: Notification) {
      
        let index = notification.object as! IndexPath
        print(index)
        let rect = self.cvLikedVideos.layoutAttributesForItem(at: index)?.frame
        self.cvLikedVideos.scrollRectToVisible(rect!, animated: true)
      }
    
    //MARK:- collectionView
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = contentView.frame.width / 3
        return CGSize(width: contentView.frame.width , height: size * 4)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row  == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QrCVC", for: indexPath) as! QrCVC
           
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "childRounded", for: indexPath) as! childRounded
            
            return cell
        }
       
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        currentindex = Int(scrollView.contentOffset.x / cvLikedVideos.frame.size.width)
        let aNum = currentindex
        
        NotificationCenter.default.post(name: Notification.Name("updateHeaderIcon"), object: aNum)
        print(aNum)
    }
    
   
   
}


//
//  AgencyTopTVC.swift
//  infoTex
//
//  Created by Mac on 09/09/2021.
//

import UIKit

class AgencyTopTVC: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var viewBackImage: UIView!
    @IBOutlet weak var viewImageBorder: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblBio: UILabel!
    @IBOutlet weak var btnAgency: UIButton!
    @IBOutlet weak var lblFollowersTotal: UILabel!
    @IBOutlet weak var lblFollowingTotal: UILabel!
    @IBOutlet weak var btnFollowers: UIButton!
    @IBOutlet weak var btnFollowings: UIButton!
    
    
    var Suscribed = false
    var userData:userMVC!
    
    //MARK:- awakeFromNib
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       
        self.viewBackImage.layer.cornerRadius = self.viewBackImage.frame.size.height / 2
        self.viewImageBorder.layer.cornerRadius = self.viewImageBorder.frame.size.height / 2
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height / 2
        
        self.viewImageBorder.layer.borderWidth = 1.8
        self.viewImageBorder.layer.borderColor = UIColor.white.cgColor
      
        
        self.btnAgency.layer.cornerRadius = self.btnAgency.frame.size.height / 2
 
        self.viewBackImage.applyGradientView(colors: [#colorLiteral(red: 0.2269999981, green: 0.4939999878, blue: 0.7289999723, alpha: 1),#colorLiteral(red: 0.7799999714, green: 0.4269999862, blue: 0.6589999795, alpha: 1)])
        
    }

    //MARK:- Button Actions
    
    @IBAction func followersPressed(_ sender: UIButton) {
    }
    
    @IBAction func followingPressed(_ sender: UIButton) {
    }
    
    @IBAction func btnAgencyPressed(_ sender: UIButton) {
    }
    
    //MARK:- SetupViews
    
    func Setup(userData:userMVC,isOtherUser:Bool){
        self.userData = userData

        self.lblUserName.text =  userData.username
        self.lblBio.text = userData.bio
  
        self.lblFollowersTotal.text = "\(userData.followers)"
        self.lblFollowingTotal.text = "\(userData.following)"
        
        let userImg = AppUtility?.detectURL(ipString: userData.userProfile_pic)
        let url = URL(string:userImg!)
        self.imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "noUserNew"))
        
        
    }
}

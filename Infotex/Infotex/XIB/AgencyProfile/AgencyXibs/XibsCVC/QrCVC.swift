//
//  QrCVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 09/09/2021.
//

import UIKit

class QrCVC: UICollectionViewCell {

    //MARK:-Outlets
    
    
    @IBOutlet weak var visualEffect: UIVisualEffectView!
    @IBOutlet weak var imgQR: UIImageView!
    @IBOutlet weak var viewForCode: UIView!
    @IBOutlet weak var lblcode: UILabel!
    @IBOutlet weak var btnCopyCode: UIButton!
    @IBOutlet weak var btnGenratecode: UIButton!
    var myUser:[User]? {didSet{}}
    var userData:userMVC!
    
    //MARK:-awakeFromNib
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.myUser =  User.readUserFromArchive()
        self.userDetail(userID: (self.myUser?[0].Id)!, OtherUserID: "")
        self.viewForCode.layer.cornerRadius = self.viewForCode.frame.size.height / 2
        self.btnCopyCode.layer.cornerRadius = self.btnCopyCode.frame.size.height / 2
        self.btnGenratecode.layer.cornerRadius = self.btnGenratecode.frame.size.height / 2
        if AppUtility?.getObject(forKey: "isOtherUser") as! String == "false"{
            self.visualEffect.isHidden =  true
        }else{
            self.visualEffect.isHidden =  false
        }
        
    }
    
    //MARK:-setupData
    @IBAction func btnCopyCode(_ sender: Any) {
        self.myUser = User.readUserFromArchive()
        UIPasteboard.general.string = self.myUser?[0].referral_code
    }
    
    @IBAction func btnGenerateNewCode(_ sender: Any) {
        self.myUser = User.readUserFromArchive()
        self.generateReferralCode(userID: (self.myUser?[0].Id)!)
    }
    
    //MARK:- QR Code
        func generateQRCode(from string: String) -> UIImage? {
            let data = string.data(using: String.Encoding.ascii)

            if let filter = CIFilter(name: "CIQRCodeGenerator") {
                filter.setValue(data, forKey: "inputMessage")
                let transform = CGAffineTransform(scaleX: 3, y: 3)

                if let output = filter.outputImage?.transformed(by: transform) {
                    return UIImage(ciImage: output)
                }
            }

            return nil
        }
        
    //MARK:- API Handler

    func generateReferralCode(userID:String){
      
        ApiHandler.sharedInstance.generateReferralCode(user_id: userID) { [self] (isSuccess, resp) in
           
            if isSuccess {
               if resp?.value(forKey: "code") as! NSNumber == 200{
                   
                let Objmsg = resp!["msg"] as! [String:Any]
                let UserObj = Objmsg["User"] as! NSDictionary
                let user = User()
                user.Id =  UserObj.value(forKey: "id") as! String
                user.first_name =  UserObj.value(forKey: "first_name") as! String
                user.last_name =  UserObj.value(forKey: "last_name") as! String
                user.email =  UserObj.value(forKey: "email") as! String
                user.phone =  UserObj.value(forKey: "phone") as? String
                user.image =  UserObj.value(forKey: "image") as? String
                user.role =  UserObj.value(forKey: "role") as? String
                user.device_token =  UserObj.value(forKey: "device_token") as? String
                user.token =  UserObj.value(forKey: "token") as? String
                user.active =  UserObj.value(forKey: "active") as! String
                user.country_id =  UserObj.value(forKey: "country_id") as? String
                user.dob =  UserObj.value(forKey: "dob") as? String
                user.gender =  UserObj.value(forKey: "gender") as? String
                user.lat =  UserObj.value(forKey: "lat") as? String
                user.long =  UserObj.value(forKey: "long") as? String
                user.online =  UserObj.value(forKey: "online") as? String
                user.password =  UserObj.value(forKey: "password") as? String
                user.auth_token =  UserObj.value(forKey: "auth_token") as? String
                user.created  = UserObj.value(forKey:  "created") as? String
                user.device  = UserObj.value(forKey:  "device") as? String
                user.ip  = UserObj.value(forKey:  "ip") as? String
                user.phone  = UserObj.value(forKey:  "phone") as? String
                user.social  = UserObj.value(forKey:  "social") as? String
                user.social_id  = UserObj.value(forKey:  "social_id") as? String
                user.username  = UserObj.value(forKey:  "username") as? String
                user.version  = UserObj.value(forKey:  "version") as? String
                user.wallet  = UserObj.value(forKey:  "wallet") as? String
                user.approve_account_agency = UserObj.value(forKey:  "approve_account_agency") as? String
                user.referral_code = UserObj.value(forKey:  "referral_code") as? String
              
                
                self.myUser = [user]
                if User.saveUserToArchive(user: self.myUser!) {
                    print("User Saved in Directory")
                }
              
               }else{
                   print("code !=200")
               }
               
           }else{
            print("isSuccess false")
           }
       }
   }
    
    //MARK:- API Handler
    
    func userDetail(userID:String,OtherUserID:String){
        ApiHandler.sharedInstance.showUserDetail(user_id: userID,other_user_id:OtherUserID) { (isSuccess, response) in
            if isSuccess{
                if let res =  response?.value(forKey: "msg") as? [String:Any]{
                 let objData = UserDetail.shared.ObjUserDetailResponse(res:res)
                    self.imgQR.image = self.generateQRCode(from: objData.referral_code)
                    self.lblcode.text = objData.referral_code
                }
            }
        }
    }

}

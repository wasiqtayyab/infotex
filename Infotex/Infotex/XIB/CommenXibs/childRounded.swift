//
//  childRounded.swift
//  infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit

class childRounded: UICollectionViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
 
    
//MARK:- Outlets
    
    
    @IBOutlet weak var cvThumb: UICollectionView!
    lazy var OtheruserID = ""
    var myUser:[User]? {didSet{}}
    var followingUsers = [followingUser]()
    var isOtherUser =  false
    
    
    //MARK:- awakeFromNib
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.myUser =  User.readUserFromArchive()
        self.showFollowing(userID:(self.myUser?[0].Id)! , starting_point: "0")
        cvThumb.register(UINib(nibName: "InfluncersFollowedCVC", bundle: nil), forCellWithReuseIdentifier: "InfluncersFollowedCVC")
    }

    
    //MARK:- collectionView
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = contentView.frame.width / 4
        return CGSize(width: size, height: size * 1.2)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.followingUsers.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InfluncersFollowedCVC", for: indexPath) as! InfluncersFollowedCVC
        cell.SetupView(followingUser: self.followingUsers[indexPath.row])
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Role",self.followingUsers[indexPath.row].role)
        if self.followingUsers[indexPath.row].role == "user"{
            if let rootViewController = UIApplication.topViewController() {
                 let storyMain = UIStoryboard(name: "User", bundle: nil)
                 let vc = storyMain.instantiateViewController(withIdentifier: "UserProfileCheckVC") as! UserProfileCheckVC
                vc.isOtherUser =  true
                vc.userID =  self.followingUsers[indexPath.row].id
                 rootViewController.navigationController?.pushViewController(vc, animated: true)
             }
        }else if self.followingUsers[indexPath.row].role == "agency"{
            if let rootViewController = UIApplication.topViewController() {
                 let storyMain = UIStoryboard(name: "Agency", bundle: nil)
                 let vc = storyMain.instantiateViewController(withIdentifier: "AgencyProfileVC") as! AgencyProfileVC
                vc.isOtherUser =  true
                vc.userID =  self.followingUsers[indexPath.row].id
                 rootViewController.navigationController?.pushViewController(vc, animated: true)
             }
        }else{
            if let rootViewController = UIApplication.topViewController() {
                 let storyMain = UIStoryboard(name: "influncer", bundle: nil)
                 let vc = storyMain.instantiateViewController(withIdentifier: "InfluncerProfileVC") as! InfluncerProfileVC
                vc.isOtherUser =  true
                vc.userID =  self.followingUsers[indexPath.row].id
                 rootViewController.navigationController?.pushViewController(vc, animated: true)
             }
        }
    }
    
    //MARK:- API Handler
    
    
    func showFollowing(userID:String,starting_point:String){
        ApiHandler.sharedInstance.showFollowing(user_id: userID,starting_point:starting_point) { (isSuccess, response) in
            if isSuccess{
                if let msg =  response?.value(forKey: "msg") as? [[String:Any]]{
                    for res in msg{
                        let objData = FollowingUsers.shared.FollowingUser(obj: res)
                        self.followingUsers.append(objData)
                        self.cvThumb.delegate = self
                        self.cvThumb.dataSource = self
                        self.cvThumb.reloadData()
                        
                    }
                }
            }
        }
    }
}

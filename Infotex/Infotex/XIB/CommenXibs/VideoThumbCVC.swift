//
//  VideoThumbCVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 01/09/2021.
//

import UIKit

class VideoThumbCVC: UICollectionViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var imgIconPlay: UIImageView!
    
    //MARK:- AwakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }

}

//
//  BookingDetailCVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 05/09/2021.
//

import UIKit
import SDWebImage

class BookingDetailCVC: UICollectionViewCell {
    
    
   //MARK:- Outlets
    
    @IBOutlet weak var viewBackImage: UIView!
    @IBOutlet weak var viewImageBorder: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgUserOnline: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblBookingType: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnStatLiveChat: UIButton!
    @IBOutlet weak var viewButtons: UIView!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var btnMoreBookingInfluencer: UIButton!
    @IBOutlet weak var imgCalender: UIImageView!
    @IBOutlet weak var mainView: UIView!
    var arrData:BookingInfluencer?
    var myUser:[User]? {didSet{}}
    //MARK:- awakeFromNib
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.myUser = User.readUserFromArchive()
        self.viewBackImage.layer.cornerRadius = self.viewBackImage.frame.size.height / 2
        self.viewImageBorder.layer.cornerRadius = self.viewImageBorder.frame.size.height / 2
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height / 2
        
        self.viewImageBorder.layer.borderWidth = 1.8
        self.viewImageBorder.layer.borderColor = UIColor.white.cgColor
        self.viewBackImage.applyGradientView(colors: [#colorLiteral(red: 0.2269999981, green: 0.4939999878, blue: 0.7289999723, alpha: 1),#colorLiteral(red: 0.7799999714, green: 0.4269999862, blue: 0.6589999795, alpha: 1)])
        
        self.btnStatLiveChat.layer.cornerRadius = self.btnStatLiveChat.frame.size.height / 2
        
        self.btnMoreBookingInfluencer.layer.cornerRadius = self.btnMoreBookingInfluencer.frame.size.height / 2
        
        self.btnMessage.layer.cornerRadius = 7
        self.btnMessage.layer.borderWidth = 1
        self.btnMessage.layer.borderColor = UIColor.white.cgColor
        
        self.btnReport.layer.cornerRadius = 7
        self.btnReport.layer.borderWidth = 1
        self.btnReport.layer.borderColor = UIColor.white.cgColor
        
        
    }

    func setup(arr:BookingInfluencer){
        self.lblUserName.text! =  arr.User.username
        let userImgPath = AppUtility?.detectURL(ipString: arr.User.userProfile_pic ?? "")
        let userImgUrl = URL(string:userImgPath!)
        self.imgUser.sd_setImage(with: userImgUrl, placeholderImage: UIImage(named: "noUserNew"))

    
        let createdDate = arr.UserBookingAvailableTiming.date
        let obj =  createdDate?.components(separatedBy: " ")
        self.lblTime.text! = "\(createdDate!) \(arr.UserBookingAvailableTiming.arrBookingTime!.time!)"
        
        if arr.User.Online == "0"{
            self.imgUserOnline.isHidden =  true
        }else{
            self.imgUserOnline.isHidden =  false
        }
        
        self.arrData =  arr
        
    }
   //MARK:- Button Actions
    
    @IBAction func btnMessageAction(_ sender: Any) {
        if let rootViewController = UIApplication.topViewController() {
            let story:UIStoryboard = UIStoryboard(name: "User", bundle: nil)
            let vc =  story.instantiateViewController(withIdentifier: "BaseChatVC") as! BaseChatViewController
            vc.ReceiverName =  arrData?.Influencer.username ?? ""
            vc.ReciverImg   =  arrData?.Influencer.userProfile_pic ?? ""
            ReceiverID      =  arrData?.Booking.influencer_id ?? ""
            rootViewController.navigationController?.pushViewController(vc, animated: true)
        }
    }
        
    @IBAction func btnReportAction(_ sender: Any) {
        if let rootViewController = UIApplication.topViewController() {
        let story:UIStoryboard = UIStoryboard(name: "influncer", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
        vc.modalPresentationStyle = .fullScreen
        rootViewController.navigationController?.present(vc, animated: true)
        }
    }
    
    @IBAction func btnMoreBooking(_ sender: Any) {
        isBooked =  false
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadCollectionView"), object: nil)
    }
}

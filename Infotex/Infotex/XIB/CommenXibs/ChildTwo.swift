//
//  ChildTwo.swift
//  infoTex
//
//  Created by Mac on 02/09/2021.
//

import UIKit

class ChildTwo: UICollectionViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
 
    
//MARK:- Outlets
    
    @IBOutlet weak var cvThumb: UICollectionView!
    var myUser:[User]? {didSet{}}
    var cvPostVideo = [profileDetailVideo]()
    var starting_point = 0
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator((UIApplication.shared.keyWindow?.rootViewController?.view)!))!
    }()
    
    //MARK:- awakeFromNib
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cvThumb.delegate = self
        cvThumb.dataSource = self
        cvThumb.register(UINib(nibName: "VideoThumbCVC", bundle: nil), forCellWithReuseIdentifier: "VideoThumbCVC")
        self.myUser = User.readUserFromArchive()
        
        if AppUtility?.getObject(forKey: "isOtherUser") as! String == "false"{
            self.showvideoAgainstID(userID: self.myUser![0].Id!, otherUserID: "" )
        }else{
            self.showUserVideosAgainstUserID(userID: self.myUser![0].Id!, otherUserID: AppUtility?.getObject(forKey: "OtherUserID") as! String)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.scrollToIndex(notification:)), name: Notification.Name("reloadAPI"), object: nil)
    }

    
    //MARK:- NotificationCenter
    @objc func scrollToIndex(notification: Notification) {
        self.cvPostVideo.removeAll()
        
        if AppUtility?.getObject(forKey: "isOtherUser") as! String == "false"{
            self.showvideoAgainstID(userID: self.myUser![0].Id!, otherUserID: "" )
        }else{
            self.showUserVideosAgainstUserID(userID: self.myUser![0].Id!, otherUserID: AppUtility?.getObject(forKey: "OtherUserID") as! String)
        }
    }

    deinit {
        NotificationCenter.default.removeObserver("", name: Notification.Name("reloadAPI"), object: nil)
    }
    
  //MARK:- CollectionView
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                    let size = contentView.frame.width / 3
                    return CGSize(width: size, height: size)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cvPostVideo.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoThumbCVC", for: indexPath) as! VideoThumbCVC
        if IsSubscribe_Package == 1 {
            let userImg = AppUtility?.detectURL(ipString: self.cvPostVideo[indexPath.row].gif)
            let url = URL(string:userImg!)
            cell.imgThumb.sd_setImage(with: url, placeholderImage: UIImage(named: "videoPlaceholder"))
        }else{
            
            cell.imgThumb.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            cell.imgThumb.image = UIImage(named: "lock_white-1")
            
        }
               
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if IsSubscribe_Package == 1 {
            if let rootViewController = UIApplication.topViewController() {
                
                let story:UIStoryboard = UIStoryboard(name: "influncer", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "HomeVideoViewController") as! HomeVideoViewController
                vc.isOtherController = true
                for obj in self.cvPostVideo{
                    vc.videosMainArr.append(contentsOf: obj.VideoMain)
                }
               
                vc.hidesBottomBarWhenPushed = true
                vc.currentIndex =  indexPath
                UserDefaults.standard.set(cvPostVideo[indexPath.row].user_id, forKey: "otherUserID")
                rootViewController.navigationController?.pushViewController(vc, animated: true)
            }
        }
       
    }
    
    
    //MARK:- API Handler
    
    func showvideoAgainstID(userID:String,otherUserID:String){
        self.loader.isHidden =  false
        ApiHandler.sharedInstance.showVideosAgainstUserID(user_id: userID,other_user_id:otherUserID, type: "video",starting_point:self.starting_point,duration:"24") { (isSuccess, response) in
         self.loader.isHidden = true
            if isSuccess{
                if let response =  response?.value(forKey: "msg") as? [String:Any]{
                    let resPublic = response["public"] as! [[String:Any]]
                    for res in resPublic{
                        let objData = InfluencerProfileDetail.shared.userProfileDetail(objres: res)
                        self.cvThumb.delegate = self
                        self.cvThumb.dataSource = self
                        self.cvPostVideo.append(objData)
                        self.cvThumb.reloadData()
                        
                    }
                }
            }
        }
    }
    
    
    func showUserVideosAgainstUserID(userID:String,otherUserID:String){
        self.loader.isHidden =  false
        ApiHandler.sharedInstance.showUserVideosAgainstUserID(user_id: userID,other_user_id:otherUserID,starting_point:self.starting_point) { (isSuccess, response) in
            self.loader.isHidden = true
            if isSuccess{
                if let response =  response?.value(forKey: "msg") as? [[String:Any]]{
                    for res in response{
                        let objData = UserProfileDetailVideo.shared.userProfileDetail(objres: res)
                        self.cvThumb.delegate = self
                        self.cvThumb.dataSource = self
                        self.cvPostVideo.append(objData)
                        self.cvThumb.reloadData()
                     }
                   }
                }
            }
        }
    }

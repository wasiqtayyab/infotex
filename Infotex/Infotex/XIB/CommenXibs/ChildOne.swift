//
//  ChildOne.swift
//  infoTex
//
//  Created by Mac on 02/09/2021.
//

import UIKit
import SDWebImage

protocol delegateUpdateSize : class{
    func size(size:CGFloat)
}

class ChildOne: UICollectionViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    //MARK:- Outlets
    
    @IBOutlet weak var contHeightCollView: NSLayoutConstraint!
    @IBOutlet weak var cvThumb: UICollectionView!
    var delegate:delegateUpdateSize!
    var myUser:[User]? {didSet{}}
    var cvPostImages = [profileDetailVideo]()
    var starting_point = 0
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator((UIApplication.shared.keyWindow?.rootViewController?.view)!))!
    }()
    //MARK:- awakeFromNib
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cvThumb.delegate = self
        cvThumb.dataSource = self
        cvThumb.register(UINib(nibName: "VideoThumbCVC", bundle: nil), forCellWithReuseIdentifier: "VideoThumbCVC")
        self.myUser = User.readUserFromArchive()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.scrollToIndex(notification:)), name: Notification.Name("reloadAPI"), object: nil)
        
        
        if AppUtility?.getObject(forKey: "isOtherUser") as! String == "false"{
            self.showvideoAgainstID(userID: self.myUser![0].Id!, otherUserID: "" )
        }else{
            self.showvideoAgainstID(userID: self.myUser![0].Id!, otherUserID: AppUtility?.getObject(forKey: "OtherUserID") as! String)
        }
    }
    
    
    
    //MARK:- NotificationCenter
    @objc func scrollToIndex(notification: Notification) {
        self.cvPostImages.removeAll()
        
        if AppUtility?.getObject(forKey: "isOtherUser") as! String == "false"{
            self.showvideoAgainstID(userID: self.myUser![0].Id!, otherUserID: "" )
        }else{
            self.showvideoAgainstID(userID: self.myUser![0].Id!, otherUserID: AppUtility?.getObject(forKey: "OtherUserID") as! String)
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("reloadAPI"), object: nil)
    }
    
    //MARK:- CollectionView
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = contentView.frame.width / 3
        return CGSize(width: size, height: size)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cvPostImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoThumbCVC", for: indexPath) as! VideoThumbCVC
        print("IsSubscribe_Package",IsSubscribe_Package)
        if IsSubscribe_Package == 1 {
            let objImage =  self.cvPostImages[indexPath.row].videoImage
            print("Image count per index:",objImage.count)
            if objImage.count == 1 {
                cell.imgIconPlay.isHidden =  true
                
            }else{
                cell.imgIconPlay.isHidden =  false
                cell.imgIconPlay.image = UIImage(named:"filter_-1")
            }
            
            for obj in objImage{
                let userImg = AppUtility?.detectURL(ipString: obj.image)
                let url = URL(string:userImg!)
                cell.imgThumb.sd_setImage(with: url, placeholderImage: UIImage(named: "videoPlaceholder"))
                
            }
        }else{
            cell.imgThumb.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            cell.imgThumb.image = UIImage(named: "lock_white-1")
        }
   
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if IsSubscribe_Package == 1 {
            if let rootViewController = UIApplication.topViewController() {
                
                let story:UIStoryboard = UIStoryboard(name: "influncer", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "SingleFeedViewController") as! SingleFeedViewController
                    vc.arrFeeds = self.cvPostImages
                if AppUtility?.getObject(forKey: "isOtherUser") as! String == "false"{
                    vc.userID =  (self.myUser?[0].Id)!
                }else{
                    vc.userID =  AppUtility?.getObject(forKey: "OtherUserID") as! String
                }
                vc.hidesBottomBarWhenPushed = true
                rootViewController.navigationController?.pushViewController(vc, animated: true)
            }
        }
      
    }
    
    //MARK:- API Handler
    
    func showvideoAgainstID(userID:String,otherUserID:String){
        self.loader.isHidden =  false
        ApiHandler.sharedInstance.showVideosAgainstUserID(user_id: userID,other_user_id:otherUserID, type: "image",starting_point:self.starting_point,duration:"") { (isSuccess, response) in
           self.loader.isHidden =  true
            if isSuccess{
                if let response =  response?.value(forKey: "msg") as? [String:Any]{
                    let resPublic = response["public"] as! [[String:Any]]
                    for res in resPublic{
                        let objData = InfluencerProfileDetail.shared.userProfileDetail(objres: res)
                        self.cvThumb.delegate = self
                        self.cvThumb.dataSource = self
                        self.cvPostImages.append(objData)
                        self.cvThumb.reloadData()
                        
                    }
                }
            }
        }
    }
}
extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if parentResponder is UIViewController {
                return parentResponder as? UIViewController
            }
        }
        return nil
    }
}

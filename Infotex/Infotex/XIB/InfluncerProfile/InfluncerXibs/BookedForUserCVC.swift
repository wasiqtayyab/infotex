//
//  BookedForUserCVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 05/09/2021.
//

import UIKit

class BookedForUserCVC: UICollectionViewCell {

    //MARK:- Outlets
    
    @IBOutlet weak var cvBookings: UICollectionView!
    var bookingInfluencer:[BookingInfluencer] = []

    
    //MARK:- awakeFromNib

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cvBookings.delegate = self
        self.cvBookings.dataSource = self
        self.cvBookings.register(UINib(nibName: "BookingDetailCVC", bundle: nil), forCellWithReuseIdentifier: "BookingDetailCVC")
        
    }
    
    func setup(arr:[BookingInfluencer]){
        self.bookingInfluencer.append(contentsOf: arr)
        self.cvBookings.reloadData()
    }
    
}
//MARK:- UICollectionView

extension BookedForUserCVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.cvBookings.frame.size.width, height: 289)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.bookingInfluencer.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookingDetailCVC", for: indexPath) as! BookingDetailCVC
        
        if self.bookingInfluencer.count > 0{
            if indexPath.row != self.bookingInfluencer.count{
                cell.btnMoreBookingInfluencer.isHidden = true
                cell.imgCalender.isHidden  = true
                cell.mainView.isHidden  = false
                cell.setup(arr:self.bookingInfluencer[indexPath.row])
                cell.tag =  indexPath.row

            }else{
                cell.mainView.isHidden  = true
                cell.btnMoreBookingInfluencer.isHidden = false
                cell.imgCalender.isHidden  = false
            }
            
        }
        return cell
    }

}

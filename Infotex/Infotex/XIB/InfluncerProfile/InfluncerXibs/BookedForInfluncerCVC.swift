//
//  BookedForInfluncerCVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 05/09/2021.
//

import UIKit

class BookedForInfluncerCVC: UICollectionViewCell {
    
    //MARK:- Outlets
    
    @IBOutlet weak var cvBookings: UICollectionView!
    @IBOutlet weak var btnManageBookingTime: UIButton!
    @IBOutlet weak var btnManageBookingFee: UIButton!
    var myUser:[User]? {didSet{}}
    lazy var bookingInfluncer:[BookingInfluencer] = []
    
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator((UIApplication.shared.keyWindow?.rootViewController?.view)!))!
    }()
    
    
    //MARK:- AwakeFromNib
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.myUser = User.readUserFromArchive()
        self.btnManageBookingTime.layer.cornerRadius = self.btnManageBookingTime.frame.size.height / 2
        self.btnManageBookingFee.layer.cornerRadius = self.btnManageBookingFee.frame.size.height / 2
        self.cvBookings.register(UINib(nibName: "BookingDetailCVC", bundle: nil), forCellWithReuseIdentifier: "BookingDetailCVC")
    
     
        self.showBookings()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadAPI(notification:)), name: Notification.Name("reloadAPI"), object: nil)
    }
    
    @objc func reloadAPI(notification:Notification){
        self.showBookings()
    }
    
    //MARK:- Button Actions
    
    @IBAction func managTimePressed(_ sender: UIButton) {
        
        if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "influncer", bundle: nil)
            let vc = storyMain.instantiateViewController(withIdentifier: "EditBookingTime") as! EditBookingTime
            vc.hidesBottomBarWhenPushed = true
            rootViewController.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func manageFeePressed(_ sender: UIButton) {
        if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "influncer", bundle: nil)
            let vc = storyMain.instantiateViewController(withIdentifier: "ManageOneOnOneFeeVC") as! ManageOneOnOneFeeVC
            vc.hidesBottomBarWhenPushed = true
            rootViewController.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK:- API Handler
    
    func showBookings(){
        self.loader.isHidden  = false
        self.myUser = User.readUserFromArchive()
        ApiHandler.sharedInstance.showBookings(user_id: (self.myUser?[0].Id)!) { (isSuccess, response) in
            self.loader.isHidden  = true
            if isSuccess{
                print("showBookings",response)
                if  response?.value(forKey: "code") as! NSNumber == 200{
                    self.bookingInfluncer.removeAll()
                    let res =  response?.value(forKey: "msg") as! [[String:Any]]
                    for obj in res{
                        isBooked =  true
                       let data =  BookedInfluencerDetail.shared.ObjUserDetailResponse(res: obj)
                        self.bookingInfluncer.append(data)
                    }
                    self.cvBookings.delegate = self
                    self.cvBookings.dataSource = self
                    self.cvBookings.isHidden = false
                    self.cvBookings.reloadData()
                }else{
                    AppUtility?.showToast(string: response?.value(forKey: "msg") as! String, view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
                }
            }else{
                AppUtility?.showToast(string: "Something went wrong", view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
            }
        }
    }
}


//MARK:- extension Collection view

extension BookedForInfluncerCVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.cvBookings.frame.size.width, height: 289)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.bookingInfluncer.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookingDetailCVC", for: indexPath) as! BookingDetailCVC
        cell.setup(arr:self.bookingInfluncer[indexPath.row])
        cell.btnMoreBookingInfluencer.isHidden =  true
        cell.imgCalender.isHidden =  true
        cell.tag =  indexPath.row
        return cell
    }
    
    
  
}

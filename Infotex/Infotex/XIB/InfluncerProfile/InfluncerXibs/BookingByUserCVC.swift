//
//  BookingByUserCVC.swift
//  infotex
//
//  Created by Mac on 05/09/2021.
//

import UIKit

class BookingByUserCVC: UICollectionViewCell,UITextFieldDelegate,SelectTime,InfluencerBookSuccess,InfluencerBookSuccessAlert {
    
    


//MARK:- Outlets
    
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var viewTime: UIView!
    @IBOutlet weak var btnDateBooking: UIButton!
    @IBOutlet weak var btnTimeBooking: UIButton!
    @IBOutlet weak var lblBookingDate: UILabel!
    @IBOutlet weak var lblBookingTime: UILabel!
    @IBOutlet weak var lblDescBeforeBooking: UILabel!
    @IBOutlet weak var btnBookInfluncer: UIButton!
    @IBOutlet weak var tfSelectBookingDate: UITextField!
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator((UIApplication.shared.keyWindow?.rootViewController?.view)!))!
    }()
    var timePicker = UIDatePicker()
    var myUser:[User]? {didSet{}}
    var strTimeID = "0"
    var arrData = [BookingInfluencer]()
    
    //MARK:- awakeFromNib
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewDate.layer.cornerRadius =  self.viewDate.frame.height / 2
        self.viewTime.layer.cornerRadius =  self.viewTime.frame.height / 2
        
        
        self.btnDateBooking.layer.cornerRadius =  self.btnDateBooking.frame.height / 2
        self.btnTimeBooking.layer.cornerRadius =  self.btnTimeBooking.frame.height / 2
        self.btnBookInfluncer.layer.cornerRadius =  self.btnBookInfluncer.frame.height / 2
        if #available(iOS 13.4, *) { timePicker.preferredDatePickerStyle = .wheels }
        
        self.showDatePickerTo()
        
        self.showBookingsWithInfluencer()
    }

    //MARK:- Button actions
    
    @IBAction func selectdatePressed(_ sender: UIButton) {
        print("SelectDate")
    }
    
    @IBAction func selectTimePressed(_ sender: UIButton) {
        if self.lblBookingDate.text == "YYYY-MM-DD"{
            AppUtility?.showToast(string: "Please select date", view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
            return
        }
        if let rootViewController = UIApplication.topViewController() {
            let story:UIStoryboard = UIStoryboard(name: "User", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "BookingInfluencerByUserViewController") as! BookingInfluencerByUserViewController
            vc.Timedelegate =  self
            rootViewController.navigationController?.pushViewController(vc, animated: true)
        }
     
        print("SelectTime")
    }
    
    @IBAction func btnBookInfluencerAction(_ sender: Any) {
        if self.lblBookingDate.text == "YYYY-MM-DD"{
            AppUtility?.showToast(string: "Please select date", view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
            return
        }
        if self.lblBookingDate.text == "HH:MM - HH:MM"{
            AppUtility?.showToast(string: "Please select date", view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
            return
        }
        
        if influencer_Subscribe != 0 {
            if let rootViewController = UIApplication.topViewController() {
                let story:UIStoryboard = UIStoryboard(name: "User", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "InfluncerBookLiveStreamingViewController") as! InfluncerBookLiveStreamingViewController
                vc.delegateSuccess =  self
                rootViewController.navigationController?.present(vc, animated: true)
            }
            
        }else{
            AppUtility?.showToast(string: "Please subscribe", view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
            return
        }
    }
    
    //MARK:- API Handler
    func addInfluencerBookings(){
        self.loader.isHidden  = false
        self.myUser = User.readUserFromArchive()
        ApiHandler.sharedInstance.addInfluecerBooking(user_id: (self.myUser?[0].Id)!, user_booking_available_timing_id: self.strTimeID,influencer_id:AppUtility?.getObject(forKey: "OtherUserID")  as! String, coins: AppUtility?.getObject(forKey: "coins") as! String) { (isSuccess, response) in
            self.loader.isHidden  = true
            if isSuccess{
                print("Bookings",response)
                if  response?.value(forKey: "code") as! NSNumber == 200 {
                 
                    if let rootViewController = UIApplication.topViewController() {
                        let story:UIStoryboard = UIStoryboard(name: "User", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "InfluncerBookSucessAlertViewController") as! InfluncerBookSucessAlertViewController
                        vc.delegateSuccessAlert =  self
                        rootViewController.navigationController?.present(vc, animated: true)
                    }
                }else{
                    AppUtility?.showToast(string:response?.value(forKey: "msg") as? String ?? "", view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
                }
            
            }else{
                AppUtility?.showToast(string: "Something went wrong" ,view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
            }
        }
    }
    
    func showBookingsWithInfluencer(){
        self.loader.isHidden  = false
        self.arrData.removeAll()
        self.myUser = User.readUserFromArchive()
        ApiHandler.sharedInstance.showBookingWithInfluencer(user_id: (self.myUser?[0].Id)!, influencer_id: AppUtility?.getObject(forKey: "OtherUserID")  as! String) { (isSuccess, response) in
            self.loader.isHidden  = true
            if isSuccess{
                print("showBookingsWithInfluencer",response)
                if  response?.value(forKey: "code") as! NSNumber == 200{
                    let res =  response?.value(forKey: "msg") as! [[String:Any]]
                    for obj in res{
                        isBooked =  true
                       let data =  BookedInfluencerDetail.shared.ObjUserDetailResponse(res: obj)
                        self.arrData.append(data)
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadCollectionView"), object: self.arrData)
                }
              
            }else{
                AppUtility?.showToast(string: "Something went wrong", view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
            }
        }
    }
    
    
    //MARK:- Protocol Function
    
    func selectTime(strTime: String,id:String,strdate:String) {
        self.lblBookingTime.text =  strTime
        self.strTimeID =  id
        self.lblBookingDate.text = strdate
        
    }
    func influencerBookSuccess() {
        self.addInfluencerBookings()
    }
    
    func influencerBookSuccessAlert() {
        self.showBookingsWithInfluencer()
    }
    
    //MARK: TextField
    func showDatePickerTo(){
        
        timePicker.datePickerMode = .date
        timePicker.minimumDate = Date()
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePickerTo));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePickerTo));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        tfSelectBookingDate.inputAccessoryView = toolbar
        tfSelectBookingDate.inputView = timePicker
        
    }
    
    @objc func donedatePickerTo(){
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd"
        self.lblBookingDate.text! = "\(formatter1.string(from: timePicker.date))"
    
        UIApplication.shared.keyWindow?.rootViewController?.view.endEditing(true)
        
    }
    @objc func cancelDatePickerTo(){
        UIApplication.shared.keyWindow?.rootViewController?.view.endEditing(true)
    }
}

//
//  InfluncerTopTVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 02/09/2021.
//

import UIKit
import SDWebImage

class InfluncerTopTVC: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var cvSuggestion: UICollectionView!
    @IBOutlet weak var viewBackImage: UIView!
    @IBOutlet weak var heightViewButton: NSLayoutConstraint!
    @IBOutlet weak var heightSubscribeButton: NSLayoutConstraint!
    @IBOutlet weak var viewButtons: UIView!
    
    @IBOutlet weak var viewImageBorder: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblBio: UILabel!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnSubcribe: UIButton!
    @IBOutlet weak var lblPostCount: UILabel!
    @IBOutlet weak var lblFollowingCount: UILabel!
    @IBOutlet weak var lblFollowerCount: UILabel!
    @IBOutlet weak var heightSuggestedCollectionView: NSLayoutConstraint!
    var userData:userMVC!
    var Suscribed = false
    var suggestedUser = [userMVC]()
    var myUser:[User]? {didSet{}}
    
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator((UIApplication.shared.keyWindow?.rootViewController?.view)!))!
    }()
    //MARK:- awakeFromNib
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
      
        self.viewBackImage.layer.cornerRadius = self.viewBackImage.frame.size.height / 2
        self.viewImageBorder.layer.cornerRadius = self.viewImageBorder.frame.size.height / 2
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height / 2
        
        self.viewImageBorder.layer.borderWidth = 1.8
        self.viewImageBorder.layer.borderColor = UIColor.white.cgColor
      
        
        self.btnFollow.layer.cornerRadius = 7
        self.btnFollow.layer.borderWidth = 1.8
        self.btnFollow.layer.borderColor = UIColor.white.cgColor
        
        self.btnSubcribe.layer.cornerRadius = 7
        self.btnSubcribe.layer.borderWidth = 1.8
        self.btnSubcribe.layer.borderColor = UIColor.white.cgColor
        
        self.viewBackImage.applyGradientView(colors: [#colorLiteral(red: 0.2269999981, green: 0.4939999878, blue: 0.7289999723, alpha: 1),#colorLiteral(red: 0.7799999714, green: 0.4269999862, blue: 0.6589999795, alpha: 1)])
        
       
        
        cvSuggestion.register(UINib(nibName: "SuggestedUserCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SuggestedUserCollectionViewCell")
        
    
        
    
    }
    
    //MARK:- Setup View
    func Setup(userData:userMVC,isOtherUser:Bool){
        self.userData = userData
        
        if isOtherUser == false{
            self.viewButtons.isHidden =  true
            self.heightViewButton.constant =  0
            self.heightSuggestedCollectionView.constant = 0
            self.cvSuggestion.isHidden = true
        }else{
            self.viewButtons.isHidden =  false
            self.heightViewButton.constant =  53
            self.heightSuggestedCollectionView.constant = 100
            self.cvSuggestion.isHidden = false
        }
        
        self.lblUserName.text =  userData.username
        self.lblBio.text = userData.bio
        
        self.lblFollowerCount.text = "\(userData.followers)"
        self.lblFollowingCount.text = "\(userData.following)"
        self.lblPostCount.text = "\(userData.videoCount)"
        
        let userImg = AppUtility?.detectURL(ipString: userData.userProfile_pic)
        let url = URL(string:userImg!)
        self.imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "noUserNew"))
        
        
        if userData.followBtn == "follow"{
            self.btnFollow.setTitle("Follow", for: .normal)
        }else {
            self.btnFollow.setTitle("Following", for: .normal)
        }
        
        
        if userData.subscribe == 0{
            influencer_Subscribe = 0
            self.heightSubscribeButton.constant = 50
            self.btnSubcribe.layer.cornerRadius =  25
            self.btnSubcribe.setBackgroundImage(UIImage(named: "gradient_"), for: .normal)
            self.btnSubcribe.setTitle("Subscribe", for: .normal)
            self.btnSubcribe.setImage(UIImage(named: "video_"), for: .normal)
            self.btnSubcribe.titleEdgeInsets =  UIEdgeInsets(top: 0, left: 22, bottom: 0, right:0)
            self.btnSubcribe.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        }else {
            influencer_Subscribe = 1
            self.heightSubscribeButton.constant = 35
            self.btnSubcribe.setTitle("Message", for: .normal)
            self.btnSubcribe.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        
        self.showSuggested(userID:userData.userID)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.userProfile(tap:)))
        viewBackImage.isUserInteractionEnabled =  true
        self.viewBackImage.addGestureRecognizer(tap)
        
        
    }
    
    //MARK:- Button Action
    
    @IBAction func btnFollowingAction(_ sender: Any) {
        self.myUser = User.readUserFromArchive()
        if (myUser?.count == 0) || self.myUser == nil{
            return
        }
        
        self.followUser(rcvrID: self.userData.userID, userID: (self.myUser?[0].Id)!)
    }
    
    @IBAction func btnSubscribeAction(_ sender: Any) {
        self.myUser = User.readUserFromArchive()
        if (myUser?.count == 0) || self.myUser == nil{
            return
        }
        if self.btnSubcribe.titleLabel?.text == "Subscribe"{
           
             let obj  = AppUtility?.getObject(forKey: "Package") as? String ?? "1"
             print("Subscribe Package",obj)
            if obj == "2" || obj == "0"{
                if self.userData.subscribe >= 1{
                    self.userSubscribe(rcvrID: self.userData.userID, userID: (self.myUser?[0].Id)!,subscribe: "1")
                }else{
    
                    if let rootViewController = UIApplication.topViewController() {
                        let storyMain = UIStoryboard(name: "User", bundle: nil)
                        let vc =  storyMain.instantiateViewController(withIdentifier: "ChoosePackageViewController") as! ChoosePackageViewController
                        rootViewController.navigationController?.pushViewController(vc, animated: true)

                    }
                }
            }else{
                self.userSubscribe(rcvrID: self.userData.userID, userID: (self.myUser?[0].Id)!,subscribe: "1")
            }
        }else{
            if let rootViewController = UIApplication.topViewController() {
                let story:UIStoryboard = UIStoryboard(name: "User", bundle: nil)
                let vc =  story.instantiateViewController(withIdentifier: "BaseChatVC") as! BaseChatViewController
                vc.ReceiverName = self.userData.username ?? ""
                vc.ReciverImg   =  self.userData.userProfile_pic ?? ""
                ReceiverID      =  self.userData.userID ?? ""
                rootViewController.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
        
    }
    
    @objc func userProfile(tap:UITapGestureRecognizer){
        print("Click on user profile")
        
        if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "influncer", bundle: nil)
            let vc =  storyMain.instantiateViewController(withIdentifier: "ShareProfileViewController") as! ShareProfileViewController
            vc.userData = self.userData
            rootViewController.present(vc, animated: true, completion: nil)

        }
    }
}

//MARK:- Extension

extension InfluncerTopTVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.suggestedUser.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SuggestedUserCollectionViewCell", for: indexPath) as! SuggestedUserCollectionViewCell
        cell.SetupView(suggestedUser:self.suggestedUser[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (cvSuggestion.frame.size.width - 20) / 4
        return CGSize(width: size, height: size * 1.2)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Role",self.suggestedUser[indexPath.row].role)
        if self.suggestedUser[indexPath.row].role == "user"{
            if let rootViewController = UIApplication.topViewController() {
                 let storyMain = UIStoryboard(name: "User", bundle: nil)
                 let vc = storyMain.instantiateViewController(withIdentifier: "UserProfileCheckVC") as! UserProfileCheckVC
                vc.isOtherUser =  true
                vc.userID =  self.suggestedUser[indexPath.row].userID
                 rootViewController.navigationController?.pushViewController(vc, animated: true)
             }
        }else if self.suggestedUser[indexPath.row].role == "agency"{
            if let rootViewController = UIApplication.topViewController() {
                 let storyMain = UIStoryboard(name: "Agency", bundle: nil)
                 let vc = storyMain.instantiateViewController(withIdentifier: "AgencyProfileVC") as! AgencyProfileVC
                vc.isOtherUser =  true
                vc.userID =  self.suggestedUser[indexPath.row].userID
                 rootViewController.navigationController?.pushViewController(vc, animated: true)
             }
        }else{
            if let rootViewController = UIApplication.topViewController() {
                 let storyMain = UIStoryboard(name: "influncer", bundle: nil)
                 let vc = storyMain.instantiateViewController(withIdentifier: "InfluncerProfileVC") as! InfluncerProfileVC
                vc.isOtherUser =  true
                vc.userID =  self.suggestedUser[indexPath.row].userID
                 rootViewController.navigationController?.pushViewController(vc, animated: true)
             }
        }
    }
    //MARK:- API Handler
    
    func showSuggested(userID:String){
        ApiHandler.sharedInstance.showSuggestedUsers(user_id: userID,starting_point:0) { (isSuccess, response) in
            if isSuccess{
                if let response =  response?.value(forKey: "msg") as? [[String:Any]]{
                    for res in response{
                        let objData = UserDetail.shared.ObjUserDetailResponse(res:res)
                        self.cvSuggestion.delegate = self
                        self.cvSuggestion.dataSource = self
                        self.suggestedUser.append(objData)
                        self.cvSuggestion.reloadData()
                    
                    }
                }
            }
        }
    }

    func followUser(rcvrID:String,userID:String){
        
        print("Recid: ",rcvrID)
        print("senderID: ",userID)
        
        ApiHandler.sharedInstance.followUser(sender_id: userID, receiver_id: rcvrID) { (isSuccess, response) in
            if isSuccess {
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    if let res =  response?.value(forKey: "msg") as? [String:Any]{
                        let receiverobj =  res["receiver"] as! [String:Any]
                        let userobj =  receiverobj["User"] as! [String:Any]
                        if userobj["button"] as! String == "follow"{
                            self.btnFollow.setTitle("Follow", for: .normal)
                        }else{
                            self.btnFollow.setTitle("Following", for: .normal)
                        }
                    }
                }else{
                    AppUtility?.showToast(string: response?.value(forKey: "msg") as? String ?? "Unable to subscribe", view:(UIApplication.shared.keyWindow?.rootViewController?.view)!)
                }
                
            }else{
                AppUtility?.showToast(string: "Something went wrong", view:(UIApplication.shared.keyWindow?.rootViewController?.view)!)
            }
        }
    }
    
    func userSubscribe(rcvrID:String,userID:String,subscribe:String){
        
        print("Recid: ",rcvrID)
        print("senderID: ",userID)
        print("subscribe: ",subscribe)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:MM:SS "
        let now = Date()
        let dateString = formatter.string(from:now)
        self.loader.isHidden =  false
        ApiHandler.sharedInstance.subscribeUser(sender_id: userID, receiver_id: rcvrID, subscribe: subscribe,subscribe_expiry_date: dateString) { (isSuccess, response) in
            
            self.loader.isHidden =  true
            
            if isSuccess {
              
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    self.heightSubscribeButton.constant = 35
                    self.btnSubcribe.layer.cornerRadius =  5
                    self.btnSubcribe.setTitle("Message", for: .normal)
                    self.btnSubcribe.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    self.btnSubcribe.setBackgroundImage(nil, for: .normal)
                    self.btnSubcribe.setImage(nil, for: .normal)
                    self.btnSubcribe.titleEdgeInsets =  UIEdgeInsets(top: 0, left: 0, bottom: 0, right:0)
                    influencer_Subscribe = 1
                }else{
                  print("Error Code 201")
                    influencer_Subscribe = 1
                    AppUtility?.showToast(string: response?.value(forKey: "msg") as? String ?? "Unable to subscribe", view:(UIApplication.shared.keyWindow?.rootViewController?.view)!)
                }
                
            }else{
                AppUtility?.showToast(string: "Something went wrong", view:(UIApplication.shared.keyWindow?.rootViewController?.view)!)
            }
        }
    }
}

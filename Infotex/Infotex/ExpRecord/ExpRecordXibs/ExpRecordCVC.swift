//
//  ExpRecordCVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 28/09/2021.
//

import UIKit

class ExpRecordCVC: UICollectionViewCell {
    @IBOutlet weak var cvViewers: UICollectionView!
    @IBOutlet weak var btnFilter: UIButton!
    var arrCount = Int()
    var arrData = [[String:Any]]()
    var lastSelectedIndex = -1
    override func awakeFromNib() {
        super.awakeFromNib()
        cvViewers.delegate = self
        cvViewers.dataSource = self
        cvViewers.register(UINib(nibName: "ExpRecordSingle", bundle: nil), forCellWithReuseIdentifier: "ExpRecordSingle")
    }
    func setup(count:Int,giftsData:[[String:Any]]){
//        if count == 0 {
//            lblComingSoon.isHidden = false
//        }else{
//            lblComingSoon.isHidden = true
//        }
        self.arrCount = count
        self.arrData = giftsData
        cvViewers.delegate = self
        cvViewers.dataSource = self
        self.cvViewers.reloadData()
    
    }
}
extension ExpRecordCVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = contentView.frame.width
        
        return CGSize(width: size , height: 80 )
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExpRecordSingle", for: indexPath) as! ExpRecordSingle
      
        
//        let url = "\(ApiHandler.sharedInstance.BaseUrl ?? "")" + "\(obj["image"] as! String)"
//        print(url)
//        cell.imgIcon.sd_imageIndicator = SDWebImageActivityIndicator.white
//        cell.imgIcon.sd_setImage(with: URL(string: url), placeholderImage: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                    lastSelectedIndex = indexPath.row
                    self.cvViewers.reloadData()
    }
    
    
}

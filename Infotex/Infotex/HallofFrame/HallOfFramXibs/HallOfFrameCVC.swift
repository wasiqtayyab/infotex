//
//  HallOfFrameCVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 28/09/2021.
//

import UIKit

class HallOfFrameCVC: UICollectionViewCell {
    @IBOutlet weak var cvViewers: UICollectionView!
    @IBOutlet weak var btnFilter: UIButton!
    var arrCount = Int()
    var arrData = [[String:Any]]()
    var lastSelectedIndex = -1
    override func awakeFromNib() {
        super.awakeFromNib()
        cvViewers.delegate = self
        cvViewers.dataSource = self
        cvViewers.register(UINib(nibName: "ViewrsCVC", bundle: nil), forCellWithReuseIdentifier: "ViewrsCVC")
    }
    func setup(count:Int,giftsData:[[String:Any]]){
//        if count == 0 {
//            lblComingSoon.isHidden = false
//        }else{
//            lblComingSoon.isHidden = true
//        }
        self.arrCount = count
        self.arrData = giftsData
        cvViewers.delegate = self
        cvViewers.dataSource = self
        self.cvViewers.reloadData()
    
    }
}
extension HallOfFrameCVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = contentView.frame.width
        
        return CGSize(width: size , height: 80 )
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ViewrsCVC", for: indexPath) as! ViewrsCVC
        cell.imgUserBlock.isHidden = true
        cell.imgChevron.isHidden = true
        cell.btnInviteBlock.isHidden = true
        cell.imgTag150.isHidden = true
        cell.lblTag150.isHidden =  true
        cell.lblCount.text = "\(indexPath.row + 1)"
        switch indexPath.row {
        case 0:
            cell.imgFor123.image = UIImage(named: "iconForFirst")
            cell.lblCount.isHidden = true
            cell.imgIcon1.isHidden = false
            cell.imgIcon2.isHidden = false
            cell.imgIcon3.isHidden = false
        case 1:
            cell.imgFor123.image = UIImage(named: "iconForSecond")
            cell.lblCount.isHidden = true
            cell.imgIcon1.isHidden = false
            cell.imgIcon2.isHidden = false
            cell.imgIcon3.isHidden = true
        case 2:
            
            cell.imgFor123.image = UIImage(named: "iconForThird")
            cell.lblCount.isHidden = true
            cell.imgIcon1.isHidden = false
            cell.imgIcon2.isHidden = true
            cell.imgIcon3.isHidden = true
        default:
            cell.imgFor123.image = nil
            cell.lblCount.isHidden = false
            cell.imgIcon1.isHidden = true
            cell.imgIcon2.isHidden = true
            cell.imgIcon3.isHidden = true
        }
        
//        let url = "\(ApiHandler.sharedInstance.BaseUrl ?? "")" + "\(obj["image"] as! String)"
//        print(url)
//        cell.imgIcon.sd_imageIndicator = SDWebImageActivityIndicator.white
//        cell.imgIcon.sd_setImage(with: URL(string: url), placeholderImage: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                    lastSelectedIndex = indexPath.row
                    self.cvViewers.reloadData()
    }
    
    
}

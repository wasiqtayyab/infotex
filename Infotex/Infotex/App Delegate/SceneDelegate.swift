//
//  SceneDelegate.swift
//  Infotex
//
//  Created by Wasiq Tayyab on 31/08/2021.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
        // self.deviceRegisterApi()
    }

    func sceneDidDisconnect(_ scene: UIScene) {
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
      
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    
    //MARK:- DEVICE REGISTER API
    
    private func deviceRegisterApi(){
        
        ApiHandler.sharedInstance.registerDevice(key: UIDevice.current.identifierForVendor!.uuidString) { (isSuccess, resp) in
            if isSuccess {
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let msg  = resp!.value(forKey:"msg") as! [String:Any]
                    let Device  = msg["Device"] as! [String:Any]
                    AppUtility!.saveObject(obj: Device["id"] as! String, forKey: strDeviceID)
                    AppUtility!.saveObject(obj: Device["key"] as! String, forKey: strDeviceKey)
                    
                }else{
                
                }
            }else {
                
                print(resp as Any)
                
            }
            
            
        }
        
        
    }


}


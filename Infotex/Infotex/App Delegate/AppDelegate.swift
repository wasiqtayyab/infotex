//
//  AppDelegate.swift
//  Infotex
//
//  Created by Wasiq Tayyab on 31/08/2021.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import  SwiftyStoreKit

@available(iOS 13.0, *)
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = .lightContent
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldPlayInputClicks = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        self.deviceRegisterApi()
        FirebaseApp.configure()
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
                for purchase in purchases {
                    switch purchase.transaction.transactionState {
                    case .purchased, .restored:
                        if purchase.needsFinishTransaction {
                            // Deliver content from server, then:
                            SwiftyStoreKit.finishTransaction(purchase.transaction)
                        }
                        // Unlock content
                    case .failed, .purchasing, .deferred:
                        break // do nothing
                    }
                }
            }

        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    //MARK:- DEVICE REGISTER API
    
    private func deviceRegisterApi(){
        
        ApiHandler.sharedInstance.registerDevice(key: UIDevice.current.identifierForVendor!.uuidString) { (isSuccess, resp) in
            if isSuccess {
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    let msg  = resp!.value(forKey:"msg") as! [String:Any]
                    let Device  = msg["Device"] as! [String:Any]
                    AppUtility!.saveObject(obj: Device["id"] as! String, forKey: strDeviceID)
                    AppUtility!.saveObject(obj: Device["key"] as! String, forKey: strDeviceKey)
                }
            }else {
                
                print(resp as Any)
                
            }
        }
    }

}


//
//  ChatViewController.swift
//  GroceryApp
//
//  Created by Mac on 06/11/2020.
//  Copyright © 2020 DinoSoftLabs. All rights reserved.
//

import UIKit

import JSQMessagesViewController
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth
import MobileCoreServices

class ChatViewController: JSQMessagesViewController,UIImagePickerControllerDelegate,JSQMessagesComposerTextViewPasteDelegate {
    
    
    //MARK:- Outlets
    
    private lazy var loader : UIView = {
          return (AppUtility?.createActivityIndicator(self.view))!
      }()
    var myUser: [User]? {didSet {}}
    var arrmessages = NSMutableArray()
    let imagePicker = UIImagePickerController()
    var timearray = NSMutableArray()
    var start_index:UInt! = 20
    var downloadURL:String! = ""
     
    var receiverID = "194"
    
    var longPressBeganLocation: CGPoint = CGPoint.zero
    let distanceBerrier: CGFloat = 90
    var audioRecoder: AudioRecorder? = nil
    var audioRecoderIndicator: AudioRecordIndicatorView? = nil

  lazy var longPressRightGesture: UILongPressGestureRecognizer = {
        return UILongPressGestureRecognizer(target: self, action: "didLongPressRightBarButtonItem:")
    }()

    lazy var rightBarButtonItemMic: UIButton = {
        let button = UIButton(frame: CGRect.zero)
        button.setImage(UIImage(named:"mic"),for:.normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.addGestureRecognizer(self.longPressRightGesture)
        return button
    }()
    
    lazy var outgoingBubble: JSQMessagesBubbleImage = {
        
        return JSQMessagesBubbleImageFactory(bubble: UIImage(named: "Untitled-1-31"), capInsets: UIEdgeInsets.zero).outgoingMessagesBubbleImage(with:UIColor.init(red:255/255.0, green:101/255.0, blue:96/255.0, alpha: 1.0))
    }()
    
    lazy var incomingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory(bubble:  UIImage(named: "Untitled-1-30"), capInsets: UIEdgeInsets.zero).outgoingMessagesBubbleImage(with:UIColor.init(red:234/255.0, green:234/255.0, blue:234/255.0, alpha: 1.0))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myUser  = User.readUserFromArchive()

        print("Chat With ReceiverID:",receiverID)
        imagePicker.delegate = self
        self.receiverID = ReceiverID
        self.senderId = self.myUser?[0].Id
        self.setupView()
        
    }
    
   
    
    //MARK:- SetupView
    
    func setupView(){
        if AppUtility!.connected() == false{
            AppUtility?.displayAlert(title: NSLocalizedString("no_network_alert_title", comment: ""), messageText: NSLocalizedString("no_network_alert_description", comment: ""), delegate: self)
            return
        }
        self.GetAllMessages()
        
        self.myUser  = User.readUserFromArchive()
        senderDisplayName =  (self.myUser?[0].first_name)! +  (self.myUser?[0].last_name)!
      
        self.toolBarSetting()
       
      
    }

    func toolBarSetting(){
        
        self.inputToolbar.contentView.textView.tintColor = UIColor.init(red:31/255, green:128/255, blue:193/255, alpha: 1.0)
        self.inputToolbar.contentView.backgroundColor = UIColor.white
        self.inputToolbar.contentView.textView.font = UIFont(name: "Verdana", size:14)
        self.inputToolbar.contentView.rightBarButtonItem.setTitle("Send", for: .normal)
        self.inputToolbar.contentView.rightBarButtonItem.setTitle("Send", for: .disabled)
        self.inputToolbar.contentView.textView.layer.cornerRadius = self.inputToolbar.contentView.textView.frame.size.height/2
        self.inputToolbar.contentView.textView.clipsToBounds = true
        
        self.collectionView.collectionViewLayout.incomingAvatarViewSize = .zero
        self.collectionView.collectionViewLayout.outgoingAvatarViewSize = .zero
        
        automaticallyScrollsToMostRecentMessage = true
       
        
    }
    
    
    //MARK:- Switch Action
    
    //MARK:- Button Action
    
    //MARK:- DELEGATE METHODS

    //MARK: TableView
    
    //MARK: CollectionView.
    
    //MARK: Segment Control
    
    //MARK:- Firebase Data
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        let childRef = Database.database().reference().child("typing_indicator").child("\((self.myUser?[0].Id)!)-\(self.receiverID)")
        let childRef1 = Database.database().reference().child("typing_indicator").child("\(self.receiverID)-\((self.myUser?[0].Id)!)")
        self.showTypingIndicator = false
        
        childRef.removeValue()
        childRef1.removeValue()
    }
    
    
    override func textViewDidChange(_ textView: UITextView) {
        
        super.textViewDidChange(textView)
        let childRef = Database.database().reference().child("typing_indicator").child("\((self.myUser?[0].Id)!)-\(self.receiverID)")
        let childRef1 = Database.database().reference().child("typing_indicator").child("\(self.receiverID)-\((self.myUser?[0].Id)!)")
        if(textView.text == nil || textView.text == "" || textView.text == " "){

            self.showTypingIndicator = false

            childRef.removeValue()
            childRef1.removeValue()


        }else{

            let message = ["sender_id":(self.myUser?[0].Id)!,"receiver_id":self.receiverID]
            childRef.setValue(message)
            childRef1.setValue(message)

        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
       
        
        let childRef = Database.database().reference().child("typing_indicator").child("\((self.myUser?[0].Id)!)-\(self.receiverID)")
        let childRef1 = Database.database().reference().child("typing_indicator").child("\(self.receiverID)-\((self.myUser?[0].Id)!)")
        childRef.observe(.value) { (snapshot) in
            let firebaseDic = snapshot.value as? [String: AnyObject]
            if(firebaseDic != nil){
                print(firebaseDic!)

                let r_id = firebaseDic!["receiver_id"] as! String
                if(self.senderId == r_id){
                    self.showTypingIndicator = true
                    self.scrollToBottom(animated: true)
                }else{
                    self.showTypingIndicator = false
                }
            }else{

                self.showTypingIndicator = false
            }

        }
        
        childRef1.observe(.value) { (snapshot) in
            let firebaseDic = snapshot.value as? [String: AnyObject]
            if(firebaseDic != nil){
                print(firebaseDic!)

                let r_id = firebaseDic!["receiver_id"] as! String
                if(self.senderId == r_id){
                    self.showTypingIndicator = true
                    self.scrollToBottom(animated: true)
                }else{
                    self.showTypingIndicator = false
                }
            }else{

                self.showTypingIndicator = false
            }

        }
    }
    
    //MARK:- API Handler
    
    func GetAllMessages(){
        self.myUser  = User.readUserFromArchive()
        self.loader.isHidden =  true
        
       if AppUtility!.connected() == false{
            self.loader.isHidden =  true
            AppUtility?.displayAlert(title: NSLocalizedString("no_network_alert_title", comment: ""), messageText: NSLocalizedString("no_network_alert_description", comment: ""), delegate: self)
            return
        }
        let childRef = Database.database().reference().child("chat").child("\((self.myUser?[0].Id)!)-\(self.receiverID)")//.queryLimited(toLast:self.start_index)
            childRef.keepSynced(true)
            childRef.observe(DataEventType.value,with: { (snapshot) in
            self.arrmessages.removeAllObjects()
            self.timearray.removeAllObjects()
            print(snapshot)
            if snapshot.childrenCount > 0 {
                
                for artists in snapshot.children.allObjects as! [DataSnapshot] {
                    let artistObject = artists.value as? [String: AnyObject]
                    print(artistObject!)
                    
                    let ChatID             = artistObject!["chat_id"] as! String
                    let picURl             = artistObject!["pic_url"] as! String
                    let receiver_id        = artistObject!["receiver_id"] as! String
                    let sender_ID          = artistObject!["sender_id"] as! String
                    let senderDisplayName  = artistObject!["sender_name"] as! String
                    let status             = artistObject!["status"] as? String
                    let messages           = artistObject!["text"]
                    let timestamp          = artistObject!["timestamp"]
                    let type               = artistObject!["type"] as! String
                    
                    print(sender_ID as! String)
                    
                    self.timearray.add(timestamp)
                    if type == "text" {
                        let message = JSQMessage(senderId: sender_ID as! String, displayName: senderDisplayName, text: messages as? String ?? " ")
                        self.arrmessages.add(message!)

                    }
                    if type == "image"{
                       
                        let imageView =  AsyncPhotoMediaItem(withURL: (URL(string: picURl as! String)!))

                          let message = JSQMessage(senderId:sender_ID as? String, displayName: senderDisplayName  as? String, media: imageView)

                          self.arrmessages.add(message!)
                    }
                    
                    if type == "audio"{
                        
                    }
                
                   
                }
                self.loader.isHidden =  true
                DispatchQueue.main.async {
                    self.finishReceivingMessage()
                }

                print(self.arrmessages)
            }else{
                 self.loader.isHidden =  true
                
            }
        })
    }
    
      
    //MARK: Alert View
    
    //MARK: TextField
    
    //MARK: Location
    
    //MARK: Google Maps
    
    //MARK:- JSQ Messenger
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData!
    {
        let message = self.arrmessages[indexPath.row]
        return message as! JSQMessageData
        
        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return arrmessages.count
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!{
        
        print(senderId)
        print((arrmessages[indexPath.item] as AnyObject).senderId)
        
        return (arrmessages[indexPath.item] as AnyObject).senderId == senderId ? outgoingBubble : incomingBubble
        
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString!
    {
        
        return (arrmessages[indexPath.item] as AnyObject).senderId == senderId ? nil : NSAttributedString(string:"")
    }
    
    func composerTextView(_ textView: JSQMessagesComposerTextView!, shouldPasteWithSender sender: Any!) -> Bool {
        return true
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat
    {
        return (arrmessages[indexPath.item] as AnyObject).senderId == senderId ? 10 : 10
    }
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!){
        
        print("Text Send")
        
        self.myUser  = User.readUserFromArchive()
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ssZ"//"dd-MM-yyyy HH:mm:ss"
        let timeStamp = formatter.string(from: date)
        
        formatter.dateFormat = "dd-MM-yyyy HH:mmZ"//"dd-MM-yyyy HH:mm:ss"
        let time = formatter.string(from: date)
        
        
        //chat folder
    
        let childRef = Database.database().reference().child("chat").child("\((self.myUser?[0].Id)!)-\((self.receiverID))")
        let key = childRef.childByAutoId().key
        let message = ["chat_id":key,"pic_url":AppUtility?.getObject(forKey: "ReciverImg"),"receiver_id":receiverID,"sender_id":(self.myUser?[0].Id)!,"sender_name":(self.myUser?[0].first_name)! +  (self.myUser?[0].last_name)!,"status":"0","text":text,"timestamp":timeStamp,"time":time,"type":"text"]
        childRef.child(key!).setValue(message)
        
        let childRef1 = Database.database().reference().child("chat").child("\((self.receiverID))-\((self.myUser?[0].Id)!)")
        childRef1.child(key!).setValue(message)
       
        
        //Inbox folder
        let childRefChat = Database.database().reference().child("Inbox").child((self.myUser?[0].Id)!).child("\((self.receiverID))-\((self.myUser?[0].Id)!)")
        let Inbox = ["rid":(self.myUser?[0].Id)!,"msg":text,"name":(self.myUser?[0].first_name)! +  (self.myUser?[0].last_name)!, "pic":AppUtility?.getObject(forKey: "ReciverImg"),"status":"0","timestamp":timeStamp]
        print(Inbox)
        childRefChat.setValue(Inbox)
        
        
        let childRefChat1 = Database.database().reference().child("Inbox").child(self.receiverID).child("\(self.receiverID)-\((self.myUser?[0].Id)!)")
        let Inbo1x = ["date":timeStamp,"id":(self.myUser?[0].Id)!,"msg":text,"name":(self.myUser?[0].first_name)! +  (self.myUser?[0].last_name)!, "pic":self.myUser?[0].image ?? "","status":"0","timestamp":timeStamp]
        childRefChat1.setValue(Inbo1x)

        
        
        let childRefUser = Database.database().reference().child("typing_indicator").child("\((self.myUser?[0].Id)!)-\((self.receiverID))")
        let childRefStore = Database.database().reference().child("typing_indicator").child("\((self.receiverID))-\((self.myUser?[0].Id)!)")
        self.showTypingIndicator = false
        
        childRefUser.removeValue()
        childRefStore.removeValue()
        
        self.inputToolbar.contentView.textView.text = nil
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = self.arrmessages[indexPath.item] as! JSQMessage
        // let typ = self.type[indexPath.item] as! String
        
        var isOutgoing = false
        
        if (message as AnyObject).senderId == senderId{
            isOutgoing = true
            
        }
        
        if isOutgoing {
            if message.isMediaMessage{
                if let audio = message.media as? AudioMediaItem {
                    self.playAudioRecoderMessage(audio: audio)
                }
                
            }else{
                
                cell.textView.textColor = UIColor.white
                cell.textView.isUserInteractionEnabled = false
                cell.textView.textAlignment = .left
            }
            
        } else {
            if message.isMediaMessage{
                if let audio = message.media as? AudioMediaItem {
                    self.playAudioRecoderMessage(audio: audio)
                }
            }else{
                
                cell.textView.textColor = UIColor.black
                cell.textView.isUserInteractionEnabled = false
                cell.textView.textAlignment = .left
                
            }
            
        }
    
        cell.cellTopLabel.font = UIFont(name: "System", size:12)
        return cell
        
    }
    
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        
        if indexPath.item == 0 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ssZ"// "dd-MM-yyyy HH:mm:ss"
            let str = self.timearray[indexPath.item] as? String
            print(str!)
            let myDate = dateFormatter.date(from:str!)
            return
                JSQMessagesTimestampFormatter.shared().attributedTimestamp(for:myDate)
        }
        
        if indexPath.item -  1 > 0{
            let previousMessage = self.timearray[indexPath.row-1] as? String
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat =  "dd-MM-yyyy HH:mm:ssZ" //"dd-MM-yyyy HH:mm:ss"
            let myDate2 = dateFormatter1.date(from:previousMessage!)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ssZ"//"dd-MM-yyyy HH:mm:ss"
            let str = self.timearray[indexPath.row] as? String
            let myDate = dateFormatter.date(from:str!)
          if  ( ( (myDate?.timeIntervalSince(myDate2!))! / 60) > 1){
                return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: myDate)
            }
        }
        
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellTopLabelAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.item == 0 {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        
        if indexPath.item -  1 > 0{
            let previousMessage = self.timearray[indexPath.row-1] as? String
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd-MM-yyyy HH:mm:ssZ"//"dd-MM-yyyy HH:mm:ss"
            let myDate2 = dateFormatter1.date(from:previousMessage!)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ssZ"//"dd-MM-yyyy HH:mm:ss"
            let str = self.timearray[indexPath.row] as? String
            let myDate = dateFormatter.date(from:str!)
            if  (((myDate?.timeIntervalSince(myDate2!))! / 60) > 1){
                return kJSQMessagesCollectionViewCellLabelHeightDefault
            }
        }
        return 0.0
    }
    
    
    /*override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString! {
   /*
       let message = self.arrmessages[indexPath.item]
//      let Str = self.seenTime[indexPath.item] as! String
        
        if(self.senderId == (message as AnyObject).senderId){
//            if(self.seenStatus[indexPath.item] as! Bool ==   true ){
//                return NSAttributedString(string: "Seen at "+Str)
//
//            }else{
//                return NSAttributedString(string: "")
//            }
             return NSAttributedString(string: "Seen")
        }else{
            
            return NSAttributedString(string: "")
        }*/
        return NSAttributedString(string: "")
       
    }*/
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        return 20
        
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        
        let childRef = Database.database().reference().child("typing_indicator").child("\((self.myUser?[0].Id)!)-\((self.receiverID))")
        let childRef1 = Database.database().reference().child("typing_indicator").child("\((self.receiverID))-\((self.myUser?[0].Id)!)")
      
        let message = ["sender_id":(self.myUser?[0].Id)!,"receiver_id":self.receiverID]
        childRef.setValue(message)
        childRef1.setValue(message)
        
        
        
        let actionSheet =  UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Camera", style: .default, handler: {
            (_:UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                
                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                self.imagePicker.cameraCaptureMode = .photo
                self.imagePicker.modalPresentationStyle = .fullScreen
                self.present(self.imagePicker,animated: true,completion: nil)
            } else {
                let alertVC = UIAlertController(
                    title: "No Camera",
                    message: "Sorry, this device has no camera",
                    preferredStyle: .alert)
                let okAction = UIAlertAction(
                    title: "OK",
                    style:.default,
                    handler: nil)
                alertVC.addAction(okAction)
                self.present(
                    alertVC,
                    animated: true,
                    completion: nil)
            }
        })
        
        let gallery = UIAlertAction(title: "Gallery", style: .default, handler: {
            (_:UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary;
                self.imagePicker.allowsEditing = true
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (_:UIAlertAction)in
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                (_:UIAlertAction)in
            
            let childRef = Database.database().reference().child("typing_indicator").child("\((self.myUser?[0].Id)!)-\((self.receiverID))")
            let childRef1 = Database.database().reference().child("typing_indicator").child("\((self.receiverID))-\((self.myUser?[0].Id)!)")
            
            self.showTypingIndicator = false
            
            childRef.removeValue()
            childRef1.removeValue()
            })
            
        })
        actionSheet.addAction(camera)
        
        actionSheet.addAction(gallery)
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        self.myUser  = User.readUserFromArchive()
        
        self.dismiss(animated:true, completion: nil)
        self.loader.isHidden = false
        let number = Int.random(in: 0 ... 1000)
        let pickedImage = info[.originalImage] as? UIImage
        
        let storageRef = Storage.storage().reference().child( "\(number)" + "_myImage.png")
        
        if let uploadData = pickedImage!.jpeg(.lowest) {
            
            
            storageRef.putData(uploadData, metadata: nil, completion: { (metadata,error ) in
                
                guard let metadata = metadata else{
                    print(error!)
                    self.loader.isHidden = true
                    self.dismiss(animated: true, completion: nil)
                    return
                }
                storageRef.downloadURL { [self] (url, error) in
                    print(url!)
                    self.downloadURL = String(describing: url!)
                    
                    let childRef = Database.database().reference().child("chat").child("\((self.receiverID))-\((self.myUser?[0].Id)!)")
                    let childRef1 = Database.database().reference().child("chat").child("\((self.myUser?[0].Id)!)-\((self.receiverID))")
                   
                    
                    let key = childRef.childByAutoId().key
                    let date = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MM-yyyy HH:mm:ssZ"//"dd-MM-yyyy HH:mm:ss"
                    let timeStamp = formatter.string(from: date)
        
                    
                    let chat = ["chat_id":key,"pic_url":self.downloadURL,"receiver_id":self.receiverID,"sender_id":(self.myUser?[0].Id)!,"sender_name":(self.myUser?[0].first_name)! +  (self.myUser?[0].last_name)!,"status":"0","text":"send an image","timestamp":timeStamp,"type":"image"]
                    
                    
                    childRef1.child(key!).setValue(chat)
                    childRef.child(key!).setValue(chat)
                    
                    //Chat folder update last message
                    let childRefChat = Database.database().reference().child("Inbox").child((self.myUser?[0].Id)!).child("\((self.receiverID))")
                    let Inbox = ["date":timeStamp,"id":(self.myUser?[0].Id)!,"msg":"send an image","name":(self.myUser?[0].first_name)! +  (self.myUser?[0].last_name)!, "pic":AppUtility?.getObject(forKey: "ReciverImg"),"status":"0","timestamp":timeStamp]
                    
                    childRefChat.setValue(Inbox)
                    
                    
                    let childRefChat1 = Database.database().reference().child("Inbox").child(self.receiverID).child("\(self.receiverID)")
                    let Inbo1x = ["date":timeStamp,"id":(self.myUser?[0].Id)!,"msg":"send an image","name":(self.myUser?[0].first_name)! +  (self.myUser?[0].last_name)!, "pic":self.myUser?[0].image ?? "","status":"0","timestamp":timeStamp]
                    childRefChat1.setValue(Inbo1x)


                    self.showTypingIndicator = false
                    print(self.downloadURL)
                    self.loader.isHidden = true
                    self.finishSendingMessage()
                    
                }
            })
            
        }
        
    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("cancelled")
        let childRef = Database.database().reference().child("typing_indicator").child("\((self.receiverID))-\((self.myUser?[0].Id)!)")
        let childRef1 = Database.database().reference().child("typing_indicator").child("\((self.myUser?[0].Id)!)-\((self.receiverID))")
            
            self.showTypingIndicator = false
            
            childRef.removeValue()
            childRef1.removeValue()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didDeleteMessageAt indexPath: IndexPath!) {
        self.arrmessages.remove(indexPath.row)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, didTapMessageBubbleAt indexPath: IndexPath) {
        
        
      /* // let chat_id = self.arrmessages["chat_id"] as! String
        let message = self.arrmessages[indexPath.item] as! JSQMessage
        
        if (message as AnyObject).senderId == senderId{
            
            if (message.isMediaMessage) {
                
                let actionSheet =  UIAlertController(title:nil, message:nil, preferredStyle: .actionSheet)
                let camera = UIAlertAction(title: "Preview", style: .default, handler: {
                    (_:UIAlertAction)in
                    
                    
                })
                
                let gallery = UIAlertAction(title: "Delete", style: .destructive, handler: {
                    (_:UIAlertAction)in
                    
                    
                    
                    self.finishReceivingMessage()
                })
                
                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                    (_:UIAlertAction)in
                    
                })
                actionSheet.addAction(camera)
                
                actionSheet.addAction(gallery)
                actionSheet.addAction(cancel)
                self.present(actionSheet, animated: true, completion: nil)
            }else{
                let actionSheet =  UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
                
                
                let gallery = UIAlertAction(title: "Delete", style: .destructive, handler: {
                    (_:UIAlertAction)in
                    
                     let childRef = Database.database().reference().child("chat").child("\((self.myUser?[0].id)!)-\(self.storeChatID)")
                     let childRef1 = Database.database().reference().child("chat").child("\((self.storeChatID))-\((self.myUser?[0].id)!)")
                     
                     
//                     childRef.child(chat_id).removeValue()
//                     childRef1.child(chat_id).removeValue()
                     self.arrmessages.removeObject(at:indexPath.row)
                    self.finishReceivingMessage()
                    
                })
                
                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                    (_:UIAlertAction)in
                    
                })
                
                
                actionSheet.addAction(gallery)
                actionSheet.addAction(cancel)
                self.present(actionSheet, animated: true, completion: nil)
            }
        }*/
    }
 
    private func calculateDistanceBetweenTwoPoints(point1: CGPoint, point2: CGPoint) -> CGFloat {
        let offsetx = point1.x - point2.x
        let offsety = point1.y - point2.y
        let offset = offsetx * offsetx + offsety * offsety
        return sqrt(offset)
    }
    
    
    func didLongPressRightBarButtonItem(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            self.longPressBeganLocation = gesture.location(in: self.view)

            let currentTime = NSDate().timeIntervalSinceReferenceDate
            self.audioRecoder = AudioRecorder(fileName: "\(currentTime).wav")
            self.audioRecoder!.delegate = self
            self.audioRecoder!.startRecord()
            
            self.audioRecoderIndicator = AudioRecordIndicatorView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 120, height: 150)))
            self.audioRecoderIndicator!.center = self.view.center
            self.view.addSubview(self.audioRecoderIndicator!)
            self.view.bringSubviewToFront(self.audioRecoderIndicator!)
        }
        if gesture.state == .changed {
            let longPressChangedLocation = gesture.location(in: self.view)
            
            let distance = self.calculateDistanceBetweenTwoPoints(point1: self.longPressBeganLocation, point2: longPressChangedLocation)
            
            if distance > distanceBerrier {
                self.audioRecoderIndicator?.textLabel.text = "Openhand，Cancel"
            } else {
                self.audioRecoderIndicator?.textLabel.text = "Recording"
            }
        }
        if gesture.state == .ended {
            let longPressEndedLocation = gesture.location(in: self.view)
            let distance = self.calculateDistanceBetweenTwoPoints(point1: self.longPressBeganLocation, point2: longPressEndedLocation)
            
            if distance > distanceBerrier {
                // cancel
                self.audioRecoder!.stopRecord()
                self.audioRecoder!.delegate = nil
                self.audioRecoderIndicator!.removeFromSuperview()
                self.audioRecoderIndicator = nil
                
                
            } else {
                // success
                self.audioRecoder!.stopRecord()
                self.audioRecoder!.delegate = nil
                self.audioRecoderIndicator!.removeFromSuperview()
                self.audioRecoderIndicator = nil
                
                
                guard self.audioRecoder!.timeInterval != nil else {
                    return
                }
               
                var name = "ail"
                if self.audioRecoder!.timeInterval.intValue < 4 {
                    name = "ais"
                }
                let audioItem = AudioMediaItem(
                    fileURL: self.audioRecoder!.recorder.url as NSURL,
                    timeInterval: Int32(self.audioRecoder!.timeInterval.intValue),
                    cachedImage: UIImage(named: name),
                    isReadyToPlay: true,
                    maskAsOutgoing: true
                )
                
                let audioMessage = JSQMessage(
                    senderId: self.senderId,
                    displayName: self.senderDisplayName,
                    media: audioItem
                )
                
                self.sendAudioMessage(audioMessage: audioMessage!)
            }
            longPressBeganLocation = CGPoint.zero
        }
        
    }
    
    
    private func sendAudioMessage(audioMessage: JSQMessage) {
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        self.arrmessages.add(audioMessage)
        
        self.finishSendingMessage()
        
       // self.checkRightBarButtonItemStatus()
    }
    
    private func checkRightBarButtonItemStatus() {
        guard let tv = self.inputToolbar?.contentView?.textView else {
            return
        }
        
        self.inputToolbar?.contentView?.rightBarButtonItem?.isEnabled = true

        if tv.hasText() {
            self.inputToolbar?.contentView?.rightBarButtonItem?.setImage(nil, for: .normal)
            self.inputToolbar?.contentView?.rightBarButtonItem?.setTitle("camera", for: .normal)
            self.inputToolbar?.contentView?.rightBarButtonItem?.setTitleColor(UIColor.blue, for: .normal)
            //self.longPressRightGesture.isEnabled = false
        } else {
            self.inputToolbar?.contentView?.rightBarButtonItem?.setTitle(nil, for: .normal)
            self.inputToolbar?.contentView?.rightBarButtonItem?.setImage(UIImage(named: "mic"), for: .normal)
        }
    }
    private func playAudioRecoderMessage(audio: AudioMediaItem) {
        let fileURL = audio.fileURL
        let timeInterval = UInt64(audio.timeInterval)
        AudioMediaItemPlayer.sharedPlayer.startPlaying(url: fileURL!)
        
        // animation start
        audio.startAudioPlayerAnimation()
        /*dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC * timeInterval)), dispatch_get_main_queue(){
           
        })*/
        
        DispatchQueue.global().asyncAfter(deadline: DispatchTime.now() + .milliseconds(100), qos: .background) {
            audio.stopAudioPlayerAnimation()
            
        }
        
      
       
    }
    

    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        sender.view?.removeFromSuperview() // This will remove image from full screen
    }
    
    
    public func convertDateFormatter(date: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        dateFormatter.locale = Locale(identifier: "your_loc_id")
        let convertedDate = dateFormatter.date(from: date)
        
        guard dateFormatter.date(from: date) != nil else {
            assert(false, "no date from string")
            return ""
        }
        dateFormatter.dateFormat = "yyyy-MM-dd"///this is what you want to convert format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let timeStamp = dateFormatter.string(from: convertedDate!)
        print(timeStamp)
        return timeStamp
    }
    
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}

// MARK: - AudioRecorderDelegate

extension ChatViewController: AudioRecorderDelegate {
    func audioRecorderUpdateMetra(metra: Float) {
        if self.audioRecoderIndicator != nil {
            self.audioRecoderIndicator!.updateLevelMetra(levelMetra: metra)
        }
    }
}


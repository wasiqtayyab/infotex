//
//  BaseChatViewController.swift
//  GroceryApp
//
//  Created by Mac on 06/11/2020.
//  Copyright © 2020 DinoSoftLabs. All rights reserved.
//

import UIKit

class BaseChatViewController: UIViewController {

    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var imgStore: CustomImageView!
    
    var ReciverImg = ""
    var ReceiverName = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(ReciverImg)
        self.lblStoreName.text = ReceiverName
        AppUtility?.saveObject(obj: ReceiverName, forKey: "ReceiverName")
        AppUtility?.saveObject(obj: ReciverImg, forKey: "ReciverImg")
        self.imgStore.sd_setImage(with: URL(string: ReciverImg), placeholderImage: UIImage(named: "NoUserImage"))
    }
    
    //MARK:- Button actions
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

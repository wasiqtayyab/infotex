//
//  InboxViewController.swift
//  Insta_tab
//
//  Created by Mac on 19/08/2020.
//  Copyright © 2020 ITQ_Plus. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import EFInternetIndicator
import SDWebImage

class InboxViewController: UIViewController,InternetStatusIndicable,UITableViewDelegate,UITableViewDataSource {
    
    //MARK:- Outlets
    
    
    @IBOutlet weak var LOADER: UIActivityIndicatorView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var NoViewData: UIView!
    @IBOutlet weak var topView: CustomView!
    var internetConnectionIndicator: InternetViewIndicator?
    
    var childRef = Database.database().reference().child("Inbox")
    var inboxArray = NSMutableArray()
    var myUser: [User]? {didSet {}}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.tableFooterView = UIView()
    }
    
    //MARK:- SetupView
    
    func setupView(){
        self.startMonitoringInternet()
        self.GetChat()
        self.tableview.separatorColor = .white
        self.LOADER.isHidden =  true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupView()
    }
    
    //MARK:- Switch Action
    
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- DELEGATE METHODS
    
    //MARK: TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if inboxArray.count == 0 {
            self.NoViewData.isHidden = false
        }else{
            self.NoViewData.isHidden = true
        }
        return self.inboxArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:InboxCell = self.tableview.dequeueReusableCell(withIdentifier: "cellInbox") as! InboxCell
        
        let obj = inboxArray[indexPath.row] as!Inbox
        
        cell.inbox_msg.text = obj.msg
        
        let fullNameArr = obj.date.components(separatedBy: " ")
        print("date",fullNameArr[1])
        let objdate = fullNameArr[1]
        let time = objdate.components(separatedBy: "+")
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "HH:mm:ss"
        let date = dateFormatter1.date(from:time[0])
        print("date1",date!)
        dateFormatter1.dateFormat = "hh:mm a"
        cell.inbox_time.text = ""
        cell.inbox_img.layer.masksToBounds = false
        cell.inbox_img.layer.cornerRadius = cell.inbox_img.frame.height/2
        cell.inbox_img.clipsToBounds = true
        cell.inbox_name.text =  obj.username
        
        let imgurl = "\(obj.picture!)"
        cell.inbox_img.sd_setImage(with: URL(string: imgurl), placeholderImage: UIImage(named: "noUserNew"))
        
        
        cell.inbox_time.text = dateFormatter1.string(from:date!)
        cell.inbox_dot.layer.masksToBounds = false
        cell.inbox_dot.layer.cornerRadius = cell.inbox_dot.frame.height/2
        cell.inbox_dot.clipsToBounds = true
        
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 111
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = inboxArray[indexPath.row] as! Inbox
        let vc =  storyboard?.instantiateViewController(withIdentifier: "BaseChatVC") as! BaseChatViewController
        vc.ReceiverName =  obj.username
        vc.ReciverImg =  obj.picture
        ReceiverID  = obj.rid
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (action, sourceView, completionHandler) in
            print("index path of delete: \(indexPath)")
            let obj = self.inboxArray[indexPath.row] as!Inbox

            let id =  obj.rid
            self.childRef.child(id!).removeValue()
    
            self.inboxArray.remove(indexPath.row)
            self.tableview.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            completionHandler(true)
        }
        
        
        let swipeActionConfig = UISwipeActionsConfiguration(actions: [delete])
        swipeActionConfig.performsFirstActionWithFullSwipe = false
        return swipeActionConfig
    }

    //MARK:- API Handler
    func GetChat(){
        
        self.myUser  = User.readUserFromArchive()
        
        self.LOADER.isHidden =  false
        self.LOADER.startAnimating()
        
        if AppUtility!.connected() == false{
        
            self.LOADER.isHidden =  true
            self.LOADER.stopAnimating()
            
            AppUtility?.displayAlert(title: NSLocalizedString("no_network_alert_title", comment: ""), messageText: NSLocalizedString("no_network_alert_description", comment: ""), delegate: self)
            return
        }
        
        if self.myUser?.count == 0 || self.myUser?.count == nil{
            self.LOADER.isHidden =  true
            self.LOADER.stopAnimating()
            return
        }
        
        childRef.child((self.myUser?[0].Id)!).observe(.value) { (snapshot) in
            self.LOADER.isHidden =  true
            self.LOADER.stopAnimating()
            if snapshot.childrenCount > 0 {
                self.inboxArray.removeAllObjects()
                if snapshot.exists(){
                    for artists in snapshot.children.allObjects as! [DataSnapshot] {
                        let firebaseDic = artists.value as? [String: AnyObject]
                        print(firebaseDic!)
                        
                        let date      = firebaseDic!["date"] as! String
                        let id        = firebaseDic!["rid"] as! String
                        let msg       = firebaseDic!["msg"] as? String
                        let name      = firebaseDic!["name"] as? String
                        let pic       = firebaseDic!["pic"] as! String
                        let timestamp = firebaseDic!["timestamp"] as? String
                        let status    = firebaseDic!["status"] as! String
                        
                        let obj = Inbox(date:date, rid:id, msg: msg,username:name,status:status, picture:pic, timestamp:timestamp)
                        
                        self.inboxArray.add(obj)
                        
                    }
                    
                    self.tableview.delegate = self
                    self.tableview.dataSource = self
                    self.tableview.reloadData()
                    
                }
            }else{
                print("NO Inbox")
                
            }
        }
    }
}

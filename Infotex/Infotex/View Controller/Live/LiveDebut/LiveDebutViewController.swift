//
//  LiveDebutViewController.swift
//  Infotex New Screen
//
//  Created by Wasiq Tayyab on 29/09/2021.
//

import UIKit

class LiveDebutViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var liveDebutCollectionView: UICollectionView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    //MARK:- VARS
    
    var currentindex = 0
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        liveDebutCollectionView.delegate = self
        liveDebutCollectionView.dataSource = self
        pageControl.numberOfPages = 2
       
    }
    
    //MARK:- SCROLL THE IMAGE
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        currentindex = Int(scrollView.contentOffset.x / liveDebutCollectionView.frame.size.width)
        pageControl.currentPage = currentindex
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
    }
    
    


}


extension LiveDebutViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LiveDebutCollectionViewCell", for: indexPath)as! LiveDebutCollectionViewCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3, height: 130)
    }
    
    
    
    
}

//
//  LiveUserCVC.swift
//  Infotex New Screen
//
//  Created by Wasiq Tayyab on 06/10/2021.
//

import UIKit

class LiveUserCVC: UICollectionViewCell {

    //MARK:- Outlets
    
    @IBOutlet weak var viewBackImg: UIView!
    @IBOutlet weak var viewborderImg: UIView!
    @IBOutlet weak var imgInfluncer: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgLiveIcon: UIImageView!
    @IBOutlet weak var imgLockIcon: UIImageView!
    @IBOutlet weak var imgOnlineIcon: UIImageView!
    @IBOutlet weak var contHeightBackView: NSLayoutConstraint!
    @IBOutlet weak var contWidthBackView: NSLayoutConstraint!
    
    //MARK:- awakeFromNib
    
    override func awakeFromNib() {
        super.awakeFromNib()
                
        self.viewBackImg.layer.cornerRadius = self.viewBackImg.frame.size.height / 2
        self.viewborderImg.layer.cornerRadius = self.viewborderImg.frame.size.height / 2
        self.imgInfluncer.layer.cornerRadius = self.imgInfluncer.frame.size.height / 2
        self.viewborderImg.layer.borderWidth = 1.8
        self.viewborderImg.layer.borderColor = UIColor.white.cgColor
        self.viewBackImg.applyGradientView(colors: [#colorLiteral(red: 0.2269999981, green: 0.4939999878, blue: 0.7289999723, alpha: 1),#colorLiteral(red: 0.7799999714, green: 0.4269999862, blue: 0.6589999795, alpha: 1)])
    }
    
    


}

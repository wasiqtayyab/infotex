//
//  LiveUserViewController.swift
//  Infotex New Screen
//
//  Created by Wasiq Tayyab on 27/09/2021.
//

import UIKit

class LiveUserViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var liveUserCollectionView: UICollectionView!
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        liveUserCollectionView.delegate = self
        liveUserCollectionView.dataSource = self
        liveUserCollectionView.register(UINib(nibName: "InfluncersFollowedCVC", bundle: nil), forCellWithReuseIdentifier: "InfluncersFollowedCVC")
       
    }
    


}

extension LiveUserViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InfluncersFollowedCVC", for: indexPath)as! InfluncersFollowedCVC
       
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: liveUserCollectionView.frame.width / 4.0 , height: 120)
    }
    
    
}

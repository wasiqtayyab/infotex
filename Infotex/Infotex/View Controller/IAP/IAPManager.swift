//
//  IAPManager.swift
//  Infotex
//
//  Created by Mac on 03/10/2021.
//

import Foundation
import StoreKit


final class IAPManager:NSObject,SKProductsRequestDelegate, SKPaymentTransactionObserver{
    
    
    static let shared = IAPManager()
    var prodcuts = [SKProduct]()
    let bundleIdentifier =  Bundle.main.bundleIdentifier

    enum Product:String,CaseIterable{
        
        case Buy70   = "com.infootex.live.Buy70"
        case Buy350   = "com.infootex.live.Buy350"
        case Buy1400  = "com.infootex.live.Buy1400"
        case Buy3500  = "com.infootex.live.Buy3500"
        case Buy7000  = "com.infootex.live.Buy7000"
    }
    
    
    public func fetchProduct(){
        let request =  SKProductsRequest(productIdentifiers:Set(Product.allCases.compactMap({$0.rawValue})))
        request.delegate =  self
        request.start()
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("Product Count:\(response.products.count)")
        self.prodcuts =  response.products
    }
    
    
    public func purchase(product:Product){
        guard  SKPaymentQueue.canMakePayments() else{
            return
        }
        guard let storeKitProduct = prodcuts.first(where: {$0.productIdentifier == product.rawValue})else{
            return
        }
        
        let paymentRquest =  SKPayment(product: storeKitProduct)
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(paymentRquest)
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased:
                print("id ===> \(transaction.transactionIdentifier ?? "")")
            case .failed:
                break
            case .restored:
                print("id ===> \(transaction.transactionIdentifier ?? "")")
            default:
                print("Deafault")
            }
        }
        
        transactions.forEach({
            switch  $0.transactionState {
            
            case .purchasing:
                break
            case .purchased:
                
                break
            case .failed:
                break
            case .restored:
                break
            case .deferred:
                break
            @unknown default:
                break
            }
        })
    }
}

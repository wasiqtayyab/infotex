//
//  customTabbar.swift
//  Infotex
//
//  Created by Mac on 01/09/2021.
//
import UIKit
import Foundation

class CustomTabBarController: UITabBarController {

    var myUser:[User]? {didSet{}}
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.myUser = User.readUserFromArchive()
        if (myUser?.count != 0) && self.myUser != nil{
            user_id = (self.myUser?[0].Id)!
            email = (self.myUser?[0].email)!
            phone_number = (self.myUser?[0].phone)!
            self.setUp(user: (self.myUser?[0].role)!)
        }else{
            self.setUp(user:"user")
        }

    }

    func setupMiddleButton() {
        
        var paddingBottom : CGFloat = 0.0
        
      
        
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        var menuButtonFrame = menuButton.frame
        
        if !DeviceType.iPhoneWithHomeButton{
            paddingBottom = 10
            menuButtonFrame.origin.y = (view.bounds.height - menuButtonFrame.height - 50) - paddingBottom
        }else{
            menuButtonFrame.origin.y = (view.bounds.height - menuButtonFrame.height - 50) + 25
        }
        
        menuButton.tag = 2
        menuButtonFrame.origin.x = (view.bounds.width/2 - menuButtonFrame.size.width/2)
        menuButton.frame = menuButtonFrame

        menuButton.backgroundColor = UIColor.clear
        menuButton.layer.cornerRadius = menuButtonFrame.height/2
        view.addSubview(menuButton)

        menuButton.setImage(UIImage(named: "camera_gradeint"), for: .normal)
        menuButton.addTarget(self, action: #selector(menuButtonAction(sender:)), for: .touchUpInside)

        view.layoutIfNeeded()
    }

    func setUp(user:String) {
        let storyboardUser = UIStoryboard(name:"User", bundle: nil)
        let storyboardInfluncer = UIStoryboard(name:"influncer", bundle: nil)
        let storyboardAgency = UIStoryboard(name:"Agency", bundle: nil)
        
        if user == "user"{
           
            let firstBookTableVc = storyboardInfluncer.instantiateViewController(withIdentifier: "HomeVideoViewController")
            firstBookTableVc.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "house_"), selectedImage: UIImage(named: "house_gradient"))
            firstBookTableVc.tabBarItem.tag = 0

            let secondBookTableVc = storyboardInfluncer.instantiateViewController(withIdentifier: "DiscoverViewController")
            secondBookTableVc.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "search_white"), selectedImage: UIImage(named: "search_gradient"))
            secondBookTableVc.tabBarItem.tag = 1
            
            let thirdBookTableVc = storyboardInfluncer.instantiateViewController(withIdentifier: "newNotificationsViewController")
            thirdBookTableVc.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "message_white"), selectedImage: UIImage(named: "message_gradient"))
            thirdBookTableVc.tabBarItem.tag = 2
            
            let fourthBookTableVc = storyboardUser.instantiateViewController(withIdentifier: "UserProfileCheckVC")
            fourthBookTableVc.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "avatar_"), selectedImage: UIImage(named: "avatar_gradient"))
            thirdBookTableVc.tabBarItem.tag = 3

            
            viewControllers = [firstBookTableVc, secondBookTableVc,thirdBookTableVc,fourthBookTableVc]
            
        }else  if user == "agency"{
            
            let firstBookTableVc = storyboardInfluncer.instantiateViewController(withIdentifier: "HomeVideoViewController")
            
            firstBookTableVc.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "house_"), selectedImage: UIImage(named: "house_gradient"))

            
            let secondBookTableVc = storyboardInfluncer.instantiateViewController(withIdentifier: "DiscoverViewController")
            
            secondBookTableVc.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "search_white"), selectedImage: UIImage(named: "search_gradient"))
            
            let thirdBookTableVc = storyboardInfluncer.instantiateViewController(withIdentifier: "newNotificationsViewController")
            
            thirdBookTableVc.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "message_white"), selectedImage: UIImage(named: "message_gradient"))
            
            
            let fourthBookTableVc = storyboardAgency.instantiateViewController(withIdentifier: "AgencyProfileVC")
            
            fourthBookTableVc.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "avatar_"), selectedImage: UIImage(named: "avatar_gradient"))
            
            viewControllers = [firstBookTableVc, secondBookTableVc,thirdBookTableVc,fourthBookTableVc]
            
            
        }else{
          
            let firstBookTableVc = storyboardInfluncer.instantiateViewController(withIdentifier: "HomeVideoViewController")
            firstBookTableVc.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "house_"), selectedImage: UIImage(named: "house_gradient"))
            firstBookTableVc.tabBarItem.tag = 0
            
            
            let secondBookTableVc = storyboardInfluncer.instantiateViewController(withIdentifier: "DiscoverViewController")
            secondBookTableVc.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "search_white"), selectedImage: UIImage(named: "search_gradient"))
            secondBookTableVc.tabBarItem.tag = 1
            
            let thirdBookTableVc = storyboardInfluncer.instantiateViewController(withIdentifier: "newNotificationsViewController")
            thirdBookTableVc.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "message_white"), selectedImage: UIImage(named: "message_gradient"))
            thirdBookTableVc.tabBarItem.tag = 3
            
            let fourthBookTableVc = storyboardInfluncer.instantiateViewController(withIdentifier: "InfluncerProfileVC")
            fourthBookTableVc.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "avatar_"), selectedImage: UIImage(named: "avatar_gradient"))
            fourthBookTableVc.tabBarItem.tag = 4

            
            viewControllers = [firstBookTableVc, secondBookTableVc,thirdBookTableVc,fourthBookTableVc]
            self.setupMiddleButton()
        }
       
    }
    
    // MARK: - Actions

    @objc private func menuButtonAction(sender: UIButton) {
        selectedIndex = 2
        let vc = storyboard?.instantiateViewController(withIdentifier: "RecordingOptionsViewController") as! RecordingOptionsViewController
        self.present(vc, animated: true, completion: nil)
    }
}

//
//  UserSettingTableViewCell.swift
//  Infotex
//
//  Created by Wasiq Tayyab on 29/09/2021.
//

import UIKit

class UserSettingTableViewCell: UITableViewCell {
    //MARK:- OUTLET
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgNext: UIImageView!
    @IBOutlet weak var imgSetting: UIImageView!
    @IBOutlet weak var lblSetting: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  CodeTicketViewController.swift
//  Infotex
//
//  Created by Mac on 04/09/2021.
//

import UIKit
import MercariQRScanner
class CodeTicketViewController: UIViewController, UITextFieldDelegate {

    //MARK:- OUTLET
    
    @IBOutlet weak var btnConfirmCodeTicket: UIButton!
    @IBOutlet weak var tfCode: UITextField!
    @IBOutlet weak var btnScanner: UIButton!
    @IBOutlet weak var lblInstruction: UILabel!
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    var myUser:[User]? {didSet{}}
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblInstruction.text = "If you are an Influencer with an agency,\n please provide the Affliated Code from your agency."
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.myUser =  User.readUserFromArchive()
        textFieldSetup()
    }
    
   
    //MARK:- TEXTFIELD PLACEHOLDER
    
    private func textFieldSetup(){
        tfCode.delegate = self
        tfCode.attributedPlaceholder = NSAttributedString(string: "8999", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        tfCode.text = affiliated_code
    }
    
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func confirmCodeTicket(_ sender: UIButton) {
        
        if !AppUtility!.isEmail(self.tfCode.text!){
            AppUtility?.showToast(string: "Please enter your coupon code", view: self.view)
            return
        }
    }
    
    @IBAction func scannerButtonPressed(_ sender: UIButton) {
       
        let vc = storyboard?.instantiateViewController(withIdentifier: "BarCodeScannerViewController")as! BarCodeScannerViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- API Handler
    private func VerifyCouponCode(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.verifyCouponCode(user_id:(self.myUser?[0].Id)!, coupon_code: self.tfCode.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    let userObj = resp!["msg"] as! [String:Any]
                    UserObject.shared.Objresponse(response: userObj,isLogin: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        let dataObj = userObj["User"] as! [String:Any]
                        print("dataObj",dataObj)
                        
                        if dataObj["role"] as! String == "influencer"{
                            
                            if dataObj["referral_used_user_id"] as! String == "0"{
                                
                                let story = UIStoryboard(name: "influncer", bundle: nil)
                                let vc = story.instantiateViewController(withIdentifier: "SignUpInfluencerViewController") as! SignUpInfluencerViewController
                                let nav = UINavigationController(rootViewController: vc)
                                nav.navigationBar.isHidden = true
                                self.view.window?.rootViewController = nav
                                
                            }else {
                                
                                let story = UIStoryboard(name: "influncer", bundle: nil)
                                let vc = story.instantiateViewController(withIdentifier: "CustomTabBarController") as! CustomTabBarController
                                let nav = UINavigationController(rootViewController: vc)
                                nav.navigationBar.isHidden = true
                                self.view.window?.rootViewController = nav
                        
                                
                            }
                    
                        }else if  dataObj["role"] as! String == "agency"{
                            
                            let story = UIStoryboard(name: "influncer", bundle: nil)
                            let vc = story.instantiateViewController(withIdentifier: "CustomTabBarController") as! CustomTabBarController
                            let nav = UINavigationController(rootViewController: vc)
                            nav.navigationBar.isHidden = true
                            self.view.window?.rootViewController = nav
            
                        }else{
                            let story = UIStoryboard(name: "influncer", bundle: nil)
                            let vc = story.instantiateViewController(withIdentifier: "CustomTabBarController") as! CustomTabBarController
                            let nav = UINavigationController(rootViewController: vc)
                            nav.navigationBar.isHidden = true
                            self.view.window?.rootViewController = nav
                        }
                    }
                  
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
    }

}

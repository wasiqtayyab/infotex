//
//  WalletTableViewCell.swift
//  Infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit

class WalletTableViewCell: UITableViewCell {
    //MARK:- OUTLET
    
    @IBOutlet weak var lblDiamond: UILabel!
    @IBOutlet weak var btnDollar: UIButton!
    @IBOutlet weak var imgDiamond: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

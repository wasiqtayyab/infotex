//
//  WalletViewController.swift
//  Infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit
import SwiftyStoreKit
class WalletViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tblDiamond: UITableView!
    @IBOutlet weak var btnCashOut: UIButton!
    @IBOutlet weak var btnBuyDiamond: UIButton!
    @IBOutlet weak var lblDiamond: UILabel!
    lazy var selectIndex = 0
    
    //MARK:- VARS
    
    var myUser: [User]? {didSet {}}
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    let diaArr = [["diamond":"70 Diamonds","dollar":"$1","product_ID":"com.infootex.live.Buy70"],
                  ["diamond":"350 Diamonds","dollar":"$5","product_ID":"com.infootex.live.Buy350"],
                  ["diamond":"1400 Diamonds","dollar":"$20","product_ID":"com.infootex.live.Buy1400"],
                  ["diamond":"3500 Diamonds","dollar":"$50","product_ID":"com.infootex.live.Buy3500"],
                  ["diamond":"7000 Diamonds","dollar":"$100","product_ID":"com.infootex.live.Buy7000"]]
    
    
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        tblDiamond.delegate = self
        tblDiamond.dataSource = self
        readArchive()
       // self.fetchProduct()
        
    }
    
    //MARK:- READ ARCHIVE
    
    private func readArchive(){
        self.myUser = User.readUserFromArchive()
        if self.myUser?.count != 0 && self.myUser != nil {
            user_id = (self.myUser?[0].Id)!
            lblDiamond.text = (self.myUser?[0].wallet)!
        }
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func buyOfficialDiamondButtonPressed(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "TopUpViewController")as! TopUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func cashOutButtonPressed(_ sender: UIButton) {
        coin_worth = lblDiamond.text!
        let vc = storyboard?.instantiateViewController(withIdentifier: "TotalDiamondsViewController")as! TotalDiamondsViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    

    
    //MARK:- PURCHASE COIN API
    
    private func purchaseCoinApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.purchaseCoin(user_id: user_id, coin: coin_purchased, title: coin_pressed, price: coin_price) { [self] (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let diamond = Int(lblDiamond.text!)
                    lblDiamond.text = String("\(diamond! + 7000)")
                    
                    let userObj = resp!["msg"] as! [String:Any]
                    UserObject.shared.Objresponse(response: userObj,isLogin: false)
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
}

//MARK:- TABLE VIEW DELEGATE
extension WalletViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return diaArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletTableViewCell", for: indexPath)as! WalletTableViewCell
        cell.btnDollar.setTitle(diaArr[indexPath.row]["dollar"], for: .normal)
        cell.lblDiamond.text = diaArr[indexPath.row]["diamond"]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectIndex =  indexPath.row
    
        let diamonds = diaArr[indexPath.row]["diamond"]!.components(separatedBy: " ")
        
        let first_diamond = diamonds[0]
        print("first_diamond",first_diamond)
        let rupee = diaArr[indexPath.row]["dollar"]!.components(separatedBy: "$")
        let second_rupee = rupee[1]
        print("second_rupee",second_rupee)
        coin_purchased = first_diamond
        coin_pressed = diaArr[indexPath.row]["diamond"]!
        coin_price = second_rupee
        self.purchase(purchase: diaArr[indexPath.row]["product_ID"] as! String)
    }
    
    
    func fetchProduct(){
        SwiftyStoreKit.retrieveProductsInfo(["com.bundleIdentifier.live.Buy700"]) { result in
            if let product = result.retrievedProducts.first {
                let priceString = product.localizedPrice!
                print("Product: \(product.localizedTitle), price: \(priceString)")
            }
            else if let invalidProductId = result.invalidProductIDs.first {
                print("Invalid product identifier: \(invalidProductId)")
            }
            else {
                print("Error: \(result.error)")
            }
        }
    }
    
    
    
    func purchase(purchase : String) {
        self.loader.isHidden =  false
        
        SwiftyStoreKit.purchaseProduct(purchase) { result in
            
            self.loader.isHidden =  true
            if case .success(let purchase) = result {
                let downloads = purchase.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                print(purchase)
            }
            if let alert = self.alertForPurchaseResult(result) {
                self.showAlert(alert)
            }
        }
    }
    
    
    func showAlert(_ alert: UIAlertController) {
        guard self.presentedViewController != nil else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func alertWithTitle(_ title: String, message: String) -> UIAlertController {

        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }

    
    // swiftlint:disable cyclomatic_complexity
    func alertForPurchaseResult(_ result: PurchaseResult) -> UIAlertController? {
        switch result {
        case .success(let purchase):
            print("Purchase Success: \(purchase.productId)")
            self.purchaseCoinApi()
            return nil
        case .error(let error):
            print("Purchase Failed: \(error)")
            switch error.code {
            case .unknown: return alertWithTitle("Purchase failed", message: error.localizedDescription)
            case .clientInvalid: // client is not allowed to issue the request, etc.
                return alertWithTitle("Purchase failed", message: "Not allowed to make the payment")
            case .paymentCancelled: // user cancelled the request, etc.
                return nil
            case .paymentInvalid: // purchase identifier was invalid, etc.
                return alertWithTitle("Purchase failed", message: "The purchase identifier was invalid")
            case .paymentNotAllowed: // this device is not allowed to make the payment
                return alertWithTitle("Purchase failed", message: "The device is not allowed to make the payment")
            case .storeProductNotAvailable: // Product is not available in the current storefront
                return alertWithTitle("Purchase failed", message: "The product is not available in the current storefront")
            case .cloudServicePermissionDenied: // user has not allowed access to cloud service information
                return alertWithTitle("Purchase failed", message: "Access to cloud service information is not allowed")
            case .cloudServiceNetworkConnectionFailed: // the device could not connect to the nework
                return alertWithTitle("Purchase failed", message: "Could not connect to the network")
            case .cloudServiceRevoked: // user has revoked permission to use this cloud service
                return alertWithTitle("Purchase failed", message: "Cloud service was revoked")
            default:
                return alertWithTitle("Purchase failed", message: (error as NSError).localizedDescription)
            }
        }
    }
}

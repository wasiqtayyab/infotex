//
//  MyPackageInfluencerViewController.swift
//  Infotex New Screen
//
//  Created by Wasiq Tayyab on 12/10/2021.
//

import UIKit

class MyPackageInfluencerViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var btnRenewOpenSea: UIButton!
    @IBOutlet weak var btnPremium: UIButton!
    
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func premiumSubscribeButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PremiumSubscribeViewController")as! PremiumSubscribeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func RenewOpenSeaButtonPressed(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "OpenSeaPackageViewController")as! OpenSeaPackageViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    


}

//
//  InfluncerBookLiveStreamingViewController.swift
//  Infotex
//
//  Created by Mac on 14/10/2021.
//

import UIKit
  
protocol InfluencerBookSuccess:class {
    func influencerBookSuccess()
}


class InfluncerBookLiveStreamingViewController: UIViewController {
    
    
    //MARK:- Outlets
    
    @IBOutlet weak var btnInfluncerFee: UIButton!
    @IBOutlet weak var lblDiamonds: UILabel!
    var myUser:[User]? {didSet{}}
    var delegateSuccess:InfluencerBookSuccess!
    
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    //MARK:- SetupView
    
    func setupView(){
        
        self.myUser = User.readUserFromArchive()
        if self.myUser?.count != 0 && self.myUser != nil {
            user_id = (self.myUser?[0].Id)!
            self.lblDiamonds.text = (self.myUser?[0].wallet)!
        }
        self.btnInfluncerFee.setTitle("1hr : \(AppUtility?.getObject(forKey: "coins") as! String) Diamonds", for: .normal)
    }
    
    //MARK:- ViewDidDisAppear
    
   
    
    //MARK:- Switch Action
    
    //MARK:- Button Action
    
    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated:true,completion:nil)
    }
    
    @IBAction func btnTopUpAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "WalletViewController")as! WalletViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnNext(_ sender: Any) {
        delegateSuccess.influencerBookSuccess()
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- DELEGATE METHODS
    
    //MARK: TableView
    
    //MARK: CollectionView.
    
    //MARK: Segment Control
    
    //MARK: Alert View
    
    //MARK: TextField
    
    //MARK: Location
    
    //MARK: Google Maps
    
    
}

//
//  LiveSecureVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 20/09/2021.
//

import UIKit
import DPOTPView

class LiveSecureVC: UIViewController {
  
    @IBOutlet weak var imgUserBack: UIImageView!
    
    @IBOutlet weak var blurEffect: UIVisualEffectView!
    @IBOutlet weak var viewBackImage: UIView!
    
    @IBOutlet weak var viewImageBorder: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var imgLock: UIImageView!
    @IBOutlet weak var viewCode: DPOTPView!
    @IBOutlet weak var btnEnterCode: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewBackImage.layer.cornerRadius = self.viewBackImage.frame.size.height / 2
        self.viewImageBorder.layer.cornerRadius = self.viewImageBorder.frame.size.height / 2
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height / 2
        
        self.viewImageBorder.layer.borderWidth = 1.8
        self.viewImageBorder.layer.borderColor = UIColor.white.cgColor
        
        self.viewBackImage.applyGradientView(colors: [#colorLiteral(red: 0.2269999981, green: 0.4939999878, blue: 0.7289999723, alpha: 1),#colorLiteral(red: 0.7799999714, green: 0.4269999862, blue: 0.6589999795, alpha: 1)])
        
        
    }

}

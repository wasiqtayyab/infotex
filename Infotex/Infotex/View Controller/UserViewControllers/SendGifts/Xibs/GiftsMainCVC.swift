//
//  GiftsMainCVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 20/09/2021.
//

import UIKit
import  SDWebImage
class GiftsMainCVC: UICollectionViewCell {

    @IBOutlet weak var cvGifts: UICollectionView!
    @IBOutlet weak var lblComingSoon: UILabel!
    var arrCount = Int()
    var arrData = [[String:Any]]()
    var lastSelectedIndex = -1
    override func awakeFromNib() {
        super.awakeFromNib()
        cvGifts.delegate = self
        cvGifts.dataSource = self
        cvGifts.register(UINib(nibName: "GiftCVC", bundle: nil), forCellWithReuseIdentifier: "GiftCVC")
    }
    
    func setup(count:Int,giftsData:[[String:Any]]){
        if count == 0 {
            lblComingSoon.isHidden = false
        }else{
            lblComingSoon.isHidden = true
        }
        self.arrCount = count
        self.arrData = giftsData
        cvGifts.delegate = self
        cvGifts.dataSource = self
        self.cvGifts.reloadData()
    
    }

}
extension GiftsMainCVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = contentView.frame.width / 3
        
        return CGSize(width: size , height: size )
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GiftCVC", for: indexPath) as! GiftCVC
        
        let obj = arrData[indexPath.row]
        
        cell.lblGiftName.text = obj["title"] as? String
        cell.lblDiamondsCount.text = "\(obj["coin"] as? String ?? "")" + " Diamonds"
        
        if indexPath.row == lastSelectedIndex {
            cell.imgSelected.isHidden =  false
        }else{
            cell.imgSelected.isHidden = true
        }
        
        
        let url = "\(BASE_URL ?? "")" + "\(obj["image"] as! String)"
        print(url)
        cell.imgIcon.sd_imageIndicator = SDWebImageActivityIndicator.white
        cell.imgIcon.sd_setImage(with: URL(string: url), placeholderImage: nil)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        lastSelectedIndex = indexPath.row
        let obj = arrData[indexPath.row]
        giftCount = obj["coin"] as! String
        StickerID = obj["id"] as! String
        self.cvGifts.reloadData()
    }
    
    
}

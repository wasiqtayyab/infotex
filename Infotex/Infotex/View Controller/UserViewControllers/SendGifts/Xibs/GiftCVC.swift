//
//  GiftCVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 20/09/2021.
//

import UIKit

class GiftCVC: UICollectionViewCell {
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    @IBOutlet weak var imgSelected: UIImageView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblGiftName: UILabel!
    @IBOutlet weak var lblDiamondsCount: UILabel!
    @IBOutlet weak var imgDiamond: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

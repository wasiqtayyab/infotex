//
//  SendGiftsVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 20/09/2021.
//

import UIKit

class SendGiftsVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    @IBOutlet weak var cvMenuTAbs: UICollectionView!
    @IBOutlet weak var cvGifts: UICollectionView!
    @IBOutlet weak var lblCoins: UILabel!
    
    //MARK:- variables

    var arrMenu = ["one","twoTwo","threeee","four","five5555","six"]
    var arrMenuCate = [[String:Any]]()
    var gifts = [[[String:Any]]]()
    var arrMenuDetail = [[String:Any]]()
    var arrBig = [[String:Any]]()
    var aDict = [String:Any]()
    var lastSelectIndex = 0
    var myUser:[User]? {didSet{}}
    var videoObj:videoMainMVC!
    
  
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cvMenuTAbs.delegate = self
        cvMenuTAbs.dataSource = self

        cvGifts.delegate = self
        cvGifts.dataSource = self
        
        cvGifts.register(UINib(nibName: "GiftsMainCVC", bundle: nil), forCellWithReuseIdentifier: "GiftsMainCVC")
        
        getCategories()
        
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.myUser = User.readUserFromArchive()
        self.lblCoins.text! = self.myUser?[0].wallet ?? "0"
    }
    
    
    //MARK:- API Handler

    func getCategories(){
        self.activityLoader.startAnimating()
        ApiHandler.sharedInstance.showGiftCategory(showGiftCategories: "") { (isSuccess, resp) in
            
            if isSuccess{
                let code = resp?.value(forKey: "code") as! NSNumber
                if code == 200{
                    let msgArr = resp?.value(forKey: "msg") as! NSArray
                    for objMsg in msgArr{
                        
                        let dict = objMsg as! NSDictionary
                        let cateName = dict.value(forKey: "GiftCategory") as! [String:Any]
                        let giftDetail = dict.value(forKey: "Gift") as! [[String:Any]]
                        
                        let aName = cateName["title"] as! String
                        
                        self.aDict["\(aName)"] = giftDetail
                       print("aswq: ",giftDetail)
                        self.gifts.append(giftDetail)
                        //self.gifts.append(contentsOf: giftDetail)
                        self.arrMenuCate.append(cateName)
                    }
                    self.activityLoader.hidesWhenStopped  = true
                    self.activityLoader.stopAnimating()
//                    refreshLoader =  false
                    self.cvMenuTAbs.delegate = self
                    self.cvMenuTAbs.dataSource =  self
                    
                    self.cvGifts.delegate = self
                    self.cvGifts.dataSource = self
                    
                    self.cvMenuTAbs.reloadData()
                    self.cvGifts.reloadData()
                }
            }else{
                print(resp)
            }
        }
    }
    
    //MARK:- Button actions

    
    @IBAction func btnDimissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnBuyCoin(_ sender: Any) {
        
        if let rootViewController = UIApplication.topViewController() {
            let story:UIStoryboard = UIStoryboard(name: "User", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
            vc.hidesBottomBarWhenPushed = true
            rootViewController.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
    @IBAction func btnSendCoin(_ sender: Any) {
        
        
        if StickerID == "0"{
            AppUtility?.showToast(string: "Please send one gift at least", view: self.view)
            return
        }
        self.send_Coin(sticker_id: StickerID, gift_count:giftCount)
    }
    
    //MARK:- API Handler
    func send_Coin(sticker_id:String,gift_count:String){
        
        self.myUser = User.readUserFromArchive()
        ApiHandler.sharedInstance.sendGifts(sender_id:(self.myUser?[0].Id)!, receiver_id: videoObj.userID, gift_id: sticker_id, gift_count:gift_count,live_streaming_id: "0",video_id: "0"){ (isSuccess, response) in
            if isSuccess{
                print(response)
                let dic = response as! NSDictionary
                let code = dic["code"] as! NSNumber
                if(code == 200){
                    let count =   Int((self.myUser?[0].wallet)!)! -  Int(giftCount)!
                    self.lblCoins.text! =  String(count)
                    giftCount = "0"
                    StickerID = "0"
                }else{
                    AppUtility?.displayAlert(title: "Infotex", messageText: response?.value(forKey: "msg") as! String, delegate: self)
                    print("failed: ",response as Any)
                }
            }
            
        }
    }
}

//MARK:- collectionView

extension SendGiftsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == cvMenuTAbs{
            let obj = arrMenuCate[indexPath.row]
                let text = obj["title"] as? String
            let cellWidth = (text!.size(withAttributes:[.font: UIFont(name: "Prompt-Medium", size: 16.0)]).width ?? 15) + 20.0
                return CGSize(width: cellWidth, height: 39.0)
            
        }else{
            return CGSize(width: self.cvGifts.frame.size.width, height: self.cvGifts.frame.size.height)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == cvMenuTAbs{
            return arrMenuCate.count
        } else{
            return aDict.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == cvMenuTAbs {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuOptionCVC", for: indexPath) as! MenuOptionCVC
            
            let obj = arrMenuCate[indexPath.row]
            cell.lblMenuOption.text = obj["title"] as? String
          
            if indexPath.row == lastSelectIndex {
                cell.viewBottomLine.isHidden = false
            }else{
                cell.viewBottomLine.isHidden = true
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GiftsMainCVC", for: indexPath) as! GiftsMainCVC
            
            let obj = arrMenuCate[indexPath.row]
            let aName = obj["title"] as? String
            let anOj = aDict[aName!] as! [[String:Any]]
            
            print(anOj.count)
            
            cell.setup(count:anOj.count, giftsData: anOj )
            return cell
   
            
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = arrMenuCate[indexPath.row]
        let aNAme = obj["title"] as? String
        
        
        
        print(aDict[aNAme!]!)
        if collectionView == cvMenuTAbs{
            self.cvGifts.scrollToItem(at: indexPath, at: .left, animated: true)
        }else{
            print("asdssdsd")
        }
        
       
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == cvGifts {
            let currentindex = Int(scrollView.contentOffset.x / cvGifts.frame.size.width)
            let aNum = currentindex
            lastSelectIndex = currentindex
            self.cvMenuTAbs.reloadData()
            print(aNum)
            
            let indexPath = IndexPath(row: currentindex, section: 0)
            self.cvMenuTAbs.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
}

//
//  InfluencerSubscribePackageViewController.swift
//  Infotex
//
//  Created by Mac on 03/09/2021.
//

import UIKit

class InfluencerSubscribePackageViewController: UIViewController {
    
    //MARK:- OUTLET
   
    @IBOutlet weak var btn12Month: UIButton!
    @IBOutlet weak var lblDiamond: UILabel!
    @IBOutlet weak var btnTopUp: UIButton!
    var daimond = "0"
    //MARK:- VARS
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    var myUser: [User]? {didSet {}}
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btn12Month.applyGradient(colors: [#colorLiteral(red: 0.2274509804, green: 0.4941176471, blue: 0.7294117647, alpha: 1), #colorLiteral(red: 0.7803921569, green: 0.4274509804, blue: 0.6588235294, alpha: 1)])
        self.btnTopUp.applyGradient(colors: [#colorLiteral(red: 0.2274509804, green: 0.4941176471, blue: 0.7294117647, alpha: 1), #colorLiteral(red: 0.7803921569, green: 0.4274509804, blue: 0.6588235294, alpha: 1)])
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.readArchive()
    }
    
    //MARK:- READ ARCHIVE
    
    private func readArchive(){
        self.myUser = User.readUserFromArchive()
        print("myUser?.count",myUser?.count)
        if (self.myUser?.count)! != 0 && self.myUser != nil {
            user_id = (self.myUser?[0].Id)!
            self.lblDiamond.text = self.daimond
        }
       
        
    }
    
    //MARK:- BUTTON ACTION
    
    
    @IBAction func topUpButtonPressed(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "WalletViewController")as! WalletViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func monthButtonPressed(_ sender: UIButton) {
        if sender.tag == 0{
            self.purchaseCoinApi(package: "one_influencer", coins: "1000", month: "1")
        }else if sender.tag == 1 {
            self.purchaseCoinApi(package: "one_influencer", coins: "2000", month: "3")
        }else if sender.tag == 2 {
            self.purchaseCoinApi(package: "one_influencer", coins: "4000", month: "6")
        }else if sender.tag == 3 {
            self.purchaseCoinApi(package: "one_influencer", coins: "7000", month: "12")
        }
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- PURCHASE COIN API
    
   func purchaseCoinApi(package:String,coins:String,month:String){
        self.loader.isHidden = false
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let now = Date()
    let dateString = formatter.string(from:now)
        self.myUser = User.readUserFromArchive()
    ApiHandler.sharedInstance.subscribePackage(user_id:(self.myUser?[0].Id)!,package:package, coins:coins, month:month, subscribe_expiry_date: dateString) {(isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    let msg =  resp?.value(forKey: "msg") as! [String:Any]
                    let diamond = Int(self.lblDiamond.text!)
                    self.lblDiamond.text = String("\(diamond! + 7000)")
                    AppUtility?.saveObject(obj: "2", forKey: "Package")
                    let story = UIStoryboard(name: "influncer", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "CustomTabBarController") as! CustomTabBarController
                    let nav = UINavigationController(rootViewController: vc)
                    nav.navigationBar.isHidden = true
                    self.view.window?.rootViewController = nav
                    
                    
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
}

//
//  OpenSeaPackageViewController.swift
//  Infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit

class OpenSeaPackageViewController: UIViewController {
    
    //MARK:- OUTLET
    
   
    @IBOutlet weak var btnTopUp: UIButton!
    @IBOutlet weak var btn1Month: UIButton!
    @IBOutlet weak var btn3Month: UIButton!
    @IBOutlet weak var btn6Month: UIButton!
    @IBOutlet weak var btn12Month: UIButton!
    @IBOutlet weak var lblDiamond: UILabel!
    var diamond = "0"
    //MARK:- VARS
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    var myUser: [User]? {didSet {}}

    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.readArchive()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.readArchive()
    }
    
    
    //MARK:- READ ARCHIVE
    
    private func readArchive(){
        self.myUser = User.readUserFromArchive()
        print("myUser?.count",myUser?.count)
      
        if (self.myUser?.count)! != 0 && self.myUser != nil {
            user_id = (self.myUser?[0].Id)!
            
            lblDiamond.text = (self.myUser?[0].wallet)!
        }
       
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func topUpButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "WalletViewController")as! WalletViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func monthButtonPressed(_ sender: UIButton) {
        if sender.tag == 0{
            self.purchaseCoinApi(package: "open_sea", coins: "3000", month: "1")
        }else if sender.tag == 1 {
            self.purchaseCoinApi(package: "open_sea", coins: "5000", month: "3")
        }else if sender.tag == 2 {
            self.purchaseCoinApi(package: "open_sea", coins: "9000", month: "6")
        }else if sender.tag == 3 {
            self.purchaseCoinApi(package: "open_sea", coins: "15000", month: "12")
        }
    }
    
  
    //MARK:- PURCHASE COIN API
    
   func purchaseCoinApi(package:String,coins:String,month:String){
        self.loader.isHidden = false
    
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let now = Date()
    let dateString = formatter.string(from:now)
    
        self.myUser = User.readUserFromArchive()
    ApiHandler.sharedInstance.subscribePackage(user_id:(self.myUser?[0].Id)!,package:package, coins:coins, month:month, subscribe_expiry_date: dateString) {(isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    self.myUser = User.readUserFromArchive()
                    self.myUser![0].subscribe_package = "1"
                    if User.saveUserToArchive(user: self.myUser!){
                        print("Package")
                        let diamond = Int(self.lblDiamond.text!)
                        self.lblDiamond.text = String("\(diamond! + 7000)")
                        AppUtility?.saveObject(obj: "1", forKey: "Package")
                        let story = UIStoryboard(name: "influncer", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "CustomTabBarController") as! CustomTabBarController
                        let nav = UINavigationController(rootViewController: vc)
                        nav.navigationBar.isHidden = true
                        self.view.window?.rootViewController = nav
                    }
                 
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                AppUtility?.displayAlert(title: "Alert", messageText:"Something went wrong" , delegate: self)

            }
        }
        
   }
}

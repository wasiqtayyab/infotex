//
//  AllActivityViewController.swift
//  Infotex
//
//  Created by Wasiq Tayyab on 07/09/2021.
//

import UIKit

class AllActivityViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var btnSignUp: UIButton!
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        if let rootViewController = UIApplication.topViewController() {
            let story:UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "InfotexViewController") as!  InfotexViewController
            rootViewController.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//
//  BookingInfluencerByUserViewController.swift
//  Infotex
//
//  Created by Mac on 11/10/2021.
//

import UIKit

protocol SelectTime: class {
    func selectTime(strTime: String,id:String,strdate:String)
}

class BookingInfluencerByUserViewController: UIViewController ,UITextFieldDelegate{
    
    //MARK:- Outlets
    
    @IBOutlet weak var cvBookingTimes: UICollectionView!
    @IBOutlet weak var btnSelectbookingDate: UIButton!
    @IBOutlet weak var tfSelectDate: UITextField!
    var Timedelegate:SelectTime!
    var arrBooked = [[String:Any]]()
    var myUser:[User]? {didSet{}}
    var datePicker = UIDatePicker()
    var arrBooking = [["time":"00:00-01:00","status":"0"],
                      ["time":"01:00-02:00","status":"0"],
                      ["time":"02:00-03:00","status":"0"],
                      ["time":"03:00-04:00","status":"0"],
                      ["time":"04:00-05:00","status":"0"],
                      ["time":"05:00-06:00","status":"0"],
                      ["time":"06:00-07:00","status":"0"],
                      ["time":"07:00-08:00","status":"0"],
                      ["time":"08:00-09:00","status":"0"],
                      ["time":"09:00-10:00","status":"0"],
                      ["time":"10:00-11:00","status":"0"],
                      ["time":"11:00-12:00","status":"0"],
                      ["time":"12:00-13:00","status":"0"],
                      ["time":"13:00-14:00","status":"0"],
                      ["time":"14:00-15:00","status":"0"],
                      ["time":"15:00-16:00","status":"0"],
                      ["time":"16:00-17:00","status":"0"],
                      ["time":"17:00-18:00","status":"0"],
                      ["time":"18:00-19:00","status":"0"],
                      ["time":"19:00-20:00","status":"0"],
                      ["time":"20:00-21:00","status":"0"],
                      ["time":"21:00-22:00","status":"0"],
                      ["time":"22:00-23:00","status":"0"],
                      ["time":"23:00-24:00","status":"0"]]
    
    
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myUser = User.readUserFromArchive()
        self.btnSelectbookingDate.setTitle("Select Booking Time", for: .normal)
        self.tfSelectDate.isHidden = false
        self.cvBookingTimes.delegate = self
        self.cvBookingTimes.dataSource = self
        self.btnSelectbookingDate.layer.cornerRadius = self.btnSelectbookingDate.frame.size.height / 2
        self.currentDate()
        self.showDatePicker()
        self.tfSelectDate.delegate =  self
        if #available(iOS 13.4, *){
            datePicker.preferredDatePickerStyle = .wheels
        }
       
    }
   
    //MARK:- Button Action
    
    @IBAction func backPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closePressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func selectBookingPressed(_ sender: UIButton) {
      
        if self.btnSelectbookingDate.titleLabel?.text == "Select Booking Time"{
            
            if self.arrBooked.count == 0{
                AppUtility?.showToast(string: "Please select at least one slot", view: self.view)
                return
            }
            self.tfSelectDate.isHidden = true
         
            
        }else{
            self.btnSelectbookingDate.setTitle("Select Booking Time", for: .normal)
            self.tfSelectDate.isHidden = false
            
        }
    }
}

//MARK:- Extensions

extension BookingInfluencerByUserViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (self.cvBookingTimes.frame.size.width - 50) / 3
        return  CGSize(width: size, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrBooking.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookingTimeCVC", for: indexPath) as! BookingTimeCVC
        
        cell.lblTime.text = "\(arrBooking[indexPath.row]["time"] ?? "")"
        
        if arrBooking[indexPath.row]["status"] == "0"{
            cell.viewBack.backgroundColor = UIColor(named: "iron")
            cell.imgBack.isHidden = true
            cell.imgUSer.isHidden = true
            
        }
        if arrBooking[indexPath.row]["status"] == "1" {
            cell.viewBack.backgroundColor = UIColor(named: "light shaft")
            cell.viewBack.isHidden = false
            cell.imgBack.isHidden = true
            cell.imgUSer.isHidden = true
            
        }
        if arrBooking[indexPath.row]["status"] == "2"{
            cell.viewBack.backgroundColor = UIColor(named: "light shaft")
            cell.viewBack.isHidden = true
            cell.imgBack.isHidden = false
            cell.imgUSer.isHidden = true
            cell.imgBack.image = UIImage(named: "gradiant_back")
        }
        if arrBooking[indexPath.row]["status"] == "3"{
            cell.viewBack.backgroundColor = UIColor(named: "light shaft")
            cell.viewBack.isHidden = true
            cell.imgUSer.isHidden = false
            cell.imgBack.isHidden = false
            cell.imgBack.image = UIImage(named: "gradient_green")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        let aNum = arrBooking[indexPath.row]["status"] ?? ""
        if aNum != "0" {
            if aNum == "1"{  //not available time 
                AppUtility?.showToast(string: "Influncer not available", view: self.view)
            }else if aNum == "3"{  //not available time
                AppUtility?.showToast(string: "Slot already booked", view: self.view)
            }else if aNum == "2" {
                var obj   =  self.arrBooking[indexPath.row]
                Timedelegate.selectTime(strTime: arrBooking[indexPath.row]["time"] as! String, id:  arrBooking[indexPath.row]["id"] as! String, strdate: self.tfSelectDate.text!)
                self.navigationController?.popViewController(animated: true)
                print("Booking Time ID", arrBooking[indexPath.row]["id"])
            }
        }
    }
    
    
    //MARK:-Date Picker End
    
    func currentDate(){
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.dateFormat = "yyyy-MM-dd"
        self.btnSelectbookingDate.setTitle(formatter.string(from: currentDateTime), for: .normal)
        self.showInfluecerBooking(booking: formatter.string(from: currentDateTime), strDate: currentDateTime)
    }
    
    
    
    func showDatePicker(){
        
        self.datePicker.datePickerMode = .date
        self.datePicker.minimumDate = Date()
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePickerEndTo));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePickerEndTo));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        self.tfSelectDate.inputAccessoryView = toolbar
        self.tfSelectDate.inputView = datePicker
    }
    
    @objc func donedatePickerEndTo(){
        
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd"
        self.btnSelectbookingDate.setTitle(formatter1.string(from: datePicker.date), for: .normal)
        print("Date Of End : \(formatter1.string(from: datePicker.date))")
        self.showInfluecerBooking(booking:formatter1.string(from: datePicker.date) , strDate: datePicker.date)
        self.view.endEditing(true)
        
    }
    
    @objc func cancelDatePickerEndTo(){
        self.view.endEditing(true)
    }
    //MARK:- API Handler
    
    func showInfluecerBooking(booking:String,strDate:Date){
        self.arrBooking.removeAll()
        ApiHandler.sharedInstance.showInfluecerBooking(user_id: AppUtility?.getObject(forKey: "OtherUserID")  as! String, strDate: booking) { (isSuccess, response) in
            
            if isSuccess{
                
                if let res = response?.value(forKey: "msg") as? [[String:Any]]{
                    for obj in res{
                        let booking =  obj["BookingTime"] as! [String:Any]
                        if let avaiable = booking["available"] as?[String:Any]{
                            let BookingAvailableTiming = avaiable["UserBookingAvailableTiming"] as?[String:Any]
                            if let Booking =  avaiable["Booking"] as?[String:Any]{
                                if Booking["id"] is NSNull{
                                    self.arrBooking.append(["time":booking["time"] as! String,"status":"2","id":BookingAvailableTiming!["id"] as! String])
                                    
                                }else{
                                    if self.myUser?[0].Id == BookingAvailableTiming!["user_id"] as! String{
                                        self.arrBooking.append(["time":booking["time"] as! String,"status":"3","id":BookingAvailableTiming!["id"] as! String])
                                    }else{
                                        self.arrBooking.append(["time":booking["time"] as! String,"status":"1","id":BookingAvailableTiming!["id"] as! String])
                                    }
                                  
                               }
                            }else{
                                self.arrBooking.append(["time":booking["time"] as! String,"status":"2","id":booking["id"] as! String,"available":"1"])
                            }
                            
                        }else{
                            self.arrBooking.append(["time":booking["time"] as! String,"status":"1","id":booking["id"] as! String])
                        }
                    }
                    //NSDate Structure
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd"
                    let now = Date()
                    let dateString = formatter.string(from:now)
                    if dateString == self.btnSelectbookingDate.titleLabel?.text! {
                        for var i in 0..<self.arrBooking.count{
                            
                            let hour = Calendar.current.component(.hour, from: strDate)
                            print("hour by looping",hour)
                            let currentTime =  "\(hour).00"
                            
                            var objTime = self.arrBooking[i]
                            let timeZone =  objTime["time"]?.components(separatedBy: " - ")
                            
                            if timeZone![0] as! String > "\(currentTime)"{}else{
                                objTime.updateValue("0", forKey: "status")
                            }
                            
                            self.arrBooking.remove(at: i)
                            self.arrBooking.insert(objTime, at: i)
                            self.cvBookingTimes.reloadData()
                        }
                    }
                    
                    
                    self.cvBookingTimes.reloadData()
                    
                    self.cvBookingTimes.reloadData()
                }else{
                    AppUtility?.showToast(string: response?.value(forKey: "msg") as! String, view: self.view)
                }
            }else{
                AppUtility?.showToast(string: "Something went wrong", view: self.view)
            }
    }
}
    
}

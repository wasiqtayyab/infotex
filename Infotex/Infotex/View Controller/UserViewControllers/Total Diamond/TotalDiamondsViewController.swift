//
//  TotalDiamondsViewController.swift
//  Infotex
//
//  Created by Mac on 03/09/2021.
//

import UIKit

class TotalDiamondsViewController: UIViewController, UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var btnCashout: UIButton!
    @IBOutlet weak var tfDiamond: UITextField!
    @IBOutlet weak var lblDollar: UILabel!
    @IBOutlet weak var lblDiamond: UILabel!
    
    
    //MARK:- VARS
    var myUser: [User]? {didSet {}}
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tfDiamond.text! = coin_worth
        lblDiamond.text! = coin_worth
        tfDiamond.delegate = self
        self.showCoinWorthApi()
        tfDiamond.addTarget(self, action: #selector(myTextFieldDidChange), for: .editingChanged)
      
        

    }
    
    //MARK:- BUTTON ACTION
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cashOutButtonPressed(_ sender: UIButton) {
        if tfDiamond.text! == "0"{
            Utility.sharedUtility()?.showToast(string: "", view: self.view)
            return
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TopUpViewController")as! TopUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    //MARK:- TEXTFIELD ACTION
    
    @objc func myTextFieldDidChange(_ textField: UITextField) {

        if let amountString = textField.text?.currencyInputFormatting() {
            if textField.text == "0" || textField.text == "" {
                lblDollar.text = "$0.0"
            }else {
                lblDollar.text = "\(amountString)"
            }
            
            
        }
        
    }

    //MARK:- TEXTFIELD DELEGATE
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 7
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    //MARK:- SHOW COIN WORTH API
    
    private func showCoinWorthApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.showCoinWorth { [self] (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    let dic = resp as! NSDictionary
                    let res =  dic.value(forKey: "msg") as! [String:Any]
                    let obj = res["CoinWorth"] as! [String:Any]
                    let price = Double(obj["price"] as! String)
                    let Coinprice = Double(self.tfDiamond.text!)
                   
                    if tfDiamond.text! == "0" {
                        lblDollar.text = "$0.0"
                    }else {
                        print("price",price)
                        print("Coinprice",Coinprice)
                        self.lblDollar.text = "$\(price! * Coinprice!)"
                    }
                    
                
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
        
        
    }
    
    
  
}

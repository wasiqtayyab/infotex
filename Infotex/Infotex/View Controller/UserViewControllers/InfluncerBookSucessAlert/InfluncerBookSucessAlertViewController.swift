//
//  InfluncerBookSucessAlertViewController.swift
//  Infotex
//
//  Created by Mac on 14/10/2021.
//

import UIKit
protocol InfluencerBookSuccessAlert:class {
    func influencerBookSuccessAlert()
}


class InfluncerBookSucessAlertViewController: UIViewController {

  
    //MARK:- Outlets
  
    var delegateSuccessAlert : InfluencerBookSuccessAlert!
      override func viewDidLoad() {
          super.viewDidLoad()
          
          self.setupView()
      }
      
      //MARK:- SetupView
      
      func setupView(){
     
      }
      
    override func viewDidDisappear(_ animated: Bool) {
        delegateSuccessAlert.influencerBookSuccessAlert()
    }
      //MARK:- Switch Action

      //MARK:- Button Action

    @IBAction func btnDismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    
    }
    
    //MARK:- DELEGATE METHODS
      
      //MARK: TableView
      
      //MARK: CollectionView.
      
      //MARK: Segment Control
      
      //MARK: Alert View
      
      //MARK: TextField
      
      //MARK: Location
        
      //MARK: Google Maps
    
     
}

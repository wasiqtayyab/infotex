//
//  TopUpViewController.swift
//  Infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit
import WebKit

class TopUpViewController: UIViewController,WKNavigationDelegate {
    
    //MARK:- Vars
    
    @IBOutlet weak var webView: WKWebView!
  
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        policyUrl()
        
    }
    //MARK:- POLICY URL
    
    private func policyUrl(){
        let url = URL(string: "https://infoex-io.herokuapp.com/rcoin")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
  
}

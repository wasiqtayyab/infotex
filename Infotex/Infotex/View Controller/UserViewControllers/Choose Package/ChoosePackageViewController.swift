//
//  ChoosePackageViewController.swift
//  Infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit

class ChoosePackageViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var btnOpenSea: UIButton!
    @IBOutlet weak var btnTopUp: UIButton!
    @IBOutlet weak var lblDiamond: UILabel!
    var myUser:[User]? {didSet{}}
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myUser = User.readUserFromArchive()
        self.userDetail(userID: (self.myUser?[0].Id)!, OtherUserID: "")
       
    }
    
    //MARK:- BUTTON ACTION
    @IBAction func btnTopUpAction(_ sender: Any) {
        if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "User", bundle: nil)
            let vc  = storyMain.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
            rootViewController.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    
    @IBAction func openSeaButtonPressed(_ sender: UIButton) {
        if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "User", bundle: nil)
            let vc  = storyMain.instantiateViewController(withIdentifier: "OpenSeaPackageViewController") as! OpenSeaPackageViewController
            vc.diamond =  self.lblDiamond.text!
            rootViewController.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    @IBAction func btnDismiss(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- API Handler
    
    func userDetail(userID:String,OtherUserID:String){
        ApiHandler.sharedInstance.showUserDetail(user_id: userID,other_user_id:OtherUserID) { (isSuccess, response) in
            if isSuccess{
                if let res =  response?.value(forKey: "msg") as? [String:Any]{
                    let objData = UserDetail.shared.ObjUserDetailResponse(res:res)
                    self.lblDiamond.text =  objData.wallet
                }
            }
        }
    }
}

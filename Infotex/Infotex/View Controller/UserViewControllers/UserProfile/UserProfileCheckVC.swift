//
//  UserProfileCheckVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 01/09/2021.
//

import UIKit

class UserProfileCheckVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var tblUserProfile: UITableView!
    @IBOutlet weak var btnUserProfile: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    var userData = [userMVC]()
    lazy var userID = "0"
    var myUser:[User]? {didSet{}}
    var isOtherUser =  false
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        
        return refreshControl
    }()
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblUserProfile.refreshControl = refresher
        
        tblUserProfile.register(UINib(nibName: "UserTopTVC", bundle: nil), forCellReuseIdentifier: "UserTopTVC")
        
        tblUserProfile.register(UINib(nibName: "UserProfileHV", bundle: nil), forHeaderFooterViewReuseIdentifier: "UserProfileHV")
        
        tblUserProfile.register(UINib(nibName: "UserProfileTVC", bundle: nil), forCellReuseIdentifier: "UserProfileTVC")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reLoadTbl(notification:)), name: Notification.Name("reloadTableView"), object: nil)
        
        self.myUser = User.readUserFromArchive()
        
       
        
        if isOtherUser == true{
            self.btnBack.isHidden =  false
            self.btnUserProfile.isUserInteractionEnabled =  false
            self.userDetail(userID: (self.myUser?[0].Id)!, OtherUserID: self.userID)
        }else{
            self.btnBack.isHidden =  true
            self.btnUserProfile.isUserInteractionEnabled =  true
            self.myUser = User.readUserFromArchive()
            if (myUser?.count != 0) && self.myUser != nil{
                self.userDetail(userID:(self.myUser?[0].Id)!, OtherUserID: "")
                AppUtility?.saveObject(obj:"" , forKey: "OtherUserID")
                AppUtility?.saveObject(obj: "false" , forKey: "isOtherUser")
            }else{
                let story:UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                let navController = UINavigationController.init(rootViewController: story.instantiateViewController(withIdentifier: "ProfileViewController"))
                navController.navigationBar.isHidden = true
                navController.modalPresentationStyle = .overFullScreen
                self.present(navController, animated: true, completion: nil)
            }
           
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("reloadTableView"), object: nil)
    }
    @objc func reLoadTbl(notification: Notification) {
        let index = notification.object as! Int
        let indexPath = IndexPath(row: index, section: 1)
        self.tblUserProfile.reloadData()
      }
    //MARK:- Refresher Controller
    
    @objc func requestData() {

        if isOtherUser == true{
            self.userDetail(userID:(self.myUser?[0].Id)!, OtherUserID:self.userID)
        }else{
            self.myUser = User.readUserFromArchive()
            if (myUser?.count != 0) && self.myUser != nil{
                self.userDetail(userID:(self.myUser?[0].Id)!, OtherUserID:self.userID)
                AppUtility?.saveObject(obj:"" , forKey: "OtherUserID")
                AppUtility?.saveObject(obj: "false" , forKey: "isOtherUser")
            }else{
                let story:UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                let navController = UINavigationController.init(rootViewController: story.instantiateViewController(withIdentifier: "ProfileViewController"))
                navController.navigationBar.isHidden = true
                navController.modalPresentationStyle = .overFullScreen
                self.present(navController, animated: true, completion: nil)
            }
           
        }
        let deadline = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.refresher.endRefreshing()
        }
        
    }
    //MARK:- Button actions
    
    @IBAction func menuPressed(_ sender: UIButton) {
        let story = UIStoryboard(name: "User", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "UserSettingViewController")as! UserSettingViewController
        vc.userData =  self.userData
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func userPressed(_ sender: UIButton) {
        let story = UIStoryboard(name: "influncer", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "SwitchAccountViewController") as! SwitchAccountViewController
        present(vc, animated: true, completion: nil)
        
    }
    @IBAction func backPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- API Handler
    
    func userDetail(userID:String,OtherUserID:String){
        ApiHandler.sharedInstance.showUserDetail(user_id: userID, other_user_id: OtherUserID) { (isSuccess, response) in
            if isSuccess{
                if let res =  response?.value(forKey: "msg") as? [String:Any]{
                 let objData = UserDetail.shared.ObjUserDetailResponse(res:res)
                    self.tblUserProfile.delegate = self
                    self.tblUserProfile.dataSource = self
                    self.userData.append(objData)
                    self.btnUserProfile.setTitle("@\(self.userData[0].username)", for: .normal)
                    self.tblUserProfile.reloadData()
                }
            }
        }
    }

}

extension UserProfileCheckVC: UITableViewDelegate, UITableViewDataSource {
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
            return 1
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let  cell = tableView.dequeueReusableCell(withIdentifier: "UserTopTVC", for: indexPath) as! UserTopTVC
            cell.Setup(userData: self.userData[indexPath.row],isOtherUser: self.isOtherUser )
              return cell
        }else{
            let  cell = tableView.dequeueReusableCell(withIdentifier: "UserProfileTVC", for: indexPath) as! UserProfileTVC
              return cell
        }
     
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            return 50
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "UserProfileHV") as! UserProfileHV
        return header
    }
    
    
}


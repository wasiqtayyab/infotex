//
//  SignUpPackageViewController.swift
//  Infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit

class SignUpPackageViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var btnOpenSea: UIButton!
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
    }
    
    //MARK:- BUTTON ACTION
   
    @IBAction func openSeaButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "OpenSeaPackageViewController")as! OpenSeaPackageViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func influencerSubscribeButtonPressed(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "InfluencerSubscribePackageViewController")as! InfluencerSubscribePackageViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func codeTicketButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CodeTicketViewController")as! CodeTicketViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
  

}

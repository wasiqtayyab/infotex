//
//  PrivacyPolicyViewController.swift
//  Infotex
//
//  Created by Mac on 01/09/2021.
//

import UIKit
import WebKit

class PrivacyPolicyViewController: UIViewController,WKUIDelegate,WKNavigationDelegate  {
 
    //MARK:- Vars
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var lblPrivacy: UILabel!
    
    
    var privacy = ""
    //MARK:- View Life Cycle Start here...

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    //MARK:- Setup View
    func setupView() {
      self.WeblinkView()
        lblPrivacy.text = privacy
    }
    
    //MARK:- Utility Methods
    @objc func InternetConnectionActive(notify : Notification){
        if notify.object as? UIViewController == self{
            self.WeblinkView()
        }
    }
    
    
    //MARK:- WeblinkView
    
    func WeblinkView(){
        
        self.webView.load(URLRequest(url: URL(string: "https://infotex.live/privacy.html")!))
        self.webView.allowsBackForwardNavigationGestures = true
        self.webView.navigationDelegate = self
    }
    //MARK:- Button Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        activity.stopAnimating()
        activity.isHidden = true
    }
    
    //MARK: WebView DELEGATE
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
         activity.stopAnimating()
          activity.isHidden = true
        let alert  = UIAlertController(title: "ArroyThai", message: NSLocalizedString("error_400", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert:UIAlertAction) in
            self.webView.reload()
        }))
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        activity.startAnimating()
        activity.isHidden = false
        print("Start to load")
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activity.stopAnimating()
        activity.isHidden = true
        print("finish to load")
      
       
    }
    
    //MARK: API Methods
    
    //MARK:- DELEGATE METHODS
    
    //MARK: TableView
    
    //MARK: Segment Control
    
    //MARK: Alert View
    
    //MARK: TextField
    
    
    
    //MARK:- View Life Cycle End here...
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

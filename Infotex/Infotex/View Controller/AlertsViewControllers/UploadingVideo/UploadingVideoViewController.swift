//
//  UploadingVideoViewController.swift
//  Infotex
//
//  Created by Wasiq Tayyab on 14/09/2021.
//

import UIKit

class UploadingVideoViewController: UIViewController {
    
    
    
    //MARK:- OUTLET
    
    @IBOutlet weak var progressBarView: GradientHorizontalProgressBar!
    @IBOutlet weak var lblUpload: UILabel!
    
    var gradientRed: CGFloat = 18
    var gradientGreen: CGFloat = 127
    var gradientBlue: CGFloat = 192
    
    //MARK:- VIEW DIDLOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.progressBarView.layer.cornerRadius = 4
        NotificationCenter.default.addObserver(self, selector: #selector(self.uploadVideo(notify:)), name: NSNotification.Name(rawValue: "uploadVideo"), object: nil)
        
    }
    
    //Dienit
    @objc func uploadVideo(notify:Notification){
        let gradient = UIColor(red: gradientRed / 255, green: gradientGreen / 255, blue: gradientBlue / 255, alpha: 1)
        let obj = notify.object as! Double
        print("Uploading progress bar:",obj)
        self.progressBarView.gradientColor = gradient
        self.progressBarView.progress = CGFloat(obj)
        self.lblUpload.text = "Uploading \(Int(obj*100))%"
    }
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
    }
    
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        self.changeBackgroundAni()
    }
    
    func changeBackgroundAni() {
        
        UIView.animate(withDuration: 0.3, delay: 0.3, options: .curveEaseOut, animations: {
            
            self.view.backgroundColor = UIColor.black.withAlphaComponent(1.0)
            
        }, completion:nil)
    }
}

//
//  SignInInfotexViewController.swift
//  Infotex
//
//  Created by Wasiq Tayyab on 28/09/2021.
//

import UIKit
import ActiveLabel
class SignInInfotexViewController: UIViewController {
    
    //MARK:- OUTLET
    @IBOutlet weak var btnUser: UIButton!
    @IBOutlet weak var lblTerm: ActiveLabel!
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        termsOfConditionsButton()
    }
    
   
    //MARK:- BUTTON ACTION
    
    @IBAction func userButtonPressed(_ sender: UIButton) {
        
        role = "user"
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
        vc.image = UIImage(named: "avatar_")
       
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func influencerButtonPressed(_ sender: UIButton) {
        
        role = "influencer"
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
        vc.image = UIImage(named: "influncer_white")
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func agencyButtonPressed(_ sender: UIButton) {
        
        role = "agency"
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
        vc.image = UIImage(named: "modeling")
       
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnCloseAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.tabBarController?.selectedIndex = 0
    }
    
    @IBAction func specialEventButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func signInButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "InfotexViewController")as! InfotexViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK:- TERM OF CONDITION
    private func termsOfConditionsButton(){
        
        let customType = ActiveType.custom(pattern: "Terms of Services")
        let customType1 = ActiveType.custom(pattern: "Privacy Policy")
        lblTerm.enabledTypes = [.mention, .hashtag, .url, customType,customType1]
        lblTerm.text = "By Signing up,You agree to our Terms of Services and\n acknowledge that you have read our Privacy Policy to\n learn how we collect, user,and share your data."
        lblTerm.textColor = #colorLiteral(red: 0.6352941176, green: 0.6431372549, blue: 0.6549019608, alpha: 1)
        lblTerm.customColor[customType1] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblTerm.customSelectedColor[customType1] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblTerm.customColor[customType] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblTerm.customSelectedColor[customType] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblTerm.handleCustomTap(for: customType) { element in
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController")as! PrivacyPolicyViewController
            vc.privacy = "Terms of Services"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        lblTerm.handleCustomTap(for: customType1) { element in
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController")as! PrivacyPolicyViewController
            vc.privacy = "Privacy Policy"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }

}

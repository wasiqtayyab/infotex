//
//  SplashScreenViewController.swift
//  Infotex
//
//  Created by Mac on 04/09/2021.
//

import UIKit

class SplashScreenViewController: UIViewController {
    
    //MARK:- VARS
    
    var myUser: [User]? {didSet {}}
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        readDataFromArchive()
        
        
    }
    
    //MARK:- READ DATA FROM ARCHIVE
    
    private func readDataFromArchive(){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { [self] in
            self.myUser = User.readUserFromArchive()
          
            if (myUser?.count != 0) && self.myUser != nil{
                print("user already login")
                if self.myUser?[0].role == "user"{
                    
                    let story = UIStoryboard(name: "influncer", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "CustomTabBarController") as! CustomTabBarController
                    let nav = UINavigationController(rootViewController: vc)
                    nav.navigationBar.isHidden = true
                    self.view.window?.rootViewController = nav
                    
                }else{
                    user_id = (self.myUser?[0].Id)!
                    self.showUserDetailApi()
                }
                
            }else {
                
               /* let vc = storyboard?.instantiateViewController(withIdentifier: "InfotexViewController")as! InfotexViewController
                self.navigationController?.pushViewController(vc, animated: true)*/
                let story = UIStoryboard(name: "influncer", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "CustomTabBarController") as! CustomTabBarController
                let nav = UINavigationController(rootViewController: vc)
                nav.navigationBar.isHidden = true
                self.view.window?.rootViewController = nav
                
            }
            
        }
        
    }
    
    
    //MARK:- Show User Detail Api
    
    private func showUserDetailApi(){
       
        ApiHandler.sharedInstance.showUserDetail(user_id: user_id, other_user_id: "") { (isSuccess, resp) in
            
            if isSuccess {
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let userObj = resp!["msg"] as! [String:Any]
                    let dataObj = userObj["User"] as! [String:Any]
                    print("dataObj",dataObj)
                    
                    if dataObj["role"] as! String == "influencer"{
                        
                        if dataObj["referral_used_user_id"] as! String == "0"{
                            
                            let story = UIStoryboard(name: "influncer", bundle: nil)
                            let vc = story.instantiateViewController(withIdentifier: "SignUpInfluencerViewController") as! SignUpInfluencerViewController
                            let nav = UINavigationController(rootViewController: vc)
                            nav.navigationBar.isHidden = true
                            self.view.window?.rootViewController = nav
                            
                        }else {
                            
                            let story = UIStoryboard(name: "influncer", bundle: nil)
                            let vc = story.instantiateViewController(withIdentifier: "CustomTabBarController") as! CustomTabBarController
                            let nav = UINavigationController(rootViewController: vc)
                            nav.navigationBar.isHidden = true
                            self.view.window?.rootViewController = nav
                    
                            
                        }
                    }else if dataObj["role"] as! String == "agency" {
                        
                        if  dataObj["approve_account_agency"] as! String == "0"{
                            
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AgencyConfirmationViewController")as! AgencyConfirmationViewController
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        }else {
                            
                            print("agency Login")
                            let story = UIStoryboard(name: "influncer", bundle: nil)
                            let vc = story.instantiateViewController(withIdentifier: "CustomTabBarController") as! CustomTabBarController
                            let nav = UINavigationController(rootViewController: vc)
                            nav.navigationBar.isHidden = true
                            self.view.window?.rootViewController = nav
                        }
                        
                    }
                    
                    
                }else{
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
}

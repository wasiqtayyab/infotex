//
//  ForgetPasswordViewController.swift
//  Infotex
//
//  Created by Mac on 03/09/2021.
//

import UIKit

class ForgetEmailViewController: UIViewController, UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var imgGradient: UIImageView!
    
    //MARK:- VARS
    
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldSetup()
        imgGradient.isHidden = true
        btnReset.backgroundColor = UIColor(named: "light shaft")
    }
    
    //MARK:- TEXTFIELD PLACEHOLDER
    
    private func textFieldSetup(){
        tfEmail.delegate = self
        tfEmail.attributedPlaceholder = NSAttributedString(string: "Email address", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
       
        
    }
    //MARK:- BUTTON ACTION
    
    @IBAction func resetButtonPressed(_ sender: UIButton) {
        
        if !AppUtility!.isEmail(tfEmail.text!){
            AppUtility?.showToast(string: "Enter your valid email", view: self.view)
            return
        }
        email = tfEmail.text!
        self.forgetPasswordApi()
        
    }
    
    
    //MARK:- TEXTFIELD DELEGATE
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if !AppUtility!.isEmail(self.tfEmail.text!){
            
            btnReset.backgroundColor = UIColor(named: "light shaft")
            imgGradient.isHidden = true
            return
        }
        
        btnReset.backgroundColor = .clear
        imgGradient.isHidden = false
    }
    
    //MARK:- FORGET PASSWORD API
    
    private func forgetPasswordApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.forgetPassword(email: tfEmail.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgetOtpViewController")as! ForgetOtpViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                   
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    
}

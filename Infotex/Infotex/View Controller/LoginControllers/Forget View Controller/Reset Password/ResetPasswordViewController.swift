//
//  ResetPasswordViewController.swift
//  Infotex
//
//  Created by Mac on 04/09/2021.
//

import UIKit

class ResetPasswordViewController: UIViewController,UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var viewGradient: UIImageView!
    
    //MARK:- VARS
    
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldSetup()
        viewGradient.isHidden = true
        btnNext.backgroundColor = UIColor(named: "light shaft")
        
        
    }
    
    //MARK:- TEXTFIELD PLACEHOLDER
    
    private func textFieldSetup(){
        tfPassword.delegate = self
        tfPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        
        
        if tfPassword.text!.count < 6 {
            AppUtility?.showToast(string: "Enter your password greater than 5", view: self.view)
            return
        }
        
        self.changePasswordForgetApi()
        
       
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK:- TEXTFIELD DELEGATE
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if tfPassword.text!.count > 5 {
            viewGradient.isHidden = false
            btnNext.backgroundColor = .clear
            
        }else {
            
            viewGradient.isHidden = true
            btnNext.backgroundColor = UIColor(named: "light shaft")
            
            
        }
    }
    
    //MARK:- CHANGE PASSWORD FORGET API
    
    private func changePasswordForgetApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.changePasswordForgot(email: email, password: tfPassword.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    if sender_button == 0 {
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InfotexViewController")as! InfotexViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                        
                    }else if sender_button == 1 {
                       
                        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "InfotexViewController")as! InfotexViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }else {
                       
                        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "InfotexViewController")as! InfotexViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
    }
    
}



//
//  ForgetOtpViewController.swift
//  Infotex
//
//  Created by Mac on 04/09/2021.
//

import UIKit
import DPOTPView
class ForgetOtpViewController: UIViewController {

    //MARK:- OUTLET
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var viewOtp: DPOTPView!
    @IBOutlet weak var viewGradient: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var lblTimer: UILabel!
    
    //MARK:- VARS
    
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    var counter = 60
    var timer = Timer()

    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewGradient.isHidden = true
        btnNext.backgroundColor = UIColor(named: "light shaft")
        lblEmail.text = "Your code was sent to \(email)"
        viewOtp.dpOTPViewDelegate = self
        btnResend.isHidden = true
        timerSetup()
       
    }
    
    //MARK:- TIMER
    
    private func timerSetup(){
       
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounter() {
       
        if counter !=  0 {
            lblTimer.text = "Resend Code 00:\(counter)"
            counter -= 1
        }else {
           
            self.counter = 60
            timer.invalidate()
            lblTimer.isHidden = true
            btnResend.isHidden = false
        }
    }
    
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func resendCodeButtonPressed(_ sender: UIButton) {
        self.forgetPasswordApi()
        
    }
    
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        
        if viewOtp.text!.count < 4 {
            AppUtility?.showToast(string: "Enter your valid otp", view: self.view)
            return
        }
       
        self.verifyChangePasswordApi()
        
    }
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- VERIFY CHNAGE PASSWORD API
    
    private func verifyChangePasswordApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.verifyforgotPasswordCode(email: email, code: viewOtp.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordViewController")as! ResetPasswordViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                   
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    //MARK:- FORGET PASSWORD API
    
    private func forgetPasswordApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.forgetPassword(email: email) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    self.timerSetup()
                    self.viewOtp.text = ""
                    self.btnResend.isHidden = true
                    self.lblTimer.isHidden = false
                  
                   
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    

}


//MARK:- Textfield Delegate

extension ForgetOtpViewController: DPOTPViewDelegate {
    
    func dpOTPViewAddText(_ text: String, at position: Int) {
       
       
      
        if position <= 3 && text.count == 4{
            
            viewGradient.isHidden = false
            btnNext.backgroundColor = .clear
            viewOtp.dismissOnLastEntry = true
            
        }else {
            viewGradient.isHidden = true
            btnNext.backgroundColor = UIColor(named: "light shaft")
            viewOtp.dismissOnLastEntry = true
            
        }
        
    }
    
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        
        if position <= 3 && text.count == 4{
            
            viewGradient.isHidden = false
            btnNext.backgroundColor = .clear
            viewOtp.dismissOnLastEntry = true
            
        }else {
            viewGradient.isHidden = true
            btnNext.backgroundColor = UIColor(named: "light shaft")
            viewOtp.dismissOnLastEntry = true
            
        }
        
        
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
        
        
        
    }
    
    func dpOTPViewBecomeFirstResponder() {
        
    }
    func dpOTPViewResignFirstResponder() {
        
        
    }
}

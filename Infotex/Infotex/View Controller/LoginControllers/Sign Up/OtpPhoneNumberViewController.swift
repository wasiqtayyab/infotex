//
//  OtpPhoneNumberViewController.swift
//  Infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit
import DPOTPView
class OtpPhoneNumberViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var viewOtp: DPOTPView!
    @IBOutlet weak var viewGradient: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var lblTimer: UILabel!
    
    //MARK:- VARS
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    var counter = 60
    var timer = Timer()

    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
    
        viewGradient.isHidden = true
        btnNext.backgroundColor = UIColor(named: "light shaft")
        lblEmail.text = "Your code was sent to \((phone_number))"
        viewOtp.dpOTPViewDelegate = self
        btnResend.isHidden = true
        timerSetup()
        
    }
    
    //MARK:- TIMER
    
    private func timerSetup(){
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounter() {
      
        if counter !=  0 {
            lblTimer.text = "Resend Code 00:\(counter)"
            counter -= 1
        }else {
            
            self.counter = 60
            timer.invalidate()
            lblTimer.isHidden = true
            btnResend.isHidden = false
        }
    }
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func resendCodeButtonPressed(_ sender: UIButton) {
        self.phoneNumberApi()
       
    }
    
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        if viewOtp.text!.count < 4 {
            AppUtility?.showToast(string: "Enter your valid otp", view: self.view)
            return
        }
        
        if sender_button == 0 {
            phoneNumberVerifyApi()
            
        }else if sender_button == 1 {
            phoneNumberVerifyApi()
           
        }else {
            phoneNumberVerifyApi()
          
        }
       
        
    }
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- VERIFY PHONE NUMBER API
    
    private func phoneNumberVerifyApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.verifyPhoneNo(phone: phone_number, verify: "1", code: viewOtp.text!, IsVerfied: "1") { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    self.registerUserApi()
                    
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    //MARK:- PHONE NUMBER API
    
    private func phoneNumberApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.verifyPhoneNo(phone: phone_number, verify: "0", code: "", IsVerfied: "0") { [self] (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    self.timerSetup()
                    self.viewOtp.text! = ""
                    self.btnResend.isHidden = true
                    self.lblTimer.isHidden = false
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    //MARK:- REGISTER USER API
    
    private func registerUserApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.registerUser(email: email, password: password, dob: dob, username: username, role: role, phone: phone_number, gender: "") { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    if sender_button == 0 {
                        let userObj = resp!["msg"] as! [String:Any]
                        UserObject.shared.Objresponse(response: userObj, isLogin: true)
                        let story = UIStoryboard(name: "User", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "SignUpPackageViewController")as! SignUpPackageViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else if sender_button == 1{
                        
                        let userObj = resp!["msg"] as! [String:Any]
                        UserObject.shared.Objresponse(response: userObj,isLogin: true)
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SplashScreenViewController")as! SplashScreenViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else {
                        let userObj = resp!["msg"] as! [String:Any]
                        UserObject.shared.Objresponse(response: userObj,isLogin: true)
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AgencyConfirmationViewController")as! AgencyConfirmationViewController
                         self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                    
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
    }
    

}


//MARK:- Textfield Delegate

extension OtpPhoneNumberViewController: DPOTPViewDelegate {
    
    func dpOTPViewAddText(_ text: String, at position: Int) {
       
        if position <= 3 && text.count == 4{
            
            viewGradient.isHidden = false
            btnNext.backgroundColor = .clear
            viewOtp.dismissOnLastEntry = true
            
        }else {
            viewGradient.isHidden = true
            btnNext.backgroundColor = UIColor(named: "light shaft")
            viewOtp.dismissOnLastEntry = true
            
        }
        
    }
    
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        
        if position <= 3 && text.count == 4{
            
            viewGradient.isHidden = false
            btnNext.backgroundColor = .clear
            viewOtp.dismissOnLastEntry = true
            
        }else {
            viewGradient.isHidden = true
            btnNext.backgroundColor = UIColor(named: "light shaft")
            viewOtp.dismissOnLastEntry = true
            
        }
        
        
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
        
        
        
    }
    
    func dpOTPViewBecomeFirstResponder() {
        
    }
    func dpOTPViewResignFirstResponder() {
        
        
    }
    

  
}

//
//  DOBViewController.swift
//  Infotex
//
//  Created by Mac on 01/09/2021.
//

import UIKit

class DOBViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var imgGradient: UIImageView!
    
    
    //MARK:- VARS
    var age_count = 0
    var dob_ = ""
    var selectedDate = ""
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if sender_button == 0 {
            print("sign up by user")
        
        }else if sender_button == 1 {
            print("sign up by influencer")
            
        }else {
            print("sign up by agency")
            
        }
        dobSetup()
        imgGradient.isHidden = true
        btnNext.backgroundColor = UIColor(named: "light shaft")
        
    }
    
    //MARK:- DOB DELEGATE
    
    private func dobSetup(){
        
        if #available(iOS 13.4, *) { datePicker.preferredDatePickerStyle = .wheels
            
        }
      
        self.datePicker.backgroundColor = UIColor(named: "Mine Shaft")
        self.datePicker.setValue(UIColor.white, forKeyPath: "textColor")
        self.datePicker.setValue(false, forKey: "highlightsToday")

        datePicker.addTarget(self, action: #selector(dobDateChanged(_:)), for: .valueChanged)
        datePicker.maximumDate = Date()
        
        /*datePicker.subviews.first?.subviews.last?.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)*/
    }
    
    //MARK:- DATE PICKER ACTION
    
    @objc func dobDateChanged(_ sender: UIDatePicker) {
        sender.datePickerMode = UIDatePicker.Mode.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateOfBirth = datePicker.date
        let selectedDate = dateFormatter.string(from: sender.date)
        
        
        let today = NSDate()
        
        let gregorian = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        
        let age = gregorian.components([.year], from: dateOfBirth, to: today as Date, options: [])
        age_count = age.year!
        if age.year! <= 17 {
            btnNext.backgroundColor = UIColor(named: "light shaft")
            imgGradient.isHidden = true
            
        }else {
            btnNext.backgroundColor = .clear
            imgGradient.isHidden = false
            
        }
        self.dob_ = selectedDate
        print("dob",dob_)
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
    
        if age_count <= 17 {
            AppUtility?.showToast(string: "Your age is less than 18 years old", view: self.view)
            return
        }
        dob = dob_
        let vc = storyboard?.instantiateViewController(withIdentifier: "EmailViewController")as! EmailViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}

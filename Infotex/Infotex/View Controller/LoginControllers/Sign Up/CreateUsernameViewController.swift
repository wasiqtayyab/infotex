//
//  CreateUsernameViewController.swift
//  Infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit

class CreateUsernameViewController: UIViewController, UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var viewGradient: UIImageView!
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var lblCount: UILabel!
    
    //MARK:- VARS
    
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    var count: Int!
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldSetup()
        viewGradient.isHidden = true
        btnSignUp.backgroundColor = UIColor(named: "light shaft")
        
    }
    
    //MARK:- TEXTFIELD PLACEHOLDER
    
    private func textFieldSetup(){
        tfUsername.delegate = self
        tfUsername.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        if tfUsername.text!.count < 4 {
            AppUtility?.showToast(string: "Enter your valid username", view: self.view)
            return
        }
        if !AppUtility!.validateUsername(str: tfUsername.text!) == true {
            AppUtility?.showToast(string: "Enter your valid username", view: self.view)
            return
        }
        username = tfUsername.text!
        self.verifyUsernameApi()
    }
    
    //MARK:- TEXTFIELD DELEGATE
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        lblCount.text = "\(tfUsername.text!.count)/30"
        if tfUsername.text!.count >= 4 && AppUtility!.validateUsername(str: tfUsername.text!) {
            
            viewGradient.isHidden = false
            btnSignUp.backgroundColor = .clear
            
        }else {
            
            viewGradient.isHidden = true
            btnSignUp.backgroundColor = UIColor(named: "light shaft")
            
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 30
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    //MARK:- VERIFY USERNAME API
    
    private func verifyUsernameApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.verifyUsername(username: tfUsername.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PhoneNumberViewController")as! PhoneNumberViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
  
}

//
//  EmailViewController.swift
//  Infotex
//
//  Created by Mac on 01/09/2021.
//

import UIKit
import ActiveLabel

class EmailViewController: UIViewController, UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var viewTicked: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var viewGradient: UIImageView!
    @IBOutlet weak var lblPrivacy: ActiveLabel!
    @IBOutlet weak var imgTicked: UIImageView!
    
    //MARK:- VARS
    
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    
    var isTicked = false
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldSetup()
        viewTicked.layer.borderWidth = 2
        viewTicked.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        termsOfConditionsButton()
        viewGradient.isHidden = true
        btnNext.backgroundColor = UIColor(named: "light shaft")
        imgTicked.isHidden = true
        tapGesture()
        
    }
    
    //MARK:- TEXTFIELD PLACEHOLDER
    
    private func textFieldSetup(){
        tfEmail.delegate = self
        tfEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
    }
    
    //MARK:- TAP GESTURE
    
    private func tapGesture(){
        
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped(_:)))
        self.viewTicked.isUserInteractionEnabled = true
        self.viewTicked.addGestureRecognizer(viewTap)
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        if !AppUtility!.isEmail(tfEmail.text!){
            AppUtility?.showToast(string: "Enter your valid email", view: self.view)
            return
        }
        email = tfEmail.text!
        self.verifyEmailApi()
        
    }
    @IBAction func tfEditingChanges(_ sender: UITextField) {
        
    }
    
    @objc func viewTapped(_ sender: UITapGestureRecognizer) {
        if isTicked == false {
            imgTicked.isHidden = false
            isTicked = true
            
        }else {
            imgTicked.isHidden = true
            isTicked = false
        }
        
    }
    
    //MARK:- TERM OF CONDITION
    
    private func termsOfConditionsButton(){
        
        let customType = ActiveType.custom(pattern: "Terms of Use")
        let customType1 = ActiveType.custom(pattern: "Privacy Policy.")
        lblPrivacy.enabledTypes = [.mention, .hashtag, .url, customType,customType1]
        lblPrivacy.text = "By signing up, you confirm that you agree to our Terms of Use and have read and understood our Privacy Policy."
        lblPrivacy.textColor = #colorLiteral(red: 0.6352941176, green: 0.6431372549, blue: 0.6549019608, alpha: 1)
        lblPrivacy.customColor[customType1] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblPrivacy.customSelectedColor[customType1] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblPrivacy.customColor[customType] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblPrivacy.customSelectedColor[customType] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblPrivacy.handleCustomTap(for: customType) { element in
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController")as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        lblPrivacy.handleCustomTap(for: customType1) { element in
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController")as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    //MARK:- TEXTFIELD DELEGATE
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if !AppUtility!.isEmail(tfEmail.text!){
            btnNext.backgroundColor = UIColor(named: "light shaft")
            viewGradient.isHidden = true
            
            return
        }
        btnNext.backgroundColor = .clear
        viewGradient.isHidden = false
    }
    
    //MARK:- VERIFY REGISTER EMAIL API
    
    private func verifyRegisterEmailApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.verifyRegisterEmailCodeWithEmail(email: tfEmail.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpViewController")as! OtpViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    
    //MARK:- VERIFY EMAIL API
    
    private func verifyEmailApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.verifyEmail(email: tfEmail.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    self.verifyRegisterEmailApi()
                    
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
}

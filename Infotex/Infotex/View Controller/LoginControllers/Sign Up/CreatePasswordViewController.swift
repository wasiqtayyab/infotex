//
//  CreatePasswordViewController.swift
//  Infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit

class CreatePasswordViewController: UIViewController, UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var viewGradient: UIImageView!
    @IBOutlet weak var btnEye: UIButton!
    
    //MARK:- VARS
    
    var isSecure = false
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldSetup()
        viewGradient.isHidden = true
        btnNext.backgroundColor = UIColor(named: "light shaft")
       
       
    }
    
    //MARK:- TEXTFIELD PLACEHOLDER
    
    private func textFieldSetup(){
        tfPassword.delegate = self
        tfPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        
        
        if tfPassword.text!.count < 6 {
            AppUtility?.showToast(string: "Enter your password greater than 5", view: self.view)
            return
        }
        password = tfPassword.text!
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "CreateUsernameViewController")as! CreateUsernameViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func eyeButtonPressed(_ sender: UIButton) {
        if isSecure == false {
            btnEye.setImage(UIImage(named: "eye_"), for: .normal)
            tfPassword.isSecureTextEntry = false
            isSecure = true
        }else {
            btnEye.setImage(UIImage(named: "eye"), for: .normal)
            tfPassword.isSecureTextEntry = true
            isSecure = false
        }
        
    }
    
    
    //MARK:- TEXTFIELD DELEGATE
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if tfPassword.text!.count > 5 {
            viewGradient.isHidden = false
            btnNext.backgroundColor = .clear
            
        }else {
            
            viewGradient.isHidden = true
            btnNext.backgroundColor = UIColor(named: "light shaft")
            
            
        }
    }
    
}

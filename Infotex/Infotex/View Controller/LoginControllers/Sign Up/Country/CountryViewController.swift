//
//  CountryViewController.swift
//  GoGrab
//
//  Created by Mac on 04/08/2021.
//

import UIKit

class CountryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchControllerDelegate, UISearchBarDelegate {
   

    //MARK:- OUTLET
    
    @IBOutlet weak var countryTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK:- Vars
    let defaults = UserDefaults.standard
    var arrCountry = [[String:Any]]()
    var arrSearch = [[String:Any]]()
    var isSearching = false
    var isCodeShow = true
    var country_code = ""
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        
        self.getCountryCodes()
        countryTableView.tableFooterView = UIView()

    }
    
    //MARK:- GET COUNTRY CODES
    
    func getCountryCodes(){
        if let path = Bundle.main.path(forResource: "countryCodes", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? [[String:Any]] {
                    self.arrCountry = jsonResult
                    self.countryTableView.reloadData()
                }
            } catch {
                // handle error
            }
        }
    }
   
    //MARK:- BUTTON ACTION
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    //MARK:- TABLEVIEW DELEGATE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearching{
            return self.arrSearch.count
        }else{
            return self.arrCountry.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryTableViewCell", for: indexPath) as! CountryTableViewCell
        if self.isSearching{
            cell.imgStatus.image = UIImage(named: self.arrSearch[indexPath.row]["code"] as! String)
            if self.isCodeShow{
                cell.countryLabel.text = "\(self.arrSearch[indexPath.row]["dial_code"] as! String) \(self.arrSearch[indexPath.row]["name"] as! String)"
            }else{
                cell.countryLabel.text = "\(self.arrSearch[indexPath.row]["name"] as! String)"
            }
            
        }else{
            cell.imgStatus.image = UIImage(named: self.arrCountry[indexPath.row]["code"] as! String)
            if self.isCodeShow{
                cell.countryLabel.text = "\(self.arrCountry[indexPath.row]["dial_code"] as! String) \(self.arrCountry[indexPath.row]["name"] as! String)"
            }else{
                cell.countryLabel.text = "\(self.arrCountry[indexPath.row]["name"] as! String)"
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.isSearching == false{
            NotificationCenter.default.post(name: Notification.Name("GetCountry"), object: self.arrCountry[indexPath.row])
            self.dismiss(animated: true, completion: nil)
        }else{
            NotificationCenter.default.post(name: Notification.Name("GetCountry"), object: self.arrSearch[indexPath.row])
            self.dismiss(animated: true, completion: nil)
        }
      
        
    }
    
    //MARK:- SEARCHBAR DELEGATE
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if AppUtility!.isEmpty(self.searchBar.text!){
            self.isSearching = false
            self.arrSearch.removeAll()
        }else{
            self.isSearching = true
            self.arrSearch.removeAll()
            for obj in self.arrCountry{
                let strName = obj["name"] as! String
                if strName.range(of: self.searchBar.text!, options: .caseInsensitive) != nil{
                    self.arrSearch.append(obj)
                }
            }
        }
        self.countryTableView.reloadData()
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("Pressed")
        self.searchBar.text = nil
        getCountryCodes()
    }
    
}

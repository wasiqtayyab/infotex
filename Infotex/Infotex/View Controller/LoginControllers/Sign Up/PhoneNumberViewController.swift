//
//  PhoneNumberViewController.swift
//  Infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit
import PhoneNumberKit


class PhoneNumberViewController: UIViewController, UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var viewGradient: UIImageView!
    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    
    //MARK:- VARS
    
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    var region = "PK"
    var country_code = "+92"
    var isTicked = false
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldSetup()
        viewGradient.isHidden = true
        btnNext.backgroundColor = UIColor(named: "light shaft")
        
        
        
    }
    
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.phoneNumber(_:)), name: Notification.Name("GetCountry"), object: nil)
        
    }
    
   
    //MARK:- BUTTON ACTION
    
    @IBAction func countryButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CountryViewController")as! CountryViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        if !AppUtility!.isValidPhoneNumber(strPhone: tfPhoneNumber.text!, strRegion: region){
            AppUtility?.showToast(string: "Enter a valid phone", view: self.view)
            return
        }
        let trimmedString = AppUtility?.validate(tfPhoneNumber.text!)
        
        let index = trimmedString!.index(trimmedString!.startIndex, offsetBy: 0)
        String(trimmedString![index])
        print( String(trimmedString![index]) )
        if String(trimmedString![index]) == "+"{
            phone_number = trimmedString!
        }else{
            phone_number = "\(country_code)\((trimmedString)!)"
        }
        
        self.phoneNumberApi()
        
    }
    
    @objc func phoneNumber(_ notification: NSNotification) {
        tfPhoneNumber.text = ""
        let obj = notification.object as! [String:Any]
        lblPhoneNumber.text = "\(obj["code"] as! String) \(obj["dial_code"] as! String)"
        region =  "\(obj["code"] as! String)"
        country_code = "\(obj["dial_code"] as! String)"
        print("region",region)
        
    }
    
    
    //MARK:- TEXTFIELD PLACEHOLDER
    
    private func textFieldSetup(){
        tfPhoneNumber.delegate = self
        tfPhoneNumber.attributedPlaceholder = NSAttributedString(string: "0301 7471435", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "gray")])
        
    }
    
   
    //MARK:- TEXTFIELD DELEGATE
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        print("regioncheck",region)
        if !AppUtility!.isValidPhoneNumber(strPhone: tfPhoneNumber.text!, strRegion: region){
            viewGradient.isHidden = true
            btnNext.backgroundColor = UIColor(named: "light shaft")
            return
        }
        
        viewGradient.isHidden = false
        btnNext.backgroundColor = .clear
    }
    
    //MARK:- PHONE NUMBER API
    
    private func phoneNumberApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.verifyPhoneNo(phone: phone_number, verify: "0", code: "", IsVerfied: "0") { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpPhoneNumberViewController")as! OtpPhoneNumberViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    
    
}

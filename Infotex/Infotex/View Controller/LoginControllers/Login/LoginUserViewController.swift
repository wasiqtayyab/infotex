//
//  LoginUserViewController.swift
//  Infotex
//
//  Created by Mac on 01/09/2021.
//

import UIKit
import ActiveLabel
class LoginUserViewController: UIViewController {
    
    //MARK:- OUTLET
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var lblTerms: ActiveLabel!
    @IBOutlet weak var imgStyle: UIImageView!
    @IBOutlet weak var lblStyle: UILabel!
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if sender_button == 0 {
            imgStyle.image = UIImage(named: "avatar_")
            lblStyle.text = "User"
          
        }else if sender_button == 1 {
            imgStyle.image = UIImage(named: "influncer_white")
            lblStyle.text = "Influencer"
          
        }else {
            imgStyle.image = UIImage(named: "agency_")
            lblStyle.text = "Agency / Modeling"
            
            
        }

        termsOfConditionsButton()
        
    }
    
    //MARK:- TERM OF CONDITION
    private func termsOfConditionsButton(){
        
        let customType = ActiveType.custom(pattern: "Terms of Services")
        let customType1 = ActiveType.custom(pattern: "Privacy Policy")
        lblTerms.enabledTypes = [.mention, .hashtag, .url, customType,customType1]
        lblTerms.text = "By Signing up,You agree to our Terms of Services and\n acknowledge that you have read our Privacy Policy to\n learn how we collect, user,and share your data."
        lblTerms.textColor = #colorLiteral(red: 0.6352941176, green: 0.6431372549, blue: 0.6549019608, alpha: 1)
        lblTerms.customColor[customType1] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblTerms.customSelectedColor[customType1] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblTerms.customColor[customType] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblTerms.customSelectedColor[customType] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblTerms.handleCustomTap(for: customType) { element in
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController")as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        lblTerms.handleCustomTap(for: customType1) { element in
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController")as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    //MARK:- BUTTON ACTION
    
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpUserViewController")as! SignUpUserViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func emailButtonPressed(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}

//
//  LoginInfotexViewController.swift
//  Infotex
//
//  Created by Mac on 03/09/2021.
//

import UIKit
import ActiveLabel
class LoginInfotexViewController: UIViewController {

    //MARK:- OUTLET
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var lblTerms: ActiveLabel!
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()

        termsOfConditionsButton()
      
    }
    
    //MARK:- TERM OF CONDITION
    private func termsOfConditionsButton(){
        
        let customType = ActiveType.custom(pattern: "Terms of Services")
        let customType1 = ActiveType.custom(pattern: "Privacy Policy")
        lblTerms.enabledTypes = [.mention, .hashtag, .url, customType,customType1]
        lblTerms.text = "By Signing up,You agree to our Terms of Services and\n acknowledge that you have read our Privacy Policy to\n learn how we collect, user,and share your data."
        lblTerms.textColor = #colorLiteral(red: 0.6352941176, green: 0.6431372549, blue: 0.6549019608, alpha: 1)
        lblTerms.customColor[customType1] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblTerms.customSelectedColor[customType1] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblTerms.customColor[customType] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblTerms.customSelectedColor[customType] = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lblTerms.handleCustomTap(for: customType) { element in
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController")as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        lblTerms.handleCustomTap(for: customType1) { element in
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController")as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpUserViewController")as! SignUpUserViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }


}

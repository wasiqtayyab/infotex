//
//  LoginViewController.swift
//  Infotex
//
//  Created by Mac on 03/09/2021.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnEye: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var viewGradient: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblProfile: UILabel!
    
    
    //MARK:- VARS
    var image : UIImage!
    var isSecure = false
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldSetup()
        viewGradient.isHidden = true
        btnLogin.backgroundColor = UIColor(named: "light shaft")
        imgProfile.image = image
        lblProfile.text = role
        
        
    }
    
    //MARK:- TEXTFIELD PLACEHOLDER
    
    private func textFieldSetup(){
        tfEmail.delegate = self
        tfPassword.delegate = self
        tfEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        tfPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func eyeButtonPressed(_ sender: UIButton) {
        if isSecure == false {
            btnEye.setImage(UIImage(named: "eye_"), for: .normal)
            tfPassword.isSecureTextEntry = false
            isSecure = true
        }else {
            btnEye.setImage(UIImage(named: "eye"), for: .normal)
            tfPassword.isSecureTextEntry = true
            isSecure = false
        }
        
    }
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        
        if !AppUtility!.isEmail(self.tfEmail.text!){
            AppUtility?.showToast(string: "Your email is not valid", view: self.view)
            return
        }
        if AppUtility!.isEmpty(tfPassword.text!){
            AppUtility?.showToast(string: "Your password is empty", view: self.view)
            return
        }
        
        self.loginApi()
        
    }
    
    @IBAction func forgetButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ForgetEmailViewController")as! ForgetEmailViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK:- TEXTFIELD DELEGATE
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if !AppUtility!.isEmail(self.tfEmail.text!){
            
            btnLogin.backgroundColor = UIColor(named: "light shaft")
            viewGradient.isHidden = true
            return
        }
        if AppUtility!.isEmpty(tfPassword.text!){
            btnLogin.backgroundColor = UIColor(named: "light shaft")
            viewGradient.isHidden = true
            return
        }
        
        btnLogin.backgroundColor = .clear
        viewGradient.isHidden = false
    }

    //MARK:- LOGIN API
    
    private func loginApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.login(email: tfEmail.text!, password: tfPassword.text!, role: role) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    let userObj = resp!["msg"] as! [String:Any]
                    UserObject.shared.Objresponse(response: userObj,isLogin: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        let dataObj = userObj["User"] as! [String:Any]
                        print("dataObj",dataObj)
                        
                        if dataObj["role"] as! String == "influencer"{
                            
                            if dataObj["referral_used_user_id"] as! String == "0"{
                                
                                let story = UIStoryboard(name: "influncer", bundle: nil)
                                let vc = story.instantiateViewController(withIdentifier: "SignUpInfluencerViewController") as! SignUpInfluencerViewController
                                let nav = UINavigationController(rootViewController: vc)
                                nav.navigationBar.isHidden = true
                                self.view.window?.rootViewController = nav
                                
                            }else {
                                
                                let story = UIStoryboard(name: "influncer", bundle: nil)
                                let vc = story.instantiateViewController(withIdentifier: "CustomTabBarController") as! CustomTabBarController
                                let nav = UINavigationController(rootViewController: vc)
                                nav.navigationBar.isHidden = true
                                self.view.window?.rootViewController = nav
                        
                                
                            }
                    
                        }else if  dataObj["role"] as! String == "agency"{
                            
                            let story = UIStoryboard(name: "influncer", bundle: nil)
                            let vc = story.instantiateViewController(withIdentifier: "CustomTabBarController") as! CustomTabBarController
                            let nav = UINavigationController(rootViewController: vc)
                            nav.navigationBar.isHidden = true
                            self.view.window?.rootViewController = nav
            
                        }else{
                            let story = UIStoryboard(name: "influncer", bundle: nil)
                            let vc = story.instantiateViewController(withIdentifier: "CustomTabBarController") as! CustomTabBarController
                            let nav = UINavigationController(rootViewController: vc)
                            nav.navigationBar.isHidden = true
                            self.view.window?.rootViewController = nav
                        }

                        
                    }
                  
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
    }
}

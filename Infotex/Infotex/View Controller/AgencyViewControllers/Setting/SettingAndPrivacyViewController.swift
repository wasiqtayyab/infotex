//
//  SettingAndPrivacyViewController.swift
//  Infotex New Screen
//
//  Created by Wasiq Tayyab on 24/09/2021.
//

import UIKit

class SettingAndPrivacyViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tblSetting: UITableView!
    
    //MARK:- VARS
    
    var arrSetting = [["name":"Manage Account","image":"setting_1"],
                      ["name":"Privacy","image":"setting_2"],
                      ["name":"Request Verification","image":"setting_3"],
                      ["name":"Balance","image":"setting_4"],
                      ["name":"Special Event","image":"setting_5"]]
    
    
    var arrDiamond = [["name":"My Diamonds","image":"setting_6"],
                      ["name":"Eco-Pedia","image":"setting_7"],
                      ["name":"Agency Dashboard","image":"setting_8"]]
    
    
    var arrNotification = [["name":"Push Notification","image":"setting_9"],
                           ["name":"App Language","image":"setting_10"]]
    
    var arrAbout = [["name":"Terms of Service","image":"setting_11"],
                    ["name":"Privacy Policy","image":"setting_12"]]
    
    var arrAccount = [["name":"Free Up Space","image":"setting_13"],
                      ["name":"Block Account","image":"setting_14"]]
    
    var arrLogin = [["name":"Switch Account","image":"setting_15"],
                    ["name":"Log Out","image":"setting_16"]]
    
    var userData = [userMVC]()
    
    var mySwitchAccount: [switchAccount]? {didSet {}}
    var myUser: [User]? {didSet {}}
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.myUser = User.readUserFromArchive()
        self.mySwitchAccount = switchAccount.readswitchAccountFromArchive()
        tblSetting.delegate = self
        tblSetting.dataSource = self
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- API Handler
    
    func logoutUserApi(){
        
        let userID = self.myUser?[0].Id
        print("user id: ",userID as Any)
        
        ApiHandler.sharedInstance.logout(user_id: userID! ) { (isSuccess, response) in
            
            if isSuccess{
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    print(response?.value(forKey: "msg") as Any)
                    UserDefaults.standard.set("", forKey: "userID")
                    self.myUser = User.readUserFromArchive()
                    
                    self.myUser?.remove(at: 0)
                    if User.saveUserToArchive(user: self.myUser!) {
                        print("User Saved in Directory")
                        
                        let story = UIStoryboard(name: "Login", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "SplashScreenViewController") as! SplashScreenViewController
                        let nav = UINavigationController(rootViewController: vc)
                        nav.navigationBar.isHidden = true
                        self.view.window?.rootViewController = nav
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                    
                }else{
                    AppUtility?.showToast(string:response?.value(forKey: "msg") as? String ?? "" , view: self.view)
                    print("logout API:",response?.value(forKey: "msg") as! String)
                }
            }else{
                print("logout API:",response?.value(forKey: "msg") as Any)
            }
        }
    }
    
    
}
//MARK:- TABLE VIEW DELEGATE

extension SettingAndPrivacyViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrSetting.count
        }else if section == 1 {
            return arrDiamond.count
        }else if section == 2 {
            return arrNotification.count
        }else if section == 3 {
            return arrAbout.count
        }else if section == 4 {
            return arrAccount.count
        }else {
            return arrLogin.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell", for: indexPath)as! SettingTableViewCell
        
        if indexPath.section == 0 {
            
            cell.lblSetting.text = arrSetting[indexPath.row]["name"]
           
            cell.imgSetting.image = UIImage(named: arrSetting[indexPath.row]["image"]!)
            if indexPath.row == 0 {
                cell.viewBackground.layer.cornerRadius = 20.0
                cell.viewBackground.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            }else if indexPath.row == 4{
                cell.viewBackground.layer.cornerRadius = 20.0
                cell.viewBackground.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
            }
            
        }else if indexPath.section == 1 {
            cell.lblSetting.text = arrDiamond[indexPath.row]["name"]
           
            cell.imgSetting.image = UIImage(named: arrDiamond[indexPath.row]["image"]!)
            if indexPath.row == 0 {
                cell.imgRedDiamond.isHidden = false
                cell.imgDiamond.isHidden = false
                cell.imgBlueDiamond.isHidden = false
                cell.lblRed.isHidden = false
                cell.lblBlue.isHidden = false
                cell.lblDiamond.isHidden = false
                cell.viewBackground.layer.cornerRadius = 20.0
                cell.viewBackground.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            }else if indexPath.row == 2{
                cell.viewBackground.layer.cornerRadius = 20.0
                cell.viewBackground.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
            }
            
        }else if indexPath.section == 2 {
            cell.lblSetting.text = arrNotification[indexPath.row]["name"]
            cell.imgSetting.image = UIImage(named: arrNotification[indexPath.row]["image"]!)
            if indexPath.row == 0 {
               
                cell.viewBackground.layer.cornerRadius = 20.0
                cell.viewBackground.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            }else {
                cell.viewBackground.layer.cornerRadius = 20.0
                cell.viewBackground.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
                cell.lblLanguage.isHidden = false
                cell.imgNext.isHidden = true
            }
        }else if indexPath.section == 3 {
            
            cell.lblSetting.text = arrAbout[indexPath.row]["name"]
           
            cell.imgSetting.image = UIImage(named: arrAbout[indexPath.row]["image"]!)
            if indexPath.row == 0 {
               
                cell.viewBackground.layer.cornerRadius = 20.0
                cell.viewBackground.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            }else {
                cell.viewBackground.layer.cornerRadius = 20.0
                cell.viewBackground.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]

            }
            
        }else if indexPath.section == 4 {
            cell.lblSetting.text = arrAccount[indexPath.row]["name"]
            cell.imgSetting.image = UIImage(named: arrAccount[indexPath.row]["image"]!)
            cell.lblLanguage.isHidden = true
            if indexPath.row == 0 {
               
                cell.viewBackground.layer.cornerRadius = 20.0
                cell.viewBackground.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            }else {
                cell.viewBackground.layer.cornerRadius = 20.0
                cell.viewBackground.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]

            }
            
        }else {
            
            cell.lblSetting.text = arrLogin[indexPath.row]["name"]
            cell.imgSetting.image = UIImage(named: arrLogin[indexPath.row]["image"]!)
           
            if indexPath.row == 0 {
               
                cell.viewBackground.layer.cornerRadius = 20.0
                cell.viewBackground.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            }else {
                cell.viewBackground.layer.cornerRadius = 20.0
                cell.viewBackground.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]

            }
            
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 40
        }else if indexPath.section == 1 {
            if indexPath.row == 0 {
                return 70
            }else {
                
                return 45
            }
            
        }else {
            return 40
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width  , height: 15))
        if #available(iOS 13.0, *) {
            headerView.backgroundColor = UIColor(named: "Shaft")
        } else {
            headerView.backgroundColor = UIColor(named: "Shaft")
        }
        
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            
            if indexPath.row == 0 {
                let story = UIStoryboard(name: "influncer", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "ManageAccountViewController")as! ManageAccountViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if indexPath.row == 1 {
                
                print("privacy")
                
            }else if indexPath.row == 2 {
                let story = UIStoryboard(name: "influncer", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "requestVerificationViewController")as! requestVerificationViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if indexPath.row == 3 {
                
                print("balance")
                
            }else {
                
                print("special event")
                
            }
            
        }else if indexPath.section == 1 {
            
            if indexPath.row == 0 {
                
                let story = UIStoryboard(name: "influncer", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "MyDiamondViewController")as! MyDiamondViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if indexPath.row == 1 {
                
                let vc = storyboard?.instantiateViewController(withIdentifier: "Eco_PediaViewController")as! Eco_PediaViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else {
               
                let vc = storyboard?.instantiateViewController(withIdentifier: "AgencyDashboardViewController")as! AgencyDashboardViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            
        }else if indexPath.section == 2 {
            
            if indexPath.row == 0 {
                let story = UIStoryboard(name: "influncer", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "pushNotiSettingsViewController")as! pushNotiSettingsViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else {
                
            }
            
            
        }else if indexPath.section == 3 {
            
            if indexPath.row == 0 {
                
                let story = UIStoryboard(name: "Login", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "PrivacyPolicyViewController")as! PrivacyPolicyViewController
                vc.privacy = "Terms of Service"
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else {
                
                let story = UIStoryboard(name: "Login", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "PrivacyPolicyViewController")as! PrivacyPolicyViewController
                vc.privacy = "Privacy Policy"
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
           
            
        }else if indexPath.section == 4 {
            
            
        }else {
            
            if indexPath.row == 0 {
                
                let story = UIStoryboard(name: "influncer", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "SwitchAccountViewController")as! SwitchAccountViewController
                self.navigationController?.present(vc, animated: true, completion: nil)
                
            }else {
                
                let alertController = UIAlertController(title: NSLocalizedString("alert_app_name", comment: ""), message: "Would you like to LOGOUT?", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                    self.tabBarController?.selectedIndex = 0
                    self.tabBarController?.tabBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.white
                    
                    for var i in 0..<self.mySwitchAccount!.count{
                        var obj = self.mySwitchAccount![i]
                        if  obj.Id == self.myUser?[0].Id{
                            self.mySwitchAccount?.remove(at: i)
                            if switchAccount.saveswitchAccountToArchive(switchAccount: self.mySwitchAccount!) {
                                print("ID\(self.myUser?[0].Id) user logout")
                                break
                            }
                        }
                    }
                    self.logoutUserApi()
                }
                alertController.addAction(OKAction)
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                    print("Cancel button tapped");
                }
                cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion:nil)
                
            }
            
            
            
        }
    }
    
}

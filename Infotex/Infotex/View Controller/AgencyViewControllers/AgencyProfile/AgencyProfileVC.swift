//
//  AgencyProfileVC.swift
//  Infotex
//
//  Created by Mac on 09/09/2021.
//

import UIKit

class AgencyProfileVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var tblAgencyProfile: UITableView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnUserProfile: UIButton!
    var userData = [userMVC]()
    lazy var userID = "0"
    var myUser:[User]? {didSet{}}
    var isOtherUser =  false
  
    
    //MARK:- ViewDidload
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myUser = User.readUserFromArchive()
        tblAgencyProfile.register(UINib(nibName: "AgencyTopTVC", bundle: nil), forCellReuseIdentifier: "AgencyTopTVC")
        
        tblAgencyProfile.register(UINib(nibName: "AgencyProfileHV", bundle: nil), forHeaderFooterViewReuseIdentifier: "AgencyProfileHV")
        
        tblAgencyProfile.register(UINib(nibName: "AgencyProfileTVC", bundle: nil), forCellReuseIdentifier: "AgencyProfileTVC")
        NotificationCenter.default.addObserver(self, selector: #selector(self.reLoadTbl(notification:)), name: Notification.Name("reloadAgencyTableView"), object: nil)
       
        
        if isOtherUser == true{
            self.userDetail(userID: (self.myUser?[0].Id)!, OtherUserID: self.userID)
            self.btnBack.isHidden =  false
            self.btnMenu.isHidden =  true
            
        }else{
            self.userDetail(userID:(self.myUser?[0].Id)!, OtherUserID: "")
            self.btnBack.isHidden =  true
            self.btnMenu.isHidden =  false
        }
    }
    
    //MARK:- deinit
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("reloadAgencyTableView"), object: nil)
    }
    
    //MARK:- Functions
    
    @objc func reLoadTbl(notification: Notification) {
        let index = notification.object as! Int
        let indexPath = IndexPath(row: index, section: 1)
        self.tblAgencyProfile.reloadData()
      }
    
    
    @IBAction func menuPressed(_ sender: UIButton) {
      
        let vc = storyboard?.instantiateViewController(withIdentifier: "SettingAndPrivacyViewController")as! SettingAndPrivacyViewController
        vc.userData =  self.userData
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func userPressed(_ sender: UIButton) {
        let story = UIStoryboard(name: "influncer", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "SwitchAccountViewController") as! SwitchAccountViewController
        present(vc, animated: true, completion: nil)
    }
    @IBAction func backPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- API Handler
    
    func userDetail(userID:String,OtherUserID:String){
        ApiHandler.sharedInstance.showUserDetail(user_id: userID, other_user_id: OtherUserID) { (isSuccess, response) in
            if isSuccess{
                if let res =  response?.value(forKey: "msg") as? [String:Any]{
                 let objData = UserDetail.shared.ObjUserDetailResponse(res:res)
                    self.tblAgencyProfile.delegate = self
                    self.tblAgencyProfile.dataSource = self
                    self.userData.append(objData)
                    self.btnUserProfile.setTitle("@\(objData.username)", for: .normal)
                    self.tblAgencyProfile.reloadData()
                }
            }
        }
    }
}


//MARK:- Extensions
extension AgencyProfileVC: UITableViewDelegate, UITableViewDataSource {
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let  cell = tableView.dequeueReusableCell(withIdentifier: "AgencyTopTVC", for: indexPath) as! AgencyTopTVC
              cell.Setup(userData: self.userData[indexPath.row],isOtherUser: self.isOtherUser )
              return cell
        }else{
            let  cell = tableView.dequeueReusableCell(withIdentifier: "AgencyProfileTVC", for: indexPath) as! AgencyProfileTVC
              return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "AgencyProfileHV") as! AgencyProfileHV
        return header
    }
}


//
//  Eco-PediaViewController.swift
//  Infotex
//
//  Created by Wasiq Tayyab on 06/10/2021.
//

import UIKit
import WebKit
class Eco_PediaViewController: UIViewController {
    
    //MARK:- OUTLET

    @IBOutlet weak var webView: WKWebView!
    
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        self.policyUrl()
        
    }
    
    //MARK:- POLICY URL
    
    private func policyUrl(){
        let url = URL(string: "https://eco-pedia.com/")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}

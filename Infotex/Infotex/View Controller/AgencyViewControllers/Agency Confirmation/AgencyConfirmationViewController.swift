//
//  AgencyConfirmationViewController.swift
//  Infotex
//
//  Created by Mac on 03/09/2021.
//

import UIKit

class AgencyConfirmationViewController: UIViewController {

    //MARK:- OUTLET
    
    @IBOutlet weak var lblConfirmation: UILabel!
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblConfirmation.text = "Please wait for confirmation\n from the administrator."
      
    }
    
    


}

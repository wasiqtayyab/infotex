//
//  EXTJSQMediaMessage.swift
//
//  Created by Lijie on 15/12/14.
//  Copyright © 2015年 HuatengIOT. All rights reserved.
//

import Foundation
import UIKit
import JSQMessagesViewController

private let AudioMediaItemSize = CGSize(width: 210, height: 44)

class AudioMediaItem: JSQMediaItem {
    var fileURL: NSURL!
    var timeInterval: Int32!
    var isReadyToPlay: Bool = true
    var cachedAudioImage: UIImage?
    var cachedAudioImageView: UIView?
    
    init(fileURL: NSURL, timeInterval: Int32, cachedImage: UIImage?, isReadyToPlay: Bool, maskAsOutgoing: Bool) {
        super.init(maskAsOutgoing: maskAsOutgoing)
        
        self.fileURL = fileURL
        self.timeInterval = timeInterval
        self.cachedAudioImage = cachedImage
        self.isReadyToPlay = isReadyToPlay
        self.cachedAudioImageView = nil
        
    }
    func startAudioPlayerAnimation() {
        guard let imageView = self.cachedAudioImageView as? UIImageView else {
            return
        }
        
        imageView.image = nil
        
        imageView.animationImages = [
            UIImage(named: "aia1")!,
            UIImage(named: "aia2")!,
            UIImage(named: "aia3")!,
            UIImage(named: "aia4")!,
            UIImage(named: "aia5")!,
            UIImage(named: "aia1")!
        ]
        imageView.animationDuration = 1
        imageView.startAnimating()
    }
    func stopAudioPlayerAnimation() {
        guard let imageView = self.cachedAudioImageView as? UIImageView else {
            return
        }
        if let image = self.cachedAudioImage {
            imageView.image = image
        } else {
            imageView.image = UIImage(named: "ail")
        }
        if imageView.isAnimating {
            imageView.stopAnimating()
        }
        imageView.animationImages = nil
    }
    override func clearCachedMediaViews() {
        super.clearCachedMediaViews()
        self.cachedAudioImageView = nil
    }
    
    // MARK: - JSQMesssageMediaData protocol
    
    override func mediaViewDisplaySize() -> CGSize {
        return AudioMediaItemSize
    }

    override func mediaView() -> UIView! {
        if self.fileURL == nil || !self.isReadyToPlay {
            return nil
        }
        if self.cachedAudioImageView == nil {
//            let playIcon = UIImage.jsq_defaultPlayImage()
            let size = self.mediaViewDisplaySize()
            let imageView = UIImageView()
            if let image = self.cachedAudioImage {
                imageView.image = image
            } else {
                imageView.image = UIImage(named: "ail")
            }
            imageView.backgroundColor = UIColor.blue
            imageView.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            imageView.contentMode = .scaleAspectFit
            imageView.clipsToBounds = true
            JSQMessagesMediaViewBubbleImageMasker.applyBubbleImageMask(toMediaView: imageView, isOutgoing: self.appliesMediaViewMaskAsOutgoing)
            self.cachedAudioImageView = imageView
        }
        
        return self.cachedAudioImageView
    }
    
    override func mediaHash() -> UInt {
        let hs = abs(self.hash)
        return UInt(hs)
    }
    
    // MARK: - NSObject -- NSObjectProtocol
    
     func isEqual(object: AnyObject?) -> Bool {
        if !super.isEqual(object) {
            return false
        }
        
        let audioItem = object as? AudioMediaItem
        
        return self.fileURL.isEqual(audioItem?.fileURL) && self.isReadyToPlay == audioItem?.isReadyToPlay
    }
    
    override var hash: Int {
        return super.hash ^ self.fileURL.hash
    }
    
    override var description: String {
        return String(format: "<%@: fileURL=%@, isReadyToPlay=%@, appliesMediaViewMaskAsOutgoing=%@", self, self.fileURL, self.isReadyToPlay, self.appliesMediaViewMaskAsOutgoing)
    }
    
    // MARK: - NSCoding
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.fileURL = (aDecoder.decodeObject(forKey: NSStringFromSelector(Selector("fileURL"))) as! NSURL)
        self.isReadyToPlay = aDecoder.decodeObject(forKey: NSStringFromSelector(Selector("isReadyToPlay"))) as! Bool
    }
     func encodeWithCoder(aCoder: NSCoder) {
        super.encode(with: aCoder)
        aCoder.encode(self.fileURL, forKey: NSStringFromSelector(Selector("fileURL")))
        aCoder.encode(self.isReadyToPlay, forKey: NSStringFromSelector(Selector("isReadyToPlay")))
    }
    
    // MARK: - NSCopying
    
     func copyWithZone(zone: NSZone) -> AnyObject {
        return AudioMediaItem(fileURL: self.fileURL, timeInterval: self.timeInterval, cachedImage: self.cachedAudioImage, isReadyToPlay: self.isReadyToPlay, maskAsOutgoing: self.appliesMediaViewMaskAsOutgoing)
    }
}

/*******************************************************************************/

import AVFoundation

protocol AudioRecorderDelegate {
    func audioRecorderUpdateMetra(metra: Float)
}

private let AoundPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!

private let AudioSettings: [String: AnyObject] = [AVLinearPCMIsFloatKey: NSNumber(value: false),
                                                  AVLinearPCMIsBigEndianKey: NSNumber(value: false),
                                                  AVLinearPCMBitDepthKey: NSNumber(value: 16),
                                                  AVFormatIDKey: NSNumber(value: kAudioFormatLinearPCM),
                                                  AVNumberOfChannelsKey: NSNumber(value: 1), AVSampleRateKey: NSNumber(value: 16000),
                                                  AVEncoderAudioQualityKey: NSNumber(value: AVAudioQuality.high.rawValue)]
 
class AudioRecorder: NSObject, AVAudioRecorderDelegate {
    
    var audioData: NSData!
    var operationQueue: OperationQueue!
    var recorder: AVAudioRecorder!
    
    var startTime: Double!
    var endTimer: Double!
    var timeInterval: NSNumber!
    
    var delegate: AudioRecorderDelegate?
    
    convenience init(fileName: String) {
        self.init()
        
        let filePath = NSURL(fileURLWithPath: (AoundPath as NSString).appendingPathComponent(fileName))
        
        recorder = try! AVAudioRecorder(url: filePath as URL, settings: AudioSettings)
        recorder.delegate = self
        recorder.isMeteringEnabled = true
    }
    
    override init() {
        operationQueue = OperationQueue()
        super.init()
    }
    
    func startRecord() {
        startTime = NSDate().timeIntervalSince1970
        perform("readyStartRecord", with: self, afterDelay: 0.5)
    }
    
    func readyStartRecord() {
        let audioSession = AVAudioSession.sharedInstance()
        
        do {
            try audioSession.setCategory(AVAudioSession.Category.record)
        } catch {
            NSLog("setCategory fail")
            return
        }
        do {
            try audioSession.setActive(true)
        } catch {
            NSLog("setActive fail")
            return
        }
        recorder.record()
        
        let operation = BlockOperation()
        operation.addExecutionBlock(updateMeters)
        operationQueue.addOperation(operation)
    }
    
    func stopRecord() {
        endTimer = NSDate().timeIntervalSince1970
        timeInterval = nil
        if (endTimer - startTime) < 0.5 {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: "readyStartRecord", object: self)
        } else {
            timeInterval = NSNumber(value: Int32(NSNumber(value: recorder.currentTime).intValue))
            if timeInterval.intValue < 1 {
                perform("readyStopRecord", with: self, afterDelay: 0.4)
            } else {
                readyStopRecord()
            }
        }
        operationQueue.cancelAllOperations()
    }
    
    func readyStopRecord() {
        recorder.stop()
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setActive(false, options: .notifyOthersOnDeactivation)
        } catch {
            // no-op
        }
        audioData = NSData(contentsOf: recorder.url)
    }
    
    func updateMeters() {
        repeat {
            recorder.updateMeters()
            timeInterval = NSNumber(value: NSNumber(value: recorder.currentTime).floatValue)
            let averagePower = recorder.averagePower(forChannel: 0)
            // let pearPower = recorder.peakPowerForChannel(0)
            //  NSLog("%@   %f  %f", timeInterval, averagePower, pearPower)
            delegate?.audioRecorderUpdateMetra(metra: averagePower)
            Thread.sleep(forTimeInterval: 0.2)
        } while(recorder.isRecording)
    }
    
    // MARK: audio delegate
    func audioRecorderEncodeErrorDidOccur(recorder: AVAudioRecorder, error: NSError?) {
        NSLog("%@", (error?.localizedDescription)!)
    }
}
 
/******************************** AudioPlayer ***********************************/

class AudioMediaItemPlayer: NSObject, AVAudioPlayerDelegate {
    static let sharedPlayer: AudioMediaItemPlayer = AudioMediaItemPlayer()
    
    var audioPlayer: AVAudioPlayer!
    
    private override init() {
        
    }
    
    func startPlaying(url: NSURL) {
        if (audioPlayer != nil && audioPlayer.isPlaying) {
            stopPlaying()
        }
        
        do {
            let data = NSData(contentsOf: url as URL)
            try audioPlayer = AVAudioPlayer(data: data! as Data)
        } catch{
            print("AudioPlayer: AVAudioPlayer initilization error")
            return
        }
        
        audioPlayer.delegate = self
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.soloAmbient)
        } catch {
            // no-op
            print("AudioPlayer: set session category")
        }
        audioPlayer.play()
    }
    
    func stopPlaying() {
        audioPlayer.stop()
    }
}

/******************************** AudioRecordIndicatorView ***********************************/


class AudioRecordIndicatorView: UIView {
    
    let imageView: UIImageView
    let textLabel: UILabel
    let images:[UIImage]
    
    override init(frame: CGRect) {
        textLabel = UILabel()
        textLabel.textAlignment = .center
        textLabel.font = UIFont.systemFont(ofSize: 12.0)
        textLabel.text = "Recording..."
        textLabel.textColor = UIColor.black
        
        images = [UIImage(named: "record_animate_01")!,
            UIImage(named: "record_animate_02")!,
            UIImage(named: "record_animate_03")!,
            UIImage(named: "record_animate_04")!,
            UIImage(named: "record_animate_05")!,
            UIImage(named: "record_animate_06")!,
            UIImage(named: "record_animate_07")!,
            UIImage(named: "record_animate_08")!,
            UIImage(named: "record_animate_09")!]
        
        imageView = UIImageView(frame: CGRect.zero)
        imageView.image = images[0]
        
        super.init(frame: frame)
        backgroundColor = UIColor(white: 0.75, alpha: 1)
        // 增加毛玻璃效果
        if #available(iOS 8.0, *) {
            let visualView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
            visualView.frame = self.bounds
            visualView.layer.cornerRadius = 10.0
            visualView.layer.masksToBounds = true
            addSubview(visualView)
        } else {
            // Fallback on earlier versions
        }
        
        self.layer.cornerRadius = 10.0
        addSubview(imageView)
        addSubview(textLabel)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: -15))
        
        self.addConstraint(NSLayoutConstraint(item: textLabel, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: textLabel, attribute: .top, relatedBy: .equal, toItem: imageView, attribute: .bottom, multiplier: 1, constant: 10))
        self.addConstraint(NSLayoutConstraint(item: textLabel, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 0))
        
        translatesAutoresizingMaskIntoConstraints = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showText(text: String, textColor: UIColor = UIColor.black) {
        textLabel.textColor = textColor
        textLabel.text = text
    }
    
    func updateLevelMetra(levelMetra: Float) {
        if levelMetra > -20 {
            showMetraLevel(level: 8)
        } else if levelMetra > -25 {
            showMetraLevel(level: 7)
        }else if levelMetra > -30 {
            showMetraLevel(level: 6)
        } else if levelMetra > -35 {
            showMetraLevel(level: 5)
        } else if levelMetra > -40 {
            showMetraLevel(level: 4)
        } else if levelMetra > -45 {
            showMetraLevel(level: 3)
        } else if levelMetra > -50 {
            showMetraLevel(level: 2)
        } else if levelMetra > -55 {
            showMetraLevel(level: 1)
        } else if levelMetra > -60 {
            showMetraLevel(level: 0)
        }
    }
    
    
    func showMetraLevel(level: Int) {
        if level > images.count {
            return
        }
        performSelector(onMainThread: "showIndicatorImage:", with: NSNumber(value: level), waitUntilDone: false)
    }
    
    func showIndicatorImage(level: NSNumber) {
        imageView.image = images[level.intValue]
    }
    
}

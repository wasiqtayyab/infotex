//
//  InfluencerSettingTableViewCell.swift
//  Infotex
//
//  Created by Wasiq Tayyab on 28/09/2021.
//

import UIKit

class InfluencerSettingTableViewCell: UITableViewCell {
    //MARK:- OUTLET
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgNext: UIImageView!
    @IBOutlet weak var imgSetting: UIImageView!
    @IBOutlet weak var lblSetting: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    
    @IBOutlet weak var imgRedDiamond: UIImageView!
    @IBOutlet weak var lblRed: UILabel!
    
    @IBOutlet weak var imgBlueDiamond: UIImageView!
    @IBOutlet weak var lblBlue: UILabel!
    
    @IBOutlet weak var imgDiamond: UIImageView!
    @IBOutlet weak var lblDiamond: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

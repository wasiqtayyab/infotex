//
//  MyDiamondTableViewCell.swift
//  Infotex New Screen
//
//  Created by Wasiq Tayyab on 24/09/2021.
//

import UIKit

class MyDiamondTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var lblDiamond: UILabel!
    @IBOutlet weak var lblInformation: UILabel!
    @IBOutlet weak var lblDiamondCount: UILabel!
    @IBOutlet weak var imgDiamond: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

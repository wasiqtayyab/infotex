//
//  MyDiamondViewController.swift
//  Infotex New Screen
//
//  Created by Wasiq Tayyab on 24/09/2021.
//

import UIKit

class MyDiamondViewController: UIViewController {
    //MARK:- OUTLET
    
    @IBOutlet weak var tblDiamond: UITableView!
    @IBOutlet weak var btnApplyWithdraw: UIButton!
    
    //MARK:- VARS
    
    var arrDiamond = [["diamond":"Red Diamond","image":"setting_38","diamondCount":"99999999","detail":"Equivalent to 99999999.00 Diamond","background":"Background-1"],["diamond":"Blue Diamond","image":"setting_39","diamondCount":"99999999","detail":"Equivalent to 99999999.00 Diamond","background":"Background-2"],["diamond":"R-Diamond","image":"setting_36","diamondCount":"99999999","detail":"Equivalent to 99999999.00 USD","background":"Background-3"]]
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblDiamond.delegate = self
        tblDiamond.dataSource = self
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func applyWithdrawButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

//MARK:- TABLE VIEW DELEGATE

extension MyDiamondViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDiamond.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyDiamondTableViewCell", for: indexPath)as! MyDiamondTableViewCell
        
        cell.imgBackground.image = UIImage(named: arrDiamond[indexPath.row]["background"]!)
        cell.lblDiamond.text = arrDiamond[indexPath.row]["diamond"]
        cell.lblDiamondCount.text = arrDiamond[indexPath.row]["diamondCount"]
        cell.lblInformation.text = arrDiamond[indexPath.row]["detail"]
        cell.imgDiamond.image = UIImage(named: arrDiamond[indexPath.row]["image"]!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "DimondDetailsVC")as! DimondDetailsVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 1 {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "DimondDetailsVC")as! DimondDetailsVC
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }else {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "DimondDetailsVC")as! DimondDetailsVC
            self.navigationController?.pushViewController(vc, animated: true)
            
            
            
        }
        
    }
    
    
    
}

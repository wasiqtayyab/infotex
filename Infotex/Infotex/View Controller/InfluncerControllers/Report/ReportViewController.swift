//
//  ReportViewController.swift
//
//
//  Created by MAC on 30/09/2021.
//

import UIKit

class ReportViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tblReport: UITableView!
    var Video_Id  = "0"
    //MARK:- VARS
    
    var arrReport = ["spam","abusive"]
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblReport.delegate = self
        tblReport.dataSource = self
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
 
}

extension ReportViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReport.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportTableViewCell", for: indexPath)as! ReportTableViewCell
        cell.lblReport.text = arrReport[indexPath.row]
        let image = UIImage(named: "1-54")
        let checkmark  = UIImageView(frame:CGRect(x:0, y:0, width:15, height:15))
        checkmark.image = image
        cell.accessoryView = checkmark
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "reportDetailViewController")as! reportDetailViewController
        vc.reportTiltleText = arrReport[indexPath.row]
        vc.videoID = self.Video_Id
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
}

//
//  reportDetailViewController.swift
//  Infotex
//
//  Created by Mac on 15/09/2021.
//  Copyright © 2021 Mac. All rights reserved.
//

import UIKit

class reportDetailViewController: UIViewController,UITextViewDelegate {
    
    //MARK:- Outlets
    
  
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var tvReport: UITextView!
    @IBOutlet weak var reportTitle: UILabel!
    @IBOutlet weak var reportDescTF: UITextField!
    
    
    //MARK:- Variables
    
    var reportTiltleText = ""
    var reportID = ""
    var videoID = ""
    var myUser:[User]? {didSet{}}
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        submitButton.layer.cornerRadius = 3.0
        self.submitButton.setBackgroundImage(UIImage(named: "gradiant_back"), for: .normal)
        self.tvReport.delegate  =  self
        
        let Border = CALayer()
        Border.borderColor = #colorLiteral(red: 0.8352941176, green: 0.8352941176, blue: 0.8352941176, alpha: 1);
        Border.borderWidth = 0.5;
        Border.frame = CGRect(x: 0, y: tvReport.frame.height, width: view.frame.width, height: 1)
        tvReport.layer.addSublayer(Border)
        
        reportTitle.text = reportTiltleText
    }
    
    //MARK:- Button Action
    
   
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        let desc = reportDescTF.text
        reportVideo(reportReason: desc!)
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- TextView
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if self.tvReport.textColor == UIColor.lightGray {
            self.tvReport.text = ""
            self.tvReport.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if self.tvReport.text == ""{
            self.tvReport.text = "Tell me Everthing"
            self.tvReport.textColor = UIColor.lightGray
        }
    }
    //MARK:- API Handler
    
    func reportVideo(reportReason: String){
        
        self.myUser = User.readUserFromArchive()
        if (myUser?.count == 0) && self.myUser == nil{
            AppUtility?.showToast(string: "Please login", view: self.view)
            return
        }
        
        ApiHandler.sharedInstance.reportVideo(user_id: (self.myUser?[0].Id)!, video_id: videoID, report_reason_id: reportID, description: reportReason) { (isSuccess, response) in
            if isSuccess{
             
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    self.showToast(message: "Report Under Review", font: .systemFont(ofSize: 12))
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                }else{
                    self.showToast(message: response?.value(forKey: "msg") as! String, font: .systemFont(ofSize: 12))
                }
            }else{
                self.showToast(message:"Soemthing went wrong", font: .systemFont(ofSize: 12))
            }
        }
    }
}

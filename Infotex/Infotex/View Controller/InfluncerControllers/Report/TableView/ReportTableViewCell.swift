//
//  ReportTableViewCell.swift
//  Infotex New Screen
//
//  Created by Wasiq Tayyab on 30/09/2021.
//

import UIKit

class ReportTableViewCell: UITableViewCell {

    @IBOutlet weak var lblReport: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

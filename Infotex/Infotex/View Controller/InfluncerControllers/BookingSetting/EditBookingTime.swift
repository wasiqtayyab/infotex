//
//  EditBookingTime.swift
//  infotex
//
//  Created by Mac on 05/09/2021.
//

import UIKit

class EditBookingTime: UIViewController,UITextFieldDelegate,dismissController {
   
    
    
    //MARK:- Outlets
    
    @IBOutlet weak var cvBookingTimes: UICollectionView!
    @IBOutlet weak var lblStrDate: UILabel!
    @IBOutlet weak var viewForDate: UIView!
    @IBOutlet weak var btnSelectbookingDate: UIButton!
    @IBOutlet weak var tfSelectDate: UITextField!
  
    var obj = [String:Any]()
    var myUser:[User]? {didSet{}}
    var datePicker = UIDatePicker()
    var arrSlot = [[String:Any]]()
    var arrBooked = [[String:Any]]()
    var arrBooking = [["time":"00:00-01:00","status":"0"],
                      ["time":"01:00-02:00","status":"0"],
                      ["time":"02:00-03:00","status":"0"],
                      ["time":"03:00-04:00","status":"0"],
                      ["time":"04:00-05:00","status":"0"],
                      ["time":"05:00-06:00","status":"0"],
                      ["time":"06:00-07:00","status":"0"],
                      ["time":"07:00-08:00","status":"0"],
                      ["time":"08:00-09:00","status":"0"],
                      ["time":"09:00-10:00","status":"0"],
                      ["time":"10:00-11:00","status":"0"],
                      ["time":"11:00-12:00","status":"0"],
                      ["time":"12:00-13:00","status":"0"],
                      ["time":"13:00-14:00","status":"0"],
                      ["time":"14:00-15:00","status":"0"],
                      ["time":"15:00-16:00","status":"0"],
                      ["time":"16:00-17:00","status":"0"],
                      ["time":"17:00-18:00","status":"0"],
                      ["time":"18:00-19:00","status":"0"],
                      ["time":"19:00-20:00","status":"0"],
                      ["time":"20:00-21:00","status":"0"],
                      ["time":"21:00-22:00","status":"0"],
                      ["time":"22:00-23:00","status":"0"],
                      ["time":"23:00-24:00","status":"0"]]
    
    
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnSelectbookingDate.setTitle("Select Booking Date", for: .normal)
        self.tfSelectDate.isHidden = false
        self.cvBookingTimes.delegate = self
        self.cvBookingTimes.dataSource = self
        self.viewForDate.layer.cornerRadius = self.viewForDate.frame.size.height / 2
        self.btnSelectbookingDate.layer.cornerRadius = self.btnSelectbookingDate.frame.size.height / 2
        self.currentDate()
        self.showDatePicker()
        self.tfSelectDate.delegate =  self
        self.btnSelectbookingDate.isHidden =  true
        if #available(iOS 13.4, *){
            datePicker.preferredDatePickerStyle = .wheels
        }
        
    }
   
    //MARK:- Delegate
    func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- Button Action
    
    @IBAction func backPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closePressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func selectBookingPressed(_ sender: UIButton) {
      
        if self.btnSelectbookingDate.titleLabel?.text == "Save" {
            
            if self.arrSlot.count == 0{
                AppUtility?.showToast(string: "Please select at least one slot", view: self.view)
                return
            }
            self.tfSelectDate.isHidden = true
            self.addInfluencerBookings(booking: self.arrSlot)
            
        }else{
            self.btnSelectbookingDate.setTitle("Select Booking Date", for: .normal)
            self.tfSelectDate.isHidden = false
            
        }
    }
}

//MARK:- Extensions

extension EditBookingTime: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (self.cvBookingTimes.frame.size.width - 50) / 3
        return  CGSize(width: size, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrBooking.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookingTimeCVC", for: indexPath) as! BookingTimeCVC
        
        cell.lblTime.text = "\(arrBooking[indexPath.row]["time"] ?? "")"
        
        if arrBooking[indexPath.row]["status"] == "0"{
            cell.viewBack.backgroundColor = UIColor(named: "iron")
            cell.imgBack.isHidden = true
            cell.imgUSer.isHidden = true
            
        }
        if arrBooking[indexPath.row]["status"] == "1" {
            cell.viewBack.backgroundColor = UIColor(named: "light shaft")
            cell.viewBack.isHidden = false
            cell.imgBack.isHidden = true
            cell.imgUSer.isHidden = true
            
        }
        
        if arrBooking[indexPath.row]["status"] == "2"{
            cell.viewBack.backgroundColor = UIColor(named: "light shaft")
            cell.viewBack.isHidden = true
            cell.imgBack.isHidden = false
            cell.imgUSer.isHidden = true
            cell.imgBack.image = UIImage(named: "gradiant_back")
        }
         if arrBooking[indexPath.row]["status"] == "3"{
            cell.viewBack.backgroundColor = UIColor(named: "light shaft")
            cell.viewBack.isHidden = true
            cell.imgUSer.isHidden = false
            cell.imgBack.isHidden = false
            cell.imgBack.image = UIImage(named: "gradient_green")
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let aNum = arrBooking[indexPath.row]["status"] ?? ""
        if aNum != "0" && aNum != "3"{
            if aNum == "1"{
                var obj   =  self.arrBooking[indexPath.row]
                obj.updateValue("2", forKey: "status")
                obj.updateValue("1", forKey: "available")
                self.arrBooking.remove(at: indexPath.item)
                self.arrBooking.insert(obj, at: indexPath.item)
                self.arrBooked.append(obj)
                self.cvBookingTimes.reloadItems(at: [indexPath])
                print("available 1", arrBooking[indexPath.row]["available"])
            }else if aNum == "2" {
                var obj   =  self.arrBooking[indexPath.row]
                obj.updateValue("1", forKey: "status")
                obj.updateValue("0", forKey: "available")
                self.arrBooking.remove(at: indexPath.item)
                self.arrBooking.insert(obj, at: indexPath.item)
                    for var j in 0..<self.arrBooked.count{
                        if obj["id"] as! String == self.arrBooked[j]["id"] as! String {
                            self.arrBooked.remove(at: j)
                            self.arrBooked.insert(obj, at: j)
                        }
                    }
                self.cvBookingTimes.reloadItems(at: [indexPath])
                print("available 2", arrBooking[indexPath.row]["available"])
            }
            self.obj.removeAll()
            self.arrSlot.removeAll()
            print(arrBooked.count)
            for var i in 0..<self.arrBooking.count{
                for var j in 0..<self.arrBooked.count{
                    if self.arrBooking[i]["id"] as! String == self.arrBooked[j]["id"] as! String {
                        obj = ["booking_time_id": self.arrBooked[j]["id"] as! String,"date":self.lblStrDate.text!,"available":self.arrBooked[j]["available"] as! String]
                        self.arrSlot.append(obj)
                    }
                }
            }
            self.btnSelectbookingDate.isHidden =  false
            self.btnSelectbookingDate.setTitle("Save", for: .normal)
            
            print("Booking Time ID", arrBooking[indexPath.row]["id"])
        }
    }
    
    
    //MARK:-Date Picker End
    
    func currentDate(){
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.dateFormat = "yyyy-MM-dd"
        self.lblStrDate.text =   formatter.string(from: currentDateTime)
        self.editBooking(booking: self.lblStrDate.text!, strDate: currentDateTime)
    }
    
    
    
    func showDatePicker(){
        
        self.datePicker.datePickerMode = .date
        self.datePicker.minimumDate = Date()
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePickerEndTo));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePickerEndTo));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        self.tfSelectDate.inputAccessoryView = toolbar
        self.tfSelectDate.inputView = datePicker
    }
    
    @objc func donedatePickerEndTo(){
        
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd"
        self.lblStrDate.text! = "\(formatter1.string(from: datePicker.date))"
        print("Date Of End : \(self.lblStrDate.text!)")
        self.editBooking(booking:self.lblStrDate.text! , strDate: datePicker.date)
        self.view.endEditing(true)
        
    }
    
    @objc func cancelDatePickerEndTo(){
        self.view.endEditing(true)
    }
    //MARK:- API Handler
    
    func editBooking(booking:String,strDate:Date){
        self.myUser = User.readUserFromArchive()
        self.arrBooking.removeAll()
        self.arrBooked.removeAll()
        ApiHandler.sharedInstance.showInfluecerBooking(user_id: (self.myUser?[0].Id)!, strDate: booking) { (isSuccess, response) in
            
            if isSuccess{
                print("showInfluecerBooking:",response)
                if let res = response?.value(forKey: "msg") as? [[String:Any]]{
                    for obj in res{
                        let booking =  obj["BookingTime"] as! [String:Any]
                        if let avaiable = booking["available"] as?[String:Any]{
                            if let Booking =  avaiable["Booking"] as?[String:Any]{
                                if Booking["id"] is NSNull{
                                    self.arrBooking.append(["time":booking["time"] as! String,"status":"2","id":booking["id"] as! String,"available":"1"])
                                    self.arrBooked.append(["time":booking["time"] as! String,"status":"2","id":booking["id"] as! String,"available":"1"])
                                }else{
                                    self.arrBooking.append(["time":booking["time"] as! String,"status":"3","id":booking["id"] as! String,"available":"0"])
                                }

                            }else{
                                self.arrBooking.append(["time":booking["time"] as! String,"status":"2","id":booking["id"] as! String,"available":"1"])
                                self.arrBooked.append(["time":booking["time"] as! String,"status":"2","id":booking["id"] as! String,"available":"1"])
                            }
                           
                        }else{
                            self.arrBooking.append(["time":booking["time"] as! String,"status":"1","id":booking["id"] as! String,"available":"0"])
                        }
                    }
                    
                    //NSDate Structure
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd"
                    let now = Date()
                    let dateString = formatter.string(from:now)
                    if dateString == self.lblStrDate.text! {
                        for var i in 0..<self.arrBooking.count{
                            
                            let hour = Calendar.current.component(.hour, from: strDate)
                            let currentTime =  "\(hour).00"
                            
                            var objTime = self.arrBooking[i]
                            let timeZone =  objTime["time"]?.components(separatedBy: " - ")
                            
                            if timeZone![0] as! String > "\(currentTime)"{
                                
                            }else{
                                objTime.updateValue("0", forKey: "status")
                            }
                            
                            self.arrBooking.remove(at: i)
                            self.arrBooking.insert(objTime, at: i)
                            self.cvBookingTimes.reloadData()
                        }
                    }
                    self.cvBookingTimes.reloadData()
                }else{
                    AppUtility?.showToast(string: response?.value(forKey: "msg") as! String, view: self.view)
                }
            }else{
                AppUtility?.showToast(string: "Something went wrong", view: self.view)
            }
        }
    }
    
    func addInfluencerBookings(booking:[[String:Any]]){
        self.myUser = User.readUserFromArchive()
        ApiHandler.sharedInstance.addInfluncerBookingsTime(user_id: (self.myUser?[0].Id)!, booking: booking) { (isSuccess, response) in
            if isSuccess{
                if response?.value(forKey: "code") as! NSNumber == 200{
                    let story:UIStoryboard = UIStoryboard(name: "influncer", bundle: nil)
                    let vc =  story.instantiateViewController(withIdentifier: "InfluencerSetTimeSuccessViewController") as! InfluencerSetTimeSuccessViewController
                    vc.dismissDelegate =  self
                    self.present(vc, animated: true, completion: nil)
                }else{
                    AppUtility?.showToast(string: response?.value(forKey: "msg") as! String, view: self.view)
                }
              
                
            }else{
                AppUtility?.showToast(string: "Something went wrong", view: self.view)
            }
        }
    }
}

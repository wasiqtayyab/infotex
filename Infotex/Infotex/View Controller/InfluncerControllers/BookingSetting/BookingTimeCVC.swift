//
//  BookingTimeCVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 03/09/2021.
//

import UIKit

class BookingTimeCVC: UICollectionViewCell {
    
    //MARK:- Outlets
    
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var imgUSer: UIImageView!
    
    //MARK:- awakeFromNib
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewBack.layer.cornerRadius = 25
        self.imgBack.layer.cornerRadius = 25
    }
}

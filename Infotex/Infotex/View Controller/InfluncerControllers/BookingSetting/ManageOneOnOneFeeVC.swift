//
//  ManageOneOnOneFeeVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 05/09/2021.
//

import UIKit

class ManageOneOnOneFeeVC: UIViewController {

    //MARK:- Button Action
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewForSetValue: UIView!
    @IBOutlet weak var lblDimondPerHour: UILabel!
    @IBOutlet weak var tfSetValue: UITextField!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnSetFee: UIButton!
    var myUser:[User]? {didSet{}}
    
    
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewForSetValue.layer.cornerRadius = self.viewForSetValue.frame.size.height / 2
        self.btnSetFee.layer.cornerRadius = self.btnSetFee.frame.size.height / 2
    }
    
    
    //MARK:- Button Action
    
    @IBAction func backPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closePressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSetOneToOne(_ sender: Any) {
        
        if AppUtility!.isEmpty(self.tfSetValue.text!){
            return
        }
        self.ManageOneToOne()
    }
    
    //MARK:- API Handler
    
    func ManageOneToOne(){
        self.myUser = User.readUserFromArchive()
        ApiHandler.sharedInstance.ManageOneToOne(booking_coins_per_hour: self.tfSetValue.text!, user_id: (self.myUser?[0].Id)!) { (isSuccess, response) in
            if isSuccess{
                self.dismiss(animated: true, completion: nil)
            }else{
                AppUtility?.showToast(string: response?.value(forKey: "msg") as! String, view: self.view)
            }
      
        }
    }
}

//
//  FeedTableViewCell.swift
//  Infotex
//
//  Created by Mac on 12/09/2021.
//  Copyright © 2021 Mac. All rights reserved.
//

import UIKit
import SDWebImage

class FeedTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var collectionImages: UICollectionView!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnComments: UIButton!
    @IBOutlet weak var lblPost: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var lblTotalcount: UILabel!
    
    var arrPhoto = [videoImage]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgUser.layer.cornerRadius = 20
        self.imgUser.clipsToBounds = true
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionImages.isPagingEnabled = true
        collectionImages.setCollectionViewLayout(layout, animated: true)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return arrPhoto.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellFeedImages", for: indexPath) as! FeedImagesCollectionViewCell
        let Images =  self.arrPhoto[indexPath.row].image
        let userImgPath = AppUtility?.detectURL(ipString: Images)
        let userImgUrl = URL(string: userImgPath!)
    
        cell.imgFeed.sd_setImage(with: userImgUrl, placeholderImage: UIImage(named: "videoPlaceholder"))
        cell.imgFeed.contentMode =  .scaleAspectFill
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.contentView.frame.width, height: 347)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.lblTotalcount.text = "\(indexPath.row + 1)\("/")\(self.pageControl.numberOfPages)"
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
      
        self.pageControl.currentPage = Int(self.collectionImages.contentOffset.x) / Int(self.collectionImages.frame.width)
    
    }
    func updateLabelWith(text: String, name:String) -> NSAttributedString {
        
        let normalAttributes = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17.0),
            NSAttributedString.Key.foregroundColor : UIColor.darkGray,
            NSAttributedString.Key.textEffect : NSAttributedString.TextEffectStyle.letterpressStyle] as [NSAttributedString.Key : Any]
        let attributedStringNormal = NSMutableAttributedString(string: text, attributes: normalAttributes)
        
        // Find and Make Bold for App Name
        let boldAttributes = [
            NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17.0),
            NSAttributedString.Key.foregroundColor : UIColor.darkGray,
            NSAttributedString.Key.textEffect : NSAttributedString.TextEffectStyle.letterpressStyle] as [NSAttributedString.Key : Any]
        let attributedStringBold = NSMutableAttributedString(string: name, attributes: boldAttributes)
        let attributedString = NSMutableAttributedString()
        attributedString.append(attributedStringBold)
        attributedString.append(attributedStringNormal)
        return attributedString
    }

}

//
//  SingleFeedViewController.swift
//  infotex
//
//  Created by Mac on 11/09/2021.
//  Copyright © 2021 Mac. All rights reserved.
//

import UIKit
import SDWebImage

class SingleFeedViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,removeVideoDelegate {
    
    //MARK: Outlets
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    @IBOutlet weak var tblFeeds: UITableView!
    var userID = "0"
    var arrFeeds = [profileDetailVideo]()
   
    
    //MARK:- View Life Cycle Start here...
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        
        print("arrFeeds count",self.arrFeeds.count)
    }
    
    
    //MARK:- Setup View
    func setupView() {
        self.tblFeeds.estimatedRowHeight = 400
        self.tblFeeds.rowHeight = UITableView.automaticDimension
    }
    
    //MARK:- Button Action

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnMoreAction(btn : UIButton){
        if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "influncer", bundle: nil)
            let vc = storyMain.instantiateViewController(withIdentifier: "PostPopUpViewController") as! PostPopUpViewController
            vc.CurrentIndex =  btn.tag
            vc.videoID = self.arrFeeds[btn.tag].id
            vc.profileDetailVideo =  self.arrFeeds[btn.tag]
            vc.type = "image"
            vc.removedelegate = self
            rootViewController.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func btnLikeUnlikeAction(btn : UIButton)
    {
        let index = IndexPath(row: btn.tag, section: 0)
        let cell =  self .tblFeeds.cellForRow(at:index) as!FeedTableViewCell
        var feed = self.arrFeeds[btn.tag]
        var intFeedLike = Int(feed.like_count)
        if feed.like == 0{
        
            feed.like = 1
            intFeedLike = intFeedLike + 1
            feed.like_count = intFeedLike
            cell.btnLike.setImage(UIImage(named: "heart_red"), for: .normal)
            if feed.like_count == 1 || feed.like_count == 0 {
                cell.lblLike.text = "\(feed.like_count) like"
            }else{
                cell.lblLike.text = "\(feed.like_count) likes"
            }
            
        }else{
            
            feed.like = 0
            intFeedLike = intFeedLike - 1
            feed.like_count = intFeedLike
            cell.btnLike.setImage(UIImage(named: "heart_1"), for: .normal)
            if feed.like_count == 1 || feed.like_count == 0 {
                cell.lblLike.text = "\(feed.like_count) like"
            }else{
                cell.lblLike.text = "\(feed.like_count) likes"
            }
            self.arrFeeds.remove(at: btn.tag)
            self.arrFeeds.insert(feed, at: btn.tag)
        }
        
        self.likeVideo(videoID: self.arrFeeds[btn.tag].id)
    }
    
    
    @objc func btnCommentAction(btn : UIButton)
    {
        if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "influncer", bundle: nil)
            let vc = storyMain.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
            vc.Video_Id = self.arrFeeds[btn.tag].id
            vc.modalPresentationStyle = .overFullScreen
            rootViewController.navigationController?.present(vc, animated: true, completion: nil)
            
        }
        
    }
    
    //MARK:- DELEGATE METHODS
    
    func updateVideoObj(index: Int, type: String) {
        if type == "image"{
            self.arrFeeds.remove(at: index)
            self.tblFeeds.reloadData()
        }
    }
    //MARK: TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.arrFeeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellFeeds", for: indexPath) as! FeedTableViewCell
        
        let feed = self.arrFeeds[indexPath.row]
        let feedImages = feed.videoImage
        let feeduser = feed.VideoMain

        let Firstname = feeduser[0].first_name
        let LastName = feeduser[0].last_name
        
        cell.lblUserName.text = "\(Firstname) \(LastName)"
        let userImgPath = AppUtility?.detectURL(ipString: feeduser[0].userProfile_pic)
        let userImgUrl = URL(string: userImgPath!)
        cell.imgUser.sd_setImage(with: userImgUrl, placeholderImage: UIImage(named: "noUserNew"))
     
        cell.arrPhoto = feedImages
        cell.pageControl.numberOfPages = feedImages.count
     
        if feedImages.count == 1{
            cell.collectionImages.isScrollEnabled = false
            cell.pageControl.numberOfPages = 0
            cell.lblTotalcount.isHidden =  true
        }else{
            cell.lblTotalcount.text = "\("1")\("/")\(feedImages.count)"
            cell.collectionImages.isScrollEnabled = true
            cell.lblTotalcount.isHidden =  false
        }
        cell.collectionImages.reloadData()
        if feed.like_count == 1 || feed.like_count == 0 {
            cell.lblLike.text = "\(feed.like_count) like"
        }else{
            cell.lblLike.text = "\(feed.like_count) likes"
        }
       
        if feed.like == 1{
            cell.btnLike.setImage(UIImage(named: "heart_red"), for: .normal)
        }else{
            cell.btnLike.setImage(UIImage(named: "heart_1"), for: .normal)
        }
        
        if self.arrFeeds[indexPath.row].videoComment.count > 0 {
            cell.lblPost.text! = "\(self.arrFeeds[indexPath.row].videoComment[0].username):\(self.arrFeeds[indexPath.row].videoComment[0].comment)"
        }else{
            cell.lblPost.text! = "No comment"
        }
        
        let stringDate = feed.created.formattedDateFromString(dateString: feed.created, withFormat: "MMM dd, yyyy")
        cell.lblTime.text = stringDate
        
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addTarget(self, action: #selector(self.btnMoreAction), for: .touchUpInside)
        
        cell.btnLike.tag = indexPath.row
        cell.btnLike.addTarget(self, action: #selector(self.btnLikeUnlikeAction), for: .touchUpInside)
        
        cell.btnComments.tag = indexPath.row
        cell.btnComments.addTarget(self, action: #selector(self.btnCommentAction), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    //MARK:- API Handler
    func likeVideo(videoID:String){
        
        ApiHandler.sharedInstance.likeVideo(user_id:self.userID, video_id: videoID,type:"image") { (isSuccess, response) in
       
            if isSuccess{
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    self.tblFeeds.reloadData()
                    //print("likeVideo response msg: ",response?.value(forKey: "msg"))
                }else{
                    self.tblFeeds.reloadData()
                    //print("likeVideo response msg: ",response?.value(forKey: "msg"))
                }
            }
        }
    }
}

class ScaledHeightImageView: UIImageView {

    override var intrinsicContentSize: CGSize {

        if let myImage = self.image {
            let myImageWidth = myImage.size.width
            let myImageHeight = myImage.size.height
            let myViewWidth = self.frame.size.width
 
            let ratio = myViewWidth/myImageWidth
            let scaledHeight = myImageHeight * ratio

            return CGSize(width: myViewWidth, height: scaledHeight)
        }

        return CGSize(width: -1.0, height: -1.0)
    }

}

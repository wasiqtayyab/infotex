//
//  ImageCaputreViewController.swift
//  Infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit
import AVFoundation
import NextLevel
import Photos
import CoreAnimator

class ImageCaputreViewController: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var capturePreviewView: UIView!
    @IBOutlet weak var toggleFlashButton: UIButton!
    @IBOutlet weak var toggleCameraButton: UIButton!
    let cameraController = CameraController()
    override var prefersStatusBarHidden: Bool { return true }
    
     
    //MARK:- viewDidLoad
      override func viewDidLoad() {
          super.viewDidLoad()
          
          self.setupView()
      }
      

      //MARK:- SetupView
      
      func setupView(){
     
        configureCameraController()

      }

    //MARK:- ConfigureCameraController
    func configureCameraController() {
        cameraController.prepare {(error) in
            if let error = error {
                print(error)
            }
            
            try? self.cameraController.displayPreview(on: self.capturePreviewView)
        }
    }
    
      //MARK:- Button Action
    @IBAction func btnCrossAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnTimerAction(_ sender: Any) {
        let vc =  storyboard?.instantiateViewController(withIdentifier: "RangeSliderViewController") as! RangeSliderViewController
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btnImagepickerActipon(_ sender: Any) {
        let vc =  storyboard?.instantiateViewController(withIdentifier:
            "ImagePickerViewController") as! ImagePickerViewController
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btnCameraFlip(_ sender: Any) {
        do {
            try cameraController.switchCameras()
        }
            
        catch {
            print(error)
        }
        let animator = CoreAnimator(view: toggleCameraButton)
        animator.rotate(angle: 180).animate(t: 0.5)
        
        switch cameraController.currentCameraPosition {
        case .some(.front):
            toggleCameraButton.setImage(UIImage(named: "camerachange_"), for: .normal)
            
        case .some(.rear):
            toggleCameraButton.setImage(UIImage(named: "camerachange_"), for: .normal)
            
        case .none:
            return
        }

    }
    @IBAction func takePhoto(_ sender: AnyObject) {
        cameraController.captureImage {(image, error) in
            guard let image = image else {
                print(error ?? "Image capture error")
                return
            }
            
            try? PHPhotoLibrary.shared().performChangesAndWait {
                PHAssetChangeRequest.creationRequestForAsset(from: image)
            }
            let vc =  self.storyboard?.instantiateViewController(withIdentifier: "ImageEffectViewController") as! ImageEffectViewController
            vc.effectImages.append(image)
            self.present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func btnFlash(_ sender: AnyObject) {
        if cameraController.flashMode == .on {
            cameraController.flashMode = .off
            toggleFlashButton.setImage(UIImage(named: "light_white"), for: .normal)
        }
            
        else {
            cameraController.flashMode = .on
            toggleFlashButton.setImage(UIImage(named: "not_light"), for: .normal)
        }
    }
   
      //MARK: TableView
      
      //MARK: CollectionView.
      
      //MARK: Segment Control
      
      //MARK: Alert View
      
      //MARK: TextField
      
      //MARK: Location
        
      //MARK: Google Maps
    
     
}


//
//  ImageEffectViewController.swift
//  Infotex
//
//  Created by Mac on 04/09/2021.
//

import UIKit
import Mantis

class ImageEffectViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,CropViewControllerDelegate {

    
    //MARK:- Outlets
    @IBOutlet weak var cvImageViews: UICollectionView!
    @IBOutlet weak var cvFilterView: UICollectionView!
    var effectImages = [UIImage]()
    @IBOutlet weak var btnCrop: UIButton!
    internal var SelectIndex = 0
    
    
    //Filters
    internal var filterIndex = 0
    internal let context = CIContext(options: nil)
    internal var image: UIImage?
    internal var smallImage: UIImage?
    var filterImages = [UIImage]()
    
    
    
    internal let filterNameList = [
        "CIColorControls",
        "CIPhotoEffectChrome",
        "CIPhotoEffectFade",
        "CIPhotoEffectInstant",
        "CIPhotoEffectMono",
        "CIPhotoEffectNoir",
        "CIPhotoEffectProcess",
        "CIPhotoEffectTonal",
        "CIPhotoEffectTransfer",
        "CILinearToSRGBToneCurve",
        
        
        "CIColorClamp",
        "CIColorMatrix",
        "CIColorPolynomial",
        "CIExposureAdjust",
        "CIGammaAdjust",
        "CIHueAdjust",
        "CILinearToSRGBToneCurve",
        "CISRGBToneCurveToLinear",
        "CITemperatureAndTint",
        "CIToneCurve",
        "CIVibrance",
        "CIWhitePointAdjust"
        
    ]
    
    internal let filterDisplayNameList = [
        "Normal",
        "Chrome",
        "Fade",
        "Instant",
        "Mono",
        "Noir",
        "Process",
        "Tonal",
        "Transfer",
        "Tone",
        "Linear",
        
        "Clamp",
        "Matrix",
        "Polynomial",
        "Exposure",
        "Gamma",
        "Hue",
        "SRGBTone",
        "Curve",
        "Temperature",
        "Vibrance",
        "WhitePoint"
    ]
    
    
    //MARK:- ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        
    }
    
    //MARK:- SetupView
    
    func setupView(){
        
        self.cvFilterView.register(FilterCollectionViewCell.self, forCellWithReuseIdentifier: "FilterCollectionViewCell")
        
        self.cvFilterView.register(UINib.init(nibName: "FilterCollectionViewCell", bundle:nil), forCellWithReuseIdentifier: "FilterCollectionViewCell")
        
        
        
        for var i in 0..<self.filterNameList.count{
            self.filterImages.append(self.createFilteredImage(filterName: self.filterNameList[i], image: self.effectImages[0]))
        }
        
        self.cvFilterView.reloadData()
        
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        cvImageViews.isPagingEnabled = true
        cvImageViews.setCollectionViewLayout(layout, animated: true)
        
    }
    
    //MARK:- Button Actions
    
    @IBAction func btnCropAction(_ sender: UIButton) {
        print(SelectIndex)
        
        guard let image = image else {
            return
        }
        
        let config = Mantis.Config()
        let cropViewController = Mantis.cropViewController(image: image,config: config)
        cropViewController.modalPresentationStyle = .fullScreen
        cropViewController.delegate = self
        present(cropViewController, animated: true)
        
        
    }
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnNextAction(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "PostImagesViewController") as! PostImagesViewController
        vc.uploadImage = self.effectImages
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    
    //MARK:- CollectionView.
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == cvFilterView{
            return filterNameList.count
        }else{
            return self.effectImages.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == cvFilterView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCollectionViewCell", for: indexPath) as! FilterCollectionViewCell
            cell.filterNameLabel.textColor = .white
            cell.filterNameLabel.text = self.filterDisplayNameList[indexPath.row]
            cell.imageView.image =  self.filterImages[indexPath.row]
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaPickerCollectionViewCell", for: indexPath) as! MediaPickerCollectionViewCell
            cell.image.image = self.effectImages[indexPath.row]
            
            return cell
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
      
        
        if collectionView == cvImageViews{
            SelectIndex = indexPath.row
            print("SelectIndex",SelectIndex)
            DispatchQueue.main.asyncAfter(deadline:.now() + 0/5) {
                self.filterImages.removeAll()
                
                for var i in 0..<self.filterNameList.count{
                    self.filterImages.append(self.createFilteredImage(filterName: self.filterNameList[i], image: self.effectImages[indexPath.row]))
                    self.image =  self.effectImages[indexPath.row]
                }
                self.cvFilterView.reloadData()
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == cvFilterView{
            return CGSize(width:120, height: 140)
        }else{
            return CGSize(width: self.view.frame.width, height: 300)
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == cvFilterView{
            print("SelectIndex",SelectIndex)
            let img =   self.createFilteredImage(filterName:self.filterNameList[indexPath.row], image: self.image!)
            
            self.effectImages.remove(at: SelectIndex)
            self.effectImages.insert(img, at: SelectIndex)
            self.cvImageViews.reloadData()
            
            
        }else{
            
//            let config = Mantis.Config()
//            let cropViewController = Mantis.cropViewController(image: self.effectImages[indexPath.row],config: config)
//            cropViewController.modalPresentationStyle = .fullScreen
//            cropViewController.delegate = self
//            present(cropViewController, animated: true)
        }
    }
    
    //MARK: Filter
    func createFilteredImage(filterName: String, image: UIImage) -> UIImage {
        
        // 1 - create source image
        let sourceImage = CIImage(image: image)
        
        // 2 - create filter using name
        let filter = CIFilter(name: filterName)
        filter?.setDefaults()
        
        // 3 - set source image
        filter?.setValue(sourceImage, forKey: kCIInputImageKey)
        
        // 4 - output filtered image as cgImage with dimension.
        let outputCGImage = context.createCGImage((filter?.outputImage!)!, from: (filter?.outputImage!.extent)!)
        
        // 5 - convert filtered CGImage to UIImage
        let filteredImage = UIImage(cgImage: outputCGImage!, scale: image.scale, orientation: image.imageOrientation)
        
        return filteredImage
    }
}

//MARK:-Image Cropped

extension ImageEffectViewController{
    func cropViewControllerDidCrop(_ cropViewController: CropViewController, cropped: UIImage, transformation: Transformation) {
        
        print(transformation)
        print(cropped)

        self.effectImages.remove(at: SelectIndex)
        self.effectImages.insert(cropped, at: SelectIndex)
        self.cvImageViews.reloadData()
        self.dismiss(animated: true)
    }
    
    func cropViewControllerDidCancel(_ cropViewController: CropViewController, original: UIImage) {
        self.dismiss(animated: true)
    }
}

extension UICollectionView {
    var visibleCurrentCellIndexPath: IndexPath? {
        for cell in self.visibleCells {
            let indexPath = self.indexPath(for: cell)
            return indexPath
        }
        
        return nil
    }
}

//
//  requestVerificationViewController.swift
//  TIK TIK
//
//  Created by Mac on 15/07/2021.
//  Copyright © 2021 Mac. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class requestVerificationViewController: UIViewController,UIImagePickerControllerDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var lblImage: UILabel!
    @IBOutlet weak var detailText: UILabel!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var tfUsername: SkyFloatingLabelTextField!
    
    @IBOutlet weak var tfFullName: SkyFloatingLabelTextField!
    
    //MARK:- VARS
    var myUser: [User]? {didSet {}}
    var imgData = ""
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailText.text = "A verification badge is check that appears next to an MusicTok accounts name to indicate that the account is the authentic presence of notable public figure,celebrity,global brand or entitu it represents. \n\nSubmitting a requeset for verification does not guarantee that your account will be verified"
        self.myUser = User.readUserFromArchive()
      
        tfUsername.text = (myUser?[0].username)!
        tfFullName.text = "\((myUser?[0].first_name)!)" + " " + "\((myUser?[0].last_name)!)"
       
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnSelectImageAction(_ sender: Any) {
        ImagePickerManager().pickImage(self){ image in
            self.imgData = (image.jpegData(compressionQuality: 0.1)?.base64EncodedString())!
            print("profilePicData: ",self.imgData)
            let number = Int.random(in: 0..<100)
            self.lblImage.text = "image.\(number).jpg"
            self.btnSend.setBackgroundImage(UIImage(named: "97-97"), for: .normal)
        }
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
       
    }
    @IBAction func btnSend(_ sender: Any) {
        if imgData != ""{
            verificationAPI()
        }else{
            self.showToast(message: "Photo is Missing", font: .systemFont(ofSize: 12))
        }
    }
    
    //MARK:- API
    
    func verificationAPI(){
        self.loader.isHidden =  false
        ApiHandler.sharedInstance.userVerificationRequest(user_id: user_id, attachment: ["file_data":self.imgData]) { (isSuccess, response) in
            self.loader.isHidden =  true
            if isSuccess{
                if response?.value(forKey: "code") as! NSNumber == 200{
                 
                    self.showToast(message: "Request Submitted", font: .systemFont(ofSize: 12))
                    sleep(1)
                    self.navigationController?.popViewController(animated: true)
                    print("response: ",response)
                }else{
                    self.loader.isHidden =  true
                    self.showToast(message: "Something Went Wrong", font: .systemFont(ofSize: 12))

                    print("response: ",response)
                }

            }
        }
    }
}

//
//  UpdateEmailOtpViewController.swift
//  Infotex
//
//  Created by Wasiq Tayyab on 29/09/2021.
//

import UIKit
import DPOTPView
class UpdateEmailOtpViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var otpView: DPOTPView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblResendCode: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var btnResendCode: UIButton!
    
    //MARK:- VARS
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    var counter = 60
    var timer = Timer()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        btnNext.backgroundColor = UIColor(named: "light shaft")
        lblCode.text = "Your code was sent to \(email)"
        otpView.dpOTPViewDelegate = self
        btnResendCode.isHidden = true
        timerSetup()
        
    }
    //MARK:- TIMER
    
    private func timerSetup(){
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounter() {
        
        if counter !=  0 {
            lblResendCode.text = "Resend Code 00:\(counter)"
            counter -= 1
        }else {
            
            self.counter = 60
            timer.invalidate()
            lblResendCode.isHidden = true
            btnResendCode.isHidden = false
        }
    }
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        if otpView.text!.count < 4 {
            AppUtility?.showToast(string: "Enter your valid otp", view: self.view)
            return
        }
        self.verifyChangeEmailApi()
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendButtonPressed(_ sender: UIButton) {
        self.changeEmailApi()
        
    }
    
    //MARK:- API
    
    private func verifyChangeEmailApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.verifyChangeEmailCode(user_id: user_id, new_email: email, code: otpView.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let userObj = resp!["msg"] as! [String:Any]
                    UserObject.shared.Objresponse(response: userObj,isLogin: false)
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: ManageAccountViewController.self) {
                            _ =  self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
        
    }
    
    
    private func changeEmailApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.changeEmailAddress(user_id: user_id, email: email, verify: "0") { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    self.timerSetup()
                    self.otpView.text! = ""
                    self.btnResendCode.isHidden = true
                    self.lblResendCode.isHidden = false
                    
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    
    
    
}

//MARK:- Textfield Delegate

extension UpdateEmailOtpViewController: DPOTPViewDelegate {
    
    func dpOTPViewAddText(_ text: String, at position: Int) {
        
        
        
        if position <= 3 && text.count == 4{
            btnNext.setBackgroundImage(UIImage(named: "97-97"), for: .normal)
            
            btnNext.backgroundColor = .clear
            otpView.dismissOnLastEntry = true
            
        }else {
            btnNext.setBackgroundImage(nil, for: .normal)
            btnNext.backgroundColor = UIColor(named: "light shaft")
            otpView.dismissOnLastEntry = true
            
        }
        
    }
    
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        
        if position <= 3 && text.count == 4{
            
            btnNext.setBackgroundImage(UIImage(named: "97-97"), for: .normal)
            btnNext.backgroundColor = .clear
            otpView.dismissOnLastEntry = true
            
        }else {
            btnNext.setBackgroundImage(nil, for: .normal)
            btnNext.backgroundColor = UIColor(named: "light shaft")
            otpView.dismissOnLastEntry = true
            
        }
        
        
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
        
        
        
    }
    
    func dpOTPViewBecomeFirstResponder() {
        
    }
    func dpOTPViewResignFirstResponder() {
        
        
    }
    
    
}


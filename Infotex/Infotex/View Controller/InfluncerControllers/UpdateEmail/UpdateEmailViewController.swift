//
//  UpdateEmailViewController.swift
//  Infotex
//
//  Created by Wasiq Tayyab on 29/09/2021.
//

import UIKit

class UpdateEmailViewController: UIViewController, UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    
    //MARK:- VARS
    
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldSetup()
        btnNext.backgroundColor = UIColor(named: "light shaft")
        
    }
    
    //MARK:- TEXTFIELD PLACEHOLDER
    
    private func textFieldSetup(){
        tfEmail.delegate = self
        tfEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        if !AppUtility!.isEmail(tfEmail.text!){
            AppUtility?.showToast(string: "Enter your valid email", view: self.view)
            return
        }
        email = tfEmail.text!
        self.changeEmailApi()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- TEXTFIELD DELEGATE
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if !AppUtility!.isEmail(tfEmail.text!){
            btnNext.backgroundColor = UIColor(named: "light shaft")
            btnNext.setBackgroundImage(nil, for: .normal)
            return
        }
        btnNext.backgroundColor = .clear
        btnNext.setBackgroundImage(UIImage(named: "97-97"), for: .normal)
    }
    
    //MARK:- API
    
    private func changeEmailApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.changeEmailAddress(user_id: user_id, email: tfEmail.text!, verify: "0") { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateEmailOtpViewController")as! UpdateEmailOtpViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    
}

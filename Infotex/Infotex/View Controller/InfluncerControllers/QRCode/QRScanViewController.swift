//
//  QRScanViewController.swift
//  InfoTex
//
//  Created by Mac on 16/07/2021.
//  Copyright © 2021 Mac. All rights reserved.
//

import UIKit
import MercariQRScanner
import AVFoundation


class QRScanViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var viewScanner: UIView!
    @IBOutlet weak var imgQRCode: UIButton!
    @IBOutlet weak var flashIconImgView:UIButton!
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let qrScannerView = QRScannerView(frame: view.bounds)
        viewScanner.addSubview(qrScannerView)
        qrScannerView.configure(delegate: self)
        qrScannerView.startRunning()
        
        self.imgQRCode.tintColor = .white
    }
    
    
    //MARK:- Button actions
    
    @IBAction func btrnFlashLight(_ sender: Any) {
        self.toggleFlash()
    }
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- funcations
    
    func toggleFlash() {
       let device = AVCaptureDevice.default(for: AVMediaType.video)

       if (device != nil) {
           if (device!.hasTorch) {
               do {
                   try device!.lockForConfiguration()
                       if (device!.torchMode == AVCaptureDevice.TorchMode.on) {
                        self.flashIconImgView.setImage(UIImage(named: "light_white"), for: .normal)
                    
                           device!.torchMode = AVCaptureDevice.TorchMode.off
                       } else {
                        
                           do {
                               try device!.setTorchModeOn(level: 1.0)
                            self.flashIconImgView.setImage(UIImage(named: "not_light"), for: .normal)
                               } catch {
                                   print(error)
                               }
                       }

                       device!.unlockForConfiguration()
               } catch {
                   print(error)
               }
           }
       }
   }
}


extension QRScanViewController: QRScannerViewDelegate {
    func qrScannerView(_ qrScannerView: QRScannerView, didFailure error: QRScannerError) {
        print(error)
    }
    
    func qrScannerView(_ qrScannerView: QRScannerView, didSuccess code: String) {
        affiliated_code = code
        self.navigationController?.popViewController(animated: true)
    }
}

//
//  HomeVideoCollectionViewCell.swift
//  Infotex
//
//  Created by Mac on 1/09/2021.
//  Copyright © 2021 MAC. All rights reserved.
//



import UIKit
import AVFoundation
import GSPlayer
import MarqueeLabel
import DSGradientProgressView
import SDWebImage

protocol videoLikeDelegate: class {
    func updateObj(obj: videoMainMVC , index: Int,islike:Bool)
}

class HomeVideoCollectionViewCell: UICollectionViewCell {
    
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var playerView: VideoPlayerView!
    @IBOutlet weak var progressView: DSGradientProgressView!
    @IBOutlet weak var nameBtn: UIButton!
    @IBOutlet weak var captionLbl: AttrTextView!
    @IBOutlet weak var musicLbl: MarqueeLabel!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var likeCountLbl: UILabel!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var commentCountLbl: UILabel!
    @IBOutlet weak var musicBtn: UIButton!
    @IBOutlet weak var imgMusicBtn: UIImageView!
    @IBOutlet weak var pauseImgView: UIImageView!
    @IBOutlet weak var heightTV: NSLayoutConstraint!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var thumbNailImage: UIImageView!
    @IBOutlet weak var btnCoin: UIButton!
    
    // MARK: - Variables
    
    private(set) var isPlaying = false
    private(set) var liked = false
    private(set) var liked_count:Int! = 0
    var myUser:[User]? {didSet{}}
    
    
    var arrVideo :videoMainMVC?
    weak var delegate: videoLikeDelegate?
    weak var removedelegate: removeVideoDelegate?
    
    // MARK: Lifecycles
    
    override func prepareForReuse() {
        super.prepareForReuse()
        playerView.isHidden = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    //MARK:- awakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("commentVideo"), object: nil)
        
        self.setupView()
        
    }
    
    //MARK:- Deniet Notification
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("commentVideo"), object: nil)
    }
    
    //MARK:- SetupView
    func setupView(){
        
        self.myUser = User.readUserFromArchive()
        
        self.likeBtn.setImage(UIImage(named: "heart_1"), for: .normal)
        self.pauseImgView.isHidden = true
        self.playerView.contentMode = .scaleAspectFill
        self.imgMusicBtn.makeRounded()
        self.musicBtn.makeRounded()
        self.videoPlayer()
        
        //Tap Gesture
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleSingleTap(_:)))
        self.playerView.isUserInteractionEnabled = true
        self.playerView.addGestureRecognizer(tap)
        
        
        //Tap longPressRecognizer
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self,action: #selector(self.longGesture(tap:)))
        self.playerView.addGestureRecognizer(longPressRecognizer)
        
        
    }
    
    
    //MARK:- Configuration
    
    func configure(post: videoMainMVC){
        self.myUser = User.readUserFromArchive()

        self.arrVideo = post
        self.nameBtn.setTitle("@" + post.username, for: .normal)
        self.nameBtn.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        self.profileImgView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        
        let userImgPath = AppUtility?.detectURL(ipString: post.userProfile_pic)
        let userImgUrl = URL(string: userImgPath!)
        self.profileImgView.sd_setImage(with: userImgUrl, placeholderImage: UIImage(named: "music_"))
        
        let playerIcon = AppUtility?.detectURL(ipString: post.CDPlayer)
        let CDPlayerUrl = URL(string: playerIcon!)
        self.imgMusicBtn.sd_setImage(with:CDPlayerUrl, placeholderImage: UIImage(named: ""))
        
        self.musicLbl.text = post.soundName
        self.likeCountLbl.text = post.like_count ?? "0".shorten()
        self.commentCountLbl.text = post.comment_count ?? "0".shorten()
        self.liked_count = Int(post.like_count) ?? 0
        
        print("Follow:\( post.followBtn)")
        
        if post.followBtn == "follow"{
            self.btnFollow.isHidden =  false
        }else{
            self.btnFollow.isHidden =  true
        }
        if post.like ==  "1"{
            likeBtn.tintColor = .red
            liked = true
        }else{
            likeBtn.tintColor = .white
            liked = false
        }
        let duetVidID = post.duetVideoID
        
        if duetVidID != "0"{
            self.playerView.contentMode = .scaleAspectFit
        }else{
            self.playerView.contentMode = .scaleAspectFill
        }
        
        
        self.captionLbl.setText(text: post.description,textColor: .white, withHashtagColor: #colorLiteral(red: 0.9008677602, green: 0.4844875932, blue: 0.6523317099, alpha: 1), andMentionColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), andCallBack: { (strng, type) in
            
            print("type: ",type)
            print("strng: ",strng)
            
            switch type{
            
            case .hashtag:
                
                if let rootViewController = UIApplication.topViewController() {
                    let storyMain = UIStoryboard(name: "influncer", bundle: nil)
                    let vc = storyMain.instantiateViewController(withIdentifier: "hashtagsVideoVC") as! hashtagsVideoViewController
                    vc.hashtag = strng
                    vc.hidesBottomBarWhenPushed = true
                    rootViewController.navigationController?.pushViewController(vc, animated: true)
                }
                break
                
            case .mention:
                
                if let rootViewController = UIApplication.topViewController() {
                    let storyMain = UIStoryboard(name: "influncer", bundle: nil)
                    print("obj user id : ",self.arrVideo!.userID)
                    let userID = UserDefaults.standard.string(forKey: "userID")
                    let vc = storyMain.instantiateViewController(withIdentifier: "InfluncerProfileVC") as!  InfluncerProfileVC
                    vc.isOtherUser = true
                    vc.hidesBottomBarWhenPushed = true
                    vc.userID = self.arrVideo!.userID
                    UserDefaults.standard.set(self.arrVideo!.userID, forKey: "otherUserID")
                    
                    rootViewController.navigationController?.pushViewController(vc, animated: true)
                }
                break
            }
            
        }, normalFont: .systemFont(ofSize: 12, weight: UIFont.Weight.medium), hashTagFont: .systemFont(ofSize: 12, weight: UIFont.Weight.bold), mentionFont: .systemFont(ofSize: 12, weight: UIFont.Weight.bold))
        
        let numberLines = captionLbl.numberOfLines(textView: captionLbl)
        print("numberLines",numberLines)
        self.heightTV.constant =  CGFloat(numberLines * 24)
        
        let videoTHUMPath = AppUtility?.detectURL(ipString: post.videoTHUM)
        let ThumbUrl = URL(string: videoTHUMPath!)
        self.thumbNailImage.sd_setImage(with: ThumbUrl, placeholderImage: UIImage(named: "videoPlaceholder"))
        
        
    }
    
    
    func replay(){
        if !isPlaying {
            playerView.play(for: NSURL(string: self.arrVideo!.videoURL ) as! URL,filterName:"",filterIndex:0)
            play()
        }
    }
    
    func play() {
        musicLbl.holdScrolling = false
        playerView.play(for: NSURL(string: self.arrVideo!.videoURL ) as! URL,filterName:"",filterIndex:0)
        playerView.isHidden = false
        isPlaying =  true
    }
    
    func pause(){
        playerView.pause(reason: .hidden)
        musicLbl.holdScrolling = true
        isPlaying =  false
    }
    
    @objc func handleSingleTap(_ gesture: UITapGestureRecognizer){
        print("singletapped")
        
        if self.playerView.state == .playing {
            self.playerView.pause(reason: .userInteraction)
            self.pauseImgView.isHidden = false
        } else {
            self.pauseImgView.isHidden = true
            self.playerView.resume()
        }
    }
    
    //MARK:- Player
    
    func videoPlayer(){
        playerView.stateDidChanged = { state in
            switch state {
            case .none:
                print("none")
            case .error(let error):
                
                print("error - \(error.localizedDescription)")
                self.progressView.wait()
                self.progressView.isHidden = false
                
                NotificationCenter.default.post(name: Notification.Name("errInPlay"), object: nil, userInfo: ["err":error.localizedDescription])
                
            case .loading:
                print("loading")
                self.progressView.wait()
                self.progressView.isHidden = false
            case .paused(let playing, let buffering):
                print("paused - progress \(Int(playing * 100))% buffering \(Int(buffering * 100))%")
                self.progressView.signal()
                self.progressView.isHidden = true
                self.musicBtn.stopRotating()
                self.imgMusicBtn.stopRotating()
            case .playing:
                self.pauseImgView.isHidden = true
                self.progressView.isHidden = true
                self.musicBtn.startRotating()
                self.imgMusicBtn.startRotating()
                print("playing")
            }
        }
        print("video Pause Reason: ",playerView.pausedReason )
    }
    
    
    // MARK: - Button Actions
    @IBAction func btnFollow(_ sender: Any) {
        self.myUser = User.readUserFromArchive()
        if (myUser?.count == 0) || self.myUser == nil{
            return
        }
        self.followUser(rcvrID: self.arrVideo!.userID, userID:  (self.myUser?[0].Id)!)
    }
    @IBAction func btnCoinShareAction(_ sender: Any) {
        self.myUser = User.readUserFromArchive()
        if (myUser?.count == 0) || self.myUser == nil{
            return
        }
        if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "User", bundle: nil)
            let vc = storyMain.instantiateViewController(withIdentifier: "SendGiftsVC") as! SendGiftsVC
            vc.videoObj = self.arrVideo
            rootViewController.navigationController?.present(vc, animated: true)
        }
    }
    
    @IBAction func like(_ sender: AnyObject) {
        btnLike(senderTag:sender.tag)
    }
    
    func btnLike(senderTag:Int){
        print("Like btn SenderTag:\(senderTag)")
        
        self.myUser = User.readUserFromArchive()
        if (myUser?.count == 0) || self.myUser == nil{
            return
        }
        var obj = self.arrVideo
        if self.liked == true{
            
            likeBtn.setImage(UIImage(named: "heart_1"), for: .normal)
            liked_count = liked_count - 1
            self.likeCountLbl.text = "\(liked_count!)"
            obj!.like = "0"
            obj!.like_count  = "\(liked_count!)"
            self.liked = false
            
        }else{
            
            likeBtn.setImage(UIImage(named: "heart"), for: .normal)
            liked_count = liked_count + 1
            self.likeCountLbl.text = "\(liked_count!)"
            obj!.like = "1"
            self.liked = true
            obj!.like_count  = "\(liked_count!)"
            
        }
        delegate?.updateObj(obj: obj!, index: senderTag, islike: true)
        
        
    }
    
    
    
    @IBAction func btnprofile (_ sender: AnyObject) {
        self.myUser = User.readUserFromArchive()
        if (myUser?.count == 0) || self.myUser == nil{
            return
        }
        if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "influncer", bundle: nil)
            print("obj user id : ",arrVideo!.userID)
            let userID = UserDefaults.standard.string(forKey: "userID")
            let vc = storyMain.instantiateViewController(withIdentifier: "InfluncerProfileVC") as!  InfluncerProfileVC
            vc.isOtherUser = true
            vc.userID = arrVideo!.userID
            vc.hidesBottomBarWhenPushed = true
            rootViewController.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func comment(_ sender: AnyObject) {
        if (myUser?.count == 0) || self.myUser == nil{
            return
        }
        if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "influncer", bundle: nil)
            let vc = storyMain.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
            vc.Video_Id = self.arrVideo!.videoID
            vc.modalPresentationStyle = .overFullScreen
            rootViewController.navigationController?.present(vc, animated: true, completion: nil)
            
        }
        
    }
    
    // MARK:- NotificationCenter
    @objc func longGesture(tap:UILongPressGestureRecognizer){
        
        
        if (myUser?.count == 0) || self.myUser == nil{
            return
        }
        
       if self.myUser?[0].Id as! String == self.arrVideo?.userID as! String{
        
            print("Tap on own video")
        
        if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "influncer", bundle: nil)
            let vc = storyMain.instantiateViewController(withIdentifier: "PostPopUpViewController") as! PostPopUpViewController
            vc.videoMain =  self.arrVideo
            vc.removedelegate =  self.removedelegate
            vc.delegate =  self.delegate
            vc.videoID = self.arrVideo!.videoID
            vc.CurrentIndex = self.tag
            vc.type = "video"
            vc.removedelegate =  self.removedelegate
            rootViewController.navigationController?.present(vc, animated: true, completion: nil)
        }
        
        
       }else{
            
            if let rootViewController = UIApplication.topViewController() {
                let storyMain = UIStoryboard(name: "influncer", bundle: nil)
                let vc = storyMain.instantiateViewController(withIdentifier: "VideoPopUpViewController") as! VideoPopUpViewController
                vc.videoID =  self.arrVideo!.videoID
                vc.receiverID =  self.arrVideo!.userID
                vc.index =  self.tag
                vc.videoMainMVC =  self.arrVideo
                vc.delegate =  self.delegate
                vc.removeDelegate =  self.removedelegate
                if self.arrVideo!.followBtn == "follow"{
                    vc.isFollow =  false
                }else{
                    vc.isFollow =  true
                }
                
                rootViewController.navigationController?.present(vc, animated: true, completion: nil)
            }
        }
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        let notif = notification.object as! [String:Any]
        var obj = self.arrVideo
        var comentCount:Int =  notif["Count"] as! Int
        comentCount = comentCount + 1
        obj?.comment_count = "\(comentCount)"
        self.commentCountLbl.text = "\(comentCount)"
        delegate?.updateObj(obj: obj!, index:  notif["SelectedIndex"] as! Int, islike: false)
    }
    
    //MARK:- API Handler
    func followUser(rcvrID:String,userID:String){
        
        print("Recid: ",rcvrID)
        print("senderID: ",userID)
        
        
        ApiHandler.sharedInstance.followUser(sender_id: userID, receiver_id: rcvrID) { (isSuccess, response) in
            if isSuccess {
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    
                    if let resmsg =  response?.value(forKey: "msg") as? [String:Any] {
                        let receiver =  resmsg["receiver"] as! [String:Any]
                        let objUser =  receiver["User"] as! [String:Any]
                        
                        if objUser["button"] as! String == "follow"{
                            self.arrVideo?.followBtn = "follow"
                            self.btnFollow.isHidden = false
                            self.delegate?.updateObj(obj: self.arrVideo!, index: self.tag, islike: false)
                        }else{
                            self.arrVideo?.followBtn = "following"
                            self.btnFollow.isHidden = true
                            self.delegate?.updateObj(obj: self.arrVideo!, index: self.tag, islike: false)
                        }
                    }
                }else{
                    self.arrVideo?.followBtn = "follow"
                    self.btnFollow.isHidden = false
                    self.delegate?.updateObj(obj: self.arrVideo!, index: self.tag, islike: false)
                }
            }
        }
    }
}

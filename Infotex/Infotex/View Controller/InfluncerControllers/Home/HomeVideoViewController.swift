//
//  HomeVideoViewController.swift
//  MusicTok
//
//  Created by Mac on 05/08/2021.
//  Copyright © 2021 Mac. All rights reserved.
//

import UIKit
import ContentLoader


class HomeVideoViewController: UIViewController,videoLikeDelegate,removeVideoDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
   
    //MARK:- Outlets
    @IBOutlet weak var videoCollectionView: UICollectionView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnLiveUser: UIButton!
    
    let spinner = UIActivityIndicatorView(style: .gray)
    
    //MARK:- Variables
    var myUser:[User]? {didSet{}}
    var videosMainArr = [videoMainMVC]()
    var isFollowing = false
    var startPoint = 0
    var videoEmpty = false
    var isOtherController =  false             //Coming from other controller
    var currentIndex : IndexPath?             //Coming from other controller
    let deviceID = AppUtility?.getObject(forKey: strDeviceID)
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        
        return refreshControl
    }()
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("errInPlay"), object: nil)
        self.setupView()
    }
    
    // MARK:- viewWillDisappear
    
    override func viewWillDisappear(_ animated: Bool) {
        let visiblePaths = self.videoCollectionView.indexPathsForVisibleItems
        for i in visiblePaths  {
            let cell = videoCollectionView.cellForItem(at: i) as? HomeVideoCollectionViewCell
            cell?.pause()
            
        }
       
    }
    // MARK:- viewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        
        let visiblePaths = self.videoCollectionView.indexPathsForVisibleItems
        for i in visiblePaths  {
            let cell = videoCollectionView.cellForItem(at: i) as? HomeVideoCollectionViewCell
            cell?.play()
            
        }
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        videoCollectionView.isPagingEnabled = true
        videoCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    // MARK:- NotificationCenter
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        if let err = notification.userInfo?["err"] as? String {
        
        }
    }
    

    //MARK:- Deniet Notification
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("errInPlay"), object: nil)
    }
    
    
    //MARK:- SetupView

    func setupView(){
        self.myUser = User.readUserFromArchive()
        
        spinner.color = UIColor.white
        spinner.hidesWhenStopped = true
        videoCollectionView.refreshControl = refresher

        self.getDataForFeeds()

    }
    
    //MARK:- Refresher Controller
    
    @objc func requestData() {
        
        if isOtherController == false{
            print("requesting data")
            self.startPoint = 0
            self.getAllVideos(startPoint: "\(startPoint)")
            let deadline = DispatchTime.now() + .milliseconds(700)
            DispatchQueue.main.asyncAfter(deadline: deadline) {
                self.refresher.endRefreshing()
            }
        }
    }
    
    //MARK:- Button Action
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnLiveUsers(_ sender: Any) {
//        if self.myUser?[0].role == "influencer"{
//            let vc = storyboard?.instantiateViewController(withIdentifier: "StartBroadcastingViewController") as! StartBroadcastingViewController
//            vc.hidesBottomBarWhenPushed = true
//            navigationController?.pushViewController(vc, animated: true)
//
//        }else if self.myUser?[0].role == "user"{
//            let vc = storyboard?.instantiateViewController(withIdentifier: "LiveSecureVC") as! LiveSecureVC
//            vc.hidesBottomBarWhenPushed = true
//            navigationController?.pushViewController(vc, animated: true)
//        }
        
        AppUtility?.showToast(string: "Under Developement", view: self.view)
        
    }
    //MARK:- collectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  self.videosMainArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeVideoCollectionViewCell", for: indexPath) as! HomeVideoCollectionViewCell
        cell.configure(post: self.videosMainArr[indexPath.row])
        cell.tag =  indexPath.row
        cell.delegate = self
        cell.removedelegate = self
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width:collectionView.layer.bounds.width , height: collectionView.layer.bounds.height)
        
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? HomeVideoCollectionViewCell {
            cell.pause()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? HomeVideoCollectionViewCell {
            cell.play()
        }
       self.pagination(index: indexPath.row)
        
    }
    
    
  
    //MARK:- Delegate
    
    //use this when video like or comment
    func updateObj(obj: videoMainMVC, index: Int,islike:Bool) {
        self.videosMainArr.remove(at: index)
        self.videosMainArr.insert(obj, at: index)
        if islike{
            self.likeVideo(uid: (self.myUser?[0].Id)!, videoID: obj.videoID)
        }else{
            self.videoCollectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
        }
       
    }
    
    //use this when video hide or delete
    func updateVideoObj(index: Int,type:String) {
        if type == "video"{
            self.videosMainArr.remove(at: index)
            self.videoCollectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
        }
    }
    //MARK:- Pagination
    
    
    func pagination(index:Int){
        let vidObj = videosMainArr[index]
        self.WatchVideo(video_id: vidObj.videoID)
        print("index@row: ",index)
        print("videoID: \( vidObj.videoID), videoURL: \( vidObj.videoURL)")
        if index == videosMainArr.count-1{
            if self.videosMainArr.count == 0 {
                self.videoCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                
            }
        }
        if self.videosMainArr.count != 0{
            if index == videosMainArr.count - 4{
                print("Pagination start")
                self.startPoint+=1
                print("StartPoint: ",startPoint)
                if isOtherController ==  false{
                    if isFollowing == true{
                      self.getFollowingVideos(startPoint: "\(self.startPoint)")
                    }else{
                      self.getAllVideos(startPoint: "\(self.startPoint)")
                    }
                }
            }
        }
    }
    
    //MARK:- API handler
    
    func getAllVideos(startPoint:String){
        
        self.myUser = User.readUserFromArchive()
        print("deviceid: ",deviceID)
        var uid = ""
        self.myUser = User.readUserFromArchive()
        if (myUser?.count != 0) && self.myUser != nil{
            uid = (self.myUser?[0].Id)!
        }
        
        ApiHandler.sharedInstance.showRelatedVideos(device_id: deviceID ?? " ", user_id: uid, starting_point: startPoint) { (isSuccess, response) in
        
            if isSuccess {
                print(response)
                self.spinner.startAnimating()
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    if startPoint == "0"{
                        self.videosMainArr.removeAll()
                    }
                   self.videoResponse(startPoint:startPoint, resMsg: response as! [String : Any])
                }
            }else{
                print("response failed getAllVideos : ",response!)
                AppUtility!.showToast(string: response?.value(forKey: "msg") as? String ?? "msg", view:self.view)
            }
        }
    }
    func getFollowingVideos(startPoint:String){
                
        let startingPoint = startPoint
        
        var uid = ""
        self.myUser = User.readUserFromArchive()
        if (myUser?.count != 0) && self.myUser != nil{
            uid = (self.myUser?[0].Id)!
        }
        
        
        
        print("deviceid: ",deviceID)
        
        
        AppUtility!.showToast(string: "Loading...", view:self.view)
        
        ApiHandler.sharedInstance.showFollowingVideos(user_id: uid, device_id: deviceID!, starting_point: startPoint) { (isSuccess, response) in
            
            AppUtility!.showToast(string: "Loading...", view:self.view)
            if isSuccess {
                AppUtility!.showToast(string: "Loading...", view:self.view)
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    if startPoint == "0"{
                        self.videosMainArr.removeAll()
                    }
                    self.videoResponse(startPoint: startPoint, resMsg: response as! [String : Any])
                    
                }
            }else{
                print("response failed getFollowingVideos : ",response!)
                AppUtility!.showToast(string: "Removed From Favorite", view:self.view)
            }
        }
    }
    
    
    func WatchVideo(video_id:String){
    
        
        var uid = ""
        self.myUser = User.readUserFromArchive()
        if (myUser?.count != 0) && self.myUser != nil{
            uid = (self.myUser?[0].Id)!
        }
        
        
        print("deviceid: ",deviceID)
        ApiHandler.sharedInstance.watchVideo(device_id: deviceID!, user_id: uid,video_id:video_id) { (isSuccess, response) in
            if isSuccess {
                if response?.value(forKey: "code") as! NSNumber == 200 {
                  //  print(" WatchVideo response@200: ",response!)
                }
                else{
                    //print("WatchVideo response@201: ",response!)
                }
            }
        }
    }
    func likeVideo(uid:String,videoID:String){
        self.myUser = User.readUserFromArchive()
        
        ApiHandler.sharedInstance.likeVideo(user_id: (self.myUser?[0].Id)!, video_id: videoID,type:"") { (isSuccess, response) in
       
            if isSuccess{
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    //print("likeVideo response msg: ",response?.value(forKey: "msg"))
                }else{
                    //print("likeVideo response msg: ",response?.value(forKey: "msg"))
                }
            }
        }
    }

    
    //MARK:- API Response
    func videoResponse(startPoint:String,resMsg:[String:Any]){
        
        
        let resMsg = resMsg["msg"] as! [[String:Any]]
        for dic in resMsg{
            let videoDic = dic["Video"] as! NSDictionary
            let userDic = dic["User"] as! NSDictionary
            let soundDic = dic["Sound"] as! NSDictionary
            
            print("videoDic: ",videoDic)
            print("userDic: ",userDic)
            print("soundDic: ",soundDic)
            
            let videoURL = videoDic.value(forKey: "video") as? String
            let desc = videoDic.value(forKey: "description") as? String
            let allowComments = videoDic.value(forKey: "allow_comments")
            let videoUserID = videoDic.value(forKey: "user_id")
            let videoID = videoDic.value(forKey: "id") as! String
            let allowDuet = videoDic.value(forKey: "allow_duet")
            let duetVidID = videoDic.value(forKey: "duet_video_id")
            let videoThumb =  videoDic.value(forKey: "thum") as? String ?? ""
            //not strings
            let commentCount = videoDic.value(forKey: "comment_count")
            let likeCount = videoDic.value(forKey: "like_count")
            
            let userImgPath = userDic.value(forKey: "profile_pic") as? String
            let userName = userDic.value(forKey: "username") as? String
            let followBtn = userDic.value(forKey: "button") as? String
            let uid = userDic.value(forKey: "id") as? String
            let verified = userDic.value(forKey: "verified")
            
            let soundName = soundDic.value(forKey: "name")
            let cdPlayer = soundDic.value(forKey: "thum") as? String ?? ""
       
                let videoObj = videoMainMVC(videoID: videoID, videoUserID: "\(videoUserID!)", fb_id: "", description: desc ?? "", videoURL: videoURL ?? "", videoTHUM: videoThumb, videoGIF: "", view: "", section: "", sound_id: "", privacy_type: "", allow_comments: "\(allowComments!)", allow_duet: "\(allowDuet!)", block: "", duet_video_id: "", old_video_id: "", created: "", like: "", favourite: "", comment_count: "\(commentCount!)", like_count: "\(likeCount!)", followBtn: followBtn ?? "", duetVideoID: "\(duetVidID!)", userID: uid ?? "", first_name: "", last_name: "", gender: "", bio: "", website: "", dob: "", social_id: "", userEmail: "", userPhone: "", password: "", userProfile_pic: userImgPath  ?? "", role: "", username: userName  ?? "", social: "", device_token: "", videoCount: "", verified: "\(verified!)", soundName: "\(soundName!)", CDPlayer: cdPlayer)
                self.videosMainArr.append(videoObj)
         
        }
      
        self.videoCollectionView.reloadData()
    }
    //MARK:- function
    
    func getDataForFeeds(){
        if isOtherController == true{
            self.videoCollectionView.performBatchUpdates(nil, completion: {
                (result) in
                self.videoCollectionView.scrollToItem(at:self.currentIndex!, at: .bottom, animated: false)
            })
            self.btnLiveUser.isHidden =  true
            self.btnBack.isHidden =  false
        }else{
            self.getAllVideos(startPoint: "0")
            if self.videosMainArr.count != 0 {
                self.videoEmpty =  false
            }
            self.btnLiveUser.isHidden =  false
            self.btnBack.isHidden =  true
        }
    }
}

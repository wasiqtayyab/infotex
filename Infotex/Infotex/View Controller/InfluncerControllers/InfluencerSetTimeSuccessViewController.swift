//
//  InfluencerSetTimeSuccessViewController.swift
//  Infotex
//
//  Created by Mac on 13/10/2021.
//

import UIKit

protocol dismissController:class {
    func dismiss()
}

class InfluencerSetTimeSuccessViewController: UIViewController {

    var dismissDelegate:dismissController!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- viewDidDisappear
    override func viewDidDisappear(_ animated: Bool) {
        dismissDelegate.dismiss()
    }
    //MARK:- Button Action
    @IBAction func btnCloseAction(_ sender: Any) {
       self.dismiss(animated: true, completion: nil)
    }
   
}

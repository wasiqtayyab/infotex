//
//  SignUpInfluencerViewController.swift
//  Infotex
//
//  Created by Mac on 03/09/2021.
//

import UIKit
import MercariQRScanner
class SignUpInfluencerViewController: UIViewController, UITextFieldDelegate{
    
    //MARK:- OUTLET
    
    @IBOutlet weak var btnAffiliatedCode: UIButton!
    @IBOutlet weak var tfCode: UITextField!
    @IBOutlet weak var btnScanner: UIButton!
    @IBOutlet weak var lblInstruction: UILabel!
    
    //MARK:- VARS
    var myUser: [User]? {didSet {}}
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.readArchive()
        self.btnAffiliatedCode.applyGradient(colors: [#colorLiteral(red: 0.2274509804, green: 0.4941176471, blue: 0.7294117647, alpha: 1), #colorLiteral(red: 0.7803921569, green: 0.4274509804, blue: 0.6588235294, alpha: 1)])
        lblInstruction.text = "If you are an Influencer with an agency,\n please provide the Affliated Code from your agency."
    }
    
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        textFieldSetup()
    }
    
    //MARK:- READ ARCHIVE
    
    private func readArchive(){
        self.myUser = User.readUserFromArchive()
        user_id = (self.myUser?[0].Id)!
        
    }
    
   
    //MARK:- TEXTFIELD PLACEHOLDER
    
    private func textFieldSetup(){
        tfCode.delegate = self
        tfCode.attributedPlaceholder = NSAttributedString(string: "8999", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        tfCode.text = affiliated_code
    }
    
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func noAgencyButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "InluencerFeeViewController")as! InluencerFeeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func confirmAffiliatedCodeButtonPressed(_ sender: UIButton) {
        
        if tfCode.text! == ""{
            AppUtility?.showToast(string: "Enter your Affiliated Code", view: self.view)
            return
        }
        
        self.verifyReferalCodeApi()
       
        
    }
    
    @IBAction func scannerButtonPressed(_ sender: UIButton) {
       
        let vc = storyboard?.instantiateViewController(withIdentifier: "BarCodeScannerViewController")as! BarCodeScannerViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK:- VERIFY REFERAL CODE API
    
    private func verifyReferalCodeApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.verifyReferralCode(user_id: user_id, referral_code: tfCode.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let userObj = resp!["msg"] as! [String:Any]
                    UserObject.shared.Objresponse(response: userObj, isLogin: true)
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomTabBarController")as! CustomTabBarController
                    self.navigationController?.pushViewController(vc, animated: true)
                 
                }else{
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
}


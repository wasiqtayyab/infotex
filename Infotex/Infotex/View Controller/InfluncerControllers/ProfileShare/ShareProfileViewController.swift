//
//  ShareProfileViewController.swift
//  Infotex
//
//  Created by Mac on 31/05/2021.
//

import UIKit
import SDWebImage


class ShareProfileViewController: UIViewController{
 
    //MARK:- Outlets
    
    @IBOutlet var userImage: UIImageView!
    @IBOutlet weak var viewImageBorder: UIView!
    @IBOutlet weak var viewBackImage: UIView!
    
    var userData :userMVC!
    
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewBackImage.layer.cornerRadius = self.viewBackImage.frame.size.height / 2
        self.viewImageBorder.layer.cornerRadius = self.viewImageBorder.frame.size.height / 2
        self.userImage.layer.cornerRadius = self.userImage.frame.size.height / 2
        
        self.viewImageBorder.layer.borderWidth = 1.8
        self.viewImageBorder.layer.borderColor = UIColor.white.cgColor
        
        
        let user = userData
        let profilePic = AppUtility?.detectURL(ipString: user!.userProfile_pic)
        userImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        userImage.sd_setImage(with: URL(string:profilePic!), placeholderImage: UIImage(named: "noUserImg"))
    }
    
    
    //MARK:- Button actions
    @IBAction func btnEditProfileAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "influncer", bundle: nil)
        let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "editProfileViewController") as! editProfileViewController
        let presentingVC = self.presentingViewController
        self.dismiss(animated: false, completion: { () -> Void   in
            destinationController.userData =  self.userData
            destinationController.hidesBottomBarWhenPushed = true
            destinationController.modalPresentationStyle = .fullScreen
            presentingVC!.present(destinationController, animated: true, completion: nil)
        })
    }
    @IBAction func cancelPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

}

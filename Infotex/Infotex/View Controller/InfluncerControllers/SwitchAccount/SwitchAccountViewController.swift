//
//  SwitchAccountViewController.swift
//  Infotex
//
//  Created by Mac on 14/09/2021.
//

import UIKit
import SDWebImage

class SwitchAccountViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var accountTableView: UITableView!
    @IBOutlet weak var heightTblView: NSLayoutConstraint!
    var mySwitchAccount:[switchAccount]?{didSet{}}
    var myUser:[User]?{didSet{}}

    
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        accountTableView.delegate = self
        accountTableView.dataSource = self

        self.myUser = User.readUserFromArchive()
        self.mySwitchAccount = switchAccount.readswitchAccountFromArchive()

        if myUser?.count != 0 && self.myUser != nil{
            for var i in 0..<self.mySwitchAccount!.count{
                var obj = self.mySwitchAccount![i]
                if obj.Id == self.myUser?[0].Id{
                    obj.isSelected = "1"
                    self.mySwitchAccount?.remove(at: i)
                    self.mySwitchAccount?.insert(obj, at: i)
                    self.accountTableView.reloadData()
                }
            }
            self.heightTblView.constant =  CGFloat(self.mySwitchAccount!.count * 80)
        }
    }
    
    
    //MARK:- Button Actions
    
    @IBAction func btnCancel(_ sender: UIButton){
        self.dismiss(animated:true , completion: nil)
     }
    @IBAction func btnAddAccountAction(_ sender: Any) {
        
        if let rootViewController = UIApplication.topViewController() {
            let story:UIStoryboard = UIStoryboard(name: "Login", bundle:nil)
            let vc = story.instantiateViewController(withIdentifier: "InfotexViewController") as! InfotexViewController
            let nav = UINavigationController(rootViewController: vc)
            nav.navigationBar.isHidden = true
            self.view.window?.rootViewController = nav
        }
    }
}
//MARK:- tableView

extension SwitchAccountViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("SwitchAccount.count",self.mySwitchAccount?.count)
        return self.mySwitchAccount!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchAccountTableViewCell", for: indexPath)as! SwitchAccountTableViewCell
        
        
        let userImgUrl = URL(string: BASE_URL + (self.mySwitchAccount![indexPath.row].image ?? "") ?? "noUserNew")
        cell.profileImage.sd_setImage(with: userImgUrl, placeholderImage: UIImage(named: "noUserNew"))
        
        cell.lblUsername.text =  self.mySwitchAccount![indexPath.row].username
        cell.lblName.text =  self.mySwitchAccount![indexPath.row].first_name! + " " + self.mySwitchAccount![indexPath.row].last_name!
         
        if self.mySwitchAccount![indexPath.row].isSelected == "1"{
            cell.tintColor = UIColor(named: "HopBush")
            cell.accessoryType = UITableViewCell.AccessoryType.checkmark
        }else{
            cell.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
            cell.accessoryType = .none
        }
      
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let account = self.mySwitchAccount![indexPath.row]
        myUser?.removeAll()
        let user = User()
        user.Id           =  account.Id
        user.first_name   =  account.first_name
        user.last_name    =  account.last_name
        user.email        =  account.email
        user.phone        =  account.phone
        user.image        =  account.image
        user.role         =  account.role
        user.device_token =  account.device_token
        user.token        =  account.token
        user.active       =  account.active
        user.country_id   =  account.country_id
        user.dob          =  account.dob
        user.gender       =  account.gender
        user.lat          =  account.lat
        user.long         =  account.long
        user.online       =  account.online
        user.password     =  account.password
        user.auth_token   =  account.auth_token
        user.created      =  account.created
        user.device       =  account.device
        user.ip           =  account.ip
        user.phone        =  account.phone
        user.social       =  account.social
        user.social_id    =  account.social_id
        user.username     =  account.username
        user.version      =  account.version
        user.wallet       =  account.wallet
        
        
        self.myUser = [user]
        if User.saveUserToArchive(user: self.myUser!) {
            print("User Saved in Directory")
        
            let story = UIStoryboard(name: "Login", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "SplashScreenViewController") as! SplashScreenViewController
            let nav = UINavigationController(rootViewController: vc)
            nav.navigationBar.isHidden = true
            self.view.window?.rootViewController = nav
            self.dismiss(animated: true, completion: nil)
        }
        print(account.username)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

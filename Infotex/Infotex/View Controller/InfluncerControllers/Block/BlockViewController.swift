//
//  BlockViewController.swift
//  Infotex New Screen
//
//  Created by Wasiq Tayyab on 12/10/2021.
//

import UIKit

class BlockViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var blockTableView: UITableView!
    
    //MARK:- VARS
    
    var arrBlock = ["berlin","data1"]
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(arrBlock.count)
        
        if arrBlock.count == 0 {
            noDataView.isHidden = false
            blockTableView.isHidden = true
        }else {
            noDataView.isHidden = true
            blockTableView.isHidden = false
            blockTableView.delegate = self
            blockTableView.dataSource = self
            blockTableView.tableFooterView = UIView()
            
        }
        
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    


}

extension BlockViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrBlock.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BlockTableViewCell", for: indexPath)as! BlockTableViewCell
        cell.lblName.text = arrBlock[indexPath.row] as! String
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
    
    
    
}

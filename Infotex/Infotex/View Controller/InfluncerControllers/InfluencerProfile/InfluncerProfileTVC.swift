//
//  InfluncerProfileTVC.swift
//  infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit

class InfluncerProfileTVC: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
   
    //MARK:- Outlets
    
    @IBOutlet weak var contCollHeight: NSLayoutConstraint!
    @IBOutlet weak var cvLikedVideos: UICollectionView!
    var currentindex = 0
    lazy var bookingInfluncer:[BookingInfluencer] = []
    
    
    //MARK:- awakeFromNib
    
    override func awakeFromNib() {
        super.awakeFromNib()
        isBooked =  false
        print("isOtherController:\(AppUtility?.getObject(forKey: "isOtherUser") as! String) with userID: \(AppUtility?.getObject(forKey: "OtherUserID") as! String)")
        
        contCollHeight.constant = (self.cvLikedVideos.frame.size.width / 3) * 5
        let sdf = (self.cvLikedVideos.frame.size.width / 3) * 5
        self.cvLikedVideos.delegate = self
        self.cvLikedVideos.dataSource = self
   
        self.cvLikedVideos.register(UINib(nibName: "ChildOne", bundle: nil), forCellWithReuseIdentifier: "ChildOne")  //Images
        self.cvLikedVideos.register(UINib(nibName: "ChildTwo", bundle: nil), forCellWithReuseIdentifier: "ChildTwo")  //Video 24 sec
        self.cvLikedVideos.register(UINib(nibName: "ChildThree", bundle: nil), forCellWithReuseIdentifier: "ChildThree")  //Video more 24 Sec
        
        self.cvLikedVideos.register(UINib(nibName: "BookingByUserCVC", bundle: nil), forCellWithReuseIdentifier: "BookingByUserCVC")
        self.cvLikedVideos.register(UINib(nibName: "BookedForUserCVC", bundle: nil), forCellWithReuseIdentifier: "BookedForUserCVC")
        self.cvLikedVideos.register(UINib(nibName: "BookedForInfluncerCVC", bundle: nil), forCellWithReuseIdentifier: "BookedForInfluncerCVC")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.scrollToIndex(notification:)), name: Notification.Name("updateScrollOnInfluncer"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadCollectionView(notification:)), name: Notification.Name("reloadCollectionView"), object: nil)  // Booking screen: Profile
    }

    //MARK:- deinit
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("updateScrollOnInfluncer"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("reloadCollectionView"), object: nil)
    }
 
    @objc func scrollToIndex(notification: Notification) {
      
        let index = notification.object as! IndexPath
        print(index)
        self.cvLikedVideos.isPagingEnabled =  false
        self.cvLikedVideos.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
        self.cvLikedVideos.isPagingEnabled =  true

      }
    
    @objc func reloadCollectionView(notification: Notification) {
        self.bookingInfluncer.removeAll()
        if  let obj  =  notification.object as? [BookingInfluencer]{
            self.bookingInfluncer.append(contentsOf: obj)
        }
        self.cvLikedVideos.reloadData()

      }
    // MARK:-  CollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let size = contentView.frame.width / 3
        return CGSize(width: contentView.frame.width , height: size * 4)
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row  == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChildOne", for: indexPath) as! ChildOne
            return cell
        }else if indexPath.row == 1{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChildTwo", for: indexPath) as! ChildTwo
            return cell
        }else if indexPath.row == 2{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChildThree", for: indexPath) as! ChildThree
            return cell
        }else{
            if AppUtility?.getObject(forKey: "isOtherUser") as! String == "true"{
                if isBooked == true{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookedForUserCVC", for: indexPath) as! BookedForUserCVC
                    cell.setup(arr: self.bookingInfluncer)
                    return cell
                }else{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookingByUserCVC", for: indexPath) as! BookingByUserCVC
                    return cell
                }

            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookedForInfluncerCVC", for: indexPath) as! BookedForInfluncerCVC
                return cell
            }
            
        }
       
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        currentindex = Int(scrollView.contentOffset.x / cvLikedVideos.frame.size.width)
        let aNum = currentindex
        NotificationCenter.default.post(name: Notification.Name("updateHeaderIconOnInfluncer"), object: aNum)
        print(aNum)
    }
}


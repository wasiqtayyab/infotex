//
//  InfluncerProfileVC.swift
//  infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit

class InfluncerProfileVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var tblInfluncerProfile: UITableView!
    @IBOutlet weak var btnUserProfile: UIButton!
    @IBOutlet weak var btnMenuAction: UIButton!
    @IBOutlet weak var lblNFT: UILabel!
    @IBOutlet weak var btnNFTLogo: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    var userData = [userMVC]()
    lazy var userID = ""
    var myUser:[User]? {didSet{}}
    var isOtherUser =  false
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        
        return refreshControl
    }()
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myUser = User.readUserFromArchive()
        
        self.tblInfluncerProfile.register(UINib(nibName: "InfluncerTopTVC", bundle: nil), forCellReuseIdentifier: "InfluncerTopTVC")
        
        self.tblInfluncerProfile.register(UINib(nibName: "InfluncerProfileHV", bundle: nil), forHeaderFooterViewReuseIdentifier: "InfluncerProfileHV")
        
        self.tblInfluncerProfile.register(UINib(nibName: "InfluncerProfileTVC", bundle: nil), forCellReuseIdentifier: "InfluncerProfileTVC")
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reLoadTbl(notification:)), name: Notification.Name("reloadTableViewInfluncer"), object: nil)
        
        
        self.myUser = User.readUserFromArchive()
      
        if isOtherUser == true{
            self.btnBack.isHidden =  false
            self.lblNFT.isHidden =  true
            self.btnNFTLogo.isHidden = true
            self.btnMenuAction.isHidden = true
            self.userDetail(userID:(self.myUser?[0].Id)!, OtherUserID:self.userID)
            AppUtility?.saveObject(obj:self.userID , forKey: "OtherUserID")
            AppUtility?.saveObject(obj: "true" , forKey: "isOtherUser")
        }else{
            IsSubscribe_Package =  1
            self.btnBack.isHidden =  true
            self.lblNFT.isHidden =  false
            self.btnNFTLogo.isHidden = false
            self.btnMenuAction.isHidden = false
            
            if (myUser?.count != 0) && self.myUser != nil{
                self.userDetail(userID:(self.myUser?[0].Id)!, OtherUserID: "")
                AppUtility?.saveObject(obj:"" , forKey: "OtherUserID")
                AppUtility?.saveObject(obj: "false" , forKey: "isOtherUser")
            }else{
                let story:UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                let navController = UINavigationController.init(rootViewController: story.instantiateViewController(withIdentifier: "ProfileViewController"))
                navController.navigationBar.isHidden = true
                navController.modalPresentationStyle = .overFullScreen
                self.present(navController, animated: true, completion: nil)
            }
        }
        self.tblInfluncerProfile.refreshControl =  refresher

    }
    
    
    //MARK:- ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
       
    }
    //MARK:- Refresher Controller
    
    @objc func requestData() {

        if isOtherUser == true{
            self.btnBack.isHidden =  false
            self.lblNFT.isHidden =  true
            self.btnNFTLogo.isHidden = true
            self.btnMenuAction.isHidden = true
            self.userDetail(userID:(self.myUser?[0].Id)!, OtherUserID:self.userID)
            
            AppUtility?.saveObject(obj:self.userID , forKey: "OtherUserID")
            AppUtility?.saveObject(obj: "true" , forKey: "isOtherUser")
        }else{
            
            self.btnBack.isHidden =  true
            self.lblNFT.isHidden =  false
            self.btnNFTLogo.isHidden = false
            self.btnMenuAction.isHidden = false
            self.userDetail(userID:(self.myUser?[0].Id)!, OtherUserID: "")
            AppUtility?.saveObject(obj:"" , forKey: "OtherUserID")
            AppUtility?.saveObject(obj: "false" , forKey: "isOtherUser")
           
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAPI"), object: nil)
        
        let deadline = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.refresher.endRefreshing()
        }
        
    }
    //MARK:- Deinit notifications
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("reloadTableViewInfluncer"), object: nil)
    }
    

    //MARK:- Delegate functions
    
    @objc func reLoadTbl(notification: Notification) {
        let index = notification.object as! Int
        let indexPath = IndexPath(row: index, section: 1)
      }
    
    
    //MARK:- Button Actions
    
    @IBAction func menuPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "InfluencerSettingViewController")as! InfluencerSettingViewController
        vc.userData =  self.userData
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func userPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SwitchAccountViewController") as! SwitchAccountViewController
        present(vc, animated: true, completion: nil)
    }
 
    @IBAction func NftPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- API Handler
    
    func userDetail(userID:String,OtherUserID:String){
        self.loader.isHidden =  false
        ApiHandler.sharedInstance.showUserDetail(user_id: userID,other_user_id:OtherUserID) { (isSuccess, response) in
            self.loader.isHidden =  true
            if isSuccess{
                
                if let res =  response?.value(forKey: "msg") as? [String:Any]{
                 let objData = UserDetail.shared.ObjUserDetailResponse(res:res)
                    self.tblInfluncerProfile.dataSource = self
                    self.tblInfluncerProfile.delegate =  self
                    self.userData.append(objData)
                    self.btnUserProfile.setTitle("@\(self.userData[0].username)", for: .normal)
                    if OtherUserID == ""{
                        IsSubscribe_Package =  1
                    }else{
                        print("ISsubscribe_package",self.myUser?[0].subscribe_package)
                        print("Role User",self.myUser?[0].role)
                        if self.myUser?[0].role == "user"{
                            if self.myUser?[0].subscribe_package == "1" || self.userData[0].subscribe == 1{
                                IsSubscribe_Package =  1

                            }else{
                                IsSubscribe_Package =  0
                            }
                        }else{
                            IsSubscribe_Package =  1
                        }
                        AppUtility?.saveObject(obj: self.userData[0].booking_coins_per_hour as! String , forKey: "coins")
                    }
                    self.tblInfluncerProfile.reloadData()
                }
            }
        }
    }
}

//MARK:- Extension
extension InfluncerProfileVC: UITableViewDelegate, UITableViewDataSource {
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            
            let  cell = tableView.dequeueReusableCell(withIdentifier: "InfluncerTopTVC", for: indexPath) as! InfluncerTopTVC
            cell.Setup(userData: self.userData[indexPath.row],isOtherUser: self.isOtherUser )
            return cell
            
        }else{
            
            let  cell = tableView.dequeueReusableCell(withIdentifier: "InfluncerProfileTVC", for: indexPath) as! InfluncerProfileTVC
              return cell
        }
     
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "InfluncerProfileHV") as! InfluncerProfileHV
        return header
    }
}


//
//  PostImagesViewController.swift
//  Infotex
//
//  Created by Mac on 07/09/2021.
//

import UIKit
import Alamofire
import AVFoundation
import Photos

class PostImagesViewController: UIViewController,UITextViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    
    
    
    //MARK:- Outlets
    @IBOutlet weak var imageColllectionView: UICollectionView!
    @IBOutlet weak var vidThumbnail: UIImageView!
    @IBOutlet weak var describeTextView: AttrTextView!
    @IBOutlet weak var lblTextCount: UILabel!
    
    //MARK:- Variables
    
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    var myUser:[User]? {didSet{}}
    
    var isEditPost = false
    var postDetail : profileDetailVideo!
    
    var uploadImage = [UIImage]()
    var desc = ""
    var allowComments = "true"
    
    
    var boxView = UIView()
    var blurView = UIView()
    
    var hashTagsArr = [String]()
    var userTagsArr = [String]()
    
    
    
    //MARK:- viewDidLoad
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myUser = User.readUserFromArchive()
        self.describeTextView.layer.cornerRadius =  5
        describeTextView.text = "write a Caption"
        describeTextView.textColor = UIColor.lightGray
        describeTextView.delegate =  self
        if isEditPost == true{
            self.describeTextView.text =  self.postDetail.description
            for obj in self.postDetail.videoImage{
                let imgURL = URL(string: obj.image)
                let imageData = try! Data(contentsOf: imgURL!)
                let image = UIImage(data: imageData)
                self.uploadImage.append(image!)
            }
            
        }
        
    }
    //MARK:- TextView
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
            textView.text = .none
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "write a Caption"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        self.lblTextCount.text = "\(self.describeTextView.text!.count)/150"
        
        describeTextView.setText(text: describeTextView.text,textColor: .black, withHashtagColor: #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1), andMentionColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), andCallBack: { (strng, type) in
            print("type: ",type)
            print("strng: ",strng)
            
            
        }, normalFont: .systemFont(ofSize: 14, weight: UIFont.Weight.light), hashTagFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold), mentionFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold))
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let rangeOfTextToReplace = Range(range, in: textView.text) else {
            return false
        }
        let substringToReplace = textView.text[rangeOfTextToReplace]
        let count = textView.text.count - substringToReplace.count + text.count
        return count <= 150
    }
    
    //MARK:- API Handler
    
    func uploadData(){
        self.loader.isHidden =  false
        let hashtags = describeTextView.text.hashtags()
        let mentions = describeTextView.text.mentions()
        
        var newHashtags = [[String:String]]()
        var newMentions = [[String:String]]()
        
        for hash in hashtags{
            newHashtags.append(["name":hash])
        }
        for mention in mentions{
            newMentions.append(["name":mention])
        }
    
        let cmnt = self.allowComments
        
        var des = self.desc
        if describeTextView.text != "write a Caption" {
            des = describeTextView.text
        }else{
            des = ""
        }
        
        print("cmnt",cmnt)
        
        
        print("des",des)
        print("hashtags",hashtags)
        print("mentions",mentions)
        
        let parameter :[String:Any] = ["user_id"       : (self.myUser?[0].Id)!,
                                       "privacy_type"  : "public",
                                       "description"   : des,
                                       "allow_comments": cmnt,
                                       "users_json"    : newMentions,
                                       "hashtags_json" : newHashtags,
                                       
        ]
        
        print(parameter)
        let url : String = ApiHandler.sharedInstance.baseApiPath+"postImage"
        AF.upload(multipartFormData: { MultipartFormData in
            
            if (!JSONSerialization.isValidJSONObject(parameter)) {
                print("is not a valid json object")
                return
            }
            for var i in 0..<self.uploadImage.count{
                let img = self.uploadImage[i]
                let imgData : Data = img.jpegData(compressionQuality: 0.5)!
                let currentTime = AppUtility!.getCurrentMillis()
                MultipartFormData.append(imgData , withName: "image\(i)", fileName: "\(currentTime)swift_file.jpg", mimeType: "image/png")
                for key in parameter.keys{
                    let name = String(key)
                    print("key",name)
                    if let val = parameter[name] as? String{
                        MultipartFormData.append(val.data(using: .utf8)!, withName: name)
                    }
                }
            }

            
        }, to: url, method: .post, headers: headers)
        .responseJSON { (response) in
            self.loader.isHidden = true
            switch response.result{
            
            case .success(let value):
                print("progress: ",Progress.current())
                let json = value
                let dic = json as! NSDictionary
                
                print("response:- ",response)
                if(dic["code"] as! NSNumber == 200){
                    print("200")
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                    
                }else{
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                    print(dic)
                    
                }
            case .failure(let error):
                print("\n\n===========Error===========")
                print("Error Code: \(error._code)")
                print("Error Messsage: \(error.localizedDescription)")
                if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                    print("Server Error: " + str)
                }
                AppUtility?.displayAlert(title: "Error", messageText: error.localizedDescription, delegate: self)
                print("===========================\n\n")
                
            }
        }
    }
    func editPost(){
        self.loader.isHidden =  false
        let hashtags = describeTextView.text.hashtags()
        let mentions = describeTextView.text.mentions()
        
        var newHashtags = [[String:String]]()
        var newMentions = [[String:String]]()
        
        for hash in hashtags{
            newHashtags.append(["name":hash])
        }
        for mention in mentions{
            newMentions.append(["name":mention])
        }
        
        let cmnt = self.allowComments
     
        var des = self.desc
        if describeTextView.text != "write a Caption" {
            des = describeTextView.text
        }else{
            des = ""
        }
        
        print("cmnt",cmnt)


        print("des",des)
        print("hashtags",hashtags)
        print("mentions",mentions)
        let parameter :[String:Any] =
            
            [
               "user_id"  : (self.myUser?[0].Id)!,
               "privacy_type"  : "public",
               "description"   : des,
               "allow_comments": cmnt,
               "users_json"    : newMentions,
               "hashtags_json" : newHashtags,
               "video_id"      : self.postDetail.id,
          ]
        
        print(parameter)
        let url : String = ApiHandler.sharedInstance.baseApiPath+"editVideo"
        AF.upload(multipartFormData: { MultipartFormData in
            
            if (!JSONSerialization.isValidJSONObject(parameter)) {
                print("is not a valid json object")
                return
            }
            for key in parameter.keys{
                let name = String(key)
                print("key",name)
                if let val = parameter[name] as? String{
                    MultipartFormData.append(val.data(using: .utf8)!, withName: name)
                }
            }
            
        }, to: url, method: .post, headers: headers)
        .responseJSON { (response) in
            self.loader.isHidden = true
            switch response.result{
          
            case .success(let value):
                print("progress: ",Progress.current())
                let json = value
                let dic = json as! NSDictionary
                
                print("response:- ",response)
                if(dic["code"] as! NSNumber == 200){
                    print("200")
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                    
                }else{
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                    
                    print(dic)
                    
                }
            case .failure(let error):
                print("\n\n===========Error===========")
                print("Error Code: \(error._code)")
                print("Error Messsage: \(error.localizedDescription)")
                if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                    print("Server Error: " + str)
                }
                AppUtility?.displayAlert(title: "Error", messageText: error.localizedDescription, delegate: self)
                print("===========================\n\n")
                
            }
        }
    }
    
    //MARK:- Button Actions
    
    @IBAction func btnPost(_ sender: Any) {
        if isEditPost == false{
            self.uploadData()
        }else{
            self.editPost()
        }
    }
    @IBAction func commentSwitch(_ sender: UISwitch) {
        if sender.isOn {
            self.allowComments = "true"
        }else{
            self.allowComments = "false"
        }
    }
    
    @IBAction func btnHashtag(_ sender: UISwitch) {
        
        guard self.describeTextView.text != "write a Caption" else {return}
        
        self.describeTextView.setText(text: describeTextView.text+" #",textColor: .black, withHashtagColor: #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1), andMentionColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), andCallBack: { (strng, type) in
            print("type: ",type)
            print("strng: ",strng)
        }, normalFont: .systemFont(ofSize: 14, weight: UIFont.Weight.light), hashTagFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold), mentionFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold))
        
    }
    
    @IBAction func btnMention(_ sender: UISwitch) {
        
        guard self.describeTextView.text != "write a Caption" else {return}
        
        self.describeTextView.setText(text: describeTextView.text+" @",textColor: .black, withHashtagColor: #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1), andMentionColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), andCallBack: { (strng, type) in
            print("type: ",type)
            print("strng: ",strng)
        }, normalFont: .systemFont(ofSize: 14, weight: UIFont.Weight.light), hashTagFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold), mentionFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold))
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- Collection View
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.uploadImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaPickerCollectionViewCell", for: indexPath) as! MediaPickerCollectionViewCell
        cell.image.image = self.uploadImage[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.uploadImage.count == 1 {
            return CGSize(width: self.view.frame.width, height: 216)
        }else{
            let yourWidth = self.view.frame.width/3.0
            let yourHeight = yourWidth
            
            return CGSize(width: yourWidth, height: yourHeight)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //MARK:- Functions
    
    func showShimmer(progress: String){
        
        boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 70, width: 180, height: 50))
        boxView.backgroundColor = UIColor.white
        boxView.alpha = 0.8
        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        let activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.gray
        textLabel.text = progress
        
        blurView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.3)
        let blurView = UIView(frame: UIScreen.main.bounds)
        
        
        boxView.addSubview(blurView)
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        
        view.addSubview(boxView)
    }
    
    func HideShimmer(){
        boxView.removeFromSuperview()
    }
    func dictToJSON(dict:[String: AnyObject]) -> AnyObject {
        let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        return jsonData as AnyObject
    }
    
}

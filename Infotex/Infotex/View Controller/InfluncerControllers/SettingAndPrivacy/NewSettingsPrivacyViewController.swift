//
//  NewSettingsPrivacyViewController.swift
//  MusicTok
//
//  Created by Mac on 29/05/2021.
//  Copyright © 2021 Mac. All rights reserved.
//

import UIKit

class NewSettingsPrivacyViewController: UIViewController {
    @IBOutlet weak var tblPrivacy: UITableView!
    
    var arrAccount = [["name":"Manage Account","image":"icons8-user"],
                      ["name":"Privacy","image":"lock_white"],
                      ["name":"Request Verification","image":"RequestVerification"],
                      ["name":"Draft","image":"email"],
                      ["name":"Payout Setting","image":"icons8-dollar_sign_7"],
                      ["name":"Topup","image":"icon____1234566"]]
    
    
    var arrContentActivity = [["name":"Push Notification","image":"icons8-appointment_reminders"],
                    ["name":"App Lanuage","image":"icons8-language"]]
    
    
   /* var arrCacheData = [["name":"Free Up Space","image":"ic_free_space"]]*/
    
    var arrAbout = [["name":"Terms of Service","image":"icons8-book"],
                    ["name":"Privacy Policy","image":"icons8-file"]]
    
    var arrLogin = [["name":"Switch Account","image":"SwitchAccount"],
                    ["name":"Log Out","image":"icons8-shutdown"]]
    
    
    var userData = [userMVC]()
    
    var mySwitchAccount: [switchAccount]? {didSet {}}
    var myUser: [User]? {didSet {}}

    override func viewDidLoad() {
        super.viewDidLoad()
        self.myUser = User.readUserFromArchive()
        self.mySwitchAccount = switchAccount.readswitchAccountFromArchive()

        tblPrivacy.delegate = self
        tblPrivacy.dataSource = self

    }
    
    @IBAction func backPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension NewSettingsPrivacyViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrAccount.count
        }else if section == 1 {
            return arrContentActivity.count
        }/*else if section == 2 {
            return arrCacheData.count
        }*/else if section == 2 {
            return arrAbout.count
        }else{
            return arrLogin.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "privacyAndSettingTableViewCell", for: indexPath) as! privacyAndSettingTableViewCell
        if indexPath.section == 0 {
            cell.imgIcon.image = UIImage(named: "\(arrAccount[indexPath.row]["image"] ?? "30")")
            cell.lblName.text = arrAccount[indexPath.row]["name"]
            cell.lblLanguageTitle.isHidden = true
            cell.nextArr0w.isHidden =  false
        }else if indexPath.section == 1{
            cell.imgIcon.image = UIImage(named: "\(arrContentActivity[indexPath.row]["image"] ?? "30")")
            cell.lblName.text = arrContentActivity[indexPath.row]["name"]
            
            if indexPath.row == 1 {
                cell.lblLanguageTitle.isHidden = false
                cell.nextArr0w.isHidden =  true
            }
            if indexPath.row == 0{
                cell.lblLanguageTitle.isHidden = true
                cell.nextArr0w.isHidden =  false

            }

        }/*else if indexPath.section == 2{
            cell.imgIcon.image = UIImage(named: "\(arrCacheData[indexPath.row]["image"] ?? "30")")
            cell.lblName.text = arrCacheData[indexPath.row]["name"]
            cell.lblLanguageTitle.isHidden = true
            cell.nextArr0w.isHidden =  false

        }*/else if indexPath.section == 2{
            cell.imgIcon.image = UIImage(named: "\(arrAbout[indexPath.row]["image"] ?? "30")")
            cell.lblName.text = arrAbout[indexPath.row]["name"]
            cell.lblLanguageTitle.isHidden = true
            cell.nextArr0w.isHidden =  false

        }else{
            cell.imgIcon.image = UIImage(named: "\(arrLogin[indexPath.row]["image"] ?? "30")")
            cell.lblName.text = arrLogin[indexPath.row]["name"]
            cell.lblLanguageTitle.isHidden = true
            cell.nextArr0w.isHidden =  false

        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if  section == 0 {
            return 35
        }else {
            return 35
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width  , height: 35))
        if #available(iOS 13.0, *) {
            headerView.backgroundColor = UIColor(named: "Shaft")
        } else {
            headerView.backgroundColor = UIColor(named: "Shaft")
        }
        let label = UILabel()
        label.frame = CGRect.init(x: 20, y: 0, width: headerView.frame.width-10, height: headerView.frame.height)
        
        label.font =  UIFont.systemFont(ofSize: 13)
        if #available(iOS 13.0, *) {
            label.textColor = .white
        } else {
            label.textColor = .white
        }
        
        headerView.addSubview(label)
        
        
        if section == 0 {
            label.text = "ACCOUNT"
            return headerView
        }else if section == 1{
            label.text = "CONTENT & ACTIVITY"
            return headerView
        }/*else if section == 2{
            label.text = "CACHE & CELLULAR DATA"
            return headerView
        }*/else if section == 2{
            label.text = "ABOUT"
            return headerView
        }else{
            label.text = "LOGIN"
            return headerView
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let FooterView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width  , height: 1))
        if #available(iOS 13.0, *) {
            FooterView.backgroundColor = UIColor(named: "Shaft")
        } else {
            FooterView.backgroundColor = UIColor(named: "Shaft")
        }
        return FooterView
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                print("Manage Account")
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManageAccountViewController") as! ManageAccountViewController
                vc.hidesBottomBarWhenPushed = true
                vc.userData = self.userData
                self.navigationController?.pushViewController(vc, animated: true)
            case 1:
                print("Privacy")
                let vc = storyboard?.instantiateViewController(withIdentifier: "privacySettingViewController") as! privacySettingViewController
                self.navigationController?.pushViewController(vc, animated: true)
            case 2:
                print("Request Verification")
                let vc = storyboard?.instantiateViewController(withIdentifier: "requestVerificationViewController") as! requestVerificationViewController
                self.navigationController?.pushViewController(vc, animated: true)
            case 3:
                print("Draft")
            case 4:
                
                print("Payout Setting")
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayoutViewController") as! PayoutViewController
//                vc.hidesBottomBarWhenPushed = true
//                vc.user = self.userData
//                self.navigationController?.pushViewController(vc, animated: true)
            
            default:
                let story:UIStoryboard = UIStoryboard(name: "User", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
                vc.hidesBottomBarWhenPushed = true

                self.navigationController?.pushViewController(vc, animated: true)
                
                print("Topup")
                
            }
        case 1:
            switch indexPath.row {
            case 0:
                print("PushNotification Settings")
                let vc = storyboard?.instantiateViewController(withIdentifier: "pushNotiSettingsViewController") as! pushNotiSettingsViewController
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("App Laguage")
            }
       /* case 2 :
           print("Free Space")*/
        case 2:
            switch indexPath.row {
            case 0:
                print("terms")
                let story:UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
                vc.privacy = "Terms of Service"
                vc.modalPresentationStyle = .fullScreen
                present(vc, animated: true, completion: nil)

            default:
                print("Privay")
                let story:UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
                vc.privacy = "Privacy Policy"
                vc.modalPresentationStyle = .fullScreen
                present(vc, animated: true, completion: nil)
            }
        default:
            switch indexPath.row {
            case 0:
                
             print("Switch account")
                
                let vc = storyboard?.instantiateViewController(withIdentifier: "SwitchAccountViewController") as! SwitchAccountViewController
                present(vc, animated: true, completion: nil)
                
             default:
                
               print("logout")
                let alertController = UIAlertController(title: NSLocalizedString("alert_app_name", comment: ""), message: "Would you like to LOGOUT?", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                    self.tabBarController?.selectedIndex = 0
                    self.tabBarController?.tabBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.white
                    
                    for var i in 0..<self.mySwitchAccount!.count{
                        var obj = self.mySwitchAccount![i]
                        if  obj.Id == self.myUser?[0].Id{
                            self.mySwitchAccount?.remove(at: i)
                            if switchAccount.saveswitchAccountToArchive(switchAccount: self.mySwitchAccount!) {
                                print("ID\(self.myUser?[0].Id) user logout")
                                break
                            }
                        }
                    }
                    self.logoutUserApi()
                }
                alertController.addAction(OKAction)
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                    print("Cancel button tapped");
                }
                cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion:nil)
            }
        }
    }
    
    //MARK:- API Handler
    
    func logoutUserApi(){
        
        let userID = self.myUser?[0].Id
        print("user id: ",userID as Any)
     
        ApiHandler.sharedInstance.logout(user_id: userID! ) { (isSuccess, response) in
         
            if isSuccess{
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    print(response?.value(forKey: "msg") as Any)
                    UserDefaults.standard.set("", forKey: "userID")
                    self.myUser = User.readUserFromArchive()
                 
                    self.myUser?.remove(at: 0)
                    if User.saveUserToArchive(user: self.myUser!) {
                        print("User Saved in Directory")
                        
                        let story = UIStoryboard(name: "Login", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "SplashScreenViewController") as! SplashScreenViewController
                        let nav = UINavigationController(rootViewController: vc)
                        nav.navigationBar.isHidden = true
                        self.view.window?.rootViewController = nav
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                   
                }else{
                    AppUtility?.showToast(string:response?.value(forKey: "msg") as? String ?? "" , view: self.view)
                    print("logout API:",response?.value(forKey: "msg") as! String)
                }
            }else{
                print("logout API:",response?.value(forKey: "msg") as Any)
            }
        }
    }
}

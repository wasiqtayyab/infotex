//
//  OrderStatusViewController.swift
//  Infotex New Screen
//
//  Created by Wasiq Tayyab on 27/09/2021.
//

import UIKit

class OrderStatusViewController: UIViewController {

    //MARK:- OUTLET
    
    @IBOutlet weak var lblWithdrawalNumber: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblRedDiamond: UILabel!
    @IBOutlet weak var lblRDiamond: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var successfulView: UIView!
     
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.successfulView.layer.cornerRadius = self.successfulView.frame.size.width / 2
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    

 
}

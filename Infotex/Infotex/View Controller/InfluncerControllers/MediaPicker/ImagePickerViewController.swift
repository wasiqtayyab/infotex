//
//  ImagePickerViewController.swift
//  Infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit
import Photos

class ImagePickerViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    //MARK:- Outlets
    @IBOutlet weak var imgDownArrow: UIImageView!
    @IBOutlet weak var cvVideoPicker: UICollectionView!
    
    var photoLibrary = [[String:Any]]()
    var selectedImage = [UIImage]()

    
    //MARK:- ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    //MARK:- SetupView
    
    func setupView(){
        self.getPhotos()
    }
    
    //MARK:- Button Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnRecentAction(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.imgDownArrow.transform = self.imgDownArrow.transform.rotated(by: .pi)
        })
    }
    @IBAction func btnNextAction(_ sender: Any) {
        if self.selectedImage.count == 0 {
            showToast(message: "Please select at least one photo", font: UIFont.systemFont(ofSize: 12.0))
            return
        }
        if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "influncer", bundle: nil)
            let vc =  storyMain.instantiateViewController(withIdentifier: "ImageEffectViewController") as! ImageEffectViewController
            vc.effectImages =  self.selectedImage
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK:- API Handler
    func getPhotos() {
        
        let manager = PHImageManager.default()
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = false
        requestOptions.deliveryMode = .highQualityFormat
        // .highQualityFormat will return better quality photos
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        
        let results: PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        if results.count > 0 {
            for i in 0..<results.count {
                let asset = results.object(at: i)
                let size = CGSize(width: 700, height: 700)
                manager.requestImage(for: asset, targetSize: size, contentMode: .aspectFill, options: requestOptions) { (image, _) in
                    if let image = image {
                        self.photoLibrary.append(["Image":image,"isSelected":"0","count":0])
                        self.cvVideoPicker.reloadData()
                    } else {
                        print("error asset to image")
                    }
                }
            }
        } else {
            print("no photos to display")
        }
        
    }
    
    //MARK: TableView
    
    //MARK: CollectionView.
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photoLibrary.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaPickerCollectionViewCell", for: indexPath) as! MediaPickerCollectionViewCell
        cell.image.image = self.photoLibrary[indexPath.row]["Image"] as! UIImage
        if  self.photoLibrary[indexPath.row]["isSelected"] as! String == "0"{
            cell.viewShowImageCount.isHidden =  true
            
        }else{
            cell.viewShowImageCount.isHidden =  false
            cell.lblImageCount.text! = "\(self.photoLibrary[indexPath.row]["count"] as! Int )"
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width/3, height: 140)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var count = 0
     
        if self.photoLibrary[indexPath.row]["isSelected"] as! String == "1"{
            self.selectedImage.removeAll()
        }
        if self.selectedImage.count < 6{
            self.selectedImage.removeAll()
            
            var obj = self.photoLibrary[indexPath.row]
            if obj["isSelected"] as! String == "0"{
                obj.updateValue("1", forKey: "isSelected")
            }else{
                obj.updateValue("0", forKey: "isSelected")
            }
            
            self.photoLibrary.remove(at:indexPath.row)
            self.photoLibrary.insert(obj, at: indexPath.row)
            for var i in 0..<self.photoLibrary.count{
                if self.photoLibrary[i]["isSelected"] as! String == "1"{
                    count = count + 1
                    var obj2 = self.photoLibrary[i]
                    obj2.updateValue(count, forKey: "count")
                    self.photoLibrary.remove(at:i)
                    self.photoLibrary.insert(obj2, at: i)
                    self.selectedImage.append(self.photoLibrary[i]["Image"] as! UIImage)
                }
            }
            print("SelectedImage:\(self.selectedImage.count)")
            self.cvVideoPicker.reloadData()
        }
        
    }
}

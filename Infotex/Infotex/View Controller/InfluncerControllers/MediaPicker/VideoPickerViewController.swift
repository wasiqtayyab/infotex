//
//  VideoPickerViewController.swift
//  Infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit
import Photos
import MobileCoreServices
import AVFoundation
import AVKit
class VideoPickerViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
  
    //MARK:- Outlets
    @IBOutlet weak var imgDownArrow: UIImageView!
    @IBOutlet weak var cvVideoPicker: UICollectionView!
    
    var photoLibrary = [UIImage]()
    var videoLength = [Float64]()
    var videoURL = [URL]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    //MARK:- SetupView
    
    func setupView(){
        self.grabPhotos()
    }
    
    //MARK:- Switch Action
    
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnRecentAction(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.imgDownArrow.transform = self.imgDownArrow.transform.rotated(by: .pi)
        })
    }
    @IBAction func btnNextAction(_ sender: Any) {
        
    }
    
    //MARK:- API Handler
    
    func grabPhotos(){
        let imgManager = PHImageManager.default()
        
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        requestOptions.deliveryMode = .highQualityFormat
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        let fetchResult : PHFetchResult = PHAsset.fetchAssets(with: .video, options: fetchOptions)
        
        if fetchResult.count > 0 {
            var assestURL:URL!
            for i in 0..<fetchResult.count{
                
                //Used for fetch Image//
                imgManager.requestImage(for: fetchResult.object(at: i) as PHAsset , targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFill, options: requestOptions, resultHandler: {
                    image, error in
                    
//                    //Used for fetch Video//
                    imgManager.requestAVAsset(forVideo: fetchResult.object(at: i) as PHAsset, options: PHVideoRequestOptions(), resultHandler: {(avAsset, audioMix, info) -> Void in
                        if let asset = avAsset as? AVURLAsset {
                            //let videoData = NSData(contentsOf: asset.url)
                            let duration : CMTime = asset.duration
                            let durationInSecond = CMTimeGetSeconds(duration)
                            print("durationInSecond",durationInSecond)
                            let asset = asset as! AVURLAsset
                            assestURL = asset.url
                            self.videoURL.append(assestURL)
                            self.videoLength.append(durationInSecond)
                        }
                    })
                    let Asurl = fetchResult.object(at: i) as PHAsset
                    let imageOfVideo = image! as UIImage
                    self.photoLibrary.append(imageOfVideo)
                })
            }
            print("photoLibrary",self.photoLibrary.count)
            print("videoLength",self.videoLength.count)
            print("videoURL",self.videoURL.count)
            self.cvVideoPicker.reloadData()
            print(self.photoLibrary)
        }
        else{
            print("Error to fetch videos")
            self.showToast(message:"Error to fetch videos", font: UIFont.systemFont(ofSize: 12.0))
        }
    }
    
    //MARK: TableView
    
    //MARK: CollectionView.
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photoLibrary.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaPickerCollectionViewCell", for: indexPath) as! MediaPickerCollectionViewCell
        cell.image.image = self.photoLibrary[indexPath.row]
        cell.lblVideoTime.text! = ""//"\(self.photoLibrary[indexPath.row]["time"] as? Float64)"
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width/3, height: 140)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaPickerCollectionViewCell", for: indexPath) as! MediaPickerCollectionViewCell
        let videoURL = self.videoURL[indexPath.row] as? URL
      if videoURL != nil
      {
        if isInfoTex == true{
            let vc =  storyboard?.instantiateViewController(withIdentifier:
                "VideoTrimViewController") as! VideoTrimViewController
            vc.url = videoURL! as NSURL
            self.present(vc, animated: true, completion: nil)
        }else{
            let vc =  self.storyboard?.instantiateViewController(withIdentifier: "AddSoundVideoViewController") as! AddSoundVideoViewController
            vc.url = videoURL! as NSURL
            self.present(vc, animated: true, completion: nil)
        }
     
      }
    }
}

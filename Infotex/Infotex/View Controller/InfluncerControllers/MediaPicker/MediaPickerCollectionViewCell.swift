//
//  VideoPickerCollectionViewCell.swift
//  Infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit

class MediaPickerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblVideoTime: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lblImageCount: UILabel!
    @IBOutlet weak var viewShowImageCount: UIView!
    
}

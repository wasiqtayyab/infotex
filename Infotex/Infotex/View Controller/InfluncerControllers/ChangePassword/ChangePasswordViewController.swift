//
//  ChangePasswordViewController.swift
//  Infotex
//
//  Created by Wasiq Tayyab on 29/09/2021.
//

import UIKit

class ChangePasswordViewController: UIViewController, UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfOldPassword: UITextField!
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfVerifyPassword: UITextField!
    @IBOutlet weak var btnChangePassword: UIButton!
    
    @IBOutlet weak var btnOldEye: UIButton!
    @IBOutlet weak var btnNewEye: UIButton!
    @IBOutlet weak var btnVerifyEye: UIButton!
    
    //MARK:- VARS
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    var isSecure = false
    var isSecure1 = false
    var isSecure2 = false
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldSetup()
        btnChangePassword.backgroundColor = UIColor(named: "light shaft")
        
    }
    
    //MARK:- TEXTFIELD PLACEHOLDER
    
    private func textFieldSetup(){
        tfOldPassword.delegate = self
        tfOldPassword.attributedPlaceholder = NSAttributedString(string: "Old Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        tfNewPassword.delegate = self
        tfNewPassword.attributedPlaceholder = NSAttributedString(string: "New Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        tfVerifyPassword.delegate = self
        tfVerifyPassword.attributedPlaceholder = NSAttributedString(string: "Verify Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func changePasswordButtonPressed(_ sender: UIButton) {
        
        if tfOldPassword.text!.count < 6 {
            AppUtility?.showToast(string: "Enter your password greater than 5", view: self.view)
            return
        }
        if tfNewPassword.text!.count < 6 {
            AppUtility?.showToast(string: "Enter your password greater than 5", view: self.view)
            return
        }
        if tfVerifyPassword.text!.count < 6 {
            AppUtility?.showToast(string: "Enter your password greater than 5", view: self.view)
            return
        }
        
        if tfOldPassword.text! == tfNewPassword.text {
            AppUtility?.showToast(string: "old Password does not match with new Password", view: self.view)
            return
        }
        
        if tfNewPassword.text != tfVerifyPassword.text! {
            AppUtility?.showToast(string: "new Password does not match with verify password", view: self.view)
            return
        }
        
        self.changePassword()
        
    }
    
    @IBAction func eyeButtonPressed(_ sender: UIButton) {
        
        if sender == btnOldEye {
            if isSecure == false {
                btnOldEye.setImage(UIImage(named: "eye_"), for: .normal)
                tfOldPassword.isSecureTextEntry = false
                isSecure = true
            }else {
                btnOldEye.setImage(UIImage(named: "eye"), for: .normal)
                tfOldPassword.isSecureTextEntry = true
                isSecure = false
            }
        }else if sender == btnNewEye {
            
            if isSecure1 == false {
                btnNewEye.setImage(UIImage(named: "eye_"), for: .normal)
                tfNewPassword.isSecureTextEntry = false
                isSecure1 = true
            }else {
                btnNewEye.setImage(UIImage(named: "eye"), for: .normal)
                tfNewPassword.isSecureTextEntry = true
                isSecure1 = false
            }
            
        }else {
            
            if isSecure2 == false {
                btnVerifyEye.setImage(UIImage(named: "eye_"), for: .normal)
                tfVerifyPassword.isSecureTextEntry = false
                isSecure2 = true
            }else {
                btnVerifyEye.setImage(UIImage(named: "eye"), for: .normal)
                tfVerifyPassword.isSecureTextEntry = true
                isSecure2 = false
            }
            
        }
        
        
    }
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- TEXTFIELD DELEGATE
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if tfOldPassword.text!.count > 5 && tfNewPassword.text!.count > 5 && tfVerifyPassword.text!.count > 5  {
            
            btnChangePassword.setBackgroundImage(UIImage(named: "97-97"), for: .normal)
            btnChangePassword.backgroundColor = .clear
            
        }else {
            
            btnChangePassword.setBackgroundImage(nil, for: .normal)
            btnChangePassword.backgroundColor = UIColor(named: "light shaft")
            
            
        }
    }
    
    //MARK:- API
    
    private func changePassword(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.changePassword(user_id: user_id, old_password: tfOldPassword.text!, new_password: tfNewPassword.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: ManageAccountViewController.self) {
                            _ =  self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
            
        }
        
    }
    
}

//
//  AddSoundVideoViewController.swift
//  Infotex
//
//  Created by Mac on 05/09/2021.
//

import UIKit
import UIKit
import Alamofire
import GSPlayer
import DSGradientProgressView
import AVFoundation
import CoreImage
import NextLevel
import MarqueeLabel

class AddSoundVideoViewController: UIViewController {
    
    
    //MARK:- Outlets
   
    @IBOutlet weak var playerView: VideoPlayerView!
    @IBOutlet weak var lblSoundName: MarqueeLabel!
    @IBOutlet weak var btnPlayAction: UIButton!
    
    //MARK:- Variables
    var url:NSURL! = nil
    var duetURL:URL!
    var audioPlayer : AVAudioPlayer?
    
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.setupView()
    }
    
    //MARK:- viewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.playerView.resume()
    }
    
    
    //MARK:- viewWillDisappear
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        playerView.pause(reason: .hidden)
    }
    
    //MARK:- SetupView
    
    func setupView(){
        self.playerSetup()
    }
    
    
    //MARK:- Button Action
    
    @IBAction func btnPlayAction(_ sender: Any) {
        playerView.play(for: url! as URL,filterName:"",filterIndex:0)
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        let vc =  storyboard?.instantiateViewController(withIdentifier: "previewPlayerViewController") as! previewPlayerViewController
        if self.duetURL != nil{
            vc.url = self.duetURL
        }else{
            vc.url = self.url! as URL
        }
        playerView.pause(reason: .hidden)
        
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- PlayerSetup
    
    func playerSetup(){
        
        btnPlayAction.isHidden = true
        
        playerView.contentMode = .scaleAspectFill
        playerView.play(for: url! as URL,filterName:"",filterIndex:0)
        
        playerView.stateDidChanged = { [self] state in
            switch state {
            case .none:
                print("none")
            case .error(let error):
                print("error - \(error.localizedDescription)")

            case .loading:
                print("loading")
              
            case .paused(let playing, let buffering):
                print("paused - progress \(Int(playing * 100))% buffering \(Int(buffering * 100))%")
                
            case .playing:
                
                print("playing")
            }
        }
        
        print("video Pause Reason: ",playerView.pausedReason )
    }
    
    
    func addAudioSound(){

        if let url = UserDefaults.standard.string(forKey: "url"), let audioUrl = URL(string: url) {
            
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            print("destinationUrl: ",destinationUrl)
            self.duetURL = destinationUrl
            audioPlayer?.rate = 1.0

            do {
                audioPlayer = try AVAudioPlayer(contentsOf: destinationUrl)
                print("loaded audio file")
                
                let soundName = UserDefaults.standard.string(forKey: "selectedSongName")
                
                self.lblSoundName.text = soundName
                self.lblSoundName.type = .continuous
                
            } catch {
                print("CouldNot load audio file")
            }
 
            print("audioPlayer?.duration:- ",audioPlayer?.duration)
            
        }
    }
}
extension AddSoundVideoViewController{
    
    func mergeVideoWithAudio(videoUrl: URL,audioUrl: URL,success: @escaping ((URL) -> Void),failure: @escaping ((Error?) -> Void)) {

           let mixComposition: AVMutableComposition = AVMutableComposition()
           var mutableCompositionVideoTrack: [AVMutableCompositionTrack] = []
           var mutableCompositionAudioTrack: [AVMutableCompositionTrack] = []
           let totalVideoCompositionInstruction: AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()

           let aVideoAsset: AVAsset = AVAsset(url: videoUrl)
           let aAudioAsset: AVAsset = AVAsset(url: audioUrl)

           if let videoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid), let audioTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid) {
               mutableCompositionVideoTrack.append( videoTrack )
               mutableCompositionAudioTrack.append( audioTrack )

               if let aVideoAssetTrack: AVAssetTrack = aVideoAsset.tracks(withMediaType: .video).first, let aAudioAssetTrack: AVAssetTrack = aAudioAsset.tracks(withMediaType: .audio).first {
                   do {
                       try mutableCompositionVideoTrack.first?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aVideoAssetTrack, at: CMTime.zero)

                       let videoDuration = aVideoAsset.duration
                       if CMTimeCompare(videoDuration, aAudioAsset.duration) == -1 {
                           try mutableCompositionAudioTrack.first?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: CMTime.zero)
                       } else if CMTimeCompare(videoDuration, aAudioAsset.duration) == 1 {
                           var currentTime = CMTime.zero
                           while true {
                               var audioDuration = aAudioAsset.duration
                               let totalDuration = CMTimeAdd(currentTime, audioDuration)
                               if CMTimeCompare(totalDuration, videoDuration) == 1 {
                                   audioDuration = CMTimeSubtract(totalDuration, videoDuration)
                               }
                               try mutableCompositionAudioTrack.first?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: currentTime)

                               currentTime = CMTimeAdd(currentTime, audioDuration)
                               if CMTimeCompare(currentTime, videoDuration) == 1 || CMTimeCompare(currentTime, videoDuration) == 0 {
                                   break
                               }
                           }
                       }
                       videoTrack.preferredTransform = aVideoAssetTrack.preferredTransform

                   } catch {
                       print(error)
                   }

                   totalVideoCompositionInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration)
               }
           }

           let mutableVideoComposition: AVMutableVideoComposition = AVMutableVideoComposition()
           mutableVideoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
           mutableVideoComposition.renderSize = CGSize(width: 480, height: 640)

           if let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
               let outputURL = URL(fileURLWithPath: documentsPath).appendingPathComponent("\("duetVideo").mov")

               do {
                   if FileManager.default.fileExists(atPath: outputURL.path) {

                       try FileManager.default.removeItem(at: outputURL)
                   }
               } catch { }

               if let exportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality) {
                   exportSession.outputURL = outputURL
                   exportSession.outputFileType = AVFileType.mov
                   exportSession.shouldOptimizeForNetworkUse = true

                   // try to export the file and handle the status cases
                   exportSession.exportAsynchronously(completionHandler: {
                       switch exportSession.status {
                       case .failed:
                           if let error = exportSession.error {
                               failure(error)
                           }

                       case .cancelled:
                           if let error = exportSession.error {
                               failure(error)
                           }

                       default:
                           print("finished")
                           success(outputURL)
                       }
                   })
               } else {
                   failure(nil)
               }
           }
       }
}

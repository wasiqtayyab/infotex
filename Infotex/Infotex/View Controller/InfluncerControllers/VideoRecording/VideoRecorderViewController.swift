//
//  VideoRecorderViewController.swift
//  Infotex
//
//  Created by Mac on 24/09/2021.
//

import UIKit

import UIKit
import NextLevel
import AVFoundation
import Photos
import GTProgressBar
import EFInternetIndicator
import CoreAnimator
import SnapKit

class VideoRecorderViewController: UIViewController {

    //MARK:OutLets
    @IBOutlet weak var progressBarView: GTProgressBar!
    @IBOutlet weak var btnRecordVideo: UIButton!
    @IBOutlet weak var btnDoneOutlet: UIButton!
    @IBOutlet weak var btnFlash: UIButton!
    @IBOutlet weak var btnCamerChange: UIButton!
    @IBOutlet weak var masterViewOutlet: UIView!
    
    //MARK:- Variables
    
    let NextLevelAlbumTitle = "NextLevel"
    internal var focusView: FocusIndicatorView?
    internal var metadataObjectViews: [UIView]?
    internal var previewView: UIView?
    var videoLengthSec = videoTrim
    var isRecording =  false
    var videoURL:URL!
    var flashToggleState = 1
    var duetURL:URL!
    var audioPlayer : AVAudioPlayer?
    //MARK:- viewDidLoad

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupView()
    }
    
    //MARK:- ViewWillDisapper
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NextLevel.shared.stop()
    }
    
    //MARK:- ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        self.audioPlayer?.stop()
        self.audioPlayer?.pause()
        self.loadAudio()
    }
    
    //MARK:- SetupView
    
    func setupView(){
        
        self.progressBarView.progress = 0
        self.nextLevelDelegate()
      
        self.cameraAudioPermission()
        NextLevel.shared.focusMode = .continuousAutoFocus
        self.focusView = FocusIndicatorView(frame: .zero)
        if isInfoTex == false{
            self.btnRecordVideo.setImage(UIImage(named: "camera_gradeint"), for: .normal)
        }else{
            self.btnRecordVideo.setImage(UIImage(named: "camera_gradeint-1"), for: .normal)
        }
    }

    //MARK:- Delegate

    func nextLevelDelegate(){
        self.CamerPreviewView()
    }
    
    
    
    //MARK:- Camera setupView
    
    func CamerPreviewView(){
        self.previewView = UIView(frame: UIScreen.main.bounds)
        
        print("masterViewOutlet view height", masterViewOutlet.frame.height)
        if let previewView = self.previewView {
            previewView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            previewView.backgroundColor = UIColor.black
            previewView.layer.cornerRadius = 10
            NextLevel.shared.previewLayer.frame = previewView.bounds
            NextLevel.shared.focusMode = .continuousAutoFocus
            previewView.layer.addSublayer(NextLevel.shared.previewLayer)
            view.insertSubview(previewView, belowSubview: masterViewOutlet)
            self.previewView?.snp.makeConstraints({ (make) in
                make.center.equalTo(self.masterViewOutlet)
                make.width.height.equalTo(self.masterViewOutlet)
            })
            
            print("preview view height", previewView.frame.height)
        }
        
        let nextLevel = NextLevel.shared
        nextLevel.delegate = self
        nextLevel.deviceDelegate = self
        nextLevel.videoDelegate = self
        nextLevel.photoDelegate = self
        nextLevel.metadataObjectsDelegate = self
        
        // video configuration
        nextLevel.videoConfiguration.preset = AVCaptureSession.Preset.hd1280x720
        nextLevel.videoConfiguration.bitRate = 5500000
        nextLevel.videoConfiguration.maxKeyFrameInterval = 30
        nextLevel.videoConfiguration.profileLevel = AVVideoProfileLevelH264HighAutoLevel
        
        NextLevel.shared.videoConfiguration.maximumCaptureDuration = CMTimeMakeWithSeconds(videoLengthSec, preferredTimescale: 600)
        
        // audio configuration
        nextLevel.audioConfiguration.bitRate = 96000
        
        // metadata objects configuration
        nextLevel.metadataObjectTypes = [AVMetadataObject.ObjectType.face, AVMetadataObject.ObjectType.qr]
    }
    
    //MARK:- Camera Persmission
    
    func cameraAudioPermission(){
        
        if NextLevel.authorizationStatus(forMediaType: AVMediaType.video) == .authorized &&
            NextLevel.authorizationStatus(forMediaType: AVMediaType.audio) == .authorized {
            do {
                try NextLevel.shared.start()
            } catch {
                print("NextLevel, failed to start camera session")
            }
        } else {
            NextLevel.requestAuthorization(forMediaType: AVMediaType.video) { (mediaType, status) in
                print("NextLevel, authorization updated for media \(mediaType) status \(status)")
                if NextLevel.authorizationStatus(forMediaType: AVMediaType.video) == .authorized &&
                    NextLevel.authorizationStatus(forMediaType: AVMediaType.audio) == .authorized {
                    do {
                        let nextLevel = NextLevel.shared
                        try nextLevel.start()
                    } catch {
                        print("NextLevel, failed to start camera session")
                    }
                } else if status == .notAuthorized {
                    
                    print("NextLevel doesn't have authorization for audio or video")
                }
            }
            NextLevel.requestAuthorization(forMediaType: AVMediaType.audio) { (mediaType, status) in
                print("NextLevel, authorization updated for media \(mediaType) status \(status)")
                if NextLevel.authorizationStatus(forMediaType: AVMediaType.video) == .authorized &&
                    NextLevel.authorizationStatus(forMediaType: AVMediaType.audio) == .authorized {
                    do {
                        let nextLevel = NextLevel.shared
                        try nextLevel.start()
                    } catch {
                        print("NextLevel, failed to start camera session")
                    }
                } else if status == .notAuthorized {
                    // gracefully handle when audio/video is not authorized
                    print("NextLevel doesn't have authorization for audio or video")
                }
            }
        }
    }
    
    
    // MARK:- Select Sound URL
    func loadAudio(){
        if let url = UserDefaults.standard.string(forKey: "url"), let audioUrl = URL(string: url) {
            
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            print("destinationUrl: ",destinationUrl)
            self.duetURL = destinationUrl
            audioPlayer?.rate = 1.0;
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: destinationUrl)
                print("loaded audio file")
                let soundName = UserDefaults.standard.string(forKey: "selectedSongName")
                
            } catch {
                print("CouldNot load audio file")
            }
            print("audioPlayer?.duration:- ",audioPlayer?.duration)
            
        }
    }
    
    
    //MARK:- Button Actions
    @IBAction func btnCloseAction(_ sender: Any) {
        self.actionSheetFunc()
    }
    @IBAction func btnCamerChangeAction(_ sender: Any) {
        AppUtility?.btnHoverAnimation(viewName:btnCamerChange)
        let animator = CoreAnimator(view: btnCamerChange)
        animator.rotate(angle: 180).animate(t: 0.5)
        
        if let metadataObjectViews = metadataObjectViews {
            for view in metadataObjectViews {
                view.removeFromSuperview()
            }
            self.metadataObjectViews = nil
        }
        
        NextLevel.shared.flipCaptureDevicePosition()
    }
    
    
    @IBAction func btnSelectSoundAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "soundsViewController") as! soundsViewController
         vc.modalPresentationStyle = .fullScreen
         self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func btnTimerAction(_ sender: Any) {
        let vc =  storyboard?.instantiateViewController(withIdentifier: "RangeSliderViewController") as! RangeSliderViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnVideoPicker(_ sender: Any) {
        let vc =  storyboard?.instantiateViewController(withIdentifier: "VideoPickerViewController") as! VideoPickerViewController
        self.present(vc, animated: true, completion: nil)
    }
   
    
    @IBAction func btnFlashAction(_ sender: Any) {
        AppUtility?.btnHoverAnimation(viewName:btnFlash)
        if self.flashToggleState == 1 {
            self.flashToggleState = 2
            self.btnFlash.setImage(UIImage(named: "not_light"), for: .normal)
            NextLevel.shared.flashMode = .on
            NextLevel.shared.torchMode = .on
        } else {
            self.flashToggleState = 1
            self.btnFlash.setImage(UIImage(named: "light_white"), for: .normal)
            NextLevel.shared.flashMode = .off
            NextLevel.shared.torchMode = .off
        }
    }
    
    
    @IBAction func btnRecordingAction(_ sender: Any) {
        if isRecording == false{
            self.isRecording =  true
            self.btnRecordVideo.setImage(UIImage(named: "borderpic_"), for: .normal)
            NextLevel.shared.record()
            
        }else{
            self.isRecording =  false
            endCapture(isDone: false)
            self.btnRecordVideo.setImage(UIImage(named: "camera_grdient"), for: .normal)
        }
    }
    
    
    @IBAction func btnDone(_ sender: Any) {
      
        self.sessionDoneFunc()
        
    }
    
    
    //MARK:- End Capture Video

    func endCapture(isDone:Bool) {
        NextLevel.shared.session?.endClip(completionHandler: { (clip, error) in
            if error == nil {
                print("Clip URL:",clip?.url)
                self.videoURL  =  clip?.url
                if isDone == true{
                    if isInfoTex == false{
                        
                        if self.duetURL !=  nil{
                            self.mergeVideoWithAudio(videoUrl: self.videoURL, audioUrl: (self.duetURL)!, success: { (url) in
                                DispatchQueue.main.async {
                                    let vc =  self.storyboard?.instantiateViewController(withIdentifier: "VideoTrimViewController") as! VideoTrimViewController
                                    vc.url =  url as NSURL
                                    self.present(vc, animated: true, completion: nil)
                                }

                            }, failure: { (error) in
                                self.showToast(message: "Error with duet sound and video,pleae try again", font: .systemFont(ofSize: 12))
                                print("Error to merge audio and video")
                            })
                        }else{
                            let vc =  self.storyboard?.instantiateViewController(withIdentifier: "VideoTrimViewController") as! VideoTrimViewController
                            vc.url =  clip?.url as NSURL?
                            self.present(vc, animated: true, completion: nil)
                        }
                        
                       
                    }else{
                        if self.duetURL !=  nil{
                            self.mergeVideoWithAudio(videoUrl: self.videoURL, audioUrl: (self.duetURL)!, success: { (url) in
                                DispatchQueue.main.async {
                                    let vc =  self.storyboard?.instantiateViewController(withIdentifier: "previewPlayerViewController") as! previewPlayerViewController
                                    vc.url =  url
                                    self.present(vc, animated: true, completion: nil)
                                }

                            }, failure: { (error) in
                                self.showToast(message: "Error with duet sound and video,pleae try again", font: .systemFont(ofSize: 12))
                                print("Error to merge audio and video")
                            })
                        }else{
                            let vc =  self.storyboard?.instantiateViewController(withIdentifier: "previewPlayerViewController") as! previewPlayerViewController
                            vc.url =  clip?.url
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                
             }
            }else{
                let alertController = UIAlertController(title: "Video Capture", message: error?.localizedDescription, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
        })
    }
    
    
    func endCapture() {
        self.audioPlayer?.stop()
        
        if let session = NextLevel.shared.session {
            
            if session.clips.count > 1 {
                session.mergeClips(usingPreset: AVAssetExportPresetHighestQuality, completionHandler: { (url: URL?, error: Error?) in
                    if let url = url {
                        
                        self.saveVideo(withURL: url)
                    } else if let _ = error {
                        print("failed to merge clips at the end of capture \(String(describing: error))")
                    }
                })
            } else if let lastClipUrl = session.lastClipUrl {
                self.saveVideo(withURL: lastClipUrl)
            } else if session.currentClipHasStarted {
                session.endClip(completionHandler: { (clip, error) in
                    if error == nil {
                        self.saveVideo(withURL: (clip?.url)!)
                    } else {
                        print("Error saving video: \(error?.localizedDescription ?? "")")
                    }
                })
            } else {
                let alertController = UIAlertController(title: "Video Capture", message: "Not enough video captured!", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
            
        }
        
    }
    
    
    func sessionDoneFunc() {
        guard let session = NextLevel.shared.session else {
            return
        }
        
        session.mergeClips(usingPreset: AVAssetExportPresetMediumQuality) { [weak self] (url, error) in
            guard let _ = self, let url = url, error == nil else {
                print("error: ",error?.localizedDescription)
                self?.showToast(message: error!.localizedDescription, font: .systemFont(ofSize: 12))
                return
            }
            if self?.duetURL == nil{
                let vc =  self?.storyboard?.instantiateViewController(withIdentifier: "previewPlayerViewController") as! previewPlayerViewController
                vc.url = url
                vc.modalPresentationStyle = .fullScreen
                self?.present(vc, animated: true, completion: nil)
            }else{
                self?.mergeVideoWithAudio(videoUrl: url, audioUrl: (self?.duetURL)!, success: { (url) in
                    DispatchQueue.main.async {
                        let vc =  self?.storyboard?.instantiateViewController(withIdentifier: "previewPlayerViewController") as! previewPlayerViewController
                        vc.url = url
                        vc.modalPresentationStyle = .fullScreen
                        self?.present(vc, animated: true, completion: nil)
                    }
                    
                }, failure: { (error) in
                    self?.showToast(message: "Error with duet sound and video,pleae try again", font: .systemFont(ofSize: 12))
                    print("Error to merge audio and video")
                })
            }
        }
    }
    //MARK:- pause Capture Video
    
    func pauseCapture() {
        NextLevel.shared.pause()
    }
    
    
    
    
    //MARK:- Save Video To Photo Libaray
    func saveVideo(withURL url: URL) {
        PHPhotoLibrary.shared().performChanges({
            let albumAssetCollection = self.albumAssetCollection(withTitle: self.NextLevelAlbumTitle)
            if albumAssetCollection == nil {
                let changeRequest = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: self.NextLevelAlbumTitle)
                let _ = changeRequest.placeholderForCreatedAssetCollection
            }
            
        }, completionHandler: { (success1: Bool, error1: Error?) in
            if let albumAssetCollection = self.albumAssetCollection(withTitle: self.NextLevelAlbumTitle) {
                PHPhotoLibrary.shared().performChanges({
                    if let assetChangeRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url) {
                        let assetCollectionChangeRequest = PHAssetCollectionChangeRequest(for: albumAssetCollection)
                        let enumeration: NSArray = [assetChangeRequest.placeholderForCreatedAsset!]
                        assetCollectionChangeRequest?.addAssets(enumeration)
                    }
                }, completionHandler: { (success2: Bool, error2: Error?) in
                    if success2 == true {
                        // prompt that the video has been saved
                        let alertController = UIAlertController(title: "Video Saved!", message: "Saved to the camera roll.", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    } else {
                        // prompt that the video has been saved
                        let alertController = UIAlertController(title: "Oops!", message: "Something failed!", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                })
            }
        })
    }
    
    // MARK: - library
    func albumAssetCollection(withTitle title: String) -> PHAssetCollection? {
        let predicate = NSPredicate(format: "localizedTitle = %@", title)
        let options = PHFetchOptions()
        options.predicate = predicate
        let result = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: options)
        if result.count > 0 {
            return result.firstObject
        }
        return nil
        
        
    }
    
    //MARK:- Action Sheet
    func actionSheetFunc(){
        // create an actionSheet
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // create an action
        let startOver: UIAlertAction = UIAlertAction(title: "Start over", style: .default) { action -> Void in
           
            self.endCapture(isDone: false)

            self.isRecording =  false
            if isInfoTex == false{
                self.btnRecordVideo.setImage(UIImage(named: "camera_gradeint"), for: .normal)
            }else{
                self.btnRecordVideo.setImage(UIImage(named: "camera_gradeint-1"), for: .normal)
            }
            self.progressBarView.animateTo(progress: CGFloat(0.0))
            let session = NextLevel.shared.session
            session?.reset()
            session?.removeAllClips()
            self.loadAudio()
            
            print("startOver pressed")
        }
        
        let discard: UIAlertAction = UIAlertAction(title: "Discard", style: .default) { action -> Void in
                        
            self.isRecording =  false
            if isInfoTex == false{
                self.btnRecordVideo.setImage(UIImage(named: "camera_gradeint"), for: .normal)
            }else{
                self.btnRecordVideo.setImage(UIImage(named: "camera_gradeint-1"), for: .normal)
            }
            self.progressBarView.animateTo(progress: CGFloat(0.0))
            let session = NextLevel.shared.session
            session?.reset()
            session?.removeAllClips()
            print("Discard Action pressed")
            self.dismiss(animated: true, completion: nil)
        }
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
        
        startOver.setValue(UIColor.red, forKey: "titleTextColor")      // add actions
        actionSheetController.addAction(startOver)
        actionSheetController.addAction(discard)
        actionSheetController.addAction(cancelAction)
        actionSheetController.popoverPresentationController?.sourceView = self.view // works for both iPhone & iPad
        
        present(actionSheetController, animated: true) {
            print("option menu presented")
        }
    }
}

//MARK:- Extension


//MARK:- NextLevel VideoDelegate

extension VideoRecorderViewController: NextLevelVideoDelegate {
    func nextLevel(_ nextLevel: NextLevel, willProcessFrame frame: AnyObject, timestamp: TimeInterval, onQueue queue: DispatchQueue) {
        "print"
    }
    
    // video zoom
    func nextLevel(_ nextLevel: NextLevel, didUpdateVideoZoomFactor videoZoomFactor: Float) {
    }
    
    // video frame processing
    func nextLevel(_ nextLevel: NextLevel, willProcessRawVideoSampleBuffer sampleBuffer: CMSampleBuffer, onQueue queue: DispatchQueue) {
    }
    
    func nextLevel(_ nextLevel: NextLevel, willProcessFrame frame: AnyObject, pixelBuffer: CVPixelBuffer, timestamp: TimeInterval, onQueue queue: DispatchQueue) {
    }
    
    // enabled by isCustomContextVideoRenderingEnabled
    func nextLevel(_ nextLevel: NextLevel, renderToCustomContextWithImageBuffer imageBuffer: CVPixelBuffer, onQueue queue: DispatchQueue) {
    }
    
    // video recording session
    func nextLevel(_ nextLevel: NextLevel, didSetupVideoInSession session: NextLevelSession) {
    }
    
    func nextLevel(_ nextLevel: NextLevel, didSetupAudioInSession session: NextLevelSession) {
        
    }
    
    func nextLevel(_ nextLevel: NextLevel, didStartClipInSession session: NextLevelSession) {
    }
    
    func nextLevel(_ nextLevel: NextLevel, didCompleteClip clip: NextLevelClip, inSession session: NextLevelSession){
    }
    
    func nextLevel(_ nextLevel: NextLevel, didAppendVideoSampleBuffer sampleBuffer: CMSampleBuffer, inSession session: NextLevelSession) {
        
        let currentProgress = (session.totalDuration.seconds / videoLengthSec).clamped(to: 0...1)
        self.progressBarView.animateTo(progress: CGFloat(currentProgress)) {
            print("progress done at:- ",currentProgress)
        }
        
        if currentProgress == 0.9 {
            pauseCapture()
            self.isRecording =  false
            if isInfoTex == false{
                self.btnRecordVideo.setImage(UIImage(named: "camera_gradeint"), for: .normal)
            }else{
                self.btnRecordVideo.setImage(UIImage(named: "camera_gradeint-1"), for: .normal)
            }
            
            btnDoneOutlet.isHidden = false
        }
        
        if currentProgress > 0.0 {
           // btnSelectSound.isUserInteractionEnabled = false
        }
        
    }
    
    func nextLevel(_ nextLevel: NextLevel, didAppendAudioSampleBuffer sampleBuffer: CMSampleBuffer, inSession session: NextLevelSession) {
    }
    
    func nextLevel(_ nextLevel: NextLevel, didAppendVideoPixelBuffer pixelBuffer: CVPixelBuffer, timestamp: TimeInterval, inSession session: NextLevelSession) {
    }
    
    func nextLevel(_ nextLevel: NextLevel, didSkipVideoPixelBuffer pixelBuffer: CVPixelBuffer, timestamp: TimeInterval, inSession session: NextLevelSession) {
    }
    
    func nextLevel(_ nextLevel: NextLevel, didSkipVideoSampleBuffer sampleBuffer: CMSampleBuffer, inSession session: NextLevelSession) {
    }
    
    func nextLevel(_ nextLevel: NextLevel, didSkipAudioSampleBuffer sampleBuffer: CMSampleBuffer, inSession session: NextLevelSession) {
    }
    
    func nextLevel(_ nextLevel: NextLevel, didCompleteSession session: NextLevelSession) {
        pauseCapture()
        self.isRecording =  false
        
        if isInfoTex == false{
            self.btnRecordVideo.setImage(UIImage(named: "camera_gradeint"), for: .normal)
        }else{
            self.btnRecordVideo.setImage(UIImage(named: "camera_gradeint-1"), for: .normal)
        }
        
        btnDoneOutlet.isHidden = false
    }
    
    // video frame photo
    
    func nextLevel(_ nextLevel: NextLevel, didCompletePhotoCaptureFromVideoFrame photoDict: [String : Any]?) {
        
        if let dictionary = photoDict,
           let photoData = dictionary[NextLevelPhotoJPEGKey] {
            
            PHPhotoLibrary.shared().performChanges({
                
                let albumAssetCollection = self.albumAssetCollection(withTitle: self.NextLevelAlbumTitle)
                if albumAssetCollection == nil {
                    let changeRequest = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: self.NextLevelAlbumTitle)
                    let _ = changeRequest.placeholderForCreatedAssetCollection
                }
                
            }, completionHandler: { (success1: Bool, error1: Error?) in
                
                if success1 == true {
                    if let albumAssetCollection = self.albumAssetCollection(withTitle: self.NextLevelAlbumTitle) {
                        PHPhotoLibrary.shared().performChanges({
                            if let data = photoData as? Data,
                               let photoImage = UIImage(data: data) {
                                let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: photoImage)
                                let assetCollectionChangeRequest = PHAssetCollectionChangeRequest(for: albumAssetCollection)
                                let enumeration: NSArray = [assetChangeRequest.placeholderForCreatedAsset!]
                                assetCollectionChangeRequest?.addAssets(enumeration)
                            }
                        }, completionHandler: { (success2: Bool, error2: Error?) in
                            if success2 == true {
                                let alertController = UIAlertController(title: "Photo Saved!", message: "Saved to the camera roll.", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                            }
                        })
                    }
                } else if let _ = error1 {
                    print("failure capturing photo from video frame \(String(describing: error1))")
                }
                
            })
            
        }
        
    }
    
}

//MARK:- NextLevel Delegate

extension VideoRecorderViewController: NextLevelDelegate {
    
    // permission
    func nextLevel(_ nextLevel: NextLevel, didUpdateAuthorizationStatus status: NextLevelAuthorizationStatus, forMediaType mediaType: AVMediaType) {
    }
    
    // configuration
    func nextLevel(_ nextLevel: NextLevel, didUpdateVideoConfiguration videoConfiguration: NextLevelVideoConfiguration) {
    }
    
    func nextLevel(_ nextLevel: NextLevel, didUpdateAudioConfiguration audioConfiguration: NextLevelAudioConfiguration) {
    }
    
    // session
    func nextLevelSessionWillStart(_ nextLevel: NextLevel) {
        print("nextLevelSessionWillStart")
        
    }
    
    func nextLevelSessionDidStart(_ nextLevel: NextLevel) {
        print("nextLevelSessionDidStart")
    }
    
    func nextLevelSessionDidStop(_ nextLevel: NextLevel) {
        print("nextLevelSessionDidStop")
    }
    
    // interruption
    func nextLevelSessionWasInterrupted(_ nextLevel: NextLevel) {
    }
    
    func nextLevelSessionInterruptionEnded(_ nextLevel: NextLevel) {
    }
    
    // mode
    func nextLevelCaptureModeWillChange(_ nextLevel: NextLevel) {
    }
    
    func nextLevelCaptureModeDidChange(_ nextLevel: NextLevel) {
    }
    
}

//MARK:- NextLevel DeviceDelegate

extension VideoRecorderViewController: NextLevelDeviceDelegate {
    
    // position, orientation
    func nextLevelDevicePositionWillChange(_ nextLevel: NextLevel) {
    }
    
    func nextLevelDevicePositionDidChange(_ nextLevel: NextLevel) {
    }
    
    func nextLevel(_ nextLevel: NextLevel, didChangeDeviceOrientation deviceOrientation: NextLevelDeviceOrientation) {
    }
    
    // format
    func nextLevel(_ nextLevel: NextLevel, didChangeDeviceFormat deviceFormat: AVCaptureDevice.Format) {
    }
    
    // aperture
    func nextLevel(_ nextLevel: NextLevel, didChangeCleanAperture cleanAperture: CGRect) {
    }
    
    // lens
    func nextLevel(_ nextLevel: NextLevel, didChangeLensPosition lensPosition: Float) {
    }
    
    // focus, exposure, white balance
    func nextLevelWillStartFocus(_ nextLevel: NextLevel) {
    }
    
    func nextLevelDidStopFocus(_  nextLevel: NextLevel) {
        if let focusView = self.focusView {
            if focusView.superview != nil {
            }
        }
    }
    
    func nextLevelWillChangeExposure(_ nextLevel: NextLevel) {
    }
    
    func nextLevelDidChangeExposure(_ nextLevel: NextLevel) {
        if let focusView = self.focusView {
            if focusView.superview != nil {
            }
        }
    }
    
    func nextLevelWillChangeWhiteBalance(_ nextLevel: NextLevel) {
    }
    
    func nextLevelDidChangeWhiteBalance(_ nextLevel: NextLevel) {
    }
}

//MARK:- NextLevel ObjectsDelegate

extension VideoRecorderViewController: NextLevelMetadataOutputObjectsDelegate {
    
    func metadataOutputObjects(_ nextLevel: NextLevel, didOutput metadataObjects: [AVMetadataObject]) {
        guard let previewView = self.previewView else {
            return
        }
        
        if let metadataObjectViews = metadataObjectViews {
            for view in metadataObjectViews {
                view.removeFromSuperview()
            }
            self.metadataObjectViews = nil
        }
        
        self.metadataObjectViews = metadataObjects.map { metadataObject in
            let view = UIView(frame: metadataObject.bounds)
            view.backgroundColor = UIColor.clear
            view.layer.borderColor = UIColor.white.cgColor
            view.layer.borderWidth = 1
            return view
        }
        
        if let metadataObjectViews = self.metadataObjectViews {
            for view in metadataObjectViews {
                previewView.addSubview(view)
            }
        }
    }
}
// MARK: - NextLevelPhotoDelegate

extension VideoRecorderViewController: NextLevelPhotoDelegate {
    
    // photo
    func nextLevel(_ nextLevel: NextLevel, willCapturePhotoWithConfiguration photoConfiguration: NextLevelPhotoConfiguration) {
    }
    
    func nextLevel(_ nextLevel: NextLevel, didCapturePhotoWithConfiguration photoConfiguration: NextLevelPhotoConfiguration) {
    }
    
    func nextLevel(_ nextLevel: NextLevel, didProcessPhotoCaptureWith photoDict: [String : Any]?, photoConfiguration: NextLevelPhotoConfiguration) {
        
        if let dictionary = photoDict,
           let photoData = dictionary[NextLevelPhotoJPEGKey] {
            
            PHPhotoLibrary.shared().performChanges({
                
                let albumAssetCollection = self.albumAssetCollection(withTitle: self.NextLevelAlbumTitle)
                if albumAssetCollection == nil {
                    let changeRequest = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: self.NextLevelAlbumTitle)
                    let _ = changeRequest.placeholderForCreatedAssetCollection
                }
                
            }, completionHandler: { (success1: Bool, error1: Error?) in
                
                if success1 == true {
                    if let albumAssetCollection = self.albumAssetCollection(withTitle: self.NextLevelAlbumTitle) {
                        PHPhotoLibrary.shared().performChanges({
                            if let data = photoData as? Data,
                               let photoImage = UIImage(data: data) {
                                let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: photoImage)
                                let assetCollectionChangeRequest = PHAssetCollectionChangeRequest(for: albumAssetCollection)
                                let enumeration: NSArray = [assetChangeRequest.placeholderForCreatedAsset!]
                                assetCollectionChangeRequest?.addAssets(enumeration)
                            }
                        }, completionHandler: { (success2: Bool, error2: Error?) in
                            if success2 == true {
                                let alertController = UIAlertController(title: "Photo Saved!", message: "Saved to the camera roll.", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                            }
                        })
                    }
                } else if let _ = error1 {
                    print("failure capturing photo from video frame \(String(describing: error1))")
                }
                
            })
        }
        
    }
    
    func nextLevel(_ nextLevel: NextLevel, didProcessRawPhotoCaptureWith photoDict: [String : Any]?, photoConfiguration: NextLevelPhotoConfiguration) {
    }
    
    func nextLevelDidCompletePhotoCapture(_ nextLevel: NextLevel) {
    }
    
    func nextLevel(_ nextLevel: NextLevel, didFinishProcessingPhoto photo: AVCapturePhoto) {
    }
    
}

//MARK:- Duet Video

extension VideoRecorderViewController{
    
    func mergeVideoWithAudio(videoUrl: URL,audioUrl: URL,success: @escaping ((URL) -> Void),failure: @escaping ((Error?) -> Void)) {
        
        let mixComposition: AVMutableComposition = AVMutableComposition()
        var mutableCompositionVideoTrack: [AVMutableCompositionTrack] = []
        var mutableCompositionAudioTrack: [AVMutableCompositionTrack] = []
        let totalVideoCompositionInstruction: AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
        
        let aVideoAsset: AVAsset = AVAsset(url: videoUrl)
        let aAudioAsset: AVAsset = AVAsset(url: audioUrl)
        
        if let videoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid), let audioTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid) {
            mutableCompositionVideoTrack.append( videoTrack )
            mutableCompositionAudioTrack.append( audioTrack )
            
            if let aVideoAssetTrack: AVAssetTrack = aVideoAsset.tracks(withMediaType: .video).first, let aAudioAssetTrack: AVAssetTrack = aAudioAsset.tracks(withMediaType: .audio).first {
                do {
                    try mutableCompositionVideoTrack.first?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aVideoAssetTrack, at: CMTime.zero)
                    
                    let videoDuration = aVideoAsset.duration
                    if CMTimeCompare(videoDuration, aAudioAsset.duration) == -1 {
                        try mutableCompositionAudioTrack.first?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: CMTime.zero)
                    } else if CMTimeCompare(videoDuration, aAudioAsset.duration) == 1 {
                        var currentTime = CMTime.zero
                        while true {
                            var audioDuration = aAudioAsset.duration
                            let totalDuration = CMTimeAdd(currentTime, audioDuration)
                            if CMTimeCompare(totalDuration, videoDuration) == 1 {
                                audioDuration = CMTimeSubtract(totalDuration, videoDuration)
                            }
                            try mutableCompositionAudioTrack.first?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: currentTime)
                            
                            currentTime = CMTimeAdd(currentTime, audioDuration)
                            if CMTimeCompare(currentTime, videoDuration) == 1 || CMTimeCompare(currentTime, videoDuration) == 0 {
                                break
                            }
                        }
                    }
                    videoTrack.preferredTransform = aVideoAssetTrack.preferredTransform
                    
                } catch {
                    print(error)
                }
                
                totalVideoCompositionInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration)
            }
        }
        
        let mutableVideoComposition: AVMutableVideoComposition = AVMutableVideoComposition()
        mutableVideoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        mutableVideoComposition.renderSize = CGSize(width: 480, height: 640)
        
        if let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            let outputURL = URL(fileURLWithPath: documentsPath).appendingPathComponent("\("duetVideo").mov")
            
            do {
                if FileManager.default.fileExists(atPath: outputURL.path) {
                    
                    try FileManager.default.removeItem(at: outputURL)
                }
            } catch { }
            
            if let exportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality) {
                exportSession.outputURL = outputURL
                exportSession.outputFileType = AVFileType.mov
                exportSession.shouldOptimizeForNetworkUse = true
                
                // try to export the file and handle the status cases
                exportSession.exportAsynchronously(completionHandler: {
                    switch exportSession.status {
                    case .failed:
                        if let error = exportSession.error {
                            failure(error)
                        }
                        
                    case .cancelled:
                        if let error = exportSession.error {
                            failure(error)
                        }
                        
                    default:
                        print("finished")
                        success(outputURL)
                    }
                })
            } else {
                failure(nil)
            }
        }
    }
}

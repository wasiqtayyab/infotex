//
//  RecordingOptionsViewController.swift
//  Infotex
//
//  Created by Mac on 02/09/2021.
//

import UIKit

class RecordingOptionsViewController: UIViewController {
    
    
    //MARK:- Outlets
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    //MARK:- SetupView
    
    func setupView(){
        
    }
    
    //MARK:- Switch Action
    
    //MARK:- Button Action
    @IBAction func btnOptions(_ sender: UIButton) {
        if sender.tag == 0{
            print("Post")
            let vc =  storyboard?.instantiateViewController(withIdentifier: "ImageCaputreViewController") as! ImageCaputreViewController
            self.present(vc, animated: true, completion: nil)
        }else if sender.tag == 1 {
            print("Recording")
           let vc =  storyboard?.instantiateViewController(withIdentifier: "VideoRecorderViewController") as! VideoRecorderViewController
            videoTrim = 22.0
            isInfoTex =  false
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
    

            
        }else if sender.tag  == 2 {
            let vc =  storyboard?.instantiateViewController(withIdentifier: "VideoRecorderViewController") as! VideoRecorderViewController
            videoTrim = 1000
            isInfoTex =  true
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }else{
            print("Live")
        }
    }
    @IBAction func dimiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }    
}

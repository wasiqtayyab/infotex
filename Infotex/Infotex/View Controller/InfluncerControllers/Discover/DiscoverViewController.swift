//
//  DiscoverViewController.swift
//  Infotex
//
//  Created by Mac on 26/10/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import ContentLoader

class DiscoverViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegateFlowLayout {
    
    
    //MARK:- Outlets
    @IBOutlet weak var scrollViewOutlet: UIScrollView!
    @IBOutlet weak var discoverBannerCollectionView: UICollectionView!
    @IBOutlet weak var discoverTblView: UITableView!
    @IBOutlet var tblheight: NSLayoutConstraint!
    @IBOutlet weak var bannerPageController: UIPageControl!
    @IBOutlet weak var viewShimmer: ShimmerView!
    
    //MARK:- Variables
    var hashtagDataArr = [[String:Any]]()
    var sliderArr = [sliderMVC]()
    var videosArr = [videoMainMVC]()
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        
        return refreshControl
    }()
    
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        discoverTblView.refreshControl = refresher
        self.setupView()
    }
    
    //MARK:- Delegate Methods
    @objc func requestData() {
        print("requesting data")
        self.hashtagDataArr.removeAll()
        self.sliderArr.removeAll()
        getSliderData()
        self.getVideosData()
        let deadline = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.refresher.endRefreshing()
        }
    }
    
    //MARK:- SetupView
    
    func setupView(){
        tblheight.constant = CGFloat(hashtagDataArr.count * 190)
        
        self.discoverTblView.refreshControl = refresher
        self.bannerPageController.tintColor = .white
        view.bringSubviewToFront(self.bannerPageController)
        self.scrollViewOutlet.delegate =  self
        self.getSliderData()
        self.getVideosData()
        
    }
    
    //MARK: TableView

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  hashtagDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "newDiscoverTVC") as! newDiscoverTableViewCell
        cell.configure(videoData: hashtagDataArr[indexPath.row])
        cell.discoverCollectionView.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "hashtagsVideoVC") as! hashtagsVideoViewController
        vc.hashtag = hashtagDataArr[indexPath.row]["hashName"] as! String
        vc.totalVideo = hashtagDataArr[indexPath.row]["videos_count"] as! String
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
    }
    
    //MARK: CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sliderArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "newDiscoverBannerCVC", for: indexPath) as! newDiscoverBannerCollectionViewCell
        let sliderUrl = AppUtility?.detectURL(ipString: sliderArr[indexPath.row].img)
        cell.img.sd_setImage(with: URL(string:sliderUrl!), placeholderImage: UIImage(named:"videoPlaceholder"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.discoverBannerCollectionView.frame.size.width, height: 192)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == discoverBannerCollectionView{
            guard let url = URL(string: sliderArr[indexPath.row].url) else { return }
            UIApplication.shared.open(url)
       }
    }
    
    
    //MARK:- ScrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.bannerPageController.currentPage = Int(self.discoverBannerCollectionView.contentOffset.x) / Int(self.discoverBannerCollectionView.frame.width)
    }
    
    
    //MARK:- Button Actions
    @IBAction func btnScanQRCodeAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "QRScanViewController")as! QRScanViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "discoverSearchViewController") as! discoverSearchViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: false)
    }
    
    //MARK:- API Handler
    func getSliderData(){
        sliderArr.removeAll()
      
        ApiHandler.sharedInstance.showAppSlider{ (isSuccess, response) in
            
            if isSuccess{
              
                print("response: ",response?.allValues as Any)
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    
                    let sliderDataArr = response?.value(forKey: "msg") as! NSArray
                    
                    for i in 0..<sliderDataArr.count{
                        let sliderObj = sliderDataArr[i] as! NSDictionary
                        let appSlider = sliderObj.value(forKey: "AppSlider") as! NSDictionary
                        
                        let id = appSlider.value(forKey: "id") as! String
                        let img = appSlider.value(forKey: "image") as! String
                        let url = appSlider.value(forKey: "url") as! String
                        
                        let obj = sliderMVC(id: id, img: img, url: url)
                        
                        self.sliderArr.append(obj)
                    }
                 
                }
                
                self.discoverBannerCollectionView.reloadData()
                self.bannerPageController.numberOfPages = self.sliderArr.count
               
            }
        }
    }
    func getVideosData(){
        self.loader.isHidden =  false
        hashtagDataArr.removeAll()
        ApiHandler.sharedInstance.showDiscoverySections { (isSuccess, response) in
           self.loader.isHidden =  true
            if isSuccess{
                
                print("response: ",response?.allValues as Any)
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    let videosHashtags = response?.value(forKey: "msg") as? NSArray
            
                    print("videosHashtags.count: ",videosHashtags)
                    for i in 0 ..< (videosHashtags?.count ?? 0){
                        
                        let dic = videosHashtags?[i] as! NSDictionary
                        let hashtagsDic = dic.value(forKey: "Hashtag") as! NSDictionary
                        
                        let hashName =  hashtagsDic.value(forKey: "name") as! String
                        let views = hashtagsDic.value(forKey: "views") as! String
                        let videoCount = "\(hashtagsDic.value(forKey: "videos_count") as? NSNumber ?? 0)"
                        let videosObj = hashtagsDic.value(forKey: "Videos") as! NSArray
            
                        var videosArr = [videoMainMVC]()
                        videosArr.removeAll()
                        
                        for j in 0 ..< videosObj.count{
                            
                            let videosData = videosObj[j] as! NSDictionary
                            
                            let videoObj = videosData.value(forKey: "Video") as! NSDictionary
                            let userObj = videoObj.value(forKey: "User") as! NSDictionary
                            let soundObj = videoObj.value(forKey: "Sound") as? [String:Any]
                            
                            let videoUrl = videoObj.value(forKey: "video") as! String
                            let videoThum = videoObj.value(forKey: "thum") as! String
                            let videoGif = videoObj.value(forKey: "gif") as! String
                            let videoLikes = "\(videoObj.value(forKey: "like_count") ?? "")"
                            let videoComments = "\(videoObj.value(forKey: "comment_count") ?? "")"
                            let like = "\(videoObj.value(forKey: "like") ?? "")"
                            let allowComment = videoObj.value(forKey: "allow_comments") as! String
                            let videoID = videoObj.value(forKey: "id") as! String
                            let videoDesc = videoObj.value(forKey: "description") as! String
                            let allowDuet = videoObj.value(forKey: "allow_duet") as! String
                            let created = videoObj.value(forKey: "created") as! String
                            let views = "\(videoObj.value(forKey: "view") ?? "")"
                            let duetVidID = videoObj.value(forKey: "duet_video_id")
                            
                            let userID = userObj.value(forKey: "id") as! String
                            let username = userObj.value(forKey: "username") as! String
                            let userOnline = userObj.value(forKey: "online") as! String
                            let userImg = userObj.value(forKey: "profile_pic") as! String
                            //                        let followBtn = userObj.value(forKey: "button") as! String
                            let verified = userObj.value(forKey: "verified")
                            
                            let soundID = soundObj?["id"] as? String
                            let soundName = soundObj?["name"] as? String
                            let cdPlayer = soundObj?["thum"] as? String ?? ""
                            
                            
                            let video = videoMainMVC(videoID: videoID, videoUserID: "", fb_id: "", description: videoDesc, videoURL: videoUrl, videoTHUM: videoThum, videoGIF: videoGif, view: views, section: "", sound_id: "", privacy_type: "", allow_comments: allowComment, allow_duet: allowDuet, block: "", duet_video_id: "", old_video_id: "", created: created, like: like, favourite: "", comment_count: videoComments, like_count: videoLikes, followBtn: "", duetVideoID: "\(duetVidID!)", userID: userID, first_name: "", last_name: "", gender: "", bio: "", website: "", dob: "", social_id: "", userEmail: "", userPhone: "", password: "", userProfile_pic: "", role: "", username: username, social: "", device_token: "", videoCount: "", verified: "\(verified!)", soundName: soundName ?? "",CDPlayer: cdPlayer)
                            
                            videosArr.append(video)
                            
                            print("videoLikes: ",videoLikes)
                        }
                        
                        self.hashtagDataArr.append(["videosObj":videosArr,"hashName":hashName,"views":views,"videos_count":videoCount])
                        print("hastag: ",videosHashtags?[i])
                        self.tblheight.constant =  CGFloat(self.hashtagDataArr.count * 190)
                        self.discoverTblView.delegate  = self
                        self.discoverTblView.dataSource =  self
                        self.discoverTblView.reloadData()
                    }
                }else{
                  
                    self.showToast(message:response?.value(forKey: "msg") as! String, font: .systemFont(ofSize: 12))
                }
            }
        
            self.discoverTblView.reloadData()
           
        }
    }
}
extension DiscoverViewController: ContentLoaderDataSource {
    
    func numSections(in contentLoaderView: UIView) -> Int {
        return 1
    }
    
    func contentLoaderView(_ contentLoaderView: UIView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func contentLoaderView(_ contentLoaderView: UIView, cellIdentifierForItemAt indexPath: IndexPath) -> String {
        if contentLoaderView == discoverBannerCollectionView {
            return "newDiscoverBannerCVC"
        }else{
           return "newDiscoverTVC"
        }
        return "newDiscoverTVC"
    }
}

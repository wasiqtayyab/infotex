//
//  newDiscoverTableViewController.swift
//  TIK TIK
//
//  Created by Mac on 26/10/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import SDWebImage

class newDiscoverTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    //MARK:- Outlets
    
    @IBOutlet weak var discoverCollectionView: UICollectionView!
    @IBOutlet weak var lblItemCount: UILabel!
    @IBOutlet weak var hashName : UILabel!
    @IBOutlet weak var hashNameSub : UILabel!
    @IBOutlet weak var viewCountVideo: UIView!
    @IBOutlet weak var arrow: UIImageView!
    
    var videosObj = [videoMainMVC]()
    
    
    //MARK:- awakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    //MARK:- SetupView
    
    func setupView(){
        self.viewCountVideo.layer.cornerRadius = 5
    }
    
    //MARK:- Configuration
    
    func configure(videoData: [String:Any]){
        
        self.hashName.text = videoData["hashName"] as! String
        self.hashNameSub.text = "Trending Hashtags"
        self.videosObj = videoData["videosObj"] as! [videoMainMVC]
        self.lblItemCount.text = "\(videoData["videos_count"] as! String)"
    }
    
    
    //MARK: CollectionView.
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.videosObj.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier:"newDiscoverCVC" , for: indexPath) as! newDiscoverCollectionViewCell
        
        let vidObj = videosObj[indexPath.row]
        cell.img.sd_setImage(with: URL(string:vidObj.videoGIF), placeholderImage: UIImage(named: "videoPlaceholder"))
        
        let gifURL : String = (AppUtility?.detectURL(ipString: vidObj.videoGIF))!
        cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.img.sd_setImage(with: URL(string:(gifURL)), placeholderImage: UIImage(named:"videoPlaceholder"))
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.discoverCollectionView.frame.size.width/4, height: 130)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "influncer", bundle: nil)
            let vc =  storyMain.instantiateViewController(withIdentifier: "HomeVideoViewController") as! HomeVideoViewController
            vc.videosMainArr =  self.videosObj
            vc.isOtherController = true
            vc.currentIndex = indexPath
            vc.hidesBottomBarWhenPushed = true
            rootViewController.navigationController?.pushViewController(vc, animated: true)
        }
        print("videosObj.count",videosObj.count)
        print(indexPath.row)
        
    }
}

//
//  postViewController.swift
//  InfoTex
//
//  Created by Mac on 28/08/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation
import Photos

class PostVideoViewController: UIViewController,UITextViewDelegate {
    
    
    //MARK:- Outlets
    
    @IBOutlet weak var vidThumbnail: UIImageView!
    @IBOutlet weak var describeTextView: AttrTextView!
    @IBOutlet weak var lblTextCount: UILabel!
    
    //MARK:- Variables
    
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    var myUser:[User]? {didSet{}}
    var videoUrl:URL?
    var desc = ""
    var allowDuet = "1"
    var allowComments = "true"
    var duet = "1"
    var soundId = "null"
    var saveV = "1"
    
    
    var boxView = UIView()
    var blurView = UIView()
    
    var hashTagsArr = [String]()
    var userTagsArr = [String]()
    
    //Edit Post
    weak var delegate: videoLikeDelegate?
    var isEditPost = false
    var videoMain:videoMainMVC!
    var currentIndex = 0

    //MARK:- viewDidLoad
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("videoURL",videoUrl)
        self.myUser = User.readUserFromArchive()
        
        self.describeTextView.layer.cornerRadius =  5
        self.describeTextView.text = "write a Caption"
        self.describeTextView.textColor = UIColor.lightGray
        self.describeTextView.delegate =  self
        self.getThumbnailImageFromVideoUrl(url: videoUrl!) { (thumb) in
            self.vidThumbnail.image = thumb
        }
        if isEditPost == true{
            self.describeTextView.text = self.videoMain.description
        }
    }
    //MARK:- TextView
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
            textView.text = .none
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "write a Caption"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        self.lblTextCount.text = "\(self.describeTextView.text!.count)/150"
        
        describeTextView.setText(text: describeTextView.text,textColor: .black, withHashtagColor: #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1), andMentionColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), andCallBack: { (strng, type) in
            print("type: ",type)
            print("strng: ",strng)
            
            
        }, normalFont: .systemFont(ofSize: 14, weight: UIFont.Weight.light), hashTagFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold), mentionFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold))
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let rangeOfTextToReplace = Range(range, in: textView.text) else {
            return false
        }
        let substringToReplace = textView.text[rangeOfTextToReplace]
        let count = textView.text.count - substringToReplace.count + text.count
        return count <= 150
    }
    
    //MARK:- API Handler
    
    func uploadData(){
        
        let hashtags = describeTextView.text.hashtags()
        let mentions = describeTextView.text.mentions()
        
        var newHashtags = [[String:String]]()
        var newMentions = [[String:String]]()
        
        for hash in hashtags{
            newHashtags.append(["name":hash])
        }
        for mention in mentions{
            newMentions.append(["name":mention])
        }
        
        
        if(UserDefaults.standard.string(forKey: "sid") == nil || UserDefaults.standard.string(forKey: "sid") == ""){
            
            UserDefaults.standard.set("null", forKey: "sid")
        }
        
        let url : String = ApiHandler.sharedInstance.baseApiPath+"postVideo"
        
        let cmnt = self.allowComments
        let allwDuet = self.allowDuet
        var des = self.desc
        if describeTextView.text != "write a Caption" {
            des = describeTextView.text
        }else{
            des = ""
        }
        
        print("cmnt",cmnt)
        print("allwDuet",allwDuet)
        
        print("des",des)
        print("hashtags",hashtags)
        print("mentions",mentions)
        
        
        let parameter :[String:Any]? = ["user_id"       : (self.myUser?[0].Id)!,
                                        "sound_id"      : self.soundId,
                                        "privacy_type"  : "public",
                                        "description"   : des,
                                        "allow_comments": cmnt,
                                        "allow_duet"    : allwDuet,
                                        "users_json"    : newMentions,
                                        "hashtags_json" : newHashtags,
                                        "video_id"      : 0,
                                        "duet"          : self.duet
        ]
        print(url)
        print(parameter!)
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        let serializer = DataResponseSerializer(emptyResponseCodes: Set([200, 204, 205]))
        
        self.loader.isHidden = false
        
        AF.upload(multipartFormData: { MultipartFormData in
            
            if (!JSONSerialization.isValidJSONObject(parameter)) {
                print("is not a valid json object")
                return
            }
            for key in parameter!.keys{
                let name = String(key)
                print("key",name)
                if let val = parameter![name] as? String{
                    MultipartFormData.append(val.data(using: .utf8)!, withName: name)
                }
            }
            print(self.videoUrl!)
            MultipartFormData.append(self.videoUrl!, withName: "video")
            
            
        }, to: url, method: .post, headers: headers)
        
        .uploadProgress(closure: { (progress) in
            
            print("Upload Progress: \(progress.fractionCompleted)")
            
            let per  =  progress.fractionCompleted
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "uploadVideo"), object: per)
            let vc =  self.storyboard?.instantiateViewController(withIdentifier: "UploadingVideoViewController") as! UploadingVideoViewController
            self.present(vc, animated: true, completion: nil)
        })
        
        .responseJSON { (response) in
            self.loader.isHidden = true
            switch response.result{
          
            case .success(let value):
                print("progress: ",Progress.current())
                let json = value
                let dic = json as! NSDictionary
                
                print("response:- ",response)
                if(dic["code"] as! NSNumber == 200){
                    print("200")
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                    
                }else{
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                    
                    print(dic)
                    
                }
            case .failure(let error):
                print("\n\n===========Error===========")
                print("Error Code: \(error._code)")
                print("Error Messsage: \(error.localizedDescription)")
                if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                    print("Server Error: " + str)
                }
                AppUtility?.displayAlert(title: "Error", messageText: error.localizedDescription, delegate: self)
                print("===========================\n\n")
                
            }
        }
    }
    /*
        .responseJSON { (response) in
            self.loader.isHidden = true
            switch response.result{
            
            case .success(let value):
            
                
                print("progress: ",Progress.current())
                
                print("Upload Progress With Int: \(Progress.current())")
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "uploadVideo"), object: Progress.current())
                let json = value
                let dic = json as! NSDictionary
                self.loader.isHidden = true

                print("response:- ",response)
                if(dic["code"] as! NSNumber == 200){
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                }else{
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                    print(dic)
                    
                }
            case .failure(let error):
                self.loader.isHidden = true
                

                print("\n\n===========Error===========")
                print("Error Code: \(error._code)")
                print("Error Messsage: \(error.localizedDescription)")
                if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                    print("Server Error: " + str)
                }
                debugPrint(error as Any)
                print("===========================\n\n")
                
            }
        }
    }*/
    
    
    func editPost(){
        self.loader.isHidden =  false
        let hashtags = describeTextView.text.hashtags()
        let mentions = describeTextView.text.mentions()
        
        var newHashtags = [[String:String]]()
        var newMentions = [[String:String]]()
        
        for hash in hashtags{
            newHashtags.append(["name":hash])
        }
        for mention in mentions{
            newMentions.append(["name":mention])
        }
        
        let cmnt = self.allowComments
        let allwDuet = self.allowDuet
        var des = self.desc
        if describeTextView.text != "write a Caption" {
            des = describeTextView.text
        }else{
            des = ""
        }
        
        print("cmnt",cmnt)
        print("allwDuet",allwDuet)

        print("des",des)
        print("hashtags",hashtags)
        print("mentions",mentions)
        
        
        let parameter :[String:Any]? = ["user_id"       : (self.myUser?[0].Id)!,
                                        "sound_id"      : self.soundId,
                                        "privacy_type"  : "public",
                                        "description"   : des,
                                        "allow_comments": cmnt,
                                        "allow_duet"    : allwDuet,
                                        "users_json"    : newMentions,
                                        "hashtags_json" : newHashtags,
                                        "video_id"      : self.videoMain.videoID,
                                        "duet"          : self.duet
        ]
        print(parameter!)
        let url : String = ApiHandler.sharedInstance.baseApiPath+"editVideo"
        AF.upload(multipartFormData: { MultipartFormData in
            
            if (!JSONSerialization.isValidJSONObject(parameter)) {
                print("is not a valid json object")
                return
            }
            for key in parameter!.keys{
                let name = String(key)
                print("key",name)
                if let val = parameter![name] as? String{
                    MultipartFormData.append(val.data(using: .utf8)!, withName: name)
                }
            }
        }, to: url, method: .post, headers: headers)
        .responseJSON { (response) in
            self.loader.isHidden = true
            switch response.result{
            
            case .success(let value):
                print("progress: ",Progress.current())
                let json = value
                let dic = json as! NSDictionary
                
                print("response:- ",response)
                if(dic["code"] as! NSNumber == 200){
                    print("200")
                    self.loader.isHidden =  true
                    var obj = self.videoMain
                    var strDscr  =   obj!.description
                    strDscr =  des
                    obj!.allow_comments = cmnt
                    obj!.allow_duet = allwDuet
                    self.delegate!.updateObj(obj: obj!, index: self.currentIndex, islike: false)
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                    
                    
                }else{
                    self.loader.isHidden =  true
                  //  AppUtility?.stopLoader(view: self.view)
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                    print(dic)
                    
                }
            case .failure(let error):
                self.loader.isHidden =  true
                //     AppUtility?.stopLoader(view: self.view)
                print("\n\n===========Error===========")
                print("Error Code: \(error._code)")
                print("Error Messsage: \(error.localizedDescription)")
                if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                    print("Server Error: " + str)
                }
                debugPrint(error as Any)
                print("===========================\n\n")
                
            }
        }
    }

    
    
    //MARK:- Button Actions
    
    @IBAction func btnPost(_ sender: Any) {
        if self.isEditPost == false{
            
            self.uploadData()
            let story:UIStoryboard = UIStoryboard(name: "influncer", bundle: nil)
            let vc  =  storyboard?.instantiateViewController(withIdentifier: "UploadingVideoViewController") as! UploadingVideoViewController
            self.present(vc, animated: true, completion: nil)
        }else{
            self.editPost()
        }
       
    }
    @IBAction func commentSwitch(_ sender: UISwitch) {
        if sender.isOn {
            self.allowComments = "true"
        }else{
            self.allowComments = "false"
        }
    }
    
    @IBAction func duetSwitch(_ sender: UISwitch) {
        if sender.isOn {
            self.allowDuet = "1"
        }else{
            self.allowDuet = "0"
        }
    }
    
    @IBAction func btnHashtag(_ sender: UISwitch) {
        
        guard self.describeTextView.text != "write a Caption" else {return}
        
        self.describeTextView.setText(text: describeTextView.text+" #",textColor: .black, withHashtagColor: #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1), andMentionColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), andCallBack: { (strng, type) in
            print("type: ",type)
            print("strng: ",strng)
        }, normalFont: .systemFont(ofSize: 14, weight: UIFont.Weight.light), hashTagFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold), mentionFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold))
        
    }
    
    @IBAction func btnMention(_ sender: UISwitch) {
        
        guard self.describeTextView.text != "write a Caption" else {return}
        
        self.describeTextView.setText(text: describeTextView.text+" @",textColor: .black, withHashtagColor: #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1), andMentionColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), andCallBack: { (strng, type) in
            print("type: ",type)
            print("strng: ",strng)
        }, normalFont: .systemFont(ofSize: 14, weight: UIFont.Weight.light), hashTagFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold), mentionFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold))
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    //MARK:- Functions
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbNailImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
    
    
    func showShimmer(progress: String){
        
        //        boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
        boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 70, width: 180, height: 50))
        boxView.backgroundColor = UIColor.white
        boxView.alpha = 0.8
        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        let activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.gray
        textLabel.text = progress
        
        blurView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.3)
        let blurView = UIView(frame: UIScreen.main.bounds)
        
        
        boxView.addSubview(blurView)
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        
        view.addSubview(boxView)
    }
    
    func HideShimmer(){
        boxView.removeFromSuperview()
    }
    func dictToJSON(dict:[String: AnyObject]) -> AnyObject {
        let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        return jsonData as AnyObject
    }
    
}

extension Dictionary {
    var jsonStringRepresentation: String? {
        guard let theJSONData = try? JSONSerialization.data(withJSONObject: self,
                                                            options: [.prettyPrinted]) else {
            return nil
        }
        
        return String(data: theJSONData, encoding: .ascii)
    }
}



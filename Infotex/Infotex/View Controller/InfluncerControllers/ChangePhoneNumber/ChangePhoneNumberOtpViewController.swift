//
//  ChangePhoneNumberOtpViewController.swift
//  Infotex
//
//  Created by Wasiq Tayyab on 01/10/2021.
//

import UIKit
import DPOTPView

class ChangePhoneNumberOtpViewController: UIViewController {

    //MARK:- OUTLET
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var viewOtp: DPOTPView!
    @IBOutlet weak var viewGradient: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var lblTimer: UILabel!
    
    //MARK:- VARS
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    var counter = 60
    var timer = Timer()

    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
    
        viewGradient.isHidden = true
        btnNext.backgroundColor = UIColor(named: "light shaft")
        lblEmail.text = "Your code was sent to \((phone_number))"
        viewOtp.dpOTPViewDelegate = self
        btnResend.isHidden = true
        timerSetup()
        
    }
    
    //MARK:- TIMER
    
    private func timerSetup(){
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounter() {
      
        if counter !=  0 {
            lblTimer.text = "Resend Code 00:\(counter)"
            counter -= 1
        }else {
            
            self.counter = 60
            timer.invalidate()
            lblTimer.isHidden = true
            btnResend.isHidden = false
        }
    }
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func resendCodeButtonPressed(_ sender: UIButton) {
        self.phoneNumberApi()
       
    }
    
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        if viewOtp.text!.count < 4 {
            AppUtility?.showToast(string: "Enter your valid otp", view: self.view)
            return
        }
        self.verifyPhoneNumber()
       
        
    }
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- API
    
    private func phoneNumberApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.changePhoneNumber(user_id: user_id, phone: phone_number) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                if resp?.value(forKey: "code") as! NSNumber == 200{
                  
                    self.timerSetup()
                    self.viewOtp.text! = ""
                    self.btnResend.isHidden = true
                    self.lblTimer.isHidden = false
                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
            
        }
        
    }
    
    private func verifyPhoneNumber(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.verifyPhoneNo(phone: phone_number, verify: "1", code: viewOtp.text!, IsVerfied: "1") { (isSuccess, resp) in
            if isSuccess {
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    self.editProfile()

                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    private func editProfile(){
       
        ApiHandler.sharedInstance.editProfile(user_id: user_id, phone: phone_number) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                if resp?.value(forKey: "code") as! NSNumber == 200{

                    let userObj = resp!["msg"] as! [String:Any]
                    UserObject.shared.Objresponse(response: userObj,isLogin: false)
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: ManageAccountViewController.self) {
                            _ =  self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }

                    
                }else{
                    
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    
}


//MARK:- Textfield Delegate

extension ChangePhoneNumberOtpViewController: DPOTPViewDelegate {
    
    func dpOTPViewAddText(_ text: String, at position: Int) {
       
        if position <= 3 && text.count == 4{
            
            viewGradient.isHidden = false
            btnNext.backgroundColor = .clear
            viewOtp.dismissOnLastEntry = true
            
        }else {
            viewGradient.isHidden = true
            btnNext.backgroundColor = UIColor(named: "light shaft")
            viewOtp.dismissOnLastEntry = true
            
        }
        
    }
    
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        
        if position <= 3 && text.count == 4{
            
            viewGradient.isHidden = false
            btnNext.backgroundColor = .clear
            viewOtp.dismissOnLastEntry = true
            
        }else {
            viewGradient.isHidden = true
            btnNext.backgroundColor = UIColor(named: "light shaft")
            viewOtp.dismissOnLastEntry = true
            
        }
        
        
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
        
        
        
    }
    
    func dpOTPViewBecomeFirstResponder() {
        
    }
    func dpOTPViewResignFirstResponder() {
        
        
    }
    


}

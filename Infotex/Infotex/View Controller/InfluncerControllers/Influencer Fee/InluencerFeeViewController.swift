//
//  InluencerFeeViewController.swift
//  Infotex
//
//  Created by Mac on 03/09/2021.
//

import UIKit

class InluencerFeeViewController: UIViewController {
    
    //MARK:- OUTLET

    @IBOutlet weak var btnInfluencerFee: UIButton!
    
    @IBOutlet weak var lblFee: UILabel!
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        lblFee.text = "If you dont't have an agency.\nThe application fee must be paid."
    
    }
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
}

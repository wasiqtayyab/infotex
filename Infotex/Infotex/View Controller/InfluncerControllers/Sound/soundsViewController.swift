//
//  soundsViewController.swift
//  infotex
//
//  Created by Mac on 16/11/2021.
//  Copyright © 2021 Mac. All rights reserved.
//

import UIKit
import SDWebImage


struct soundSectionsStruct {
    
    var secName:String
    var sectionObjects = [[String:Any]]()
}

class soundsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    //MARK:- Outlets
   
    @IBOutlet weak var favSoundsTblView: UITableView!
    @IBOutlet weak var lblFetching: UILabel!
    @IBOutlet weak var btnDiscover: UIButton!
    @IBOutlet weak var btnFav: UIButton!
    
    @IBOutlet weak var btnDiscoverBottomView: UIView!
    @IBOutlet weak var btnFavBottomView: UIView!
    var myUser:[User]? {didSet{}}
    //MARK:- Variables
    
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    var soundsDataArr = [[String:Any]]()
    var favSoundDataArr = [soundsMVC]()
    var soundSecArr = [soundSectionsStruct]()
    var isFvt = false
    var startingPoint = "0"
   
    
    var objectArray = [soundSectionsStruct]()
    var objSound = [[String:Any]]()
    var favSoundArr = [[String:Any]]()
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .black
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        
        return refreshControl
    }()
    
    
   //MARK:- Delegates
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myUser =  User.readUserFromArchive()
        self.isFvt =  false
        favSoundsTblView.delegate = self
        favSoundsTblView.dataSource = self
       
        
        btnFavBottomView.backgroundColor = .lightGray
        btnFav.setTitleColor(.lightGray, for: .normal)
        
        btnDiscoverBottomView.backgroundColor = .white
        btnDiscover.setTitleColor(.white, for: .normal)
        setupView()
        GetAllSounds()
        
        favSoundsTblView.register(UINib(nibName: "TblHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "TblHeader")

        self.favSoundsTblView.tableFooterView = UIView()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.dissmissControllerNoti(notification:)), name: Notification.Name("dismissController"), object: nil)
    }
    
    @objc
    func requestData() {
        print("requesting data")
        
        
        let deadline = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.refresher.endRefreshing()
        }
    }
    
    @objc func dissmissControllerNoti(notification: Notification) {
        
        print("dissmissControllerNoti received")
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 10.0, *) {
            favSoundsTblView.refreshControl = refresher
        } else {
            favSoundsTblView.addSubview(refresher)
        }
        
    }
    
    //MARK:- Button Actions
    
    @IBAction func btnDiscoverAction(_ sender: Any) {
        
        TPGAudioPlayer.sharedInstance().player.pause()
        
        btnFavBottomView.backgroundColor = .lightGray
        btnFav.setTitleColor(.lightGray, for: .normal)
        
        
        btnDiscover.setTitleColor(.white, for: .normal)
        btnDiscoverBottomView.backgroundColor = .white
        
        GetAllSounds()
        self.isFvt = false
    }
    
    @IBAction func btnFavAction(_ sender: Any) {
        self.favSoundsTblView.isHidden =  false
        TPGAudioPlayer.sharedInstance().player.pause()
        
        btnFavBottomView.backgroundColor = .white
        btnFav.setTitleColor(.white, for: .normal)
        
        
        btnDiscover.setTitleColor(.lightGray, for: .normal)
        btnDiscoverBottomView.backgroundColor = .lightGray
        
        getFavSounds()

        self.isFvt = true
    }
    
    //MARK:- SetupView
    
    func setupView(){
        
        print("soundsDataArr.count * 300: ",favSoundArr.count * 300)
        
    }
    
    //MARK: TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        if isFvt == false{
            print("soundSecArr",self.soundSecArr.count)
            return self.objectArray.count
        }else{
            return 1
        }
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if isFvt == false{
           
            return self.objectArray[section].sectionObjects.count
            
        }else{
            return self.favSoundArr.count
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == favSoundsTblView{
        let visiblePaths = favSoundsTblView.indexPathsForVisibleRows
        for i in visiblePaths!  {
            let visiblePaths = favSoundsTblView.indexPathsForVisibleRows
            for i in visiblePaths!  {
                let cell = favSoundsTblView.cellForRow(at: i) as? favSoundsTableViewCell
                cell?.playImg.image = UIImage(named: "ic_play_icon")
                cell?.btnSelect.isHidden = true
            }
            
        }
    }
    
}
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == favSoundsTblView{
            let obj = favSoundDataArr[indexPath.row]
            loadAudio(audioURL: (AppUtility?.detectURL(ipString: obj.audioURL))!, ip: indexPath)
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let favSoundCell =  tableView.dequeueReusableCell(withIdentifier: "favSoundsTVC") as! favSoundsTableViewCell
     
       if isFvt  == false {
            
            let favObj =  self.objectArray[indexPath.section].sectionObjects[indexPath.row]
            
            favSoundCell.soundName.text! =  favObj["name"] as! String
            favSoundCell.soundDesc.text! =  favObj["description"] as! String
            favSoundCell.duration.text    = favObj["duration"] as! String
            
            favSoundCell.soundImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
            favSoundCell.soundImg.sd_setImage(with: URL(string:(AppUtility?.detectURL(ipString: favObj["thum"] as! String))!), placeholderImage: UIImage(named:"videoPlaceholder"))
            if favObj["favourite"] as! NSNumber == 1{
                favSoundCell.btnFav.setImage(UIImage(named: "saved_done"), for: .normal)
            }else{
                favSoundCell.btnFav.setImage(UIImage(named:"saved_white"), for: .normal)
            }
            
            favSoundCell.btnFav.addTarget(self, action: #selector(soundsViewController.btnSoundFavAction(_:)), for:.touchUpInside)
            favSoundCell.btnSelect.addTarget(self, action: #selector(soundsViewController.btnSelectAction(_:)), for:.touchUpInside)
            favSoundCell.btnSelect.isHidden = true
            
      }else{
     /*   let favObj =  self.favSoundArr[indexPath.row]
        
        favSoundCell.soundName.text! =  favObj["name"] as! String
        favSoundCell.soundDesc.text! =  favObj["description"] as! String
        favSoundCell.duration.text    = favObj["duration"] as! String
        
        favSoundCell.soundImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        favSoundCell.soundImg.sd_setImage(with: URL(string:(AppUtility?.detectURL(ipString: favObj["thum"] as! String))!), placeholderImage: UIImage(named:"videoPlaceholder"))
        if favObj["favourite"] as! NSNumber == 1{
            favSoundCell.btnFav.setImage(UIImage(named: "saved_done"), for: .normal)
        }else{
            favSoundCell.btnFav.setImage(UIImage(named:"saved_white"), for: .normal)
        }
        
        favSoundCell.btnFav.addTarget(self, action: #selector(soundsViewController.btnSoundFavAction(_:)), for:.touchUpInside)
        favSoundCell.btnSelect.addTarget(self, action: #selector(soundsViewController.btnSelectAction(_:)), for:.touchUpInside)
        favSoundCell.btnSelect.isHidden = true*/
        }
       
            
            return favSoundCell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
        if isFvt == false{
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TblHeader") as? TblHeader
            header?.lblTitle.text = self.objectArray[section].secName as! String
            header?.btn.isHidden =  false
            return header
        }else{
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0 , height: 0))
            return headerView
        }
      
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return  30
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        
        return 30
        
    }
    func loadAudio(audioURL: String,ip:IndexPath) {
        
        let cell = favSoundsTblView.cellForRow(at: ip) as! favSoundsTableViewCell
        
        let kTestImage = "26"
        
        let dictionary: Dictionary <String, AnyObject> = SpringboardData.springboardDictionary(title: "audioP", artist: "audioP Artist", duration: Int (300.0), listScreenTitle: "audioP Screen Title", imagePath: Bundle.main.path(forResource: "Spinner-1s-200px", ofType: "gif")!)
        
        cell.playImg.isHidden = true
        cell.loadIndicator.startAnimating()

        TPGAudioPlayer.sharedInstance().playPauseMediaFile(audioUrl: URL(string: audioURL)! as NSURL, springboardInfo: dictionary, startTime: 0.0, completion: {(_ , stopTime) -> () in
          
            cell.playImg.isHidden = false
            cell.loadIndicator.stopAnimating()
            self.updatePlayButton(ip: ip)
            
            print("there",stopTime)
        } )
        
        
    }
 
 
    
    func updatePlayButton(ip:IndexPath) {
        
        let cell = favSoundsTblView.cellForRow(at: ip) as! favSoundsTableViewCell
        
        let playPauseImage = (TPGAudioPlayer.sharedInstance().isPlaying ? UIImage(named: "ic_pause_icon") : UIImage(named: "ic_play_icon"))
        
        cell.btnSelect.isHidden = TPGAudioPlayer.sharedInstance().isPlaying ? false : true
        cell.playImg.image = playPauseImage
    }

    //MARK:- Btn fav sound Action
    
        @objc func btnSoundFavAction(_ sender : UIButton) {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.favSoundsTblView)
            let indexPath = self.favSoundsTblView.indexPathForRow(at: buttonPosition)
            let cell = self.favSoundsTblView.cellForRow(at: indexPath!) as! favSoundsTableViewCell
            
            let btnFavImg = cell.btnFav.currentImage
            
            if btnFavImg == UIImage(named: "btnFavEmpty"){
                cell.btnFav.setImage(UIImage(named: "btnFavFilled"), for: .normal)
            }else{
                cell.btnFav.setImage(UIImage(named: "btnFavEmpty"), for: .normal)
            }
            
            let obj = favSoundDataArr[indexPath!.row]
            
            addFavSong(soundID: obj.id, btnFav: cell.btnFav)

        }
        
    //    MARK:- Btn select Action
    @objc func btnSelectAction(_ sender : UIButton) {
        
        TPGAudioPlayer.sharedInstance().player.pause()
        print("btnSelect Tapped")
        let newObj = favSoundDataArr[sender.tag]
        
        UserDefaults.standard.set(newObj.audioURL, forKey: "url")
        UserDefaults.standard.set(newObj.name, forKey: "selectedSongName")
        
        if let audioUrl = URL(string: (AppUtility?.detectURL(ipString: newObj.audioURL))!) {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                print("The file already exists at path")
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: Notification.Name("loadAudio"), object: nil)
                    self.dismiss(animated: true, completion: nil)
                    
                }
            } else {
                
                // you can use NSURLSession.sharedSession to download the data asynchronously
                URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                    guard let location = location, error == nil else { return }
                    do {
                        // after downloading your file you need to move it to your destination url
                        try FileManager.default.moveItem(at: location, to: destinationUrl)
                        print("File moved to documents folder @ loc: ",location)
                        DispatchQueue.main.async {
                            
                            NotificationCenter.default.post(name: Notification.Name("loadAudio"), object: nil)
                            self.dismiss(animated: true, completion: nil)
                        }
                        
                        
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }
                    
                }).resume()
            }
        }
        
    }
    
//    MARK:- BTN ALL ACTION
    @objc func btnAllAction(_ sender : UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.favSoundsTblView)
        let indexPath = self.favSoundsTblView.indexPathForRow(at:buttonPosition)
        let cell = self.favSoundsTblView.cellForRow(at: indexPath!) as! soundsTableViewCell

        let obj = soundSecArr[indexPath!.row]
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "allSoundsVC") as! allSoundsViewController
      //  vc.sectionID = obj.secID
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        print("soundsDataArr.count: ",soundsDataArr.count*100)
//
//        if tableView == favSoundsTblView{
//            return 80.0
//        }
//
//        if tableView == favSoundsTblView{
//            let soundObj = soundsDataArr[indexPath.row]["soundObj"] as! [soundsMVC]
//            for i in 0 ..< soundsDataArr.count{
//                if indexPath.row == i{
//                    if soundObj.count >= 3{
//                        return 320
//                    }else{
//                        print("soundObj.count*108: ",soundObj.count*130)
//                        return (CGFloat(soundObj.count*130))
//                    }
//                }
//            }
//        }
//        return 180.0
//    }
    
    
//Mark:- Favorite sounds API
    func getFavSounds(){
        
        favSoundDataArr.removeAll()
        
       // AppUtility?.startLoader(view: self.view)
        self.lblFetching.isHidden = false
        self.loader.isHidden = false
        ApiHandler.sharedInstance.showSounds(user_id:  (self.myUser?[0].Id)!, starting_point: startingPoint) { (isSuccess, response) in
            self.loader.isHidden = true
            self.lblFetching.isHidden = true
            if isSuccess{
               /* if (response?.value(forKey: "code") as! NSNumber) == 200{
                    let msgDic = response?.value(forKey: "msg") as! NSArray
                    
                    for dic in msgDic{
                        let soundDic = dic as! NSDictionary
                        let soundObj = soundDic.value(forKey: "Sound") as! NSDictionary
                        
                        
                      /*  let id = soundObj.value(forKey: "id") as! String
                        let audioUrl = soundObj.value(forKey: "audio") as! String
                        let duration = soundObj.value(forKey: "duration") as! String
                        let name = soundObj.value(forKey: "name") as! String
                        let description = soundObj.value(forKey: "description") as! String
                        let thum = soundObj.value(forKey: "thum") as! String
                        let section = soundObj.value(forKey: "sound_section_id") as! String
                        let uploaded_by = soundObj.value(forKey: "uploaded_by") as! String
                        let created = soundObj.value(forKey: "created") as! String
                        //                        let favourite = soundObj.value(forKey: "favourite")
                        let publish = soundObj.value(forKey: "publish") as! String
                        
                        let obj = soundsMVC(id: id, audioURL: audioUrl, duration: duration, name: name, description: description, thum: thum, section: section, uploaded_by: uploaded_by, created: created, favourite: "", publish: publish)*/
                      
                    }
                  //  AppUtility?.stopLoader(view: self.view)*/
                
                if response?.value(forKey: "code") as! NSNumber == 200{
                    let soundMsg = response?.value(forKey: "msg") as! NSArray
                    
                    print("soundMsg.count: ",soundMsg.count)
                    for i in 0 ..< soundMsg.count{
                        let dic = soundMsg[i] as! NSDictionary
                        let soundsObj = dic.value(forKey: "Sound") as! [[String:Any]]
                        for obj in soundsObj{
                            self.favSoundArr.append(obj)
                        }
                        print("favSoundArr",self.favSoundArr)

                    }
                    self.favSoundsTblView.reloadData()
                }else{
                    print("!200: ",response?.value(forKey: "msg")!)
                }
                self.setupView()
            }
        }
    }
    
    func GetAllSounds(){
        
        soundsDataArr.removeAll()
      
        var arrSectionName = [[String:Any]]()
        
        self.lblFetching.isHidden = false
        ApiHandler.sharedInstance.showSounds(user_id:  (self.myUser?[0].Id)!, starting_point: startingPoint) { [self] (isSuccess, response) in
            self.lblFetching.isHidden = true
            if isSuccess{
                if response?.value(forKey: "code") as! NSNumber == 200{
                    let soundMsg = response?.value(forKey: "msg") as! NSArray
                    
                    print("soundMsg.count: ",soundMsg.count)
                    for i in 0 ..< soundMsg.count{
                        let dic = soundMsg[i] as! NSDictionary
                        let soundSecDict = dic.value(forKey: "SoundSection") as! NSDictionary
                        let sectionID = soundSecDict.value(forKey: "id") as! String
                        let sectionName = soundSecDict.value(forKey: "name") as! String
                        
                        arrSectionName.append(["sectionName":sectionName,"sectionID":sectionID])
                        
                        let soundsObj = dic.value(forKey: "Sound") as! [[String:Any]]
                        for obj in soundsObj{
                            self.objSound.append(obj)
                        }
                        print("objSound",objSound)
                        print("sectionname  ",sectionName)
                        

                    }

                    self.objectArray.removeAll()
                    
                    for objSec in arrSectionName{
                        for var i in 0..<self.objSound.count{
                            if objSec["sectionID"] as! String == self.objSound[i]["sound_section_id"] as! String{
                                var obj =  self.objSound[i]
                                obj.updateValue(objSec["sectionName"] as! String, forKey: "sec_name")
                                self.objSound.remove(at: i)
                                self.objSound.insert(obj, at: i)
                               break
                            }
                        }
                     }
              
                        print(self.objSound.map{$0["sec_name"]})
                        var section = self.objSound.map{$0["sec_name"] as? String}
                        section.removeDuplicates()
                        self.objectArray = section.map{ category in
                            print("Cat: ", category)
                            let post = self.objSound.filter{ ($0["sec_name"] as! String) == category }
                            return soundSectionsStruct(secName: category!, sectionObjects: post)
                            self.favSoundsTblView.reloadData()
                        }
                    
                   
                    
                }else{
                    print("!200: ",response as! Any)
                }
                self.favSoundsTblView.reloadData()
            }
        }
    }
    
//MARK:- ADD FAV SOUND FUNC API
    func addFavSong(soundID:String,btnFav:UIButton){
        
        ApiHandler.sharedInstance.addSoundFavourite(user_id: (self.myUser?[0].Id)!, sound_id: soundID) { (isSuccess, response) in
            if isSuccess{
                if response?.value(forKey: "code") as! NSNumber == 200{
                    print("msg: ",response?.value(forKey: "msg")!)
                    
                    if btnFav.currentImage == UIImage(named: "btnFavEmpty"){
                        AppUtility!.showToast(string: "Removed From Favorite", view:self.view)
                    }
                    
                }else{
                    print("!200: ",response)
                }
            }
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

//MARK:- remove duplicate elements in Array
extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}

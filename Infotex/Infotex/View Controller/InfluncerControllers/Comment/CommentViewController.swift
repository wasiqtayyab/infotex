//
//  CommentViewController.swift
//  Infotex
//
//  Created by Mac on 08/09/2021.
//

import UIKit
import SDWebImage
import GrowingTextView


class CommentViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    //MARK:- Outlets
    
    @IBOutlet weak var tblComment: UITableView!
    @IBOutlet weak var tfGowingTextField: GrowingTextView!
    @IBOutlet weak var imgProfile: CustomImageView!
    var myUser:[User]? {didSet{}}
    var sections: [Section] = []
    var Video_Id = "0"
    var startingPoint = 0
    var commentID = "0"
    private(set) var liked_count:Int! = 0
    private(set) var liked = false
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    //MARK:- SetupView
    
    func setupView(){
        self.myUser = User.readUserFromArchive()
        print("login user image:\(self.myUser?[0].image)")
        self.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.white
        self.imgProfile.sd_setImage(with: URL(string:AppUtility?.detectURL(ipString:self.myUser?[0].image ?? "") ?? ""), placeholderImage: UIImage(named:"noUserNew"))
        self.registerXIB()
        self.showVideoComments(videoID: Video_Id)
    }
    
    //MARK:- XIB Register
    
    func registerXIB(){
        
        tblComment.register(UINib(nibName: "CommentRowTableViewCell", bundle: nil), forCellReuseIdentifier: "CommentRowTableViewCell")
        
        tblComment.register(UINib(nibName: "CommentHeaderTableView", bundle: nil), forHeaderFooterViewReuseIdentifier: "CommentHeaderTableView")
        
    }
    
    
    //MARK:- Button Actions
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSendMessage(_ sender: Any) {
        if commentID != "0"{
            self.postCommentReply(commentID: self.commentID)
        }else{
            self.postComment()
        }
        
    }
    
    //MARK: TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sections[section].collapsed! ? 0 : sections[section].items!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  =  tableView.dequeueReusableCell(withIdentifier: "CommentRowTableViewCell") as! CommentRowTableViewCell
        
        let item: Item = sections[indexPath.section].items![indexPath.row]
        
        cell.lblComments.text = item.comment
        cell.lblTotalLike.text = "\((item.like_count) ?? 0)".shorten()
        print("Pic",sections[indexPath.section].items![indexPath.row].pic )
        let userImgPath = AppUtility?.detectURL(ipString: sections[indexPath.section].items![indexPath.row].pic ?? "")
        let userImgUrl = URL(string: userImgPath!)
        cell.userImage.sd_setImage(with: userImgUrl, placeholderImage: UIImage(named: "noUserNew"))
        
        if item.like == 1{
            
            cell.btnLike.setImage(UIImage(named: "heart_"), for: .normal)
        }else{
            cell.btnLike.setImage(UIImage(named: "heart_1"), for: .normal)
        }
        
        cell.btnLike.tag = indexPath.row
        cell.btnLike.superview?.tag = indexPath.section
        cell.btnReply.tag =  indexPath.section
        
        cell.btnLike.addTarget(self, action: #selector(self.CommentLiked(btn:)), for: .touchUpInside)
        cell.btnReply.addTarget(self, action: #selector(self.hideRow(section:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CommentHeaderTableView") as? CommentHeaderTableView
        header?.backgroundColor = .red
        header?.lblComments.text = sections[section].comment
        
        header?.btnViewAllReply.tag = section
        header?.btnLike.tag =  section
        header?.btnReply.tag =  section
        
        header?.btnReply.addTarget(self, action: #selector(self.btnReply(section:)), for: .touchUpInside)
        
        
        header?.btnViewAllReply.addTarget(self, action: #selector(self.hideRow(section:)), for: .touchUpInside)
        header?.btnLike.addTarget(self, action: #selector(self.headerCommentLiked(btn:)), for: .touchUpInside)
        
        if sections[section].items?.count  == 0 {
            header?.btnViewAllReply.isHidden = true
        }
        
        let userImgPath = AppUtility?.detectURL(ipString: sections[section].pic ?? "")
        print("Header Imge:",userImgPath)
        let userImgUrl = URL(string:userImgPath!)
        header?.userImage.sd_setImage(with: userImgUrl, placeholderImage: UIImage(named: "noUserNew"))
        
        let dateFormatterNow = DateFormatter()
        dateFormatterNow.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let createdDate = sections[section].createDate! as? String ?? "2019-06-23 12:54:00"
        let dateComment = dateFormatterNow.date(from: createdDate)
        
        header?.lblCommentTime.text  = AppUtility?.timeAgoSinceDate(dateComment!, currentDate: Date(), numericDates: false)
        
        header?.lblTotalLike.text = "\((sections[section].like_count) ?? 0)".shorten()
        
        if sections[section].like == 1{
            header?.btnLike.setImage(UIImage(named: "heart_"), for: .normal)
            self.liked = true
        }else{
            self.liked = false
            header?.btnLike.setImage(UIImage(named: "heart_1"), for: .normal)
        }
        
        let objItems =  sections[section].items
        for var i in  0..<objItems!.count{
            if objItems![i].id as! String == ""{
                header?.btnViewAllReply.isHidden = true
            }else{
                header?.btnViewAllReply.setTitle("View all \(sections[section].items!.count) reply" , for:.normal)
            }
        }
        return header
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 80
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat{
        return 32
    }
    
    //MARK:- Delegate funcations
    
    @objc func hideRow(section:UIButton){
        
        
        let collapsed = !sections[section.tag].collapsed!
        if collapsed == false{
            self.commentID =  sections[section.tag].id!
        }else{
            self.commentID =  "0"
        }
        print("collapsed",collapsed)
        print("commentID",commentID)
        sections[section.tag].collapsed = collapsed
        tblComment.reloadSections(NSIndexSet(index: section.tag) as IndexSet, with: .automatic)
    }
    
    @objc func btnReply(section:UIButton){
        self.commentID =  sections[section.tag].id!
    }
    
    
    
    @objc func headerCommentLiked(btn:UIButton){
        print("section",btn.superview!.tag)
        print("index",btn.tag)
        
        var  obj =  self.sections[btn.tag]
        if obj.like == 1 {
            liked_count = liked_count - 1
            obj.like = 0
            obj.like_count  = liked_count!
            self.headerCommentLiked(commentID: obj.id! )
        }else{
            liked_count = liked_count + 1
            obj.like = 1
            obj.like_count  = liked_count!
            self.headerCommentLiked(commentID: obj.id!)
        }
        self.sections.remove(at: btn.tag)
        self.sections.insert(obj, at: btn.tag)
//        self.tblComment.reloadData()
        tblComment.reloadSections(NSIndexSet(index: btn.tag) as IndexSet, with: .automatic)
        
    }
    
    
    @objc func CommentLiked(btn:UIButton){
        print("section",btn.superview!.tag)
        print("index",btn.tag)
        
        var  objItems =  self.sections[btn.superview!.tag].items![btn.tag]
        if objItems.like == 1 {
            liked_count = liked_count - 1
            objItems.like = 0
            objItems.like_count  = liked_count!
            self.ReplyCommentLiked(commentID: objItems.id! )
        }else{
            liked_count = liked_count + 1
            objItems.like = 1
            objItems.like_count  = liked_count!
            self.ReplyCommentLiked(commentID: objItems.id!)
        }
        self.sections[btn.superview!.tag].items!.remove(at: btn.tag)
        self.sections[btn.superview!.tag].items!.insert(objItems, at: btn.tag)
        self.tblComment.reloadRows(at: [IndexPath(row: btn.tag, section: btn.superview!.tag)], with: .none)
        
    }
    
}
//MARK:- API Handler



extension CommentViewController{
    
    func headerCommentLiked(commentID:String){
        self.myUser = User.readUserFromArchive()
        ApiHandler.sharedInstance.likeComments(user_id:(self.myUser?[0].Id)!, comment_id: commentID) { (isSuccess, response) in
            if isSuccess {
                print(response)
            }else{
                AppUtility!.showToast(string: response?.value(forKey: "msg") as! String, view:self.view)
            }
        }
    }
    func ReplyCommentLiked(commentID:String){
        self.myUser = User.readUserFromArchive()
        ApiHandler.sharedInstance.likeReplyComments(user_id:(self.myUser?[0].Id)!, comment_reply_id: commentID) { (isSuccess, response) in
            if isSuccess {
                print(response)
            }else{
                AppUtility!.showToast(string: response?.value(forKey: "msg") as! String, view:self.view)
            }
        }
    }
    
    func postComment(){
        self.myUser = User.readUserFromArchive()
        ApiHandler.sharedInstance.postCommentOnVideo(user_id:(self.myUser?[0].Id)!, comment: self.tfGowingTextField.text!,video_id:self.Video_Id) { (isSuccess, response, error ) in
            if isSuccess {
                print(response)
                self.tfGowingTextField.text = nil
                self.showVideoComments(videoID: self.Video_Id)
            }else{
                AppUtility!.showToast(string:error, view:self.view)
            }
        }
    }
    func postCommentReply(commentID:String){
        self.myUser = User.readUserFromArchive()
        ApiHandler.sharedInstance.postCommentReply(user_id:(self.myUser?[0].Id)!, comment: self.tfGowingTextField.text!,comment_id:commentID){ (isSuccess, response, error ) in
            if isSuccess {
                print(response)
                self.tfGowingTextField.text = nil
                self.showVideoComments(videoID: self.Video_Id)
            }else{
                AppUtility!.showToast(string:error, view:self.view)
            }
        }
    }
    
    func showVideoComments(videoID:String){
        
        self.sections.removeAll()
        self.myUser = User.readUserFromArchive()
        self.loader.isHidden =  false
        var userID = ""
        if (myUser?.count != 0) && self.myUser != nil{
            userID = (self.myUser?[0].Id)!
        }else{
            return
        }
        AppUtility!.showToast(string: "Loading...", view:self.view)
        
        ApiHandler.sharedInstance.showVideoComments(video_id:videoID,user_id:userID, starting_point: startingPoint) { (isSuccess, response) in
            self.loader.isHidden =  true
            AppUtility!.showToast(string: "Loading...", view:self.view)
            if isSuccess {
                AppUtility!.showToast(string: "Loading...", view:self.view)
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    let msg = response?.value(forKey: "msg")as![[String:Any]]
                    for objres in msg{
                     let data =  userComments.shared.userCommentDetail(obj:objres)
                        self.sections.append(data)
                    }
                    
                    self.tblComment.reloadData()
                }
            }else{
                self.loader.isHidden =  true
                print("response failed getFollowingVideos : ",response!)
                AppUtility!.showToast(string: "Loading", view:self.view)
            }
        }
    }
}

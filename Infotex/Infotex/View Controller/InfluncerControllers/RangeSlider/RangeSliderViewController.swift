//
//  TagsDataViewController.swift
//  Infotex
//
//  Created by Mac on 05/09/2021.
//  Copyright © 2021 Mac. All rights reserved.
//
import UIKit
import RangeSeekSlider


class RangeSliderViewController: UIViewController,RangeSeekSliderDelegate {
    
    
    
    //MARK:- Outlets
   
    @IBOutlet weak var priceSlider: RangeSeekSlider!
    @IBOutlet weak var priceSliderView: UIView!
    @IBOutlet var mainView: UIView!
    
    //MARK:- Variables
    var myUser: [User]? {didSet {}}
    var Start = "1"
    var End = "24"
    
    //MARK:- ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("index:\(index)")
        self.setupView()
    }
    
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
    }
    
    //MARK:- viewWillAppear

    override func viewWillAppear(_ animated: Bool) {
        
        self.changeBackgroundAni()
    }
    
    func changeBackgroundAni() {
        
        UIView.animate(withDuration: 0.3, delay: 0.3, options: .curveEaseOut, animations: {
            
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            
        }, completion:nil)
    }
    
    
    //MARK:- SetupView
    
    func setupView(){
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dimissView(tap:)))
        self.mainView.isUserInteractionEnabled =  true
        self.mainView.addGestureRecognizer(tap)
        
        self.sliderProperties()
    }
    
    
    //MARK:- RRangeSlider
    
    func sliderProperties(){
        priceSlider.delegate =  self
        priceSlider.minValue = CGFloat(1)
        priceSlider.maxValue = CGFloat(24)
        priceSlider.selectedMinValue =  CGFloat(5)
        priceSlider.selectedMaxValue =  CGFloat(24)
        priceSlider.handleColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        priceSlider.handleDiameter = 20
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        print(String(format: "Currency slider updated. Min Value: %.0f Max Value: %.0f", minValue, maxValue))
        self.Start = "\(Int(minValue))"
        self.End =  "\(Int(maxValue))"
        videoTrim = Double(maxValue)
    }
    
    
    //MARK:- Button Actions
    
    @objc func dimissView(tap:UITapGestureRecognizer){
        videoTrim = 24.0
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnStartShooting(_ sender: Any) {
        self.dismiss(animated: true, completion:nil)
    }
}

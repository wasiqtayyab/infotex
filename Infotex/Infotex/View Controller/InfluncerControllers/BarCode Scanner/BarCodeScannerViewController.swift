//
//  BarCodeScannerViewController.swift
//  Infotex
//
//  Created by Mac on 03/09/2021.
//

import UIKit
import MercariQRScanner
class BarCodeScannerViewController: UIViewController {
    
    //MARK:- OUTLET

    @IBOutlet weak var viewScanner: UIView!
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let qrScannerView = QRScannerView(frame: view.bounds)
        viewScanner.addSubview(qrScannerView)
        qrScannerView.configure(delegate: self)
        qrScannerView.startRunning()
    }
}


extension BarCodeScannerViewController: QRScannerViewDelegate {
    func qrScannerView(_ qrScannerView: QRScannerView, didFailure error: QRScannerError) {
        print(error)
    }
    
    func qrScannerView(_ qrScannerView: QRScannerView, didSuccess code: String) {
        affiliated_code = code
        self.navigationController?.popViewController(animated: true)
    }
}

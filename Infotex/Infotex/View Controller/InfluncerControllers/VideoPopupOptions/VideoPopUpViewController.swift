//
//  VideoPopUpViewController.swift
//  Infotex
//
//  Created by Wasiq Tayyab on 10/09/2021.
//

import UIKit

protocol removeVideoDelegate: class {
    func updateVideoObj(index: Int,type:String)
}
class VideoPopUpViewController: UIViewController {
    
    //MARK:- OUTLET
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint! //260
    
    @IBOutlet weak var heightViewUnFollow: NSLayoutConstraint!  //50
    @IBOutlet weak var btnNotInterested: UIButton!
    @IBOutlet weak var btnReportContent: UIButton!
    @IBOutlet weak var btnHide: UIButton!
    @IBOutlet weak var btnUnfollow: UIButton!
    @IBOutlet weak var imgFollow: UIImageView!
    
    var delegate:videoLikeDelegate!
    var removeDelegate:removeVideoDelegate!
    var myUser:[User]? {didSet{}}
    var isFollow =  false
    var videoID = "0"
    var receiverID = "0"
    var index = 0
    var videoMainMVC:videoMainMVC!
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
    }
    
    //MARK:- viewWillAppear

    override func viewWillAppear(_ animated: Bool) {
        
        self.changeBackgroundAni()
    }
    
    func changeBackgroundAni() {
        
        UIView.animate(withDuration: 0.3, delay: 0.3, options: .curveEaseOut, animations: {
            
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            
        }, completion:nil)
    }
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("index:\(self.index)")
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.addGesture(tap:)))
        self.mainView.isUserInteractionEnabled = true
        self.mainView.addGestureRecognizer(tap)
        
        if isFollow == false{
            self.heightViewUnFollow.constant = 0
            self.viewHeight.constant  = 210
            self.btnUnfollow.isHidden  = true
            self.imgFollow.isHidden =  true
        }else{
            self.heightViewUnFollow.constant = 50
            self.btnUnfollow.isHidden  = false
            self.imgFollow.isHidden =  false
            self.viewHeight.constant  = 260
        }
        
        
    }
    
    
    //MARK:- Add gesture
    
    @objc func addGesture(tap:UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Button Actions
    
    @IBAction func notInterestedAction(_ sender:UIButton){
        self.myUser = User.readUserFromArchive()
        if (myUser?.count == 0) && self.myUser == nil{
            AppUtility?.showToast(string: "Please login", view: self.view)
            return
        }
        self.notInterestedVide(videoid: self.videoID, UserID: (self.myUser?[0].Id)!)
    }
    
    @IBAction func btnReportContent(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
        vc.Video_Id =  self.videoID
        vc.modalPresentationStyle = .overFullScreen
        
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btnHideActio(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnFollowUserAction(_ sender: Any) {
        
        self.myUser = User.readUserFromArchive()
        if (myUser?.count == 0) && self.myUser == nil{
            AppUtility?.showToast(string: "Please login", view: self.view)
            return
        }
        self.followUser(rcvrID: self.receiverID, userID:  (self.myUser?[0].Id)!)
        
    }
    //MARK:- API Handler
    
    func notInterestedVide(videoid:String,UserID:String){
        self.loader.isHidden =  false
        ApiHandler.sharedInstance.NotInterestedVideo(video_id: videoid, user_id: UserID) { (isSuccess, response) in
            self.loader.isHidden =  true
            if isSuccess{
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    print("Video Not Interested")
                    self.removeDelegate!.updateVideoObj(index: self.index, type: "video")
                }else{
                   
                    print("Video Not Interested != 200 ")
                }
                self.dismiss(animated: true, completion: nil)
            }else{
                AppUtility?.showToast(string: "Somwthing went wrong,Please try again later", view: self.view)
            }
        }
    }
    
    
    func followUser(rcvrID:String,userID:String){
        
        print("Recid: ",rcvrID)
        print("senderID: ",userID)
        
        
        ApiHandler.sharedInstance.followUser(sender_id: userID, receiver_id: rcvrID) { (isSuccess, response) in
            if isSuccess {
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    
                    if let resmsg =  response?.value(forKey: "msg") as? [String:Any] {
                        let receiver =  resmsg["receiver"] as! [String:Any]
                        let objUser =  receiver["User"] as! [String:Any]
                        if objUser["button"] as! String == "follow"{
                            self.videoMainMVC?.followBtn = "follow"
                            self.delegate.updateObj(obj: self.videoMainMVC,index: self.index, islike: false)
                        }else{
                            self.videoMainMVC?.followBtn = "following"
                            self.delegate.updateObj(obj: self.videoMainMVC,index: self.index, islike: false)
                        }
                        self.dismiss(animated: true, completion: nil)
                    }
                }else{
                    
                    self.showToast(message: (response?.value(forKey: "msg") as? String)!, font: .systemFont(ofSize: 12))
                }
                
            }else{
                self.showToast(message: (response?.value(forKey: "msg") as? String)!, font: .systemFont(ofSize: 12))
            }
        }
    }
}

//
//  PostPopUpViewController.swift
//  Infotex
//
//  Created by Mac on 15/09/2021.
//

import UIKit

class PostPopUpViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var btnEditPost: UIButton!
    @IBOutlet weak var btnNotifications: UIButton!
    @IBOutlet weak var btnDeletePost: UIButton!
    weak var removedelegate: removeVideoDelegate?
    weak var delegate: videoLikeDelegate?
    var myUser:[User]? {didSet{}}
    var CurrentIndex = 0
    var videoID =  "0"
    var videoMain:videoMainMVC!
    var profileDetailVideo:profileDetailVideo!
    var type = ""
    
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    //MARK:- ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.SetupView()
        
    }
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
    }
    
    //MARK:- viewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.changeBackgroundAni()
    }
    
    func changeBackgroundAni() {
        
        UIView.animate(withDuration: 0.3, delay: 0.3, options: .curveEaseOut, animations: {
            
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            
        }, completion:nil)
    }
    
    
    //MARK:- SetupView
    
    func SetupView(){
        self.myUser = User.readUserFromArchive()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.addGesture(tap:)))
        self.mainView.isUserInteractionEnabled = true
        self.mainView.addGestureRecognizer(tap)
        
        self.btnEditPost.setBackgroundImage(UIImage(named: "gradient_"), for: .normal)
    }
    
    //MARK:- Button Actions
    
    
    @IBAction func btnEditPost(_ sender: Any) {
        if type == "video"{
            
            let storyMain = UIStoryboard(name: "influncer", bundle: nil)
            let vc = storyMain.instantiateViewController(withIdentifier: "PostVideoViewController") as! PostVideoViewController
            vc.videoUrl =  URL(string: self.videoMain.videoURL)
            vc.videoMain =  self.videoMain
            vc.currentIndex =  self.CurrentIndex
            vc.isEditPost = true
            vc.delegate =  self.delegate
            let nav = UINavigationController(rootViewController: vc)
            nav.isNavigationBarHidden =  true
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
            
        }else{
            
            let storyMain = UIStoryboard(name: "influncer", bundle: nil)
            let vc = storyMain.instantiateViewController(withIdentifier: "PostImagesViewController") as! PostImagesViewController
            vc.postDetail =  self.profileDetailVideo
            vc.isEditPost = true
            let nav = UINavigationController(rootViewController: vc)
            nav.isNavigationBarHidden =  true
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
            
        }
    }
    @IBAction func btnNotifications(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnDeletePost(_ sender: Any) {
        
        if (myUser?.count == 0) && self.myUser == nil{
            AppUtility?.showToast(string: "Please login", view: self.view)
            return
        }
        
        self.deleteVideo(videoid: self.videoID, UserID: (self.myUser?[0].Id)!)
    }
    
    
    //MARK:- Add gesture
    
    @objc func addGesture(tap:UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- API Handler
    
    func deleteVideo(videoid:String,UserID:String){
        self.loader.isHidden =  false
        ApiHandler.sharedInstance.deleteVideo(video_id: videoid ,image:self.type, UserID: UserID) { (isSuccess, response) in
            if isSuccess{
                self.loader.isHidden =  true
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    print("Video Delete")
                    self.removedelegate!.updateVideoObj(index: self.CurrentIndex, type: self.type)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.loader.isHidden =  true
                    AppUtility?.showToast(string: response?.value(forKey: "msg") as? String ?? "", view: self.view)
                    print("Video not delete")
                }
            }else{
                self.loader.isHidden =  true
                AppUtility?.showToast(string: "Somwthing went wrong,Please try again later", view: self.view)
            }
        }
    }
}

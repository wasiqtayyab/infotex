//
//  StartBroadcastingViewController.swift
//  Infotex
//
//  Created by Wasiq Tayyab on 07/09/2021.
//

import UIKit

class StartBroadcastingViewController: UIViewController {

    @IBOutlet weak var btnLiveBroadcast: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnLiveBroadcast.applyGradient(colors: [#colorLiteral(red: 0.2274509804, green: 0.4941176471, blue: 0.7294117647, alpha: 1), #colorLiteral(red: 0.7803921569, green: 0.4274509804, blue: 0.6588235294, alpha: 1)])
    }
    

}

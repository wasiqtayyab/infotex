//
//  newNotificationsViewController.swift

//  Infotex
//
//  Created by Mac on 01/09/2021.
//  Copyright © 2021 Mac. All rights reserved.
//

import UIKit

class newNotificationsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    //MARK:-Outlets
    
    @IBOutlet weak var notificationTblView: UITableView!
    @IBOutlet weak var noDataView: UIView!
    
    
    //MARK:-Variables
    var userID = "0"
    var myUser:[User]? {didSet{}}
    var notificationsArr = [notificationsMVC]()
    var notiVidDataArr = [videoMainMVC]()
    var startPoint = 0
    
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        
        return refreshControl
    }()
    //MARK:-viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationTblView.refreshControl = refresher
        self.notificationTblView .tableFooterView = UIView()
        self.myUser =  User.readUserFromArchive()
        if (myUser?.count != 0) && self.myUser != nil{
            self.userID =  (self.myUser?[0].Id)!
            getNotifications(startPoint: "\(startPoint)")
        }else{
            
            if let rootViewController = UIApplication.topViewController() {
                
                
                let story:UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                let vc =  story.instantiateViewController(withIdentifier: "AllActivityViewController")
                vc.modalPresentationStyle = .fullScreen
                self.hidesBottomBarWhenPushed = false
                rootViewController.navigationController?.pushViewController(vc, animated: true)
            }
            
            
        }
    }
    
    //MARK:- Refresher Controller
    
    @objc func requestData() {
        
        print("requesting data")
        self.getNotifications(startPoint: "0")
        let deadline = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.refresher.endRefreshing()
        }
        
    }
    
    //MARK:- ButtonActions
    
    @IBAction func btnInbox(_ sender: Any) {
        let story = UIStoryboard(name: "User", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier:"InboxViewController") as! InboxViewController
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:-TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.notificationsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:NotificationsTableViewCell = self.notificationTblView.dequeueReusableCell(withIdentifier: "NotificationsTableViewCell") as! NotificationsTableViewCell
        
        let obj = self.notificationsArr[indexPath.row]
        
        cell.folow_name.text = obj.senderName
        cell.folow_username.text = obj.notificationString
        if(obj.type == "video_like"){
            cell.foolow_btn_view.alpha = 1
        }else if(obj.type == "video_comment"){
            cell.foolow_btn_view.alpha = 1
        }else{
            cell.foolow_btn_view.alpha = 0
        }
        
        
        let senderImg = AppUtility?.detectURL(ipString: obj.senderImg) 
        cell.follow_img.sd_setImage(with: URL(string:senderImg!), placeholderImage: UIImage(named:"noUserNew"))
        cell.follow_img.layer.cornerRadius = cell.follow_img.frame.size.width / 2
        cell.follow_img.clipsToBounds = true
        
        cell.follow_view.layer.cornerRadius = cell.follow_view.frame.size.width / 2
        cell.follow_view.clipsToBounds = true
        
        cell.foolow_btn_view.layer.cornerRadius = 5
        cell.foolow_btn_view.clipsToBounds = true
        
        cell.btn_follow.tag = indexPath.item
        
        cell.btnWatch.addTarget(self, action: #selector(newNotificationsViewController.btnWatchAction(_:)), for:.touchUpInside)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = notificationsArr[indexPath.row]
        let storyMain = UIStoryboard(name: "influncer", bundle: nil)
        print("obj user id : ",obj.sender_id)
        let vc = storyMain.instantiateViewController(withIdentifier: "InfluncerProfileVC") as!  InfluncerProfileVC
        vc.isOtherUser = true
        vc.userID = obj.sender_id
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func btnWatchAction(_ sender : UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.notificationTblView)
        let indexPath = self.notificationTblView.indexPathForRow(at:buttonPosition)
        self.getVideo(ip:indexPath!)
        print("btn watch tapped")
        
    }
    
    //MARK:-API Handler
    
    func getNotifications(startPoint:String){
        self.loader.isHidden =  false
        ApiHandler.sharedInstance.showAllNotifications(user_id: self.userID, starting_point: "\(startPoint)") { (isSuccess, response) in
            if isSuccess{
                self.loader.isHidden =  true
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    let resMsg = response?.value(forKey: "msg") as! [[String:Any]]
                    for dic in resMsg{
                        let notiDic = dic["Notification"] as! NSDictionary
                        
                        let senderDic = dic["Sender"] as! NSDictionary
                        let receiverDic = dic["Receiver"] as! NSDictionary
                        
                        let notiString = notiDic.value(forKey: "string") as! String
                        let type = notiDic.value(forKey: "type") as! String
                        let receiver_id = notiDic.value(forKey: "receiver_id") as! String
                        let sender_id = notiDic.value(forKey: "sender_id") as! String
                        let video_id = notiDic.value(forKey: "video_id") as! String
                        let notiID  = notiDic.value(forKey: "id") as! String
                        
                        let senderUserame  = senderDic.value(forKey: "username") as? String ?? ""
                        let senderImg = senderDic.value(forKey: "profile_pic") as? String ?? ""
                        let senderFirstName = senderDic.value(forKey: "first_name") as? String ?? ""
                        
                        let receiverName = receiverDic.value(forKey: "username") as? String ?? ""
                        
                        let notiObj = notificationsMVC(notificationString: notiString, id: notiID, sender_id: sender_id, receiver_id: receiver_id, type: type, video_id: video_id, senderName: senderUserame, senderFirstName: senderFirstName, receiverName: receiverName, senderImg: senderImg)
                        
                        self.notificationsArr.append(notiObj)
                        
                    }
                    self.notificationTblView.reloadData()
                }else{
                    print("!200",response?.value(forKey: "msg"))
                }
            }else{
                self.loader.isHidden  = true
                self.showToast(message: "Failed to load notifications", font: .systemFont(ofSize: 12))
            }
        }
    }
    func getVideo(ip:IndexPath){
        let obj = notificationsArr[ip.row]
        ApiHandler.sharedInstance.showVideoDetail(user_id:obj.id, video_id: obj.video_id) { (isSuccess, response) in
            if isSuccess{
                if response?.value(forKey: "code") as! NSNumber == 200 {
                    
                    let resMsg = response?.value(forKey: "msg") as! NSDictionary
                    
                    let videoDic = resMsg.value(forKey: "Video") as! NSDictionary
                    let userDic = resMsg.value(forKey: "User") as! NSDictionary
                    let soundDic = resMsg.value(forKey: "Sound") as! NSDictionary
                    
                    print("videoDic: ",videoDic)
                    print("userDic: ",userDic)
                    print("soundDic: ",soundDic)
                    
                    let videoURL = videoDic.value(forKey: "video") as! String
                    let desc = videoDic.value(forKey: "description") as! String
                    let allowComments = videoDic.value(forKey: "allow_comments")
                    let videoUserID = videoDic.value(forKey: "user_id")
                    let videoID = videoDic.value(forKey: "id") as! String
                    let allowDuet = videoDic.value(forKey: "allow_duet")
                    
                    // not strings
                    let commentCount = videoDic.value(forKey: "comment_count")
                    let likeCount = videoDic.value(forKey: "like_count")
                    let duetVidID = videoDic.value(forKey: "duet_video_id")
                    
                    let userImgPath = userDic.value(forKey: "profile_pic") as! String
                    let userName = userDic.value(forKey: "username") as! String
                    let followBtn = userDic.value(forKey: "button") as! String
                    let uid = userDic.value(forKey: "id") as! String
                    let verified = userDic.value(forKey: "verified")
                    
                    let soundName = soundDic.value(forKey: "name")
                    let cdPlayer = soundDic.value(forKey: "thum") as? String ?? ""
                    
                    let videoObj = videoMainMVC(videoID: videoID, videoUserID: "\(videoUserID!)", fb_id: "", description: desc, videoURL: videoURL, videoTHUM: "", videoGIF: "", view: "", section: "", sound_id: "", privacy_type: "", allow_comments: "\(allowComments!)", allow_duet: "\(allowDuet!)", block: "", duet_video_id: "", old_video_id: "", created: "", like: "", favourite: "", comment_count: "\(commentCount!)", like_count: "\(likeCount!)", followBtn: followBtn, duetVideoID: "\(duetVidID!)", userID: uid, first_name: "", last_name: "", gender: "", bio: "", website: "", dob: "", social_id: "", userEmail: "", userPhone: "", password: "", userProfile_pic: userImgPath, role: "", username: userName, social: "", device_token: "", videoCount: "", verified: "\(verified!)", soundName: "\(soundName!)", CDPlayer: cdPlayer)
                    
                    self.notiVidDataArr.append(videoObj)
                    
                    print("response@200: ",response!)
                    
                    let vc =  self.storyboard?.instantiateViewController(withIdentifier: "HomeVideoViewController") as! HomeVideoViewController
                    vc.videosMainArr = self.notiVidDataArr
                    vc.currentIndex = ip
                    vc.isOtherController =  true
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    print("!200: ",response as Any)
                }
                
                
            }else{
                
                let vc =  self.storyboard?.instantiateViewController(withIdentifier: "HomeVideoViewController") as! HomeVideoViewController
                vc.videosMainArr = self.notiVidDataArr
                vc.currentIndex = ip
                vc.isOtherController =  true
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if notificationsArr.isEmpty{
            noDataView.isHidden = false
        }else{
            noDataView.isHidden = true
        }
        
        if indexPath.row == notificationsArr.count - 4{
            self.startPoint+=1
            print("StartPoint: ",startPoint)
            self.getNotifications(startPoint: "\(self.startPoint)")
            print("index@row: ",indexPath.row)
        }
    }
}

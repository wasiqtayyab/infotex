//
//  editProfileViewController.swift
//  Infotex
//
//  Created by Mac on 15/09/2021.
//  Copyright © 2021 Mac. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
class editProfileViewController: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var websiteView: UIView!
    @IBOutlet weak var bioView: UIView!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var editPhotoIcon: UIImageView!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var nameTF: CustomTextField!
    @IBOutlet weak var second_nameTF: CustomTextField!
    @IBOutlet weak var first_nameTF: CustomTextField!
    
    
    @IBOutlet weak var selectedbtn: UIButton!
    @IBOutlet weak var unselectedbtn: UIButton!
    
    @IBOutlet weak var websiteTF: CustomTextField!
    @IBOutlet weak var bioTF: CustomTextField!
    
    //MARK:- variables
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    var userData : userMVC!
    var profilePicData = ""
    var profilePicSmall = ""
    var isMale = true
    var myUser:[User]? {didSet{}}
    
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myUser = User.readUserFromArchive()
        screenSetup()
        
        let bottomBorder = CALayer()
        bottomBorder.borderColor = #colorLiteral(red: 0.8352941176, green: 0.8352941176, blue: 0.8352941176, alpha: 1);
        bottomBorder.borderWidth = 0.5;
        bottomBorder.frame = CGRect(x: 0, y: topView.frame.height, width: view.frame.width, height: 1)
        topView.layer.addSublayer(bottomBorder)
        
        
        let Border = CALayer()
        Border.borderColor = #colorLiteral(red: 0.9137254902, green: 0.9137254902, blue: 0.9137254902, alpha: 1);
        Border.borderWidth = 0.5;
        Border.frame = CGRect(x: 0, y: genderView.frame.height, width: genderView.frame.width, height: 1)
        genderView.layer.addSublayer(Border)
        
        
        let bo = CALayer()
        bo.borderColor = #colorLiteral(red: 0.8352941176, green: 0.8352941176, blue: 0.8352941176, alpha: 1);
        bo.borderWidth = 0.5;
        bo.frame = CGRect(x: 0, y: websiteView.frame.height, width: websiteView.frame.width, height: 1)
        websiteView.layer.addSublayer(bo)
        
        
        let bottom = CALayer()
        bottom.borderColor = #colorLiteral(red: 0.8352941176, green: 0.8352941176, blue: 0.8352941176, alpha: 1);
        bottom.borderWidth = 0.5;
        bottom.frame = CGRect(x: 0, y: bioView.frame.height, width: bioView.frame.width, height: 1)
        bioView.layer.addSublayer(bottom)
        
        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2
        profileImage.clipsToBounds = true
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(profileImage(tapGestureRecognizer:)))
        editPhotoIcon.isUserInteractionEnabled = true
        editPhotoIcon.addGestureRecognizer(tapGestureRecognizer)
        
        
    }
    
    
    
    //MARK:- Button Actions

    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        
        guard (AppUtility?.validateUsername(str: nameTF.text!)) == true else {
            self.showToast(message: "Invalid Username", font: .systemFont(ofSize: 12))
            return
        }
        guard (AppUtility?.validateUsername(str: first_nameTF.text!)) == true else {
            self.showToast(message: "Invalid First Name", font: .systemFont(ofSize: 12))
            return
        }
        guard (AppUtility?.validateUsername(str: first_nameTF.text!)) == true else {
            self.showToast(message: "Invalid Second Name", font: .systemFont(ofSize: 12))
            return
        }
        addProfileDataAPI()
    }
    
    @IBAction func selectedButtonPressed(_ sender: UIButton) {
        
        selectedbtn.setImage(UIImage(named: "selected"), for: .normal)
        unselectedbtn.setImage(UIImage(named: "unselected"), for: .normal)
        isMale = true
        
    }
    
    
    @IBAction func unselectedButtonPressed(_ sender: UIButton) {
        
        unselectedbtn.setImage(UIImage(named: "selected"), for: .normal)
        selectedbtn.setImage(UIImage(named: "unselected"), for: .normal)
        isMale = false
    }
    
    
    
    //MARK:- functions
    
    @objc func profileImage(tapGestureRecognizer: UITapGestureRecognizer){

        ImagePickerManager().pickImage(self){ image in
            self.profilePicData = (image.jpegData(compressionQuality: 0.1)?.base64EncodedString())!
            print("profilePicData: ",self.profilePicData)
            
            self.profileImage.image = image
            
            image.resizeImage(CGFloat(300), opaque: false)
            self.profilePicSmall = (image.jpegData(compressionQuality: 0.5)?.base64EncodedString())!
            self.addUserImgAPI()
            
        }
    }
    
    func screenSetup(){
        
        let userObj = userData
        self.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.profileImage.sd_setImage(with: URL(string:(AppUtility?.detectURL(ipString: userObj?.userProfile_pic ?? " "))!), placeholderImage: UIImage(named:"noUserImg"))
        
        self.nameTF.text = userObj?.username
        self.first_nameTF.text = userObj?.first_name
        self.second_nameTF.text = userObj?.last_name
        self.bioTF.text = userObj?.bio
        self.websiteTF.text = userObj?.website
    }

    
    //MARK:- API Handler
    
    func addProfileDataAPI(){
        let username = self.nameTF.text
        let firstName = self.first_nameTF.text
        let lastName = self.second_nameTF.text
        let userID = (self.myUser?[0].Id)!
        let web = self.websiteTF.text
        let bio = self.bioTF.text
        var gender = "Male"
        if isMale == false{
            gender = "female"
        }
        self.myUser = User.readUserFromArchive()
        ApiHandler.sharedInstance.editProfile(username: username ?? "", user_id: userID , first_name: firstName ?? "", last_name: lastName ?? "", gender: gender , website: web ?? "", bio: bio ?? "", phone: (self.myUser?[0].phone)!) { (isSuccess, response) in
            if isSuccess{
                if response?.value(forKey: "code") as! NSNumber == 200{
                    let msgDict = response?.value(forKey: "msg") as! NSDictionary
                    UserObject.shared.Objresponse(response: msgDict as! [String : Any],isLogin: false)

                    self.showToast(message: "Porfile Upated", font: .systemFont(ofSize: 12))
                }else{
                    self.showToast(message: response?.value(forKey: "msg") as! String ?? "Unable To Update", font: .systemFont(ofSize: 12))
                    print("!200: ",response as! Any)
                }
            }
        }
    }
    func addUserImgAPI(){
        self.loader.isHidden =  false
        ApiHandler.sharedInstance.addUserImage(user_id: (self.myUser?[0].Id)!, profile_pic: ["file_data":self.profilePicData], profile_pic_small: ["file_data":self.profilePicSmall]) { (isSuccess, response) in
            self.loader.isHidden =  true
            if response?.value(forKey: "code") as! NSNumber == 200{
                let msgDict = response?.value(forKey: "msg") as! NSDictionary
                let userDict = msgDict.value(forKey: "User") as! NSDictionary
                let profImgUrl = userDict.value(forKey: "profile_pic") as! String
                UserObject.shared.Objresponse(response: msgDict as! [String : Any],isLogin: false)

                self.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
                self.profileImage.sd_setImage(with: URL(string:(AppUtility?.detectURL(ipString: profImgUrl))!), placeholderImage: UIImage(named:"noUserImg"))
                
            }else{
                self.loader.isHidden =  true
                self.showToast(message: "Error Occur", font: .systemFont(ofSize: 12))
            }
        }
    } 
}


import UIKit
import CoreLocation
import PhoneNumberKit
import CoreServices
//import LanguageManager_iOS


protocol DatePickerDelegate {
    func timePicked(time: String)
}

//import Reachability

let AppUtility =  Utility.sharedUtility()

let ud = UserDefaults.standard

struct Platform {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
        isSim = true
        #endif
        return isSim
    }()
}


extension Data {
    
    func getDecodedObject<T>(from object : T.Type)->T? where T : Decodable {
        
        do {
            
            return try JSONDecoder().decode(object, from: self)
            
        } catch let error {
            
            print("Manually parsed  ", (try? JSONSerialization.jsonObject(with: self, options: .allowFragments)) ?? "nil")
            
            print("Error in Decoding OBject ", error)
            return nil
        }
        
    }
    
}


//MARK:- Server URL Definition
let imageDownloadUrl  = ""

//MARK:- Loading Colour
let strloadingColour = "FFCC02"

//MARK:- User Defaults Keys
let strToken = "strToken"
let strAPNSToken = "strAPNSToken"
let strDeviceID = "strDeviceID"
let strDeviceKey = "strDeviceKey"
let strDate = "strDate"
let strURL = "strURL"
let InternetConnected = "InternetConnected"

@available(iOS 13.0, *)
let delegate:AppDelegate = UIApplication.shared.delegate as!  AppDelegate



var isPickUp =  false
var statusPickup = "0"
//Pickup
var strLatCurrent  = ""
var strLongCurrent = ""
var currentAddress = ""
//Drop
var strLatDestination  = ""
var strLongDestination = ""
var destinationAddress = ""

var selectMenuIndex = "0"

var numberForPageController = 0

//MARK:- Utility Initialization Methods
class Utility: NSObject{
    
    var percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
    var panGestureRecognizer = UIPanGestureRecognizer()
    
    // fileprivate let connectivity: Connectivity = Connectivity()
    fileprivate var isCheckingConnectivity: Bool = false
    let phoneNumberKit = PhoneNumberKit()
    
    //var fbLogin:FBSDKLoginManager!
    var datePickerDelegate : DatePickerDelegate?
    class func sharedUtility()->Utility!
    {
        struct Static
        {
            static var sharedInstance:Utility?=nil;
            static var onceToken = 0
        }
        Static.sharedInstance = self.init();
        return Static.sharedInstance!
    }
    required override init() {
        
    }
    
    //Aniamtion
    func startLoaderAnimation(){
        
    }
   
    
    func showToast(string:String,view:UIView) {
        view.makeToast(string, duration: 1.0, position: .bottom)
    }
    


    func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString,
                             options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
    
    func calculateAge(dob: String, format: String = "MM/dd/yyyy") -> String{
        let df = DateFormatter()
        df.dateFormat = format
        let date = df.date(from: dob)
        guard let val = date else{
            return ""
        }
        var years = 0
        var months = 0
        var days = 0
        
        let cal = Calendar.current
        years = cal.component(.year, from: Date()) - cal.component(.year, from: val)
        
        let currentMonth = cal.component(.month, from: Date())
        let birthMonth = cal.component(.month, from: val)
        
        months = currentMonth - birthMonth
        
        if months < 0 {
            years = years - 1
            months = 12 - birthMonth
            if cal.component(.day, from: Date()) < cal.component(.day, from: val){
                months = months - 1
            }
        }else if months == 0 && cal.component(.day, from: Date()) < cal.component(.day, from: val){
            years = years - 1
            months = 11
        }
        if cal.component(.day, from: Date()) > cal.component(.day, from: val){
            days = cal.component(.day, from: Date()) - cal.component(.day, from: val)
        }else if cal.component(.day, from: Date()) < cal.component(.day, from: val){
            let today = cal.component(.day, from: Date())
            let date = cal.date(byAdding: .month, value: -1, to: Date())
            days = date!.daysInMonth - cal.component(.day, from: val) + today
        }else{
            days = 0
            if months == 12 {
                years = years + 1
                months = 0
            }
        }
        print("Years: \(years), Months: \(months), Days: \(days)")
        if years > 0{
            if years == 1{
                return "\(years)"
            }else{
                return "\(years)"
            }
        }else if months > 4{
            return "\(months)"
        }else {
            if  months >= 1{
                let daysleftInMonth = months * 30
                print(daysleftInMonth)
                days += daysleftInMonth
            }
            
            let weeks = days / 7
            return "\(weeks)"
        }
    }
    
    
    
    
    //MARK:- Get Date Method
    func getDateFromUnixTime(_ unixTime:String) -> String {
        let date = Date(timeIntervalSince1970: Double(unixTime)!)
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        let currentTime =  formatter.string(from: date as Date)
        return currentTime;
    }
    
    func getAgeYears(birthday:String)-> Int{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd,MMMM yyyy" //"dd-MM-yyyy"
        let birthdate = formatter.date(from: birthday)
        let now = Date()
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: birthdate!, to: now)
        let age = ageComponents.year!
        return age
    }
    
    func validate(_ value: String) -> String {
        var subscriberNumber = value
        let prefixCase = "0"
        if subscriberNumber.hasPrefix(prefixCase) {
            subscriberNumber.remove(at: subscriberNumber.startIndex)
        }
        return subscriberNumber
    }
    
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return  dateFormatter.string(from: date!)
        
    }
    
    func convertDateFormater1(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
        
    }
    
    func getAgeFromDOF(date: String) -> (Int,Int,Int) {
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        let dateOfBirth = dateFormater.date(from: date)
        
        let calender = Calendar.current
        
        let dateComponent = calender.dateComponents([.year, .month, .day], from:
                                                        dateOfBirth!, to: Date())
        
        return (dateComponent.year!, dateComponent.month!, dateComponent.day!)
    }
    func showDatePicker(fromVC : UIViewController){
        
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 250)
        let pickerView = UIDatePicker(frame: CGRect(x: 0, y: 0, width: 250, height: 250))
        pickerView.datePickerMode = .date
        //pickerView.locale = NSLocale(localeIdentifier: "\(Formatter.getInstance.getAppTimeFormat().rawValue)") as Locale
        vc.view.addSubview(pickerView)
        
        let alertCont = UIAlertController(title: "Choose Date", message: "", preferredStyle: UIAlertController.Style.alert)
        
        alertCont.setValue(vc, forKey: "contentViewController")
        let setAction = UIAlertAction(title: "Select", style: .default) { (action) in
            if self.datePickerDelegate != nil{
                
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                formatter.timeZone = TimeZone.current
                let selectedTime = formatter.string(from: pickerView.date)
                self.datePickerDelegate!.timePicked(time: selectedTime)
            }
        }
        alertCont.addAction(setAction)
        alertCont.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        fromVC.present(alertCont, animated: true)
    }
    
    
        //MARK: Check Internet
        func connected() -> Bool
        {
            let reachibility = Reachability.forInternetConnection()
            let networkStatus = reachibility?.currentReachabilityStatus()

            return networkStatus != NotReachable

        }

    //MARK:- Validation Text
    func hasValidText(_ text:String?) -> Bool
    {
        if let data = text
        {
            let str = data.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if str.count>0
            {
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            return false
        }
        
    }
    
    
    
    //MARK:- Validation Atleast 1 special schracter or number
    
    func checkTextHaveChracterOrNumber( text : String) -> Bool{
        
        /* let numberRegEx  = ".*[0-9]+.*"
         let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
         let numberresult = texttest1.evaluate(with: text)
         print("\(numberresult)")*/
        
        
        let specialCharacterRegEx  = ".*[!&^%$#@()/_-]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        
        let specialresult = texttest2.evaluate(with: text)
        print("\(specialresult)")
        
        //return capitalresult || numberresult || specialresult
        return /*numberresult ||*/ specialresult
        
    }
    
    //MARK:- Validation Email
    func isEmail(_ email:String  ) -> Bool
    {
        let strEmailMatchstring = "\\b([a-zA-Z0-9%_.+\\-]+)@([a-zA-Z0-9.\\-]+?\\.[a-zA-Z]{2,6})\\b"
        let regExPredicate = NSPredicate(format: "SELF MATCHES %@", strEmailMatchstring)
        if(!isEmpty(email as String?) && regExPredicate.evaluate(with: email))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    //MARK:- Validation Empty
    func isEmpty(_ thing : String? )->Bool {
        if (thing?.count == 0) {
            return true
        }
        return false;
    }
    
    //MARK:- CNIC Validation
    func isValidIdentityNumber(_ value: String) -> Bool {
        guard
            value.count == 11,
            let digits = value.map({ Int(String($0)) }) as? [Int],
            digits[0] != 0
        else { return false }
        
        let check1 = (
            (digits[0] + digits[2] + digits[4] + digits[6] + digits[8]) * 7
                - (digits[1] + digits[3] + digits[5] + digits[7])
        ) % 10
        
        guard check1 == digits[9] else { return false }
        
        let check2 = (digits[0...8].reduce(0, +) + check1) % 10
        
        return check2 == digits[10]
    }
    
    func showLocationPermissionAlert(controller:UIViewController){
        let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
            //Redirect to Settings app
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        alertController.addAction(cancelAction)
        
        alertController.addAction(okAction)
        
        controller.present(alertController, animated: true, completion: nil)
        
        //let customNotification = CustomCRNotification(textColor: .black, backgroundColor: .white, image: UIImage(named: "LogoSignup"))
        //CRNotifications.showNotification(type: customNotification, title: "Location Permission Required".localiz(), message: "Please enable location permissions in settings for better experience".localiz(), dismissDelay: 3)
        
    }
    //MARK:- Show Alert
    func displayAlert(title titleTxt:String, messageText msg:String, delegate controller:UIViewController) ->()
    {
        let alertController = UIAlertController(title: titleTxt, message: msg, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        controller.present(alertController, animated: true, completion: nil)
        alertController.view.tintColor = UIColor(named:strloadingColour )
        
    }
    
    

    
    //MARK:- Color with HEXA
    func hexStringToUIColor (hex:String,alpa:CGFloat) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpa)
        )
    }
    
    //MARK:- Get & Set Methods For UserDefaults
    
    func saveObject(obj:String,forKey strKey:String){
        ud.set(obj, forKey: strKey)
    }
    
    func getObject(forKey strKey:String) -> String {
        if let obj = ud.value(forKey: strKey) as? String{
            let obj2 = ud.value(forKey: strKey) as! String
            return obj2
        }else{
            return ""
        }
    }
    
    func deleteObject(forKey strKey:String) {
        ud.set(nil, forKey: strKey)
    }
    
    // Mark:-   Convert concatinate url with BASEURL
    func detectURL(ipString:String)-> String{
        let input = ipString
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
        
        if matches.isEmpty{
            return BASE_URL+ipString
        }else{
            
            var urlString = ""
            for match in matches {
                guard let range = Range(match.range, in: input) else { continue }
                let url = input[range]
                print(url)
                
                urlString = String(url)
            }
            
            return urlString
        }
        
    }
    
    //MARK:- crash Alet
    
    func openfiledialog (windowTitle: String, message: String, filetypelist: String) -> String
    {
        var path: String = ""
        var finished: Bool = false

        suspendprocess (t: 0.02) // Wait 20 ms., enough time to do screen updates regarding to the background job, which calls this function
        DispatchQueue.main.async
        {

            let story:UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
            let vc =  story.instantiateViewController(withIdentifier: "ErrorViewController") as! ErrorViewController
            UIApplication.shared.keyWindow?.rootViewController =  vc
            finished = true
        }

        while not(b:finished)
        {
            suspendprocess (t:0.001) // Wait 1 ms., loop until main thread finished
        }

        return (path)
    }
    
    func not (b: Bool) -> Bool
    {
        return (!b)
    }

    func suspendprocess (t: Double)
    {
        var secs: Int = Int(abs(t))
        var nanosecs: Int = Int(Float(abs(t)) * 1000000000)
        var time = timespec(tv_sec: secs, tv_nsec: nanosecs)
        let result = nanosleep(&time, nil)
    }
    
    //MARK:- Animation
    
    func btnHoverAnimation(viewName:UIView)
    {
        UIView.animate(withDuration: 0.2,
                       animations: {
                        viewName.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                        viewName.layer.cornerRadius = 6
                       },
                       completion: { _ in
                        UIView.animate(withDuration: 0.2) {
                            viewName.transform = CGAffineTransform.identity
                        }
                       })
        
    }
    
    
    
    //MARK: Add Delay
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    //MARK: Calculate Time
    
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
    
    func calculateTimeDifference(start: String) -> String {
        let formatter = DateFormatter()
        //        2018-12-17 18:01:34
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var startString = "\(start)"
        if startString.count < 4 {
            for _ in 0..<(4 - startString.count) {
                startString = "0" + startString
            }
        }
        let currentDateTime = Date()
        let strcurrentDateTime = formatter.string(from: currentDateTime)
        var endString = "\(strcurrentDateTime)"
        if endString.count < 4 {
            for _ in 0..<(4 - endString.count) {
                endString = "0" + endString
            }
        }
        let startDate = formatter.date(from: startString)!
        let endDate = formatter.date(from: endString)!
        let difference = endDate.timeIntervalSince(startDate)
        if (difference / 3600) > 24{
            let differenceInDays = Int(difference/(60 * 60 * 24 ))
            return "\(differenceInDays) DAY AGO"
        }else{
            return "\(Int(difference) / 3600)HOURS \(Int(difference) % 3600 / 60)MIN AGO"
        }
    }
    
    func offsetFrom(dateFrom : Date,dateTo:Date) -> String {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: dateFrom, to: dateTo);
        
        let seconds = "\(difference.second ?? 0) s"
        let minutes = "\(difference.minute ?? 0) m" + ":" + seconds
        let hours = "\(difference.hour ?? 0) h" + ":" + minutes
        let days = "\(difference.day ?? 0) d" + ":" + hours
        
        if let day = difference.day, day          > 0 { return days }
        if let hour = difference.hour, hour       > 0 { return hours }
        if let minute = difference.minute, minute > 0 { return minutes }
        if let second = difference.second, second > 0 { return seconds }
        return ""
    }
    
    func dateFor(timeStamp: String) -> NSDate
    {
        let formater = DateFormatter()
        formater.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formater.date(from: timeStamp)! as NSDate
    }

    
    //MARK: Random Number
    func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
   
    
    internal func createActivityIndicator(_ uiView : UIView)->UIView{
           
           let container: UIView = UIView(frame: CGRect.zero)
           container.layer.frame.size = uiView.frame.size
           container.center = CGPoint(x: uiView.bounds.width/2, y: uiView.bounds.height/2)
            container.backgroundColor = #colorLiteral(red: 0.07058823529, green: 0.07058823529, blue: 0.07058823529, alpha: 0.5)
           
           let loadingView: UIView = UIView()
           loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
           loadingView.center = container.center
           loadingView.backgroundColor = UIColor(named: "Mine Shaft")
        
           loadingView.clipsToBounds = true
           loadingView.layer.cornerRadius = 15
           loadingView.layer.shadowRadius = 5
           loadingView.layer.shadowOffset = CGSize(width: 0, height: 4)
           loadingView.layer.opacity = 2
           loadingView.layer.masksToBounds = false
         
           
           let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
           actInd.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
           actInd.clipsToBounds = true
           actInd.color = UIColor(named: "HopBush")
           actInd.style = .whiteLarge
           
           actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
           loadingView.addSubview(actInd)
           container.addSubview(loadingView)
           container.isHidden = true
           uiView.addSubview(container)
           actInd.startAnimating()
           
           return container
           
       }
    
    //MARK:- Phone Number Validation
    func isValidPhoneNumber(strPhone:String,strRegion:String) -> Bool{
        do {
//            _ = try phoneNumberKit.parse(strPhone)
            _ = try phoneNumberKit.parse(strPhone, withRegion: strRegion, ignoreType: true)
          

            return true
        }
        catch {
            print("Generic parser error")
            return false
        }
    }
    
    //    MARK:- validate username
    func validateUsername(str: String) -> Bool
    {
        let RegEx = "\\A\\w{4,12}\\z"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: str)
    }
    
  
    
    
}

extension UIImage {

    convenience init?(withContentsOfUrl url: URL) throws {
        let imageData = try Data(contentsOf: url)
    
        self.init(data: imageData)
    }

}

extension UIViewController: UINavigationControllerDelegate {
    
    
    func addNewGesture() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            return
        }
        
        AppUtility!.panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.newhandlePanGesture(_:)))
        self.view.addGestureRecognizer(AppUtility!.panGestureRecognizer)
        
        
    }
    
    @objc  func newhandlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
        
        case .began:
            navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            if AppUtility!.percentDrivenInteractiveTransition != nil {
                AppUtility!.percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                AppUtility!.percentDrivenInteractiveTransition.finish()
                
            } else {
                AppUtility!.percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            AppUtility!.percentDrivenInteractiveTransition.cancel()
            break
        default:
            break
        }
    }

    
    public func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if AppUtility!.panGestureRecognizer.state == .began {
            AppUtility!.percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            AppUtility!.percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            //  AppUtility!.percentDrivenInteractiveTransition = nil
        }
        
        return AppUtility!.percentDrivenInteractiveTransition
    }
    
   
}


class Loader{
    
    var vc = UIApplication.shared.keyWindow?.rootViewController?.view
    let backgroundView = UIView()
    
    func activityStartAnimating() {

        vc!.isUserInteractionEnabled = false
        
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        backgroundView.backgroundColor = UIColor(named: "Shaft")
        backgroundView.tag = 475647
        
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 80, height: 80))
        activityIndicator.center = backgroundView.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.color = UIColor(named: "HopBush")
        activityIndicator.startAnimating()
        activityIndicator.isHidden =  false

                
        backgroundView.addSubview(activityIndicator)

        DispatchQueue.main.async { [self] in
            UIApplication.shared.keyWindow!.addSubview(backgroundView)
            UIApplication.shared.keyWindow!.bringSubviewToFront(backgroundView)
            self.backgroundView.bringSubviewToFront(backgroundView)
        }
        
    }
    
    func activityStopAnimating() {

        vc!.isUserInteractionEnabled = false
        
        
        
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        backgroundView.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 0.6)
        backgroundView.tag = 475647
        
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 80, height: 80))
        activityIndicator.center = backgroundView.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.color = .black
        activityIndicator.stopAnimating()
        activityIndicator.isHidden =  true

        backgroundView.removeFromSuperview()
    }
    
  
}

extension Date{
    var daysInMonth:Int{
        let calendar = Calendar.current
        
        let dateComponents = DateComponents(year: calendar.component(.year, from: self), month: calendar.component(.month, from: self))
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        
        return numDays
    }
}
extension String
{
    func hashtags() -> [String]
    {
        if let regex = try? NSRegularExpression(pattern: "#[a-z0-9]+", options: .caseInsensitive)
        {
            let string = self as NSString

            return regex.matches(in: self, options: [], range: NSRange(location: 0, length: string.length)).map {
                string.substring(with: $0.range).replacingOccurrences(of: "#", with: "").lowercased()
            }
        }

        return []
    }

    func mentions() -> [String]
    {
        if let regex = try? NSRegularExpression(pattern: "@[a-z0-9]+", options: .caseInsensitive)
        {
            let string = self as NSString

            return regex.matches(in: self, options: [], range: NSRange(location: 0, length: string.length)).map {
                string.substring(with: $0.range).replacingOccurrences(of: "@", with: "").lowercased()
            }
        }

        return []
    }
}
extension UIViewController {
    
    func showToast(message : String, font: UIFont) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 175, y: self.view.frame.size.height*0.12, width: 350, height: 40))
        //    toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.backgroundColor = #colorLiteral(red: 0.3129099309, green: 0.3177284598, blue: 0.3219906092, alpha: 0.8590539384)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 2;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
    

extension UIView {
    
    func makeRounded() {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
    func startRotating(duration: Double = 3) {
        let kAnimationKey = "rotation"
        
        if self.layer.animation(forKey: kAnimationKey) == nil {
            let animate = CABasicAnimation(keyPath: "transform.rotation")
            animate.duration = duration
            animate.repeatCount = Float.infinity
            animate.fromValue = 0.0
            animate.toValue = Float(M_PI * 2.0)
            self.layer.add(animate, forKey: kAnimationKey)
        }
    }
    func stopRotating() {
        let kAnimationKey = "rotation"
        
        if self.layer.animation(forKey: kAnimationKey) != nil {
            self.layer.removeAnimation(forKey: kAnimationKey)
        }
    }
}
extension String {
    func shorten() -> String{
        let number = Double(self)
        let thousand = number! / 1000
        let million = number! / 1000000
        if million >= 1.0 {
            return "\(round(million*10)/10)M"
        }
        else if thousand >= 1.0 {
            return "\(round(thousand*10)/10)K"
        }
        else {
            return "\(self)"
        }
    }
}
extension UITextView {
       func numberOfLines(textView: UITextView) -> Int {
        let layoutManager = textView.layoutManager
        let numberOfGlyphs = layoutManager.numberOfGlyphs
        var lineRange: NSRange = NSMakeRange(0, 1)
        var index = 0
        var numberOfLines = 0
        
        while index < numberOfGlyphs {
            layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
            index = NSMaxRange(lineRange)
            numberOfLines += 1
        }
        return numberOfLines
    }
}
// Put this piece of code anywhere you like
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

var rowHeight = 100

extension UIButton
{
    func applyBtnGradient(colors: [CGColor])
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
       gradientLayer.frame = self.bounds
       // gradientLayer.cornerRadius = 25.0
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}

extension UIView
{
    func applyGradientView(colors: [CGColor])
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
extension UIImage {
    func resizeImage(_ dimension: CGFloat, opaque: Bool, contentMode: UIView.ContentMode = .scaleAspectFit) -> UIImage {
           var width: CGFloat
           var height: CGFloat
           var newImage: UIImage

           let size = self.size
           let aspectRatio =  size.width/size.height

           switch contentMode {
               case .scaleAspectFit:
                   if aspectRatio > 1 {                            // Landscape image
                       width = dimension
                       height = dimension / aspectRatio
                   } else {                                        // Portrait image
                       height = dimension
                       width = dimension * aspectRatio
                   }

           default:
               fatalError("UIIMage.resizeToFit(): FATAL: Unimplemented ContentMode")
           }

           if #available(iOS 10.0, *) {
               let renderFormat = UIGraphicsImageRendererFormat.default()
               renderFormat.opaque = opaque
               let renderer = UIGraphicsImageRenderer(size: CGSize(width: width, height: height), format: renderFormat)
               newImage = renderer.image {
                   (context) in
                   self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
               }
           } else {
               UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), opaque, 0)
                   self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
                   newImage = UIGraphicsGetImageFromCurrentImageContext()!
               UIGraphicsEndImageContext()
           }

           return newImage
       }
   }

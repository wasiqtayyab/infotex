//
//  FollowingUser.swift
//  Infotex
//
//  Created by Mac on 16/09/2021.
//

import Foundation



class FollowingUsers {
    
    static let shared = FollowingUsers()
    
    func FollowingUser(obj:[String:Any]) -> followingUser {
    
        let FollowingList = obj["FollowingList"] as![String:Any]
     
        let data = followingUser(id: FollowingList["id"] as? String ?? "0", first_name: FollowingList["first_name"] as? String ?? "0", last_name: FollowingList["last_name"] as? String ?? "0", gender: FollowingList["gender"] as? String ?? "0", bio: FollowingList["bio"] as? String ?? "0", website: FollowingList["website"] as? String ?? "0", dob: FollowingList["dob"] as? String ?? "0", social_id: FollowingList["social_id"] as? String ?? "0", email: FollowingList["email"] as? String ?? "0", phone: FollowingList["phone"] as? String ?? "0", password: FollowingList["password"] as? String ?? "0", profile_pic: FollowingList["profile_pic"] as? String ?? "0", profile_pic_small: FollowingList["profile_pic_small"] as? String ?? "0", role: FollowingList["role"] as? String ?? "0", username: FollowingList["username"] as? String ?? "0", device_token: FollowingList["device_token"] as? String ?? "0", token: FollowingList["token"] as? String ?? "0", active: FollowingList["active"] as? String ?? "0", lat: FollowingList["lat"] as? String ?? "0", long: FollowingList["long"] as? String ?? "0", online: FollowingList["online"] as? String ?? "0", verified: FollowingList["verified"] as? String ?? "0", auth_token: FollowingList["auth_token"] as? String ?? "0", version: FollowingList["version"] as? String ?? "0", device: FollowingList["device"] as? String ?? "0", ip: FollowingList["ip"] as? String ?? "0", city: FollowingList["city"] as? String ?? "0", country: FollowingList["country"] as? String ?? "0", city_id: FollowingList["city_id"] as? String ?? "0", state_id: FollowingList["state_id"] as? String ?? "0", country_id: FollowingList["country_id"] as? String ?? "0", wallet: FollowingList["wallet"] as? String ?? "0", coins_from_subscription: FollowingList["coins_from_subscription"] as? String ?? "0", coins_from_gift: FollowingList["coins_from_gift"] as? String ?? "0", paypal: FollowingList["paypal"] as? String ?? "0", referral_code: FollowingList["referral_code"] as? String ?? "0", referral_used_user_id: FollowingList["referral_used_user_id"] as? String ?? "0", live_streaming: FollowingList["live_streaming"] as? String ?? "0", subscribe_package: FollowingList["subscribe_package"] as? String ?? "0", subscribe_date: FollowingList["subscribe_date"] as? String ?? "0", month: FollowingList["month"] as? String ?? "0", package: FollowingList["package"] as? String ?? "0", approve_account_agency: FollowingList["approve_account_agency"] as? String ?? "0", infotex_auth_token: FollowingList["infotex_auth_token"] as? String ?? "0", booking_coins_per_hour: FollowingList["booking_coins_per_hour"] as? String ?? "0", created: FollowingList["created"] as? String ?? "0", subscribe: FollowingList["subscribe"]as? Int ?? 0, following_count: FollowingList["following_count"] as? Int ?? 0, video_count: FollowingList["video_count"] as? Int ?? 0, button: FollowingList["button"] as? String ?? "0")
        
        return data
        
    }
}

//
//  UserDetail.swift
//  Infotex
//
//  Created by Mac on 10/09/2021.
//


import Foundation
import UIKit

class UserDetail{
    
    static let shared = UserDetail()
    
    func ObjUserDetailResponse(res:[String:Any]) -> userMVC{
        
        
        let userObj = res["User"] as! [String:Any]
        let privacySetting = res["PrivacySetting"] as! [String:Any]
        let PushNotification = res["PushNotification"] as! [String:Any]
        
        let data = userMVC(userID: userObj["id"] as! String, first_name: userObj["first_name"] as? String ?? "", last_name: userObj["last_name"] as? String ?? "", gender: userObj["gender"] as? String ?? "", bio: userObj["bio"] as? String ?? "", website: userObj["website"]as? String ?? "", dob: userObj["dob"] as? String ?? "", social_id: userObj["social_id"] as? String ?? "", userEmail: userObj["email"] as? String ?? "", userPhone: userObj["phone"] as? String ?? "", password: userObj["password"] as? String ?? "", userProfile_pic: userObj["profile_pic"] as? String ?? "", role: userObj["role"] as? String ?? "", username: userObj["username"]as? String ?? "", social: userObj["social"] as? String ?? "" ,device_token: userObj["device_token"] as? String ?? "", videoCount: userObj["video_count"] as? Int ?? 0, likesCount: userObj["likes_count"] as? Int ?? 0, followers: userObj["followers_count"] as? Int ?? 0, following: userObj["following_count"] as?Int ?? 0, followBtn: userObj["follow"] as? String ?? "follow", wallet: userObj["wallet"] as? String ?? "", paypal: userObj["paypal"] as? String ?? "",referral_code: userObj["referral_code"] as? String ?? "",referral_used_user_id: userObj["referral_used_user_id"] as? String ?? "",live_streaming:userObj["live_streaming"] as? String ?? "",subscribe_package: userObj["subscribe_package"] as? String ?? "0", subscribe_date:userObj["subscribe_date"] as? String ?? "", subscribe: userObj["subscribe"] as? Int ?? 0,month:["month"] as? String ?? "" ,package: userObj["package"] as? String ?? "",approve_account_agency:userObj["approve_account_agency"] as? String ?? "",infotex_auth_token:userObj["infotex_auth_token"] as? String ?? "",booking_coins_per_hour:userObj["booking_coins_per_hour"] as? String ?? "",Online:userObj["online"] as? String ?? "0", verified: userObj["verified"] as? String ?? "0" ,privacySetting: [privacySettingMVC(direct_message: privacySetting["direct_message"] as? String ?? "", duet: privacySetting["duet"] as? String ?? "" ,liked_videos: privacySetting["liked_videos"] as? String ?? "", video_comment: privacySetting["video_comment"] as? String ?? "", videos_download: privacySetting["videos_download"] as? String ?? "", id: privacySetting["id"] as? String ?? "")],PushNotification:[pushNotiSettingMVC(comments: PushNotification["comments"] as? String ?? "", direct_messages: PushNotification["direct_messages"] as? String ?? "", likes: PushNotification["likes"] as? String ?? "", mentions: PushNotification["mentions"] as? String ?? "", new_followers: PushNotification["new_followers"] as? String ?? "", video_updates: PushNotification["video_updates"] as? String ?? "", id: PushNotification["id"] as? String ?? "")])
          return data
    }
}

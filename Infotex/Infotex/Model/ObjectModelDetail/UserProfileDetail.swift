//
//  UserProfileDetail.swift
//  Infotex
//
//  Created by Mac on 10/09/2021.
//

import Foundation
import UIKit

//Influencer
class InfluencerProfileDetail{      //Get user profile detail on profile Screen
    
    var videoImages = [videoImage]()
    var videoComments = [videoComment]()
    var videoLike = [videolike]()
 
    static let shared = InfluencerProfileDetail()
    
    func userProfileDetail(objres:[String:Any]) -> profileDetailVideo {
        
        let Video = objres["Video"] as! [String:Any]
        let user = objres["User"] as? [String:Any] ?? ["":""]
        let Sound = objres["Sound"] as? [String:Any] ?? ["":""]
        
        let comment = objres["VideoComment"] as? [[String:Any]] ?? [["":""]]
        
        let videoLike = objres["VideoLike"] as? [[String:Any]]  ?? [["":""]]
        
        let VideoImage = objres["VideoImage"] as? [[String:Any]]  ?? [["":""]]
        
        //Video Images
        
        self.videoImages.removeAll()
        for obj in VideoImage{
            let imgData = videoImage(id: obj["id"] as? String ?? "", video_id: obj["video_id"] as? String ?? "", image: obj["image"] as? String ?? "")
            self.videoImages.append(imgData)
        }
        
        //Video Comment
        
        self.videoComments.removeAll()
        for obj in comment{
            let commentUser = obj["User"] as! [String:Any]
            let imgData = videoComment(id: obj["id"] as! String, user_id: obj["user_id"] as! String, video_id: obj["video_id"] as! String, comment: obj["comment"] as! String, username:commentUser["username"] as! String)
            self.videoComments.append(imgData)
        }
        
        //Video like
        
        self.videoLike.removeAll()
        for obj in videoLike{
            let likedUser = obj["User"] as! [String:Any]
            let imgData = videolike(id: obj["id"] as? String ?? "",user_id: obj["id"] as? String ?? "",video_id: obj["video_id"] as? String ?? "",username:likedUser["username"] as? String ?? "", user_pic:likedUser["profile_pic"] as? String ?? "")
            self.videoLike.append(imgData)
        }
        
        
        
        let data = profileDetailVideo(id: Video["id"] as? String ?? "", user_id: Video["user_id"] as? String ?? "", fb_id: Video["fb_id"] as? String ?? "", description: Video["description"] as? String ?? "", video: Video["video"] as? String ?? "", thum: Video["thum"] as? String ?? "", gif: Video["gif"] as? String ?? "", view: Video["view"] as? String ?? "", section: Video["section"] as? String ?? "", sound_id: Video["sound_id"] as? String ?? "", privacy_type: Video["privacy_type"] as? String ?? "", allow_comments: Video["allow_comments"] as? String ?? "", allow_duet: Video["allow_duet"] as? String ?? "", block: Video["block"] as? String ?? "", duet_video_id: Video["duet_video_id"] as? String ?? "", old_video_id: Video["old_video_id"] as? String ?? "", duration: Video["duration"] as? String ?? "", promote: Video["promote"] as? String ?? "", type: Video["type"] as? String ?? "", notification: Video["notification"] as? String ?? "", created: Video["created"] as? String ?? "", like: Video["like"] as? Int ?? 0, favourite: Video["favourite"] as? Int ?? 0, comment_count: Video["comment_count"] as? Int ?? 0, like_count: Video["like_count"] as? Int ?? 0, VideoMain: [videoMainMVC(videoID: Video["id"] as? String ?? "", videoUserID: Video["user_id"] as? String ?? "", fb_id:  Video["fb_id"] as? String ?? "", description: Video["description"] as? String ?? "", videoURL: Video["video"] as? String ?? "", videoTHUM: Video["thum"] as? String ?? "", videoGIF: Video["gif"] as? String ?? "", view: Video["view"] as? String ?? "", section: Video["section"] as? String ?? "", sound_id: Video["sound_id"] as? String ?? "", privacy_type: Video["privacy_type"] as? String ?? "", allow_comments: Video["allow_comments"] as? String ?? "", allow_duet: Video["allow_duet"] as? String ?? "", block: Video["block"] as? String ?? "", duet_video_id: Video["duet_video_id"] as? String ?? "", old_video_id: Video["old_video_id"] as? String ?? "", created: Video["created"] as? String ?? "", like: "\(Video["like"] as? Int ?? 0)", favourite: "\(Video["favourite"] as? Int ?? 0)", comment_count: "\(Video["comment_count"] as? Int ?? 0)", like_count: "\(Video["like_count"] as? Int ?? 0)", followBtn: "", duetVideoID: Video["duet_video_id"] as? String ?? "", userID: user["id"] as? String ?? "", first_name: user["first_name"] as? String ?? "", last_name: user["last_name"] as? String ?? "", gender: user["gender"] as? String ?? "", bio: user["bio"] as? String ?? "", website: user["website"] as? String ?? "", dob: user["dob"] as? String ?? "", social_id: user["social_id"] as? String ?? "", userEmail: user["email"] as? String ?? "", userPhone: user["phone"] as? String ?? "", password: user["password"] as? String ?? "", userProfile_pic: user["profile_pic"] as? String ?? "", role: user["role"] as? String ?? "", username: user["username"] as? String ?? "", social: user["social"] as? String ?? "", device_token: user["device"] as? String ?? "", videoCount:"", verified: "", soundName: Sound["name"] as? String ?? "", CDPlayer: Sound["thum"] as? String ?? "")], soundDetail: [soundsMVC(id: Sound["id"] as? String ?? "", audioURL: Sound["audio"] as? String ?? "", duration: Sound["duration"] as? String ?? "", name: Sound["name"] as? String ?? "", description: Sound["description"] as? String ?? "", thum: Sound["thum"] as? String ?? "", section: Sound["sound_section_id"] as? String ?? "", uploaded_by: Sound["uploaded_by"] as? String ?? "", created: Sound["created"] as? String ?? "", favourite: Sound["id"] as? String ?? "", publish: Sound["publish"] as? String ?? "")], videoImage: self.videoImages,videoComment:self.videoComments,videolike:self.videoLike)
        
        return data
        
        
    }
    
}


//User

class UserProfileDetailVideo{      //Get user profile detail on profile Screen
    
    var videoImages = [videoImage]()
    var videoComments = [videoComment]()
    var videoLike = [videolike]()
    
  
    
    static let shared = UserProfileDetailVideo()
    
    func userProfileDetail(objres:[String:Any]) -> profileDetailVideo {
        
        let Video = objres["Video"] as! [String:Any]
        let user = Video["User"] as? [String:Any] ?? ["":""]
        let Sound = Video["Sound"] as? [String:Any] ?? ["":""]
        
        let comment = Video["VideoComment"] as? [[String:Any]] ?? [["":""]]
        
        let videoLike = objres["VideoLike"] as! [String:Any]
        
        let VideoImage = Video["VideoImage"] as? [[String:Any]]  ?? [["":""]]
        
        //Video Images
        
        self.videoImages.removeAll()
        for obj in VideoImage{
            let imgData = videoImage(id: obj["id"] as? String ?? "", video_id: obj["video_id"] as? String ?? "", image: obj["image"] as? String ?? "")
            self.videoImages.append(imgData)
        }
        
        //Video Comment
        
        self.videoComments.removeAll()
        for obj in comment{
            let commentUser = Video["User"] as! [String:Any]
            let imgData = videoComment(id: obj["id"] as! String, user_id: obj["user_id"] as! String, video_id: obj["video_id"] as! String, comment: obj["comment"] as! String, username:commentUser["username"] as! String)
            self.videoComments.append(imgData)
        }
        
        //Video like
        
        self.videoLike.removeAll()
      
            let likedUser = user
            let imgData = videolike(id: videoLike["id"] as? String ?? "",user_id: videoLike["id"] as? String ?? "",video_id: videoLike["video_id"] as? String ?? "",username:likedUser["username"] as? String ?? "", user_pic:likedUser["profile_pic"] as? String ?? "")
            self.videoLike.append(imgData)
        
        
        
        
        let data = profileDetailVideo(id: Video["id"] as? String ?? "", user_id: Video["user_id"] as? String ?? "", fb_id: Video["fb_id"] as? String ?? "", description: Video["description"] as? String ?? "", video: Video["video"] as? String ?? "", thum: Video["thum"] as? String ?? "", gif: Video["gif"] as? String ?? "", view: Video["view"] as? String ?? "", section: Video["section"] as? String ?? "", sound_id: Video["sound_id"] as? String ?? "", privacy_type: Video["privacy_type"] as? String ?? "", allow_comments: Video["allow_comments"] as? String ?? "", allow_duet: Video["allow_duet"] as? String ?? "", block: Video["block"] as? String ?? "", duet_video_id: Video["duet_video_id"] as? String ?? "", old_video_id: Video["old_video_id"] as? String ?? "", duration: Video["duration"] as? String ?? "", promote: Video["promote"] as? String ?? "", type: Video["type"] as? String ?? "", notification: Video["notification"] as? String ?? "", created: Video["created"] as? String ?? "", like: Video["like"] as? Int ?? 0, favourite: Video["favourite"] as? Int ?? 0, comment_count: Video["comment_count"] as? Int ?? 0, like_count: Video["like_count"] as? Int ?? 0, VideoMain: [videoMainMVC(videoID: Video["id"] as? String ?? "", videoUserID: Video["user_id"] as? String ?? "", fb_id:  Video["fb_id"] as? String ?? "", description: Video["description"] as? String ?? "", videoURL: Video["video"] as? String ?? "", videoTHUM: Video["thum"] as? String ?? "", videoGIF: Video["gif"] as? String ?? "", view: Video["view"] as? String ?? "", section: Video["section"] as? String ?? "", sound_id: Video["sound_id"] as? String ?? "", privacy_type: Video["privacy_type"] as? String ?? "", allow_comments: Video["allow_comments"] as? String ?? "", allow_duet: Video["allow_duet"] as? String ?? "", block: Video["block"] as? String ?? "", duet_video_id: Video["duet_video_id"] as? String ?? "", old_video_id: Video["old_video_id"] as? String ?? "", created: Video["created"] as? String ?? "", like: "\(Video["like"] as? Int ?? 0)", favourite: "\(Video["favourite"] as? Int ?? 0)", comment_count: "\(Video["comment_count"] as? Int ?? 0)", like_count: "\(Video["like_count"] as? Int ?? 0)", followBtn: "", duetVideoID: Video["duet_video_id"] as? String ?? "", userID: user["id"] as? String ?? "", first_name: user["first_name"] as? String ?? "", last_name: user["last_name"] as? String ?? "", gender: user["gender"] as? String ?? "", bio: user["bio"] as? String ?? "", website: user["website"] as? String ?? "", dob: user["dob"] as? String ?? "", social_id: user["social_id"] as? String ?? "", userEmail: user["email"] as? String ?? "", userPhone: user["phone"] as? String ?? "", password: user["password"] as? String ?? "", userProfile_pic: user["profile_pic"] as? String ?? "", role: user["role"] as? String ?? "", username: user["username"] as? String ?? "", social: user["social"] as? String ?? "", device_token: user["device"] as? String ?? "", videoCount:"", verified: "", soundName: Sound["name"] as? String ?? "", CDPlayer: Sound["thum"] as? String ?? "")], soundDetail: [soundsMVC(id: Sound["id"] as? String ?? "", audioURL: Sound["audio"] as? String ?? "", duration: Sound["duration"] as? String ?? "", name: Sound["name"] as? String ?? "", description: Sound["description"] as? String ?? "", thum: Sound["thum"] as? String ?? "", section: Sound["sound_section_id"] as? String ?? "", uploaded_by: Sound["uploaded_by"] as? String ?? "", created: Sound["created"] as? String ?? "", favourite: Sound["id"] as? String ?? "", publish: Sound["publish"] as? String ?? "")], videoImage: self.videoImages,videoComment:self.videoComments,videolike:self.videoLike)
        
        return data
        
        
    }
    
}

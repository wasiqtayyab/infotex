//
//  VideoMainVC.swift
//  Infotex
//
//  Created by Mac on 01/09/2021.
//

import Foundation


struct videoMainMVC {
//    video
    let videoID:String
    let videoUserID:String
    let fb_id:String
    let description:String
    let videoURL:String
    let videoTHUM:String
    let videoGIF:String
    let view:String
    let section:String
    let sound_id:String
    let privacy_type:String
    var allow_comments:String
    var allow_duet:String
    let block:String
    let duet_video_id:String
    let old_video_id:String
    let created:String
    var like:String
    let favourite:String
    var comment_count:String
    var like_count:String
    var followBtn:String
    let duetVideoID:String
    
//    user
    let userID:String
    let first_name:String
    let last_name:String
    let gender:String
    let bio:String
    let website:String
    let dob:String
    let social_id:String
    let userEmail:String
    let userPhone:String
    let password:String
    let userProfile_pic:String
    let role:String
    let username:String
    let social:String
    let device_token:String
    let videoCount:String
    let verified:String

//    sound
    let soundName:String
    let CDPlayer:String


}

struct videoDetailMVC {
    
    let vidLikes:String
    let vidComments:String
    let isLike:String
    
}


//
//  FollowingUser.swift
//  Infotex
//
//  Created by Mac on 16/09/2021.
//

import Foundation


struct followingUser {
    
    var id: String
    var first_name: String
    var last_name: String
    var gender: String
    var bio: String
    var website: String
    var dob: String
    var social_id: String
    var email: String
    var phone: String
    var password: String
    var profile_pic: String
    var profile_pic_small: String
    var role: String
    var username: String
    var device_token: String
    var token: String
    var active: String
    var lat: String
    var long: String
    var online: String
    var verified: String
    var auth_token: String
    var version: String
    var device: String
    var ip: String
    var city: String
    var country: String
    var city_id: String
    var state_id: String
        
    var country_id: String
    var wallet: String
    var coins_from_subscription: String
    var coins_from_gift: String
    var paypal: String
    var referral_code: String
    var referral_used_user_id: String
    var live_streaming: String
    var subscribe_package: String
    var subscribe_date: String
    var month: String
    var package: String
    var approve_account_agency: String
    var infotex_auth_token: String
    var booking_coins_per_hour: String
    var created: String
    var subscribe: Int
                 
    var following_count: Int
    var video_count: Int
    var button: String
          
}

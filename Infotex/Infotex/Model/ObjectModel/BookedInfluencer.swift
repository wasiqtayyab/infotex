//
//  BookedInfluencer.swift
//  Infotex
//
//  Created by Mac on 12/10/2021.
//

import Foundation

struct  BookingInfluencer {
    var Booking       : Booking
    var User          : userMVC
    var Influencer    : userMVC
    var UserBookingAvailableTiming : UserBookingAvailableTiming
}

struct Booking{
    let id :String?
    let user_id:String?
    let influencer_id:String?
    let user_booking_available_timing_id:String?
    let live_streaming_session:String?
    let created:String?
}
struct UserBookingAvailableTiming{
    let id :String?
    let user_id:String?
    let date:String?
    let booking_time_id:String?
    let created:String?
    var arrBookingTime: BookingTime?
}
struct BookingTime{
    let id :String?
    let time:String?
   
}

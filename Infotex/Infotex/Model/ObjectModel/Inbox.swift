//
//  Inbox.swift
//  Freshly
//
//  Created by Mac on 12/11/20.


import UIKit

class Inbox: NSObject {
    
    var date:String! = ""
    var rid:String! = ""
    var msg:String! = ""
    var username:String! = ""
    var status:String?
    var picture:String! = ""
    var timestamp:String! = ""
 
    init(date:String,rid:String!, msg: String!,username:String!,status:String?,picture:String!,timestamp: String!) {
        
        self.date = date
        self.rid = rid
        self.msg = msg
        self.username = username
        self.status = status
        self.picture = picture
        self.timestamp = timestamp
    
    }

}

//
//  VideoMainVC.swift
//  Infotex
//
//  Created by Mac on 01/09/2021.
//


import Foundation
struct userMVC {
    
    let userID:String
    let first_name:String
    let last_name:String
    let gender:String
    let bio:String
    let website:String
    let dob:String
    let social_id:String
    let userEmail:String
    let userPhone:String
    let password:String
    let userProfile_pic:String
    let role:String
    let username:String
    let social:String
    let device_token:String
    let videoCount:Int
    let likesCount:Int
    let followers:Int
    let following:Int
    let followBtn:String
    let wallet:String
    let paypal:String
    let referral_code:String
    let referral_used_user_id:String
    let live_streaming:String
    let subscribe_package:String
    let subscribe_date:String
    let subscribe:Int
    let month:String
    let package:String
    let approve_account_agency:String
    let infotex_auth_token:String
    let booking_coins_per_hour:String
    let Online:String
    let verified:String
    var privacySetting:[privacySettingMVC]
    var PushNotification:[pushNotiSettingMVC]
}

//
//  ProfileDetailMVC.swift
//  Infotex
//
//  Created by Mac on 10/09/2021.
//

import Foundation

struct profileDetailVideo{
    var id:String
    var user_id:String
    var fb_id:String
    var description:String
    var video:String
    var thum:String
    var gif:String
    var view:String
    var section:String
    var sound_id:String
    var privacy_type:String
    var allow_comments:String
    var allow_duet:String
    var block:String
    var duet_video_id:String
    var old_video_id:String
    var duration:String
    var promote:String
    var type:String
    var notification:String
    var created:String
    var like:Int
    var favourite:Int
    var comment_count:Int
    var like_count:Int
    var VideoMain : [videoMainMVC]
    var soundDetail:[soundsMVC]
    var videoImage:[videoImage]
    var videoComment:[videoComment]
    var videolike:[videolike]
}


struct videoImage{
    var id :String
    var video_id :String
    var image:String
}
struct videoComment{
    var id :String
    var user_id :String
    var video_id :String
    var comment:String
    var username:String
}
struct videolike{
    var id :String
    var user_id :String
    var video_id :String
    var username:String
    var user_pic:String
}

//
//  VideoMainVC.swift
//  Infotex
//
//  Created by Mac on 01/09/2021.
//



import Foundation

struct notificationsMVC {
    let notificationString:String
    let id:String
    let sender_id:String
    let receiver_id:String
    let type:String
    let video_id:String
    
    let senderName:String
    let senderFirstName:String

    let receiverName:String
    let senderImg:String
}

//
//  VideoMainVC.swift
//  Infotex
//
//  Created by Mac on 01/09/2021.
//



import Foundation

struct pushNotiSettingMVC {
    let comments : String
    let direct_messages : String
    let likes : String
    let mentions : String
    let new_followers : String
    let video_updates : String
    let id : String
}

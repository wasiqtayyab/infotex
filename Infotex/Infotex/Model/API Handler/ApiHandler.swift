//
//  ApiHandler.swift
//  Infotex
//
//  Created by Mac on 03/09/2021.
//  Copyright © 2021 Mac. All rights reserved.

import Foundation
import UIKit
import Alamofire

var BASE_URL = "https://infotex.club/mobileapp_api/"
let API_KEY = "156c4675-9608-4591-b2ec-427503464aac-123"
let API_BASE_URL = BASE_URL+"api/"

let headers: HTTPHeaders = [
    "Api-Key":API_KEY
    
]

private let SharedInstance = ApiHandler()

enum Endpoint : String {
    
    case showSounds              = "showSounds"
    case postImage               = "postImage"
    case registerUser            = "registerUser"
    case verifyRegisterEmailCode = "verifyRegisterEmailCode"
    case login                   = "login"
    case verifyPhoneNo           = "verifyPhoneNo"
    case registerDevice          = "registerDevice"
    case postVideo               = "postVideo"
    case editVideo               = "editVideo"
    
    case showRelatedVideos       = "showRelatedVideos"
    case showFollowingVideos     = "showFollowingVideos"
    case showVideosAgainstUserID = "showVideosAgainstUserID"
    
    case generateReferralCode    = "generateReferralCode"
    
    case showUserDetail          = "showUserDetail"
    case likeVideo               = "likeVideo"
    case watchVideo              = "watchVideo"
    case showSoundsAgainstSection   = "showSoundsAgainstSection"
    case addSoundFavourite       = "addSoundFavourite"
    case showFavouriteSounds     = "showFavouriteSounds"
    case showUserLikedVideos     = "showUserLikedVideos"
    case followUser              = "followUser"
    case showDiscoverySections   = "showDiscoverySections"
    case subscribePackage        = "subscribePackage"
    
    //MARK:- Influencer Booking
    
    case addInfluncerBookingsTime = "addUserBookingTime"
    case showInfluncerBookings    = "showInfluencerBookings"
    case addBooking               = "addBooking"
    case showBooking              = "showBookings"
    case showBookingWithInfluencer = "showBookingWithInfluencer"
    //MARK:- Comments
    
    case showVideoComments       = "showVideoComments"
    case likeComment             = "likeComment"
    case likeCommentReply        = "likeCommentReply"
    case postCommentOnVideo      = "postCommentOnVideo"
    case postCommentReply        = "postCommentReply"
    
    //MARK:- Gift
    case showGifts               = "showGifts"
    case sendGift                = "sendGift"
    case showGiftCategories      = "showGiftCategories"
    case verfiyCouponCode        = "verifyCouponCode"
    case blockUser               = "blockUser"
    
    
    case editProfile             = "editProfile"
    case showFollowers           = "showFollowers"
    case showFollowing           = "showFollowing"
    case showVideosAgainstHashtag = "showVideosAgainstHashtag"
    case sendMessageNotification = "sendMessageNotification"
    case addUserImage            = "addUserImage"
    case deleteVideo             = "deleteVideo"
    case search                  = "search"
    case showVideoDetail         = "showVideoDetail"
    case showAllNotifications    = "showAllNotifications"
    case userVerificationRequest = "userVerificationRequest"
    case downloadVideo           = "downloadVideo"
    case deleteWaterMarkVideo    = "deleteWaterMarkVideo"
    case showAppSlider           = "showAppSlider"
    case logout                  = "logout"
    case showVideosAgainstSound  = "showVideosAgainstSound"
    case showReportReasons       = "showReportReasons"
    case reportVideo             = "reportVideo"
    case verifyUsername          = "verifyUsername"
    case verifyEmail             = "verifyEmail"
    case addDeviceData           = "addDeviceData"
    case forgotPassword          = "forgotPassword"
    case changePasswordForgot    = "changePasswordForgot"
    case verifyforgotPasswordCode = "verifyforgotPasswordCode"
    case verifyReferralCode       = "verifyReferralCode"
    case subscribeUser            = "subscribeUser"
    case verifyChangeEmailCode    = "verifyChangeEmailCode"
    case changeEmailAddress       = "changeEmailAddress"
    
    
    case NotInterestedVideo      = "NotInterestedVideo"
    case addVideoFavourite       = "addVideoFavourite"
    case showFavouriteVideos     = "showFavouriteVideos"
    case updatePushNotificationSettings  = "updatePushNotificationSettings"
    case addHashtagFavourite     =  "addHashtagFavourite"
    case showFavouriteHashtags   = "showFavouriteHashtags"
    case reportUser              = "reportUser"
    case addPrivacySetting       = "addPrivacySetting"

    
    case purchaseCoin             = "purchaseCoin"
    case coinWorth                = "showCoinWorth"
    case coinWithDrawRequest      = "withdrawRequest"
    case addPayOut                = "addPayout"
    case showSuggestedUsers       = "showSuggestedUsers"
    case changePhoneNo            = "changePhoneNo"
    case changePassword           = "changePassword"
    case DeleteAccount            = "deleteUserAccount"
    
}
class ApiHandler:NSObject{
    var baseApiPath:String!
    
    let fcm = UserDefaults.standard.string(forKey: "DeviceToken") as? String ?? ""
    let ip = UserDefaults.standard.string(forKey: "ipAddress") as? String ?? ""
    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.0"
    let deviceType = "iOS"
    
    class var sharedInstance : ApiHandler {
        return SharedInstance
    }
    
    override init() {
        self.baseApiPath = API_BASE_URL
    }
    
    //    MARK:- editProfile
    
    func editProfile(user_id:String,phone:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            
            "user_id"         : user_id,
            "phone"         : phone
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.editProfile.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //    MARK:- changeEmailAddress
    
    func changeEmailAddress(user_id:String,email:String,verify:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            
            "user_id"         : user_id,
            "email"         : email,
            "verify"             : verify
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.changeEmailAddress.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
  
    
    
    //    MARK:- verifyChangeEmailCode
    
    func verifyChangeEmailCode(user_id:String,new_email:String,code:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            
            "user_id"         : user_id,
            "new_email"         : new_email,
            "code"             : code
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.verifyChangeEmailCode.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
  
    
    //    MARK:- verifyReferralCode
    
    func verifyReferralCode(user_id:String,referral_code:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            
            "user_id"         : user_id,
            "referral_code"         : referral_code
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.verifyReferralCode.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //    MARK:- changePasswordForgot
    
    func changePasswordForgot(email:String,password:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            
            "email"         : email,
            "password"         : password
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.changePasswordForgot.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //    MARK:- verifyforgotPasswordCode
    
    func verifyforgotPasswordCode(email:String,code:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            
            "email"         : email,
            "code"         : code
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.verifyforgotPasswordCode.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //    MARK:- forgetPassword
    
    func forgetPassword(email:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            
            "email"         : email
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.forgotPassword.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //    MARK:- verifyUsername
    
    func verifyUsername(username:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            
            "username"         : username
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.verifyUsername.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //    MARK:- verifyEmail
    
    func verifyEmail(email:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            
            "email"         : email
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.verifyEmail.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //    MARK:- registerUser
    func registerUser(email:String,password:String,dob:String,username:String,role:String,phone:String,gender:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "dob"         : dob,
            "email"         : email,
            "username"         : username,
            "password"         : password,
            "gender" : gender,
            "phone" : phone,
            "role" : role,
            "device_token" : AppUtility?.getObject(forKey: strAPNSToken) as! String
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.registerUser.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK: verifyRegisterEmailCodeWithEmail
    
    func verifyRegisterEmailCodeWithEmail(email:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
            
        ]
        var parameters = [String : String]()
        parameters = [
            "email"         : email
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.verifyRegisterEmailCode.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK: verifyRegisterEmailCodeWithCode
    
    func verifyRegisterEmailCodeWithCode(email:String,code:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
            
        ]
        var parameters = [String : String]()
        parameters = [
            "email" : email,
            "code" : code
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.verifyRegisterEmailCode.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- Login
    func login(email:String,password:String,role:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY,
            
        ]
        var parameters = [String : String]()
        parameters = [
            
            "email"   : email,
            "password": password,
            "role": role
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.login.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- Verify Phone number
    func verifyPhoneNo(phone:String,verify:String,code:String,IsVerfied:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        if IsVerfied == "0"{
            parameters = [
                
                "phone" : phone,
                "verify": verify
                
                
            ]
        }else{
            parameters = [
                
                "phone" : phone,
                "verify": verify,
                "code"  : code
                
                
            ]
        }
        
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.verifyPhoneNo.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK:- Add Device Data
    
    func addDeviceData(user_id:String,device:String,version:String,ip:String,device_token:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"      : user_id,
            "device"       : device,
            "version"      : version,
            "ip"           : ip,
            "device_token" : device_token,
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.addDeviceData.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- registerDevice
    
    func registerDevice(key:String,completionHandler:@escaping(_ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "key":key
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.registerDevice.rawValue)"
        
        print(finalUrl)
        print(parameters)
        print(headers)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK:- postVideo
    
    //    IN POST VIEW CONTROLLER CODE MULTIPART
    func postVideo(User_id :String ,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY,
            "User-Id":User_id
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.postVideo.rawValue)"
        
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK:-showRelatedVideos
    func showRelatedVideos(device_id:String,user_id:String,starting_point:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        
        
        
        
        let headers: HTTPHeaders = [
            "Api-Key"      : API_KEY,
            "user-id"      : user_id,
            "device"       : deviceType,
            "version"      : appVersion,
            "ip"           : ip,
            "device_token" : fcm,
            "Auth-Token"   : ""
            
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"        : user_id,
            "device_id"      : device_id,
            "starting_point" : starting_point
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showRelatedVideos.rawValue)"
        
        print("finalURL",finalUrl)
        print(parameters)
        print(headers)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default
                   , headers: headers).responseJSON { (response) in
                    print("response: related vid",response.value)
                    
                    switch response.result {
                    
                    case .success(_):
                        if let json = response.value
                        {
                            do {
                                let dict = json as? NSDictionary
                                print(dict)
                                completionHandler(true, dict)
                                
                            } catch {
                                completionHandler(false, nil)
                            }
                        }
                        break
                    case .failure(let error):
                        print("Failure")
                        if let json = response.value
                        {
                            do {
                                let dict = json as? NSDictionary
                                print(dict)
                                completionHandler(true, dict)
                                
                            } catch {
                                completionHandler(false, nil)
                            }
                        }
                        break
                    }
                   }
    }
    //MARK:-showFollowingVideos
    func showFollowingVideos(user_id:String,device_id:String,starting_point:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY,
            "User_Id":user_id,
            //            "Auth_token" : UserDefaults.standard.string(forKey: "authToken")!
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"        : user_id,
            "device_id"      : device_id,
            "starting_point" : starting_point
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showFollowingVideos.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    //MARK:-showVideosAgainstUserID
    func showVideosAgainstUserID(user_id:String,other_user_id:String,type:String,starting_point:Int,duration:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        
        var strtype = ""
        if type == "image"{
        strtype  = "image"
        
        }else if type == "24"{
            strtype  = "24"
        
        }else{
            strtype  = "video"
            
        }
        
        var parameters = [String : Any]()
       
        if other_user_id == ""{
            parameters = [
                "user_id"         : user_id,
                "type"            : strtype,
                "starting_point"  : starting_point,
                "duration"        : duration
                
                
            ]
        }else{
            parameters = [
                "user_id"         : user_id,
                "other_user_id"   : other_user_id,
                "type"            : strtype,
                "starting_point"  : starting_point,
                "duration"        : duration
            ]
        }
       
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showVideosAgainstUserID.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK:-showUserVideosAgainstUserID
    func showUserVideosAgainstUserID(user_id:String,other_user_id:String,starting_point:Int,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
    
        var parameters = [String : Any]()
       
        if other_user_id == ""{
            parameters = [
                "user_id"         : user_id,
                "starting_point"  : starting_point
                
                
            ]
        }else{
            parameters = [
                "user_id"         : user_id,
                "starting_point"  : starting_point,
                "other_user_id"   : other_user_id,
            ]
        
        }
            
            
        
       
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showUserLikedVideos.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK:-showOtherUserDetail
    func showUserDetail(user_id:String,other_user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
     
        var parameters = [String : Any]()
       
        if other_user_id == "" || other_user_id == "0"{
            parameters = [
                "user_id"         : user_id,
            ]
        }else{
            parameters = [
                "user_id"         : user_id,
                "other_user_id"   : other_user_id,
            ]
        
        }
            
        
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showUserDetail.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:-showOwnDetail
    func showOwnDetailByName(username:String,user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "username" : username,
            "user_id" : user_id
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showUserDetail.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:-showOwnDetail
    func showOwnDetail(user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"        : user_id
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showUserDetail.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    //MARK:-LikeVideo
    func likeVideo(user_id:String,video_id:String,type:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        if type  == "" {
            parameters = [
                "user_id"        : user_id,
                "video_id"       : video_id,

            ]
        }else{
            parameters = [
                "user_id"        : user_id,
                "video_id"       : video_id,
                "type"           : type
            ]
        }
      
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.likeVideo.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    
    //MARK:-WatchVideo
    func watchVideo(device_id:String,user_id:String,video_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "device_id"        : device_id,
            "video_id"         : video_id,
            "user_id"          : user_id
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.watchVideo.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        // print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:-showSounds
    func showSounds(user_id:String,starting_point:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"          : user_id,
            "starting_point"   : starting_point,
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showSounds.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK:-addSoundFavourite
    func addSoundFavourite(user_id:String,sound_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"    : user_id,
            "sound_id"   : sound_id,
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.addSoundFavourite.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK:-showSounds
    func showSoundsAgainstSection(user_id:String,starting_point:String,sectionID:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"          : user_id,
            "starting_point"   : starting_point,
            "sound_section_id"   : sectionID
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showSoundsAgainstSection.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    //MARK:-showFavouriteSounds
    func showFavouriteSounds(user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"    : user_id,
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showFavouriteSounds.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:-showUserLikedVideos
    func showUserLikedVideos(user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"    : user_id,
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showUserLikedVideos.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK:-followUser
    func followUser(sender_id:String,receiver_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "sender_id"    : sender_id,
            "receiver_id"  : receiver_id
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.followUser.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:-Subscribe User
    func subscribeUser(sender_id:String,receiver_id:String,subscribe:String,subscribe_expiry_date:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "sender_id"    : sender_id,
            "receiver_id"  : receiver_id,
            "subscribe"    : subscribe,
            "coins":"0",
            "subscribe_expiry_date":subscribe_expiry_date
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.subscribeUser.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- showDiscoverySections
    
    func showDiscoverySections(completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showDiscoverySections.rawValue)"
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [ "":""
                       
        ]
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:-showVideoComments
    func showVideoComments(video_id:String,user_id:String,starting_point:Int,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : Any]()
        parameters = [
            "video_id"          : video_id,
            "user_id"           : user_id,
            "starting_point"    : starting_point
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showVideoComments.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    //MARK:- postCommentOnVideo
    func postCommentOnVideo(user_id:String,comment:String,video_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?,_ err:String)->Void){
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.postCommentOnVideo.rawValue)"
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "video_id"    : video_id,
            "comment"    : comment,
            "user_id"    : user_id
        ]
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict, "")
                        
                    } catch {
                        completionHandler(false, nil, "")
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict, error.localizedDescription)
                        
                    } catch {
                        completionHandler(false, nil, error.localizedDescription)
                    }
                }
                break
            }
        }
    }
    
    
    
    //MARK:- postCommentReply
    func postCommentReply(user_id:String,comment:String,comment_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?,_ err:String)->Void){
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.postCommentReply.rawValue)"
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "comment_id" : comment_id,
            "comment"    : comment,
            "user_id"    : user_id
        ]
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict, "")
                        
                    } catch {
                        completionHandler(false, nil, "")
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict, error.localizedDescription)
                        
                    } catch {
                        completionHandler(false, nil, error.localizedDescription)
                    }
                }
                break
            }
        }
    }
    
    //MARK:-like comments
    func likeComments(user_id:String,comment_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"    : user_id,
            "comment_id"    : comment_id
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.likeComment.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    
    //MARK:-likeReplycomments
    
    func likeReplyComments(user_id:String,comment_reply_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"    : user_id,
            "comment_reply_id"    : comment_reply_id
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.likeCommentReply.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- editProfile
    func editProfile(username:String,user_id:String,first_name:String,last_name:String,gender:String,website:String,bio:String,phone:String ,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.editProfile.rawValue)"
        
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        
        var parameteres = [String:String]()
        parameteres = [
            "username":username,
            "user_id":user_id,
            "first_name":first_name,
            "last_name":last_name,
            "gender":gender,
            "website":website,
            "phone"  : phone,
            "bio":bio
        ]
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameteres, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    //MARK:- showFollowers
    func showFollowers(user_id:String,other_user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"          : user_id,
            "other_user_id"    : other_user_id,
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showFollowers.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- ShowFollowing
    func showFollowing(user_id:String,starting_point:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
      
            parameters = [
                "user_id"          : user_id,
                "starting_point"    :starting_point
            ]
        
       
        
       
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showFollowing.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- showVideosAgainstHashtag
    func showVideosAgainstHashtag(user_id:String,hashtag:String,starting_point:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"    : user_id,
            "hashtag"    : hashtag,
            "starting_point" : starting_point,
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showVideosAgainstHashtag.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- sendMessageNotification
    
    func sendMessageNotification(senderID:String,receiverID:String,msg:String,title:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "sender_id": senderID,
            "receiver_id": receiverID,
            "message": msg,
            "title": title
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.sendMessageNotification.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- Add User Image
    func addUserImage(user_id:String,profile_pic:Any,profile_pic_small:Any,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : Any]()
        parameters = [
            "user_id"    : user_id,
            "profile_pic"    : profile_pic,
            "profile_pic_small" : profile_pic_small
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.addUserImage.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- admin/deleteVideo
    
    func deleteVideo(video_id:String,image:String,UserID:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        if image == "video"{
            
            parameters = [
                "video_id"    : video_id,
                "type"        : "video",
                "user_id"     : UserID
            ]
        }else{
            parameters = [
                "video_id"    : video_id,
                "type"        : "image",
                "user_id"     : UserID
            ]
        }
     
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.deleteVideo.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    //MARK:- Search
    func Search(user_id:String,type:String,keyword:String,starting_point:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"        : user_id,
            "type"           : type,
            "keyword"        : keyword,
            "starting_point" : starting_point
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.search.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    //MARK:- showVideoDetail
    func showVideoDetail(user_id:String,video_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"    : user_id,
            "video_id"   : video_id
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showVideoDetail.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK:- showAllNotifications
    func showAllNotifications(user_id:String,starting_point:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id":user_id,
            "starting_point":starting_point
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showAllNotifications.rawValue)"
        
        print(finalUrl)
        print(parameters)
        
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- userVerificationRequest
    func userVerificationRequest(user_id:String,attachment:Any,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : Any]()
        parameters = [
            "user_id"      : user_id,
            "attachment"   : attachment
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.userVerificationRequest.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- downloadVideo
    func downloadVideo(video_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key" : API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "video_id" : video_id
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.downloadVideo.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- deleteWaterMarkVideo
    func deleteWaterMarkVideo(video_url:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "video_url" : video_url
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.deleteWaterMarkVideo.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    //MARK:- showAppSlider
    func showAppSlider(completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "" : ""
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showAppSlider.rawValue)"
        
        print(finalUrl)
        print(parameters)
        
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- logout
    func logout(user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY,
            "User_Id":user_id
            //            "Auth_token" : UserDefaults.standard.string(forKey: "authToken")!
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.logout.rawValue)"
        var parameters = [String : String]()
        parameters = [
            "user_id"        : user_id
        ]
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- showVideosAgainstSound
    
    func showVideosAgainstSound(sound_id:String,starting_point:String,device_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        
        var parameters = [String : String]()
        parameters = [
            "sound_id"   : sound_id,
            "sound_id"   : starting_point,
            "device_id"  : device_id,
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showVideosAgainstSound.rawValue)"
        print(finalUrl)
        print(parameters)
        
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:-showReportReasons
    func showReportReasons(completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        
        var parameters = [String : String]()
        parameters = [
            "":""
            
        ]
        
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showReportReasons.rawValue)"
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:-reportVideo
    func reportVideo(user_id:String,video_id:String,report_reason_id:String,description:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.reportVideo.rawValue)"
        var parameters = [String : String]()
        parameters = [
            "user_id"           : user_id,
            "video_id"          : video_id,
            "report_reason_id"  : report_reason_id,
            "description"       : description,
            
        ]
        print(finalUrl)
        print(parameters)
        
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- Block User
    func blockUser(user_id:String,block_user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.blockUser.rawValue)"
        var parameters = [String : String]()
        parameters = [
            "user_id" : user_id,
            "block_user_id" : block_user_id
        ]
        print(finalUrl)
        print(parameters)
        
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    //MARK:- NotInterestedVideo
    func NotInterestedVideo(video_id:String,user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.NotInterestedVideo.rawValue)"
        var parameters = [String : String]()
        parameters = [
            "video_id"  : video_id,
            "user_id"   : user_id,
        ]
        print(finalUrl)
        print(parameters)
        
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- addVideoFavourite
    func addVideoFavourite(video_id:String,user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.addVideoFavourite.rawValue)"
        var parameters = [String : String]()
        parameters = [
            "video_id"   : video_id,
            "user_id"    : user_id,
            
            
        ]
        print(finalUrl)
        print(parameters)
        
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK:- showFavouriteVideos
    func showFavouriteVideos(user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showFavouriteVideos.rawValue)"
        var parameters = [String : String]()
        parameters = [
            "user_id"          : user_id,
        ]
        print(finalUrl)
        print(parameters)
        
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- updatePushNotificationSettings
    func updatePushNotificationSettings(user_id:String,comments:String,new_followers:String,mentions:String,likes:String,direct_messages:String,video_updates:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.updatePushNotificationSettings.rawValue)"
        var parameters = [String : String]()
        parameters = [
            "likes"          : likes,
            "comments"       : comments,
            "new_followers"  : new_followers,
            "mentions"       : mentions,
            "video_updates"  : video_updates,
            "direct_messages"  : direct_messages,
            "user_id"        : user_id
        ]
        print(finalUrl)
        print(parameters)
        
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    //MARK:- addHashtagFavourite
    func addHashtagFavourite(user_id:String,hashtag_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.addHashtagFavourite.rawValue)"
        var parameters = [String : String]()
        parameters = [
            "hashtag_id"    : hashtag_id,
            "user_id"       : user_id,
            
            
        ]
        print(finalUrl)
        print(parameters)
        
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- showFavouriteHashtags
    func showFavouriteHashtags(user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showFavouriteHashtags.rawValue)"
        var parameters = [String : String]()
        parameters = [
            "user_id"       : user_id,
            
        ]
        print(finalUrl)
        print(parameters)
        
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- reportUser
    func reportUser(user_id:String,report_user_id:String,report_reason_id:String,description:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.reportUser.rawValue)"
        var parameters = [String : String]()
        parameters = [
            "user_id"          : user_id,
            "report_user_id"   : report_user_id,
            "report_reason_id" : report_reason_id,
            "description"      : description
            
        ]
        print(finalUrl)
        print(parameters)
        
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- addPrivacySetting
    func addPrivacySetting(videos_download:String,direct_message:String,duet:String,liked_videos:String,video_comment:String,user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.addPrivacySetting.rawValue)"
        var parameters = [String : String]()
        parameters = [
            "videos_download" : videos_download,
            "direct_message"  : direct_message,
            "duet"            : duet,
            "liked_videos"    : liked_videos,
            "video_comment"   : video_comment,
            "user_id"         : user_id,
            
            
        ]
        print(finalUrl)
        print(parameters)
        
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK:- Suggested Users
    
    func showSuggestedUsers(user_id:String,starting_point:Int,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showSuggestedUsers.rawValue)"
        var parameters = [String : Any]()
        parameters = [
            "user_id" : user_id,
            "starting_point"  : starting_point,
        ]
        print(finalUrl)
        print(parameters)
        
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
//    //MARK: - search hashtags
//    func getAllHashtags(uid:String,starting_point:String, completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?, _ userObj:Hashtag?)->Void){
//
//        //        self.ldr.activityStartAnimating()
//
//        let headers: HTTPHeaders = [
//            "Api-Key":API_KEY
//
//        ]
//        var parameters = [String : String]()
//        parameters = [
//            "keyword": "",
//            "type": "hashtag",
//            "starting_point": starting_point,
//            "user_id": uid
//        ]
//        let finalUrl = "\(self.baseApiPath!)\(Endpoint.search.rawValue)"
//
//        print(finalUrl)
//        print(parameters)
//        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
//            print(response.result)
//
//            //            self.ldr.activityStopAnimating()
//            //            let obj = response.data?.getDecodedObject(from: Hashtag.self)
//
//
//
//
//            switch response.result {
//
//            case .success(_):
//
//                if let json = response.value
//                {
//                    do {
//                        let dict = json as? NSDictionary
//                        print(dict)
//                        completionHandler(true, dict, obj)
//
//                    } catch {
//                        completionHandler(false, nil,nil)
//                    }
//                }
//                break
//            case .failure(let error):
//
//                if let json = response.value
//                {
//                    do {
//                        let dict = json as? NSDictionary
//                        print(dict)
//                        completionHandler(true, dict, obj)
//
//                    } catch {
//                        completionHandler(false, nil,nil)
//                    }
//                }
//                break
//            }
//        }
//    }
//
//
    
    
    
    //MARK:- Coins Purchase
    func purchaseCoin(user_id:String,coin:String, title:String, price:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
            
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id":user_id,
            "coin":coin,
            "title":title,
            "price":price,
            "transaction_id":"ios.test.purchased",
            "device":"ios"
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.purchaseCoin.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
        
            switch response.result {
            
            case .success(_):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    func showCoinWorth(completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
            
        ]
        var parameters = [String : Any]()
        parameters = ["":""]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.coinWorth.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
          
            
            switch response.result {
            
            case .success(_):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    func coinWithDrawRequest(user_id:String,amount:Int,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
            
        ]
        var parameters = [String : Any]()
        
        parameters = ["user_id":user_id,
                      "amount":amount]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.coinWithDrawRequest.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
          
            switch response.result {
            
            case .success(_):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict )
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    func addPayout(user_id:String,email:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
            
        ]
        var parameters = [String : Any]()
        
        parameters = ["user_id":user_id,
                      "email":email]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.addPayOut.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
           
            switch response.result {
            
            case .success(_):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
//    func sendGifts(sender_id:String,receiver_id:String,gift_id:String,gift_count:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
//
//        let headers: HTTPHeaders = [
//            "Api-Key":API_KEY
//
//        ]
//        var parameters = [String : Any]()
//
//        parameters = ["sender_id":sender_id,
//                      "receiver_id":receiver_id,
//                      "gift_id":gift_id,
//                      "gift_count": gift_count,
//        ]
//        let finalUrl = "\(self.baseApiPath!)\(Endpoint.sendGift.rawValue)"
//
//        print(finalUrl)
//        print(parameters)
//        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
//            print(response.result)
//
//
//            switch response.result {
//
//            case .success(_):
//
//                if let json = response.value
//                {
//                    do {
//                        let dict = json as? NSDictionary
//                        print(dict)
//                        completionHandler(true, dict)
//
//                    } catch {
//                        completionHandler(false, nil)
//                    }
//                }
//                break
//            case .failure(let error):
//
//                if let json = response.value
//                {
//                    do {
//                        let dict = json as? NSDictionary
//                        print(dict)
//                        completionHandler(true, dict)
//
//                    } catch {
//                        completionHandler(false, nil)
//                    }
//                }
//                break
//            }
//        }
//    }
    
    
    func showGifts(completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
            
        ]
        var parameters = [String : Any]()
        
        parameters = ["":""]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showGifts.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
          
            switch response.result {
            
            case .success(_):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    //MARK:-ShowGiftsCategory
    func showGiftCategory(showGiftCategories:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            
            "showGiftCategories"  : showGiftCategories
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showGiftCategories.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    
    func sendGifts(sender_id:String,receiver_id:String,gift_id:String,gift_count:String,live_streaming_id:String,video_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
            
        ]
        var parameters = [String : Any]()
        
        if live_streaming_id == "0"{
            
            parameters = ["sender_id":sender_id,
                          "receiver_id":receiver_id,
                          "gift_id":gift_id,
                          "gift_count": gift_count,
            ]
        }else{
            parameters = ["sender_id":sender_id,
                          "receiver_id":receiver_id,
                          "gift_id":gift_id,
                          "gift_count": gift_count,
                          "live_streaming_id":live_streaming_id,
                          "video_id":video_id
            ]
        }
    
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.sendGift.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
        
            switch response.result {
            
            case .success(_):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    //MARK:ChangePhonenumber
    func changePhoneNumber(user_id:String,phone:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
            
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"         : user_id,
            "phone"           : phone,
            "verify"          : "0"
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.changePhoneNo.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict as Any)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- Update Password
    func changePassword(user_id:String,old_password:String,new_password:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"         : user_id,
            "old_password"    : old_password,
            "new_password"    : new_password
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.changePassword.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK:- Delete Account
    func DeleteAccount(user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"         : user_id,
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.DeleteAccount.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    //MARK:-Post Images
    func PostImage(userImage:[UIImage] ,parameters:[String:Any], completionHandler:@escaping (_ result:Any?, _ resultInData : Data , _ status:Bool,  _ error: Error?)-> Void) -> ()
    {
        let finalURL = "\(ApiHandler.sharedInstance.baseApiPath!)\(Endpoint.postImage.rawValue)"
        
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        
       
        AF.upload(multipartFormData: { multipartFormData in
            
            for var i in 0..<userImage.count{
                let img = userImage[i]
                let imgData : Data = img.jpegData(compressionQuality: 0.5)!
                let currentTime = AppUtility!.getCurrentMillis()
                multipartFormData.append(imgData , withName: "image\(i)", fileName: "\(currentTime)swift_file.jpg", mimeType: "image/png")
                for key in parameters.keys{
                    let name = String(key)
                    print("key",name)
                    if let val = parameters[name] as? String{
                        multipartFormData.append(val.data(using: .utf8)!, withName: name)
                    }
                }
            }
           
        }, to:finalURL, headers:headers )
        .responseJSON { (response) in
          
            switch response.result{
            
            case .success(let value):
            
                print("progress: ",Progress.current())
                
                let json = value
                
                let dic = json as! NSDictionary
                
                print("response:- ",response)
                
                if(dic["code"] as! NSNumber == 200){
                    print("200")
                    print("SUCCESS RESPONSE: \(response)")
                    print("Dict: ",dic)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "uploadImage"), object: Progress.current())
                    
                }else{
                    print(dic)
                }
            
                
            case .failure(let error):
                print(error.localizedDescription)
                break
            }
            
        }
    }
    
    
    func editPost(completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        
        let finalURL = "\(ApiHandler.sharedInstance.baseApiPath!)\(Endpoint.editVideo.rawValue)"
        print(finalURL)
    
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        AF.request(URL.init(string: finalURL)!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }

    
    //MARK:- ShowInfluencerBookings
    
    func showInfluecerBooking(user_id:String,strDate:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id"         : user_id,
            "date"            : strDate
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showInfluncerBookings.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- Add Influencer Bookings Time
    
    func addInfluncerBookingsTime(user_id:String,booking:[[String:Any]],completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : Any]()
        parameters = [
            "user_id"         : user_id,
            "booking"         : booking
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.addInfluncerBookingsTime.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- Add Influencer Bookings
    
    func addInfluecerBooking(user_id:String,user_booking_available_timing_id:String,influencer_id:String,coins:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : Any]()
        parameters = [
            "user_id"                            : user_id,
            "user_booking_available_timing_id"   : user_booking_available_timing_id,
            "influencer_id"                      : influencer_id,
            "coins"                              : coins
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.addBooking.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK:- Show Booking Wih influencer
    
    func showBookingWithInfluencer(user_id:String,influencer_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : Any]()
        parameters = [
            "user_id"         : user_id,
            "influencer_id"   : influencer_id,
           
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showBookingWithInfluencer.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:- Show Bookings
    
    func showBookings(user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        var parameters = [String : Any]()
        parameters = [
            "user_id"  : user_id,
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showBooking.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    //MARK:- ManageOneToOne
    func ManageOneToOne(booking_coins_per_hour:String,user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.editProfile.rawValue)"
        
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        
        var parameteres = [String:String]()
        parameteres = [
            "booking_coins_per_hour":booking_coins_per_hour,
            "user_id":user_id,
        ]
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameteres, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK:-generateReferralCode
    func generateReferralCode(user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key": API_KEY
        ]
        
    
        
        var parameters = [String : Any]()
    
            parameters = [
                "user_id"         : user_id,
            ]
      
       
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.generateReferralCode.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }

    
    //MARK:- subscribe package
    func subscribePackage(user_id:String,package:String, coins:String, month:String,subscribe_expiry_date:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
            
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id":user_id,
            "package":package,
            "coins":coins,
            "month":month,
            "subscribe_expiry_date":subscribe_expiry_date
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.subscribePackage.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
        
            switch response.result {
            
            case .success(_):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK:- Verify Coupon Code
    func verifyCouponCode(user_id:String,coupon_code:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
            
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id":user_id,
            "coupon_code":coupon_code,
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.verfiyCouponCode.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
        
            switch response.result {
            
            case .success(_):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        print(dict)
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    func stopAllSessions() {
        AF.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
            uploadData.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
    }
}

//
//  SongVc.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 29/09/2021.
//

import UIKit

class SongVc: UIViewController {
    @IBOutlet weak var tblDetails: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblDetails.delegate = self
        tblDetails.dataSource = self
    }
    

   

}
extension SongVc: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SongUsedTVC", for: indexPath) as! SongUsedTVC
        return cell
    }
    
    
}

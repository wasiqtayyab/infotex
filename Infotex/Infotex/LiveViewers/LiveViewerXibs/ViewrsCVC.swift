//
//  ViewrsCVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 27/09/2021.
//

import UIKit

class ViewrsCVC: UICollectionViewCell {
    @IBOutlet weak var viewBackImage: UIView!
    
    @IBOutlet weak var viewImageBorder: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var imgFor123: UIImageView!
    
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var imgTag150: UIImageView!
    @IBOutlet weak var lblTag150: UILabel!
    @IBOutlet weak var imgBlueDiamond: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblDiamondsCount: UILabel!
    @IBOutlet weak var imgIcon1: UIImageView!
    @IBOutlet weak var imgIcon2: UIImageView!
    @IBOutlet weak var imgIcon3: UIImageView!
    @IBOutlet weak var btnInviteBlock: UIButton!
    @IBOutlet weak var imgChevron: UIImageView!
    @IBOutlet weak var imgUserBlock: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewBackImage.layer.cornerRadius = self.viewBackImage.frame.size.height / 2
        self.viewImageBorder.layer.cornerRadius = self.viewImageBorder.frame.size.height / 2
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height / 2
        
        self.viewImageBorder.layer.borderWidth = 1.8
        self.viewImageBorder.layer.borderColor = UIColor.white.cgColor
        self.viewBackImage.applyGradient(colors: [#colorLiteral(red: 0.2269999981, green: 0.4939999878, blue: 0.7289999723, alpha: 1),#colorLiteral(red: 0.7799999714, green: 0.4269999862, blue: 0.6589999795, alpha: 1)])
    }

}

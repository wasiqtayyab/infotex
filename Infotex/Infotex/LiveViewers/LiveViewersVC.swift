//
//  LiveViewersVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 27/09/2021.
//

import UIKit

class LiveViewersVC: UIViewController {
    @IBOutlet weak var cvMenuTab: UICollectionView!
    @IBOutlet weak var cvViewers: UICollectionView!
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    
    var arrMenu = ["Viewers","VIP"]
    var lastSelectIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        
        cvMenuTab.delegate = self
        cvMenuTab.dataSource = self
        
        cvViewers.delegate = self
        cvViewers.dataSource = self
        
        cvViewers.register(UINib(nibName: "ViewersListCVC", bundle: nil), forCellWithReuseIdentifier: "ViewersListCVC")

    }
    

   

}
extension LiveViewersVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == cvMenuTab{
            return CGSize(width: self.cvMenuTab.frame.size.width / 2 , height: self.cvMenuTab.frame.size.height)
            
        }else{
            return CGSize(width: self.cvViewers.frame.size.width, height: self.cvViewers.frame.size.height)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == cvMenuTab{
            return 2
        } else{
            return 2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == cvMenuTab {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DimondTabMenuCVC", for: indexPath) as! DimondTabMenuCVC
          //  cell.viewBottomLine.applyGradient(colors: [#colorLiteral(red: 0.2269999981, green: 0.4939999878, blue: 0.7289999723, alpha: 1),#colorLiteral(red: 0.7799999714, green: 0.4269999862, blue: 0.6589999795, alpha: 1)])
            let obj = arrMenu[indexPath.row]
            cell.lblMenuOption.text = obj
          
            
            
                let text = arrMenu[indexPath.row]
            let textWidth = (text.size(withAttributes:[.font: UIFont(name: "Prompt-Medium", size: 16.0)]).width ?? 15) + 20.0
            
            cell.contWidthLable.constant = textWidth
            if indexPath.row == lastSelectIndex {
                cell.imgBottomLine.isHidden = false
            }else{
                cell.imgBottomLine.isHidden = true
            }
         
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ViewersListCVC", for: indexPath) as! ViewersListCVC
            if indexPath.row == 0 {
                cell.lblTotalViewers.text = "10K Viewers"
            }else{
                cell.lblTotalViewers.text = ""
            }
            
          //  cell.setup(count:anOj.count, giftsData: anOj )
            return cell
   
            
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

//        let obj = arrMenuCate[indexPath.row]
//        let aNAme = obj["title"] as? String
//        print(aDict[aNAme!]!)
        if collectionView == cvMenuTab{
//            lastSelectIndex = indexPath.row
//            self.cvMenuTAbs.reloadData()

            self.cvViewers.scrollToItem(at: indexPath, at: .left, animated: true)
        }else{
            print("asdssdsd")
        }
        
       
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == cvViewers {
            let currentindex = Int(scrollView.contentOffset.x / cvViewers.frame.size.width)
            let aNum = currentindex
            lastSelectIndex = currentindex
            self.cvMenuTab.reloadData()
            print(aNum)
            
            let indexPath = IndexPath(row: currentindex, section: 0)
            let rect = self.cvMenuTab.layoutAttributesForItem(at: indexPath)?.frame
            
             self.cvMenuTab.scrollRectToVisible(rect!, animated: true)
           // self.cvMenuTab.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
        
        
    }
}

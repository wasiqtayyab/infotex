//
//  DimondDetailTypeCVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 25/09/2021.
//

import UIKit

class DimondDetailTypeCVC: UICollectionViewCell {

 
    @IBOutlet weak var cvDetatilTypes: UICollectionView!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var lblComingSoon: UILabel!
    var arrCount = Int()
    var arrData = [[String:Any]]()
    var lastSelectedIndex = -1
    override func awakeFromNib() {
        super.awakeFromNib()
        cvDetatilTypes.delegate = self
        cvDetatilTypes.dataSource = self
        cvDetatilTypes.register(UINib(nibName: "DimondDetailCVC", bundle: nil), forCellWithReuseIdentifier: "DimondDetailCVC")
        
        cvDetatilTypes.register(UINib(nibName: "DimondDetailSecCVC", bundle: nil), forCellWithReuseIdentifier: "DimondDetailSecCVC")
    }
    
    func setup(count:Int,giftsData:[[String:Any]]){
//        if count == 0 {
//            lblComingSoon.isHidden = false
//        }else{
//            lblComingSoon.isHidden = true
//        }
        self.arrCount = count
        self.arrData = giftsData
        cvDetatilTypes.delegate = self
        cvDetatilTypes.dataSource = self
        self.cvDetatilTypes.reloadData()
    
    }

}
extension DimondDetailTypeCVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = contentView.frame.width
        
        return CGSize(width: size , height: 80 )
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        
        let obj = arrData[indexPath.row]["type"] as? String
        
        if obj == "transfer"{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DimondDetailCVC", for: indexPath) as! DimondDetailCVC
            cell.lblPrize.textColor = UIColor(named: "Blue")
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DimondDetailSecCVC", for: indexPath) as! DimondDetailSecCVC
            return cell
        }
        
     
        
        
//        let obj = arrData[indexPath.row]
//
//        cell.lblGiftName.text = obj["title"] as? String
//        cell.lblDiamondsCount.text = "\(obj["coin"] as? String ?? "")" + " Diamonds"
//
//        if indexPath.row == lastSelectedIndex {
//            cell.imgSelected.isHidden =  false
//        }else{
//            cell.imgSelected.isHidden = true
//        }
//
//
//        let url = "\(ApiHandler.sharedInstance.BaseUrl ?? "")" + "\(obj["image"] as! String)"
//        print(url)
//        cell.imgIcon.sd_imageIndicator = SDWebImageActivityIndicator.white
//        cell.imgIcon.sd_setImage(with: URL(string: url), placeholderImage: nil)
       
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                    lastSelectedIndex = indexPath.row
                    self.cvDetatilTypes.reloadData()
    }
    
    
}

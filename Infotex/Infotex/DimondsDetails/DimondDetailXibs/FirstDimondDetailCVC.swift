//
//  DimondDetailTypeCVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 25/09/2021.
//

import UIKit

class FirstDimondDetailCVC: UICollectionViewCell {
    @IBOutlet weak var cvDetails: UICollectionView!
    @IBOutlet weak var btnFilter: UIButton!
    
    var arrCount = Int()
    var arrData = [[String:Any]]()
    var lastSelectedIndex = -1
    override func awakeFromNib() {
        super.awakeFromNib()
        cvDetails.delegate = self
        cvDetails.dataSource = self
        cvDetails.register(UINib(nibName: "DimondDetailCVC", bundle: nil), forCellWithReuseIdentifier: "DimondDetailCVC")
    }

}
extension FirstDimondDetailCVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = contentView.frame.width
        
        return CGSize(width: width , height: 80 )
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DimondDetailCVC", for: indexPath) as! DimondDetailCVC
//
//        let obj = arrData[indexPath.row]
//
//        cell.lblGiftName.text = obj["title"] as? String
//        cell.lblDiamondsCount.text = "\(obj["coin"] as? String ?? "")" + " Diamonds"
//
//        if indexPath.row == lastSelectedIndex {
//            cell.imgSelected.isHidden =  false
//        }else{
//            cell.imgSelected.isHidden = true
//        }
        
        
//        let url = "\(ApiHandler.sharedInstance.BaseUrl ?? "")" + "\(obj["image"] as! String)"
//        print(url)
//        cell.imgIcon.sd_imageIndicator = SDWebImageActivityIndicator.white
//        cell.imgIcon.sd_setImage(with: URL(string: url), placeholderImage: nil)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                    lastSelectedIndex = indexPath.row
                    self.cvDetails.reloadData()
    }
    
    
}

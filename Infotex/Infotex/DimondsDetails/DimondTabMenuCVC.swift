//
//  DimondTabMenuCVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 25/09/2021.
//

import UIKit

class DimondTabMenuCVC: UICollectionViewCell {
    @IBOutlet weak var lblMenuOption: UILabel!
    
    @IBOutlet weak var viewBottomLine: UIView!
    @IBOutlet weak var imgBottomLine: UIImageView!
    
    @IBOutlet weak var contWidthLable: NSLayoutConstraint!
    
}

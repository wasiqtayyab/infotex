//
//  DimondDetailsVC.swift
//  infotexRoughNaqash
//
//  Created by Naqash Ali on 25/09/2021.
//

import UIKit

class DimondDetailsVC: UIViewController {
    @IBOutlet weak var cvMenuTab: UICollectionView!
    @IBOutlet weak var cvDetails: UICollectionView!
    
    var arrMenu = ["one","Two222"]
    var arrCount = [5,8]
    var arrData = [["type":"transfer"],
                   ["type":"system"],
                   ["type":"transfer"],
                   ["type":"system"],
                   ["type":"transfer"],
                   ["type":"system"],
                   ["type":"transfer"],
                   ["type":"system"]]
    var arrDataOne = [["type":"transfer"],
                   ["type":"system"],
                   ["type":"transfer"],
                   ["type":"system"],
                   ["type":"transfer"],
                   ["type":"system"],
                   ["type":"transfer"],
                   ["type":"system"],
                   ["type":"transfer"],
                   ["type":"system"]]
    var lastSelectIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        cvMenuTab.delegate = self
        cvMenuTab.dataSource = self
        
        cvDetails.delegate = self
        cvDetails.dataSource = self
        cvDetails.register(UINib(nibName: "DimondDetailTypeCVC", bundle: nil), forCellWithReuseIdentifier: "DimondDetailTypeCVC")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DimondDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == cvMenuTab{
           
            return CGSize(width: self.cvMenuTab.frame.size.width / 2 , height: self.cvMenuTab.frame.size.height)
            
        }else{
            return CGSize(width: self.cvDetails.frame.size.width, height: self.cvDetails.frame.size.height)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == cvMenuTab{
            return arrMenu.count
        } else{
            return 2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == cvMenuTab {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DimondTabMenuCVC", for: indexPath) as! DimondTabMenuCVC
            
            let obj = arrMenu[indexPath.row]
            cell.lblMenuOption.text = obj
          
            
            
                let text = arrMenu[indexPath.row]
            let textWidth = (text.size(withAttributes:[.font: UIFont(name: "Prompt-Medium", size: 16.0)]).width ?? 15) + 20.0
            cell.contWidthLable.constant = textWidth
            if indexPath.row == lastSelectIndex {
                cell.imgBottomLine.isHidden = false
            }else{
                cell.imgBottomLine.isHidden = true
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DimondDetailTypeCVC", for: indexPath) as! DimondDetailTypeCVC
            
//            let obj = a[indexPath.row]
//            let aName = obj["title"] as? String
//            let anOj = a[aName!] as! [[String:Any]]
            
//            print(anOj.count)
//
            if indexPath.row == 0 {
                cell.setup(count:arrCount[indexPath.row], giftsData: arrData )
            }else{
                cell.setup(count:arrCount[indexPath.row], giftsData: arrDataOne )
            }
           
            return cell
   
            
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let obj = arrMenu[indexPath.row]
    //    let aNAme = obj["title"] as? String
       // print(aDict[aNAme!]!)
        if collectionView == cvMenuTab{
//            lastSelectIndex = indexPath.row
//            self.cvMenuTAbs.reloadData()

            self.cvDetails.scrollToItem(at: indexPath, at: .left, animated: true)
            
        }else{
            print("asdssdsd")
        }
        
       
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == cvDetails {
            let currentindex = Int(scrollView.contentOffset.x / cvDetails.frame.size.width)
            let aNum = currentindex
            lastSelectIndex = currentindex
            self.cvMenuTab.reloadData()
            print(aNum)
            
            let indexPath = IndexPath(row: currentindex, section: 0)
            self.cvMenuTab.scrollToItem(at: indexPath, at: .left, animated: true)
        }
        
        
    }
}
